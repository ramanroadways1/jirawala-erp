<?php
require "_session.php";
include "../_header.php";
 include "left.php";

try {
    $conn->query("START TRANSACTION"); 
  
?>
 
<div class="page-wrapper">
        <div class="content">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstarp-multi.css">
<script type="text/javascript" src="../assets/js/multi-js.js"></script>
<script>
$(document).ready(function() {
    $('#multiselect').multiselect({    
     buttonWidth : '180px',
     includeSelectAllOption : true,
     maxHeight: '280',
      nonSelectedText: 'Select an Option'
    });
    
  });

  function getSelectedValues() {
    var selectedVal = $("#multiselect").val();
    for(var i=0; i<selectedVal.length; i++){
      function innerFunc(i) {
        setTimeout(function() {
          location.href = selectedVal[i];
        }, i*200000000000000000000);
      }
      innerFunc(i);
    }
  }

</script>
 

 <div class="row">
 <div id="loadicon" style="display:none; position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#ffffff; z-index: 30001; opacity:0.8; cursor: wait;">
  <center><img src="../images/loader.gif" style="margin-top:50px;" /> </center>
  </div>

<div class="col-md-4">

<form  id="formid" action="#" method="POST" >
            <div class="dash-widget clearfix card-box">
 
                <span class="dash-widget-icon"><i class="fa fa-building-o"></i></span>
                <div class="dash-widget-info">
                    <h3> YATRIK BHAVAN </h3>
                    <div class="col-md-8 row" style="padding:0px; margin:0px;">
                        <select  multiple="multiple" class="form-control select" id="multiselect" name="sys[]"  required>
                          <?php
                          $sql="select * from bigdharmshala where status='7'";
                          $res=$conn->query($sql);
                          if($res===FALSE)
                          {
                          throw new Exception("Code 001: ".mysqli_error($conn));   
                          }
                          while($row=mysqli_fetch_array($res))
                          {
                          ?>
                          <option value="<?php echo $row["id"]; ?>"><?php echo $row["id"]; echo $row["big"]; ?>&nbsp; </option> 
                          <?php
                          }
                          ?>
                        </select>
                    </div>
                </div>
                <div style="float: right; padding-top:20px;"> 
                  <button type="submit" style="color : #fff; " id ="formbutton" class="btn btn-warning btn-sm m-t-10"   /> <i class='fa fa-check-square-o'> </i> Clean </button> 
                </div>
            </div>
      </form>


</div>
<div class="col-md-4">



</div>
<div class="col-md-4">

</div>

<div class="col-md-12">


 
 <br><br>
 <table id="BookTable"  class="table table-border table-striped custom-table datatable" >
<thead>
		<tr align="center">
			<th>id</th>
			<th >R. No</th>
			<th>R. Type </th>
			<th>Check In </th>
			<th>Check out </th>
			<th>Update status  </th>
		
	      </tr>
      </thead>
	
 <?php


$sql="select * from bookroom where allstatus='1'";
$res=$conn->query($sql);
if($res===FALSE)
{
throw new Exception("Code 002: ".mysqli_error($conn));   
}

while($row=mysqli_fetch_array($res))
{

  $SmallDhrm=$row["BigDharmshala"];
  $token = strtok($SmallDhrm, ",");
  while ($token !== false)
  {

      $sql="select * from bigdharmshala where id= '$token'";
      $res2=$conn->query($sql);
      if($res2===FALSE)
      {
      throw new Exception("Code 003: ".mysqli_error($conn));   
      }

  
  $token = strtok(",");

while($row2=mysqli_fetch_array($res2))
{
$Bookid=$row["Bookid"];
$sql="select * from status_checkout where Bookid='$Bookid'";

    $res3=$conn->query($sql);
    if($res3===FALSE)
    {
    throw new Exception("Code 004: ".mysqli_error($conn));   
    }
while($row3=mysqli_fetch_array($res3))
{
 ?>
        
<tr align="center">
<th><?php echo $row["Bookid"]; ?></th>
<th><?php echo $row2["sid"]; ?></th>
<th><?php echo $row2["stype"]; ?></th>
<th><?php echo $row["checkindate"]; ?><br>(<?php echo $row["intime"]; ?>)</th>
<th><?php echo $row["checkoutdate"]; ?><br>(<?php echo $row["outtime"]; ?>)</th>
<?php
$status=$row3["status"];

if($status=="1")
{
    ?>
   

     <th style="color:green;"> 
       <form id="update" method="POST" action="update_r_st.php">
      <input type="hidden" name="Book_id" value="<?php echo $row3["Bookid"]; ?>">
      <input type="hidden" name="status" value="<?php echo "give"; ?>">
      <button type="submit"class="btn btn-primary" id="updatebutton">Gave</button>
    </form>

<br>
     Room booked by user <?php echo $row3["book_By"]; ?>
  Please gave the room keys 
    </th>
    <?php
}
if($status=="2")
{
    ?>
      <th style="color:red;">
     <form id="update" action="update_r_st.php" method="POST">
      <input type="hidden" name="Book_id" value="<?php echo $row3["Bookid"]; ?>">
      <input type="hidden" name="status" value="<?php echo "rec"; ?>">
      <button type="submit"class="btn btn-primary" id="updatebutton">Receive</button>
    </form>


 <br>
     Sucessfully gave room key by user <?php echo $row3["gave_key"]; ?> 
      Please Receive the key 
    </th>

    <?php
}

if($status=="3")
{
    ?>

<th style="color:blue;" >Sucessfully Receive the key Reciver name --><?php echo $row3["recevie_key"]; ?>  </th>

    <?php
}

?>


</tr> 
     
  <?php
}
}
}
}
  ?>
  </table>
</div>
 </div>

	   
<script type="text/javascript">
  $(document).ready(function()
              { 
                $(document).on('submit', '#formid', function()
                {  
                  $("#formbutton").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'clean11.php',
                    data : data,
                    success: function(data) {  
                      //$('#exampleModal').modal("hide");  // agar koi modal show hide karna h
                      $('#response').html(data);  // response div par data aaega
                     // $('#user_data').DataTable().ajax.reload(null,false); // datatable refresh hoga 
                      $('#loadicon').hide();   // loading division hide kar dega
                      $("#formbutton").attr("disabled", false);
                       $('#formid')[0].reset();

                    }
                  });
                  return false;
                });
              }); 


</script>
     




	   <?php 
	   
    $conn->query("COMMIT");
   

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
          echo ("<script LANGUAGE='JavaScript'>
        window.alert('Error: $content');
        window.location.href = \"../error.php\";
        </script>");   
} 


$conn->close();
	   require "../_footer.php"; ?>
