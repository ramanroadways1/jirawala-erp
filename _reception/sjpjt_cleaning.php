<?php
//done
    require "_session.php";
    require "_header.php";
 
	$name=$_SESSION["username"];
	date_default_timezone_set('Asia/Calcutta'); 
	$Check=date("d-m-Y");
	$check11=date("Y-m-d");

	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>
 <link rel="stylesheet" type="text/css" href="../assets/css/bootstarp-multi.css">
<script type="text/javascript" src="../assets/js/multi-js.js"></script>
<script>
$(document).ready(function() {
  $('#multiselect').multiselect({    
   buttonWidth : '180px',
   includeSelectAllOption : true,
   maxHeight: '280',
        nonSelectedText: 'Select an Option'
  });
  

});

function getSelectedValues() {
  var selectedVal = $("#multiselect").val();
    for(var i=0; i<selectedVal.length; i++){
        function innerFunc(i) {
            setTimeout(function() {
                location.href = selectedVal[i];
            }, i*200000000000000000000);
        }
        innerFunc(i);
    }
}
 
$(document).ready(function() {
  $('#multiselect1').multiselect({
  buttonWidth : '180px',
    includeSelectAllOption : true,
    maxHeight: '280',
        nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect1").val();
    for(var i=0; i<selectedVal.length; i++){
        function innerFunc(i) {
            setTimeout(function() {
                location.href = selectedVal[i];
            }, i*2000);
        }
        innerFunc(i);
    }
}

$(document).ready(function() {
  $('#multiselect2').multiselect({
    buttonWidth : '180px',
    includeSelectAllOption : true,
    maxHeight: '300',
        nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect2").val();
    for(var i=0; i<selectedVal.length; i++){
        function innerFunc(i) {
            setTimeout(function() {
                location.href = selectedVal[i];
            }, i*2000);
        }
        innerFunc(i);
    }
}

$(document).ready(function() {
  $('#multiselect3').multiselect({
    buttonWidth : '180px',
    includeSelectAllOption : true,
    maxHeight: '300',
        nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect3").val();
    for(var i=0; i<selectedVal.length; i++){
        function innerFunc(i) {
            setTimeout(function() {
                location.href = selectedVal[i];
            }, i*2000);
        }
        innerFunc(i);
    }
}
</script>


<div class="page-wrapper">
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Room Cleaning </h4>
        </div>
    </div>
 
    <div class="row">

			<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
			<form class="" action="#" method="POST" >
            <div class="dash-widget clearfix card-box">
 
                <span class="dash-widget-icon"><i class="fa fa-building-o"></i></span>
                <div class="dash-widget-info">
                    <h3> YATRIK BHAVAN </h3>
                    <div class="col-md-8 row" style="padding:0px; margin:0px;">
                        <select  multiple="multiple"  id="multiselect" name="sys[]"  required>
                        <?php 
                        $sql="select * from bigdharmshala where status='7'";
                        $Yatrik=$conn->query($sql);
                        if($Yatrik===FALSE)
                        {
                        throw new Exception("Code 001 : ".mysqli_error($conn));   
                        }
                        while($row=mysqli_fetch_array($Yatrik))
                        {
                        ?>
                        <option value="<?php echo $row["id"]; ?>" class="green" ><?php echo $row["id"]; ?> &nbsp;<?php echo $row["big"] ?> </option>

                        <?php 
                        }
                        ?> 
                        </select>
                    </div>
                </div>
                <div style="float: right; padding-top:20px;"> 
                	<button type="submit" style="color : #fff; " id="submit1" name="submit1" class="btn btn-warning btn-sm m-t-10"   /> <i class='fa fa-check-square-o'> </i> Clean </button> 
                </div>
            </div>
			</form>
        	</div>


            <div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
            <form class="" action="#" method="POST" >
            <div class="dash-widget clearfix card-box">
 
                <span class="dash-widget-icon"><i class="fa fa-university"></i></span>
                <div class="dash-widget-info">
                    <h3> VIP BHAVAN </h3>
                    <div class="col-md-8 row" style="padding:0px; margin:0px;">
                        <select  multiple="multiple"  id="multiselect1" name="sys2[]" required>
                            <?php 
                            $sql="select * from bigdharmshala where status_vid='8'";
                            $vip=$conn->query($sql);
                            if($vip===FALSE)
                            {
                            throw new Exception("Code 003 : ".mysqli_error($conn));   
                            }
                            while($row=mysqli_fetch_array($vip))
                            {
                            ?>
                            <option value="<?php echo $row["vid"]; ?>" class="green" ><?php echo $row["vid"]; ?> &nbsp;<?php echo $row["vtype"] ?> </option>
                            <?php 
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div style="float: right; padding-top:20px;"> 
                    <button type="submit" style="color : #fff; " id="submit2" name="submit2" class="btn btn-warning btn-sm m-t-10"   /> <i class='fa fa-check-square-o'> </i> Clean </button> 
                </div>
            </div>
            </form>
            </div>

            <div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
            <form class="" action="#" method="POST" >
            <div class="dash-widget clearfix card-box">
 
                <span class="dash-widget-icon"><i class="fa fa-building"></i></span>
                <div class="dash-widget-info">
                    <h3> ATITHI BHAVAN </h3>
                    <div class="col-md-8 row" style="padding:0px; margin:0px;">
                        <select multiple="multiple2"  id="multiselect2" name="sys3[]" required>
                        <?php 
                        $sql="select * from bigdharmshala where sstatus='9'";
                        $atithi=$conn->query($sql);
                        if($atithi===FALSE)
                        {
                        throw new Exception("Code 005 : ".mysqli_error($conn));   
                        }
                        while($row=mysqli_fetch_array($atithi))
                        {
                        ?>
                        <option value="<?php echo $row["sid"]; ?>" class="green" ><?php echo $row["sid"]; ?> &nbsp;<?php echo $row["stype"] ?> </option>
                        <?php 
                        }
                        ?>
                        </select>
                    </div>
                </div>
                <div style="float: right; padding-top:20px;"> 
                    <button type="submit" style="color : #fff; " id="submit3" name="submit3" class="btn btn-warning btn-sm m-t-10"   /> <i class='fa fa-check-square-o'> </i> Clean </button> 
                </div>
            </div>
            </form>
            </div>

    </div>
</div>
</div>

    <?php  
    if(isset($_POST["submit1"]))
    {
        foreach ($_POST["sys"] as $room)
        {
            $sql="UPDATE bigdharmshala set status='0'  WHERE id ='$room'";
            if($conn->query($sql) === FALSE) {
                throw new Exception("Code 002 : ".mysqli_error($conn));             
            }   
        }
        ?>
        <script LANGUAGE='JavaScript'>
        window.alert('Succesfully Updated');
        window.location.href = 'sjpjt_cleaning.php';
        </script>
    <?php
    }
 
    if(isset($_POST["submit2"]))
    {
        foreach ($_POST["sys2"] as $room2)
        {
            $sql="UPDATE bigdharmshala set  status_vid='2'  WHERE vid ='$room2'";
            if($conn->query($sql) === FALSE) {
                throw new Exception("Code 004 : ".mysqli_error($conn));             
            }   
        }
    ?>
        <script LANGUAGE='JavaScript'>
        window.alert('Succesfully Updated');
        window.location.href = 'sjpjt_cleaning.php';
        </script>
    <?php   
    }
 
    if(isset($_POST["submit3"]))
    {
        foreach ($_POST["sys3"] as $room3)
        {
            $sql="UPDATE bigdharmshala set sstatus='4'  WHERE sid='$room3'";
            if($conn->query($sql) === FALSE) {
                throw new Exception("Code 006 : ".mysqli_error($conn));             
            }   
        }   
    ?>
        <script LANGUAGE='JavaScript'>
        window.alert('Succesfully Updated');
        window.location.href = 'sjpjt_cleaning.php';
        </script>
    <?php
    }
 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

	} catch(Exception $e) { 

                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }

                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 

$conn->close();

require "_footer.php"; ?>
