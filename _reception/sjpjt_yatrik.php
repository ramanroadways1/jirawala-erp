<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>

<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title"> Booked Rooms Status Chart of <b>Yatrik Bhavan</b> </h4>
	        </div>
	    </div>


		<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>  Room No  	</th>
										<th>  Room Type  	</th>
										<th>  Check-in Date  	</th>
										<th>  Check-out Date  	</th>
										<th>   Booking Id  </th>
										<th class="text-right">Action</th>
									</tr>
								</thead>
								<tbody>
								<?php

									$sql="select * from bookroom where allstatus='1'";

									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}


									while($row=mysqli_fetch_array($res))
									    {
									   $BigDharmshala=$row["BigDharmshala"];
									    $token = strtok($BigDharmshala, ",");
									    while ($token !== false)
									{

									$sql="select * from bigdharmshala where id= '$token'";
									$res1=$conn->query($sql);
									if($res1===FALSE)
									{
									throw new Exception("Code 002 : ".mysqli_error($conn));   
									}


									$token = strtok(",");
									while($row1=mysqli_fetch_array($res1))
									{
							  	?>

									<tr>
										<td><?php echo $row1["id"]; ?></td>
										<td><?php echo $row1["big"]; ?></td>
										<td><?php echo $row["checkindate"]; ?> (<?php echo $row["intime"]; ?>)</td>
										<td><?php echo $row["checkoutdate"]; ?> (<?php echo $row["outtime"]; ?>)</td>
										<td><?php echo $row["Bookid"]; ?></td>
										<td class="text-right">
											<form action="CheckoutRoom.php" method="POST">
											<button style="color : #fff; " class="btn btn-danger btn-sm "><span> <i class="fa fa-key" aria-hidden="true"></i> Check out</span></button>
											<input type="hidden" name="id" value="<?php echo $row["Bookid"]; ?>">
											</form>
										</td>
									</tr>

									<?php  } } }  ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>

	    </div>
	</div>

	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>