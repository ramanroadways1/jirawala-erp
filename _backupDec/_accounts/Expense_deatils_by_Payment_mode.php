<?php

require "_session.php";
 

$file_name = basename($_SERVER['PHP_SELF']);
$name=$_SESSION["username"];


$starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
$endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime= date("Y-m-d", strtotime($starttime1));
$endtime= date("Y-m-d", strtotime($endtime1));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));

try {
    $conn->query("START TRANSACTION"); 

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <script src="../assets/sweetalert.min.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
  <title> Shri Jirawala Parshwanath Jain Tirth </title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
#sidebar{
display: none; 
}

.main-content{


width: 250mm !important;

padding: 0px !important;
margin: auto !important;
background: white !important;

}
#navbar{
display: none !important;
visibility: hidden !important;
}

@media print {
  a[href]:after {
    content: none !important;
  }
}
</style>
<style type="text/css">
.inpsty{
  max-width: 60px !important; 

 }  
 .table>tbody>tr>td {
  vertical-align: middle !important;
  padding-bottom: 6px !important;
 }


@media print
   {
    .inpsty{
    border: none !important;
    } 
    

 
   }

 .table>thead>tr>th, .table-bordered>thead>tr>th,  
 .table-bordered>tbody>tr>td{
  border-color: #000;
 }
</style>
</head>
<body>
<script  >        
function DisableBackButton() {
     window.history.forward()
 }
 DisableBackButton();
 window.onload = DisableBackButton;
 window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
 window.onunload = function () { void (0) }
</script>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>


<div class="main-content">
<div class="main-content-inner">


<div class="page-content">

<style type="text/css">
.form-group{
margin-bottom: 10px;
}
label{
color: #000 !important;
}
@media print
   {
      .hideborder {
    
        border: 1px solid #e5e5e5;
      }
   }
</style>

 

          <div class="row">
             <div class="col-xs-12">
                <div class="panel panel-default" style="border-color: #000 !important;">
                     <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important;">
                            <div class="row">

                              <div class="col-xs-12" style=" ">
                              <center><img src="../images/jp.jpg" width="490px" height="90px" >
                               <hr style="margin: 5px !important; padding-bottom: 10px !important;"> </center> 
                              </div> 
<div class="col-xs-12" style="padding-bottom: 10px !important;">
 
<div class="col-xs-6" >
<div class="form-group" style="margin-bottom: 5px !important;">
 <label style="font-size: 12px !important;">EXPENSE REPORT -  <?php echo $sys; ?></label>



</div>

    </div>  


<div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;">DATE :
    &nbsp;<?php  echo $starttime1; ?>&nbsp;to&nbsp; <?php  echo $endtime1; ?></label>
    </div>
</div>



</div>
 <div class="col-xs-12">
  <table class="table table-bordered" id="tblCustomers">

       <tr>

      <th>SlipNo</th>
      <th>Date </th>
      <th>Username </th>
      <th>To  </th>
      <th>Head</th>
      <th>Narration</th>
      <?php
      if($sys=="Cheque")
      {
      ?>
      <th>Cheque Number</th>
      <th>Name of Bank</th>
      <?php
      }
      if($sys=="NEFT")
      {
      ?>
      <th>PAN Number</th>
      <th>A/C Holder Name</th>
      <th>A/C Number</th>
      <th>Bank Name</th>         
      <th>IFSC Code</th>  
      <?php
      }?>
      <th>Amount </th>
      <!-- <th>Amount in Word </th> -->
      </tr>

<?php   
     $sql="SELECT * from expense where ptype='$sys' and  mysqldate between '$starttime' and '$endtime'";
          $res=$conn->query($sql);
          if($res===FALSE)
          {
          throw new Exception("Code 001 : ".mysqli_error($conn));   
          }
      $sql="SELECT SUM(amt) as amt from expense where ptype='$sys' and mysqldate between '$starttime' and '$endtime'";
          $res1=$conn->query($sql);
          if($res1===FALSE)
          {
          throw new Exception("Code 002 : ".mysqli_error($conn));   
          }

while($row=mysqli_fetch_array($res))
            {
              ?>
       <tr>

     <th style="font-weight: normal; font-size: 14px;"><a href="expense_Slip3.php?id=<?php echo $row["id"]; ?>"><?php echo $row["id"]; ?></a></th>
     <th style="font-weight: normal; font-size: 14px; width: 100px;"><?php echo $row["date"]; ?> </th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["name"]; ?> </th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["given"]; ?> </th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["expense"]; ?></th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["narration"]; ?></th>
      <?php
      if($sys=="Cheque")
      {
      ?>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["pcheque"]; ?></th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["pbank"]; ?></th>
      <?php
      }
      if($sys=="NEFT")
      {
      ?>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["ppan"]; ?></th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["pachol"]; ?></th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["paccount"]; ?></th>
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["pbank"]; ?></th>         
      <th style="font-weight: normal; font-size: 14px;"><?php echo $row["pifsc"]; ?></th>  
      <?php
      }?>
      <th style="font-weight: normal; font-size: 14px; text-align: center;"><?php echo $row["amt"]; ?></th>
      <!-- <th style="font-weight: normal; font-size: 14px;"><?php echo $row["inword"]; ?></th> -->
      </tr>
   
    <?php } ?>
	   </table>
	
    <div class="form-group" style="padding: 0px !important; ">
   <?php  

while($row1=mysqli_fetch_array($res1))
            {

?>
  <div class="col-xs-12" style="border: 1px solid #999;">
  <div class="col-xs-4" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
  <label></label>
  &nbsp;                 
  </div>
  </div>

  <div class="col-xs-8" style="padding-top: 8px; text-align: right;">
  <div class="form-group" style="margin-bottom: 5px !important;">
  <label style="text-align: right;" >Total Deposit : &nbsp; ₹ <?php echo $row1["amt"];  ?></label>
  &nbsp;  
  </div>  
  </div>
  <?php
}



 $conn->query("COMMIT");

//     echo "
//     <script>
//    swal({
//   title: \"Good job!\",
//   text: \"You clicked the button!\",
//   icon: \"success\",
//   button: \"OK\",
// });
//     </script>";

} catch(Exception $e) { 

$conn->query("ROLLBACK"); 
$content = htmlspecialchars($e->getMessage());
$content = htmlentities($conn->real_escape_string($content));

$sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$name','$content')";

if ($conn->query($sql) === TRUE) {
  // echo "New record created successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}
    echo "
    <script>
   swal({
  title: \"Error !\",
  text: \"$content\",
  icon: \"error\",
  button: \"OK\",
});
    </script>";    
} 

$conn->close();



  ?>

  </table>


</div>
 <div> &nbsp; </div>


</div>  </div>
    </div>
</div><script type="text/javascript">
function myFunction() { 
window.print();
}
</script>
          </div>
          </div>
          </div>
          </div>


 
					</div><!-- /.page-content -->
				</div>      



        <center class="hidden-print">
      <a href="acc_Expense_report.php" class="btn btn-app btn-danger">
      <i class="ace-icon fa fa-remove bigger-160"></i>
      Exit
      </a>
      &nbsp; &nbsp; 
      <button onclick="myFunction()" class="btn btn-app btn-primary">
      <i class="ace-icon fa fa-print bigger-160"></i>
      Print
      </button>
&nbsp; &nbsp;

      <button class="btn btn-app btn-success" id="btnExport">
      <i class="ace-icon fa fa-download bigger-160"></i>
      Export
      </button>

       <!-- <input type="submit" class="btn btn-app btn-success"   value="Export" /> -->

  </center>

  <br>
		</div>



</div>  </div>
    </div>
</div>
          </div>
          </div>
			
			</div>
			
			</div>
			
			</div>
		

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="table2excel.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "ExpenseReport.xls"
                });
            });
        });
    </script>


</body>
</html>