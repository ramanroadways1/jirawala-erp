<?php

 session_start();
  $_SESSION['last_login_timestamp'] = time();  

 if (isset($_SESSION['username'])) {

  if($_SESSION['dep']=="rep"){
    header('Location: _reception/'); 

  } else if($_SESSION['dep']=="admin"){
    header('Location: _accounts/'); 

  } else if($_SESSION['dep']=="admin121"){
    header('Location: _admin/'); 

  } 
  
}

?>  
<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="assets/fav.jpg">
<title> Shri Jirawala Parshwanath Jain Tirth  </title>
<?php
include "connection.php";
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />

<!--   for multiselection-->
<script   src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<link  href="css/bootstarp-multi.css" rel="stylesheet" type="text/css" />
<script src="js/multi-js.js"></script>


<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<script src="js/jquery.min.js"> </script>
<!-- Mainly scripts -->
<script src="js/jquery.metisMenu.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Custom and plugin javascript -->
<link href="css/custom.css" rel="stylesheet">
<script src="js/custom.js"></script>
<script src="js/screenfull.js"></script>
        <script>
        $(function () {
            $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

            if (!screenfull.enabled) {
                return false;
            }

            

            $('#toggle').click(function () {
                screenfull.toggle($('#container')[0]);
            });
            

            
        });
        </script>

<!----->

<!--pie-chart--->
<script src="js/pie-chart.js" type="text/javascript"></script>
 <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#3bb2d0',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#fbb03b',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

 $('#demo-pie-4').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

           
        });

    </script>
<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->
</head>

<style type="text/css">
    body{
background:url('images/view004.jpg');
     height: 100% !important;
min-height: 800px !important;
     background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  box-shadow:inset 0 0 0 2000px rgba(236,240,241,0.2);
    }   
#log{
 
background: #fff !important;
padding:35px !important;
}
img{ width:150px;
    margin:auto;
}
h1{
color:white;
text-align:center;
font-family: "Times New Roman", Times, serif;
margin-top:-20px;
}
label{font-size:15px; color:white;}
button{

}
#page-wrapper {
    margin: 0px !important;
}
</style>
<body>

    <div id="page-wrapper" class="gray-bg dashbard-1">
    <br><br><br><br><br><br>
    <div class="row">
    <div class="col-md-1">
    </div>
    <div class="col-md-3" >
       
    
    <form id="log" method="POST" action="#" autocomplete="off" style=" padding: 35px 20px 30px 20px  !Important; border-radius: 20px;  background: rgb(255 255 255 / 95%) !important;">
    <h1  style="color: #2c3e50;"> <img style="width: 100%; padding-bottom: 20px; padding-top: 10px;" src="assets/logo-head.png"></h1>

    <div style="padding-left: 25px; padding-right: 25px;">
                <div class="form-group">
                    <label style="color: #2c3e50; ">Username  </label>
                    <input type="username" class="form-control" placeholder="Enter your Username" name="username">
                </div>
                <div class="form-group">
                    <label style="color: #2c3e50; ">Password</label>
                    <input type="password" class="form-control" placeholder="Enter Your Password" name="password">
                </div>
                
                
                <div class="form-group">
                    <label style="color: #2c3e50; "> Department</label>
                    
                    <select name="dep" class="form-control" required="required">
                    <option value=""> -- select -- </option>
                    <option value="rep"> Reception Desk</option>
                    <option value="admin"> Accounts </option>
                    <option value="admin121"> Admin </option>
                    
                    </select>
                </div>
                
                <button style="margin-top: 20px;" type="submit" class="btn btn-warning btn-block " name="login">Login</button>
    </div>
    </form>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-12"></div>
<?php
      if(isset($_POST["login"]))
      {
         $count=0;
         $fullname=$_POST["dep"];
         
         // $_SESSION["username"]=$_POST["username"];
         $password=md5($_POST["password"]);

         $res=mysqli_query($conn, "SELECT fullname FROM `admin` WHERE active='0' and username='$_POST[username]'AND password='$password'AND fullname='$fullname'") or die(mysqli_error($conn));
         $count=mysqli_num_rows($res); 
         if($count>0)
         {
           $_SESSION["username"]=$_POST["username"];
            //$_SESSION['dep'] = $_POST["dep"];
           while($row=mysqli_fetch_array($res))
           {
             if($fullname=="rep")
             {
                $_SESSION["dep"]="rep";
                ?>
                        <script type="text/javascript">
                        window.location = "_reception/";   
                        </script>
                <?php
             }
             if($fullname=="admin")
             {
                $_SESSION["dep"]="admin";
                ?>
                        <script type="text/javascript">
                        window.location = "_accounts/";   
                        </script>
                <?php
             }
             if($fullname=="admin121")
             {
                $_SESSION["dep"]="admin121";
                ?>
                        <script type="text/javascript">
                        window.location = "_admin/";   
                        </script>
                <?php
             }
           }
         }
         else
         {
          ?>
            <script type="text/javascript">
                    swal("Error !!!", "Incorrect Username or Password.", "error");
            </script>
          <?php
         
         }
     }
?>
 
</body>
</html>
