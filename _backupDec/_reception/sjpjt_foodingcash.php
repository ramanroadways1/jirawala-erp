<?php
//done
    require "_session.php";
    require "_header.php";
 
	$name=$_SESSION["username"];
	date_default_timezone_set('Asia/Calcutta'); 
	$Check=date("d-m-Y");
	$check11=date("Y-m-d");

	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>


	<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title">Bhojanshala Food Cash Collection {<?php echo $Check; ?>} </h4>
	        </div>
	    </div>

	    <div class="row">

	<?php

		$sql="SELECT * from foodlogin";
		$food=$conn->query($sql);
		if($food===FALSE)
		{
		throw new Exception("Code 001 : ".mysqli_error($conn));   
		}

		while($row=mysqli_fetch_array($food))
		{
		$sys=$row["username"];
		$bhojan = "Bhojanshala";

		$sql="SELECT * from overallcash where username='$sys' and Branch ='$bhojan'";
		$food1=$conn->query($sql);
		if($food1===FALSE)
		{
		throw new Exception("Code 002 : ".mysqli_error($conn));   
		}
		while($row=mysqli_fetch_array($food1))
		{
		$rs=$row["amount"];
           
	?>
	        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
	<form class="" action="bhojanshala_confirm.php" method="POST" >
	            <div class="dash-widget clearfix card-box">
	                <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
	                <div class="dash-widget-info">
	                    <h3><?php echo $rs; ?></h3>
	                    <span style="font-size:18px; font-weight: bold;"><?php echo strtoupper($sys); ?></span>
	                    <input type="hidden" value="<?php echo $rs; ?>" name="overall">
	                </div>



	                <input type="hidden" name="amt" value ="<?php echo $rs  ?>" onkeypress="return isNumber(event)" class="form-control" placeholder="Enter Received Amount" required="required">
	                <div style="float: right;"> 
	                	<button type="submit" style="color : #fff; " id="btnSubmit" name="btnSubmit" class="btn btn-warning btn-sm m-t-10" <?php if($rs=='0'){ echo "disabled"; } ?> /> <i class='fa fa-check-circle'> </i> Received </button> 
	                </div>
	                 
	            </div>
	        </div>
<?php 

	$sql="SELECT * from balance where Branch='reception'";
	$food3=$conn->query($sql);
	if($food3===FALSE)
	{
	throw new Exception("Code 003 : ".mysqli_error($conn));   
	}

	while($row=mysqli_fetch_array($food3))
	{
		$val=$row["Balance"];
		if($val>0)
		{
		?>
		<input type="hidden" name="amt1" value="<?php echo $val;  ?>" class="form-control" placeholder="Enter Received Amount">
	<?php
		}
		else
		{
	?>
		<input type="hidden" name="amt1" value="0" class="form-control" placeholder="Enter Received Amount">
	<?php
		}
	}
?> 
	<input type="hidden" name="rec" value="<?php echo $bhojan;  ?>">
	<input type="hidden" name="usename" value="<?php echo $name; ?>">
	<input type="hidden" name="dep" value="Reception">
	<input type="hidden" name="debit" value="0">
	<input type="hidden" name="recdate" value="<?php echo $Check; ?>">
	<input type="hidden" name="mysqldate" value="<?php echo $check11; ?>">
	<input type="hidden" name="sys" value="<?php echo $sys; ?>">
	<input type="hidden" name="bhojanshala" value="Bojanshala">
	<input type="hidden" name="vo" value="Fooding">
	<input type="hidden" name="des" value="Fooding Amount of date : <?php echo $Check; ?> Collected by <?php echo $name; ?>  from the <?php echo $sys; ?> "> 
</form>

<?php  }}
 
    $conn->query("COMMIT");

    } catch(Exception $e) { 
 
        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
        
        if ($conn->query($sql) === FALSE) { 
        echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo "
        <script>
        swal({
        title: \"Error !\",
        text: \"$content\",
        icon: \"error\",
        button: \"OK\",
        });
        </script>";    
	}  

	$conn->close();

?>

	    </div>
	   </div>
	</div>

<?php require "_footer.php"; ?>