<?php

require "_session.php";

$id=$conn->real_escape_string(htmlspecialchars($_GET["id"]));

$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 

?>
<!DOCTYPE html>
<html lang="en">
<head>
   <script src="../assets/sweetalert.min.js"></script>

    <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
    <title> Shri Jirawala Parshwanath Jain Tirth - <?php  echo $id;  ?></title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
    @media print {
    a[href]:after {
    content: none !important;
    }
    }
.table>thead>tr>th{
    border-bottom: none !important;
    border-width: 0px !important;
    padding: 4px !important;
}

.table>tbody>tr>td {
    padding: 4px !important;
}

 body{
    font-color: #000 !important;
 }
.row1, .row2, .row3, .row4, .row5, .row6, .row7 {
  font-size: 12px !important;
}

@media print{
 
  @page {margin:0; margin-top: 5px !important; }

    .table-bordered>tbody>tr>td, .table>thead>tr>th{
    border:1px solid #000 !important ;
   }
 
}
 
#sidebar{
display: none; 
}

.main-content2{
 
width: 150mm !important; 
padding: 0px !important;
margin: auto !important;
margin-top: 2% !important;
margin-right: 3% !important;
float: right; 
background: white !important; 

}

.main-content{
 
width: 145mm !important; 
padding: 0px !important;
margin: auto !important;
margin-top: 4% !important;
margin-right: 4% !important;

float: left; 
background: white !important; 

}
#navbar{
display: none !important;
visibility: hidden !important;
}


</style>
<style type="text/css">
.form-group{
margin-bottom: 10px;
}
label{
color: #000 !important;
}

   .row1, .row2, .row3, .row5 , .row6 , .row7 {
    border:1px solid #000  ;
   }

</style>

<body class="no-skin">
<script >        
function DisableBackButton() {
     window.history.forward()
 }
 DisableBackButton();
 window.onload = DisableBackButton;
 window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
 window.onunload = function () { void (0) }
</script>





<div class="main-container ace-save-state" id="main-container">


       <div class="main-content2">
    <div class="main-content-inner">

    <div class="page-content">

    <div class="dtrec">
    <div class="col-xs-12">
    <div class="panel panel-default" style="border-color: #000 !important; border-width: 2.5px !important;">
    <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important;  padding-bottom: 4px !important;">
    <div class="row">

    <div class="col-xs-12" style=" ">
    <center><img src="../images/jp.jpg" width="490px" height="90px" >
    <hr style="border-color: #000; margin: 5px !important; padding-bottom: 2px !important;"> </center> 
    </div> 
    <div class="col-xs-12" style="padding-bottom: 02px !important;">

    <div class="col-xs-6" >
    <div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">
<?php

$sql="SELECT * from bookonline where bookid = '$id'";
$res=$conn->query($sql);

            if($res===FALSE)
            {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
            }



while($row=mysqli_fetch_array($res))
            {
?>

    <label style="font-size: 12px !important;"> R. No:</label>
    &nbsp;<?php echo $row["bookid"]; ?>
    </div>


    <div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;"> Name:</label>
    &nbsp;<?php echo strtoupper($row["name"]); ?>                                          
    
    
    </div>

    </div>
  
    <div class="col-xs-6">

    <div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;">Date:</label>
     &nbsp;<?php echo $row["Bookingtime"]; ?>
    </div>

    <div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">
    <label style="font-size: 12px !important;" >City: </label>
      <?php echo ucwords(strtolower($row["city"])); ?> &nbsp; <label style="font-size: 12px !important;"> <b> | </b> &nbsp; Mo: </label> <?php echo $row["Phone"]; ?> </div>       

    </div>
    </div>



<br><br><br><br><br><br><br><br>
<div  style="border-style: dotted; margin-left: auto; margin-right: 1%;; margin-top: 01%;"></div>

<div class="col-xs-12" >


<div class="row" style="margin-top:2%;   ">

<h4 align="center" style="" ><b> एडवांस  बुकिंग विवरण</b> </h4>

</div> 



<div class="row" style="margin-left: auto; margin-top:2%;" >
<div class="col-xs-6">


<p>आगमन .....<?php echo $row["city"]; ?>.....</p>
</div>
<div class="col-xs-6">
<p>से  .....Jirawala ji.........</p>
</div>

</div>







<div class="row" style="margin-left: auto;">
<div class="col-xs-4">


<p>दिनांक  ...<?php echo $row["arrive"]; ?>...</p>
</div>
<div class="col-xs-4">
<p>को  ....<?php echo str_replace("null","",str_replace(":null","",$row["intime"])); ?>....</p>
</div>

<div class="col-xs-4">
<p>बजे यात्री संख्या  .....<?php echo $row["total"]; ?>.....</p>
</div>


</div>








<div class="row" style="margin-left: auto;">
<div class="col-xs-6">


<p>कमरे की श्रेणियां  ...<?php echo $row["rtype"]; ?>...</p>
</div>
<div class="col-xs-6">
<p>व  कमरे की  संख्या   ...<?php echo $row["noofroom"]; ?>....</p>
</div>




</div>






<div class="row" style="margin-left: auto;">
<div class="col-xs-4">


<p>दिनांक  ...<?php echo $row["depart"]; ?>...</p>
</div>
<div class="col-xs-4">
<p>को   ...<?php echo $row["outtime"]; ?>....</p>
</div>

<div class="col-xs-4">
<p>प्रस्थान  करेंगे ........... !</p>
</div>


</div>

</div>
<br><br>


<br><br><br><br><br><br><br>

<div  style="border-style: dotted; margin-left: auto; margin-right: 1%;; margin-top: 3%;"></div>

<div class="row" style=" margin-top:4%; margin-left:3%;">
<div class="col-xs-7">
<?php
$res1=mysqli_query($conn,"SELECT * from bankpayment where bookind_id = '$id'");
while($row1=mysqli_fetch_array($res1))
{
?>

<p>एडवांस  रुपये: ...<?php echo $row1["tr_amt"]; ?>... उपयुक्त राशि प्राप्त हुई  </p>
</div>
<div class="col-xs-5">

<p>बकाया  राशि: .................... ! </p>
</div>


<div class="col-xs-7">

<p>चेक / लेनदेन आईडी: ...<?php echo $row1["tr_id"]; ?>...  </p>
</div>
<div class="col-xs-5">

<p> 
बैंक: ...ICICI Bank... </p>
</div>

</div>

<div  style="border-style: dotted; margin-left: auto; margin-right: 1%;; margin-top: 2%;"></div>
<br><br>
<div class="col-xs-12">
<div class="col-xs-6">

हस्ताक्षर देनेवाला      ..................

</div>
<div class="col-xs-6">

हस्ताक्षर प्राप्तकर्ता      ..................

</div>
</div>



<?php  }} 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 



$conn->close();



?>
    </div>  
    </div>
    </div>
    </div>
    </div>
    </div>
    </div> 
    </div>
    </div>
   


<script type="text/javascript">
function myFunction() { 
window.print();
}
</script>


 
<script>
function myFunction() {
    window.print();
}
</script> 
 
  <center class="hidden-print">
   <div> &nbsp; </div>
    <a href="sjpjt_onlinebookingreg.php" class="btn btn-app btn-danger ">
    <i class="ace-icon fa fa-remove bigger-160"></i>
    Exit
    </a>
    &nbsp; &nbsp; &nbsp; &nbsp;
    <button onclick="myFunction()" class="btn btn-app btn-primary  ">
    <i class="ace-icon fa fa-print bigger-160"></i>
    Print
    </button>
</center>
</body>
</html>
