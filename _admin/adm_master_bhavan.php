<?php
    require "_session.php";
    require "_header.php";
    $username1=$_SESSION["username"];
    $file_name = basename($_SERVER['PHP_SELF']);

    date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");
	$file_name = basename($_SERVER['PHP_SELF']);

?>
<div class="page-wrapper">
	<div class="content"> 
  		<div class="row">
			<div class="col-md-10">
				<div class="card-box"> 
         <CENTER> <h2>ADD WASHROOM</h2></CENTER>
					<form action="add_washroom_details.php" method="POST" autocomplete="off">
              <div class="col-md-12" style="margin-top: 30px;">
				        <div class="row">
				                <div class="col-md-4 form-group">
                        <label> BHAVAN : </label>
                        <Select name="bhavan" class="form-control " required>
                        <option value=""> -- Select -- </option>
                        <option value="YATRIK BHAVAN">YATRIK BHAVAN</option>
                        <option value="VIP BHAVAN">VIP BHAVAN</option>
                        <option value="ATHITI BHAVAN">ATHITI BHAVAN</option>
                        
                        </Select>
                      </div>
                        <div class="col-md-4 form-group">
                      <label> FLOOR : </label>
                        <Select name="floor" class="form-control " required>
                        <option value=""> -- Select -- </option>
                        <option value="UG">UG</option>
                        <option value="G">G</option>
                        </Select>
                        </div>

                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4 form-group">
                      <label>TYPE: </label>
                        <Select name="typewashroom" class="form-control " required>
                        <option value=""> -- Select -- </option>
                        <option value="MALE">MALE</option>
                        <option value="FEMALE">FEMALE</option>
                        <option value="COOMAN">COMMON</option>
                        </Select>
                  </div>
                       <div class="col-md-4 form-group">
                         <label> POSITION: </label>
                        <Select name="position" class="form-control " required>
                        <option value=""> -- Select -- </option>
                        <option value="LEFT">LEFT</option>
                        <option value="RIGHT">RIGHT</option>
                        <option value="NON">NONE</option>

                        </Select>
                        </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-2">
                  <label>LAT</label>
                  <input type="text" class="form-control"value="" name="latlong" id="latlong" required="required">
                  </div>
                  <div class="col-md-2">
                  <label>LONG</label>
                  <input type="text" class="form-control"value="" name="long" id="long" required="required">
                  </div>
                  <div class="col-md-4">
                  <label>NAME </label>
                  <input type="text" class="form-control"value="" name="nameu" id="nameu" required="required">
                  </div>
                </div>
              </div>
              <div class="col-md-12" style="margin-top: 10px;">
                <div class="row">
                <div class="col-md-8 form-group">
                <label> Narration (Description)   </label>

                <textarea name="comment" rows="5" cols="50" class="form-control" placeholder="Enter Narration here ..." required="required"></textarea>
                </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
              <div class="col-md-5">
                <button type="submit" id="btnSubmit" class="btn btn-primary pull-right" value="Submit" > <i class="fa fa-check-circle"></i> Save </button>
                </div></center>
                </div>
              </div>
            </div>

				    </form>
				</div>
				</div>
			</div>
		</div>
	</div>