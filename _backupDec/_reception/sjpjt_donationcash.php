<?php
//done
    require "_session.php";
    require "_header.php";
 
	$name=$_SESSION["username"];
	date_default_timezone_set('Asia/Calcutta'); 
	$Check=date("d-m-Y");
	$check11=date("Y-m-d");
    $starttime = "Donation";

	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>
 
<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title">Donation Cash Collection {<?php echo $Check; ?>} </h4>
	        </div>
	    </div>
	    <div class="row">
 
 <?php   
            $sql="select * from admin group by username";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            {
               $sys=$row["username"];
               $sql="SELECT * from overallcash where username='$sys' and Branch = '$starttime'";
               $res1=$conn->query($sql);
            if($res1===FALSE)
            {
            throw new Exception("Code 002 : ".mysqli_error($conn));   
            }
            while($row1=mysqli_fetch_array($res1))
            { 
            	$Bankamount = 0;
            	$Donationcheuqecancelamount = 0;
            	$DonationCancelAmount = 0;
?>
	    	 <div class="col-md-4">
	            <div class="dash-widget clearfix card-box">
					<form class="" action="Foodcashupdate.php" method="POST" >
 					<input type="hidden" name="sys" value="<?php echo $sys; ?>">

	                <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
	                <div class="dash-widget-info">
	                    <h3><?php echo $row1["amount"]; ?></h3>
	                    <span  style="font-size:18px; font-weight: bold; text-transform: uppercase; "><?php echo strtoupper($sys); ?></span>
	                </div> 
					<input type="hidden" value="<?php echo $row1["amount"]; ?>" name="overall">
					<input type="hidden" value="<?php echo $row1["amount"]; ?>" name="totalamt">

			<?php
			$sql="SELECT * from overallcash where username='$sys' and Branch = 'Donation_cheque'";
			$res2=$conn->query($sql);
			if($res2===FALSE)
			{
			throw new Exception("Code 003: ".mysqli_error($conn));   
			}
			while($row2=mysqli_fetch_array($res2))
			{
			?> 
				<?php $Bankamount = $row2["amount"]; ?>
				<input type="hidden" value="<?php echo $row2["amount"]; ?>" name="b1"> 
			<?php
			}

			$sql="SELECT * from overallcash where username='$sys' and Branch = 'Donation_cheque_cancel'";
			$res3=$conn->query($sql);
			if($res3===FALSE)
			{
			throw new Exception("Code 004 : ".mysqli_error($conn));   
			}
			while($row3=mysqli_fetch_array($res3))
			{
			?>
				<?php $Donationcheuqecancelamount = $row3["amount"]; ?>
				<input type="hidden" value="<?php echo $row3["amount"]; ?>" name="b2">
			<?php
			}
			$sql=" SELECT * from overallcash where username='$sys' and Branch = 'Donation_Cancel'";
			$res4=$conn->query($sql);
			if($res4===FALSE)
			{
			throw new Exception("Code 005 : ".mysqli_error($conn));   
			}
			while($row4=mysqli_fetch_array($res4))
			{
			?>
				<?php $DonationCancelAmount = $row4["amount"]; ?>
				<input type="hidden" value="<?php echo $row4["amount"]; ?>" name="cancelamt">
			<?php
			}
			?>   
          	<input type="hidden" name="rec" value="<?php echo $starttime; ?>">
   
			<?php
			$amount=$row1["amount"];
			if($amount=="0")
			{

			}
			else
			{
			?>
				<input type="hidden" name="amt" value ="<?php echo $amount; ?>" onkeypress="return isNumber(event)" class="form-control" placeholder="Enter Received Amount">
			<?php
			}

			$sql="SELECT * from balance where Branch='reception'";
			$res5=$conn->query($sql);
			if($res5===FALSE)
			{
				throw new Exception("Code 006: ".mysqli_error($conn));   
			}
			if($row5=mysqli_fetch_array($res5))
			{
			?>
				<input type="hidden" name="amt1" value="<?php $row5["Balance"]; ?>" class="form-control" placeholder="Enter Received Amount">
			<?php
			}
			else
			{ ?>
				<input type="hidden" name="amt1" value="0" class="form-control" placeholder="Enter Received Amount">
			<?php
			}
			?>

			<input type="hidden" name="usename" value="<?php echo $name; ?>">
			<input type="hidden" name="dep" value="Reception">
			<input type="hidden" name="debit" value="0">
			<input type="hidden" name="recdate" value="<?php echo $Check; ?>">
			<input type="hidden" name="mysqldate" value="<?php echo $check11; ?>">
			<input type="hidden" name="bhojanshala" value="Donation_Rep">
			<input type="hidden" name="vo" value="Donation">
			<input type="hidden" name="des" value="Donation Amount of date : <?php echo $Check; ?> Collected by <?php echo $name; ?>  from the <?php echo $sys; ?> ">
 
					<div class="user-analytics">
					    <div class="row">
					    	<div class="col-sm-6 col-6 border-right">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $Bankamount; ?></h5>
					                <span class="analytics-title">Bank Amount</span>
					            </div>
					        </div>

					        <div class="col-sm-6 col-6 ">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $DonationCancelAmount; ?></h5>
					                <span class="analytics-title">Donation Cancel Amount</span>
					            </div>
					        </div>
					        <div class="col-sm-12 col-12 ">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $Donationcheuqecancelamount; ?></h5>
					                <span class="analytics-title">Donation Cheque Cancel Amount</span>
					            </div>
					        </div>

					        
					    </div>
					</div> 
	                <div style="float: right;"> 
	                	<button type="submit" style="color : #fff; " id="btnSubmit" name="btnSubmit" class="btn btn-warning btn-sm m-t-10"  /> <i class='fa fa-check-square'> </i> Confirm </button> 
	                </div> 
	            	</form>
				</div>
	        </div>	
	<?php
		}

		}
	?>
	    </div>
	</div>
</div>
  <script type="text/javascript">
        window.onbeforeunload = function () {
            var inputs = document.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "button" || inputs[i].type == "submit") {
                    inputs[i].disabled = true;
                }
            }
        };
    </script>
 <?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }

                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 

$conn->close();

require "_footer.php"; ?>
