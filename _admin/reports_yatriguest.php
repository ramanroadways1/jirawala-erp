<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("y-m-d");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");


try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
			<div class="page-wrapper">
				<div class="content"> 
  					<div class="row">

				<!-- ################### ################### REPORT 3 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="accountdonation.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">DONATION REGISTER

				                [YATRI]</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt5" name="starttime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt6" name="endtime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    <select class="select" id="" name="sys" required="required">
				                            <option value="0"> -- ALL -- </option>
											<?php
											$sql="select * from admin group by username ";
											$rep1=$conn->query($sql);
											if($rep1===FALSE)
											{
												throw new Exception("Code 001 : ".mysqli_error($conn));   
											}
											while($row=mysqli_fetch_array($rep1))
											{
											?>
												<option value="<?php echo $row["id"]  ?>"><?php echo strtolower($row["username"]);  ?></option> 
											<?php } ?>
				                     </select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>




				<div class="col-md-3">
				<div class="card-box"> 
					<form action="accountdonation2.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">DONATION REGISTER

				                [GUEST]</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt21" name="starttime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt22" name="endtime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    <select class="select" id="" name="sys" required="required">
				                            <option value="0"> -- ALL -- </option>
											<?php
											$sql="select * from admin group by username ";
											$rep1=$conn->query($sql);
											if($rep1===FALSE)
											{
												throw new Exception("Code 001 : ".mysqli_error($conn));   
											}
											while($row=mysqli_fetch_array($rep1))
											{
											?>
												<option value="<?php echo $row["id"]  ?>"><?php echo strtolower($row["username"]);  ?></option> 
											<?php } ?>
				                     </select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>


	<!-- ################### ################### REPORT 12 ################### ###################  --> 

	          <div class="col-md-12" style="margin-top:10px;">
	          		<u><b><center><h3 style="color: #000000">LIVE DONATION REPORTS
	          		</h3><?php echo $date;?></center></b></u>
            <div class="table-responsive">
              <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                <thead>

                  <tr>
                    <th>D.R.</th>
                    <th>NAME</th>
                    <th>CITY</th>
                    <th>AMOUNT</th>
                    <th>PAYMENT TYPE</th>
                    <th>RECIVING TIME</th>
                    <th>YATRI/GUEST</th>
                   
                  </tr>
                </thead>
                <tbody>
                <?php

                $sql="SELECT * from donation WHERE date between '$date' and '$date'";
                // echo $sql;
                  $res=$conn->query($sql);
                  if($res===FALSE)
                  {
                  throw new Exception("Code 001 : ".mysqli_error($conn));   
                  }
                  if ($res->num_rows > 0) {
                  while($row=mysqli_fetch_array($res))
                  {
                  ?>
 
                    <tr>
                    	 <td  > <a target="_blank" href="donationSlip.php?id=<?php echo $row["rec_id"]; ?>"><?php echo $row["rec_id"]; ?></a>  </td>
             
                    <td><?php echo $row["name"]; ?></td>
                    <td><?php echo ucwords(strtolower($row["city"])); ?></td>
                    <td><?php echo $row["amount"]; ?> </td>
                    <td><?php echo $row["paymentype"]; ?> </td>
                    <td><?php echo $row["rec_time"]; ?> </td>
                    <th>
                      <?php 
                      $Bookingid=$row["Bookingid"];

                      if($Bookingid=="0")
                      {
                      	$gest="GUEST";
                        ?>
                     
                    
                    <button  class="btn btn-warning btn-sm " type="submit" ><span> <i class="fa fa-user" aria-hidden="true" ></i>GUEST</span></button>
                 
                      <?php
                      }
                      else
                      {
                        echo '<button  class="btn btn-success btn-sm " type="submit" ><span> <i class="fa fa-user" aria-hidden="true"></i>YATRI</span></button>';

                      }



                        ?>
                    

                    </th>

                  
                    </tr>

                <?php  } } else {

                  echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
                }?>
                </tbody>
              </table>
            </div>
          </div>	

                </div>
	    	</div>
		</div>


<script type="text/javascript">

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$(document).ready(function () {

	// ############ start ############
	$("#dt1").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt2');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt2').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############
	
	// ############ start ############
	$("#dt3").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt4');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt4').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt5").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt6');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt6').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt7").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt8');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt8').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt9").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt10');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt10').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt11").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt12');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt12').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt13").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt14');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt14').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt15").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt16');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt16').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt17").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt18');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt18').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt19").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt20');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt20').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt21").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt22');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt22').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 



	$("#dt25").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt26');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt26').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	




	$("#dt27").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt28');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt28').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


	$("#dt29").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt30');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt30').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


$("#dt31").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt32');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt32').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


});



function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>