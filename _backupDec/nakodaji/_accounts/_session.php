
<?php

	session_start();
     if((time() - $_SESSION['last_login_timestamp']) > 3600) // 3600= 60 * 60  
           {  
                header("location:logout.php");  
           }  

	if(!isset($_SESSION["username"])) {
		header('Location: ../index.php');
		exit();
	}

	if($_SESSION["dep"]!="admin") {
		header('Location: ../index.php');
		exit();
	}

	require('../connection.php');
	$username=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);