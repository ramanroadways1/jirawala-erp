<?php
//done
    require "_session.php";
    require "_header.php";
 
 	$name1=$_SESSION["username"];
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");

	$Check=date("d-m-Y");
	$sqldate=date("Y-m-d");
	$hh=date("h:i:A");

	$state=$conn->real_escape_string(htmlspecialchars($_POST["state"]));
	$name=$conn->real_escape_string(htmlspecialchars($_POST["name"]));
	$address=$conn->real_escape_string(htmlspecialchars($_POST["address"]));
	$mnumber=$conn->real_escape_string(htmlspecialchars($_POST["mnumber"]));
	$male=$conn->real_escape_string(htmlspecialchars($_POST["male"]));
	$female=$conn->real_escape_string(htmlspecialchars($_POST["female"]));
	$child=$conn->real_escape_string(htmlspecialchars($_POST["child"]));
	$driver=$conn->real_escape_string(htmlspecialchars($_POST["driver"]));
	$total=$conn->real_escape_string(htmlspecialchars($_POST["total"]));
	$identity=$conn->real_escape_string(htmlspecialchars($_POST["identity"]));
	$address=$conn->real_escape_string(htmlspecialchars($_POST["address"]));
	$snumber=$conn->real_escape_string(htmlspecialchars($_POST["snumber"]));
	$checkin=$conn->real_escape_string(htmlspecialchars($_POST["checkin"]));
	$checkout=$conn->real_escape_string(htmlspecialchars($_POST["checkout"]));
	$intime=$conn->real_escape_string(htmlspecialchars($_POST["intime"]));
	$kkk=$conn->real_escape_string(htmlspecialchars($_POST["kkk"]));
	$outtime=$conn->real_escape_string(htmlspecialchars($_POST["outtime"]));
	$apm=$conn->real_escape_string(htmlspecialchars($_POST["apm"]));

	$name = strtoupper($name);
	$address = strtoupper($address);
	$snumber = strtoupper($snumber);

	$diff = abs(strtotime($checkin) - strtotime($checkout));
	$differenceInDays=($diff/(60*60*24));

try
 {
    $conn->query("START TRANSACTION"); 
?>

<link rel="stylesheet" type="text/css" href="../assets/css/bootstarp-multi.css">
<script type="text/javascript" src="../assets/js/multi-js.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#multiselect').multiselect({    
   buttonWidth : '250px',
   includeSelectAllOption : true,
   maxHeight: '280',
		nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect").val();
 
	for(var i=0; i<selectedVal.length; i++){
		function innerFunc(i) {
			setTimeout(function() {
				location.href = selectedVal[i];
			}, i*200000000000000000000);
		}
		innerFunc(i);
	}
}

$(document).ready(function() {
  $('#multiselect1').multiselect({
  buttonWidth : '250px',
    includeSelectAllOption : true,
    maxHeight: '280',
		nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect1").val();
	for(var i=0; i<selectedVal.length; i++){
		function innerFunc(i) {
			setTimeout(function() {
				location.href = selectedVal[i];
			}, i*2000);
		}
		innerFunc(i);
	}
}

$(document).ready(function() {
  $('#multiselect2').multiselect({
    buttonWidth : '250px',
    includeSelectAllOption : true,
    maxHeight: '300',
		nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect2").val();
	for(var i=0; i<selectedVal.length; i++){
		function innerFunc(i) {
			setTimeout(function() {
				location.href = selectedVal[i];
			}, i*2000);
		}
		innerFunc(i);
	}
}
</script>

<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title"> Bhavan Room Booking </h4>
	        </div>
	    </div>

		<div class="row">
        <div class="col-md-12">
            <div class="card-box">
             <!--    <h4 class="card-title">Two Column Horizontal Form</h4>
                <h4 class="card-title">Personal Information</h4> -->
				<form action="Room_Booking_insert.php" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label>FULL NAME</label>
                                <input type="text" class="form-control" value="<?php echo $name;  ?>" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>MOBILE NUMBER</label>
                                <input type="text" class="form-control" value="<?php echo $mnumber;  ?>" readonly>
                            </div>
                            
                            <div class="col-md-4 form-group">
                                <label> ADDRESS</label>
                                <input type="text" class="form-control" value="<?php echo $address;  ?>, <?php echo $state;  ?>" readonly>
                            </div> 
 
                             <div class="col-md-2 form-group">
                                <label>TOTAL PERSON</label>
                                <input type="text" class="form-control" value="<?php echo $total;  ?>" readonly>
                            </div>

                            <div class="col-md-2 form-group">
                                <label><?php echo $identity;  ?></label>
                                <input type="text" class="form-control" value="<?php echo $snumber;  ?>" readonly>
                            </div>

                            <div class="col-md-2 form-group">
                                <label>CHECK IN </label>
                                <input type="text" value="<?php echo $checkin; ?> <?php echo $intime; ?> <?php echo $kkk; ?>" class="form-control" readonly>
                            </div>

                            <div class="col-md-2 form-group">
                                <label>CHECK OUT </label>
                                <input type="text" value="<?php echo $checkout; ?> <?php echo $outtime; ?> <?php echo $apm; ?>" class="form-control" readonly>
                            </div>

					<?php
					$sql="select * from admin where username = '$name1'";
					$res=$conn->query($sql);
					if($res===FALSE)
					{
						throw new Exception("Code 001 : ".mysqli_error($conn));   
					}
					while($row=mysqli_fetch_array($res))
					{

					?>
						<input type="hidden" name="sys" value="<?php echo $row["id"]; ?> ">
					<?php
					}
					?>
					<input type="hidden" name="lst" value="<?php echo $name1; ?>">
					<input type="hidden" name="name" value="<?php echo $name; ?>">
					<input type="hidden" name="address" value="<?php echo $address; ?>">
					<input type="hidden" name="adv" value="0">
					<input type="hidden" name="mnumber" value="<?php echo $mnumber; ?>">
					<input type="hidden" name="identity" value="<?php echo $identity; ?>">
					<input type="hidden" name="snumber" value="<?php echo $snumber; ?>">
					<input type="hidden" name="apm" value="<?php echo $differenceInDays;?>">
					<input type="hidden" name="guestname" value="0">
					<input type="hidden" name="guesttype" value="0">
					<input type="hidden" name="guestage" value="0">
					<input type="hidden" name="membernum" value="0">
					<input type="hidden" name="email21" value="0">
					<input type="hidden" name="mode" value="Offline">
					<input type="hidden" name="noofroom" value="0">
					<input type="hidden" name="roomtype" value="0">
					<input type="hidden" id="checkout" name="BookingTime" value="<?php echo $Check; ?> <?php echo $hh; ?>" >
					<input type="hidden" name="checkin" value="<?php echo $checkin;  ?>">
					<input type="hidden" name="intime" value="<?php echo $intime.":".$kkk; ?> ">
					<input type="hidden" name="checkout" value="<?php echo $checkout;  ?>">
					<input type="hidden" name="outtime" value="<?php echo $outtime.":".$apm;  ?>">
					<input type="hidden" name="male" value="<?php echo $male;  ?>">
					<input type="hidden" name="female" value="<?php echo $female;  ?>">
					<input type="hidden" name="child" value="<?php echo $child;  ?>">
					<input type="hidden" name="driver" value="<?php echo $driver;  ?>">
					<input type="hidden" name="total" value="<?php echo $total;  ?>">
					<input type="hidden" name="state" value="<?php echo $state;  ?>">
 

						<div class="col-md-3 form-group">
						    <label>YATRIK BHAVAN <font color="red"><b>*</b></font> </label>
							<select id="multiselect" name="multiselect[]" multiple="multiple" >
							<?php 
							$sql="select * from bigdharmshala where status='0'";
							$Yatrik=$conn->query($sql);
							if($Yatrik===FALSE)
							{
							throw new Exception("Code 002 : ".mysqli_error($conn));   
							}
							while($row=mysqli_fetch_array($Yatrik))
							{
							?>
							<option value="<?php echo $row["id"]; ?>" class="green" ><?php echo $row["id"]; ?> &nbsp;<?php echo $row["big"] ?> </option>
							<?php 
							}
							?>
							</select>
						</div>

  
						<div class="col-md-3 form-group">
						    <label>VIP BHAVAN <font color="red"><b>*</b></font></label>
							<select id="multiselect1"  class="form-control" name="multiselect1[]" multiple="multiple">
							<?php 
							$sql="select * from bigdharmshala where status_vid='2'";
							$Yatrik=$conn->query($sql);
							if($Yatrik===FALSE)
							{
							throw new Exception("Code 003 : ".mysqli_error($conn));   
							}
							while($row=mysqli_fetch_array($Yatrik))
							{
							?>
							<option value="<?php echo $row["vid"]; ?>" class="green" ><?php echo $row["vid"]; ?> &nbsp;<?php echo $row["vtype"] ?> </option>
							<?php 
							}
							?>
							</select>
						</div>


						<div class="col-md-3 form-group">
						    <label>ATHITI BHAVAN <font color="red"><b>*</b></font></label>
							<select id="multiselect2" name="multiselect2[]" multiple="multiple" >
								<option value="1001">(UPPER HALL)</option>
								<option value="14">(SB HALL)</option>
								<option value="115">(M-HALL)</option>
                                 <option value="215" >(P HALL)</option>
                                  <option value="230" >(Store)</option>
							<?php 
							//$sql="select * from bigdharmshala where sstatus='4' ";
							$sql="select * from bigdharmshala where sstatus='4' AND NOT(sid='14' OR sid='115' OR sid='215' OR sid='230' OR sid='1001')";
							$Yatrik=$conn->query($sql);
							if($Yatrik===FALSE)
							{
							throw new Exception("Code 004 : ".mysqli_error($conn));   
							}
							while($row=mysqli_fetch_array($Yatrik))
							{
							?>
							<option value="<?php echo $row["sid"]; ?>" class="green" ><?php echo $row["sid"]; ?> &nbsp;<?php echo $row["stype"] ?> </option>
							<?php 
							}
							?>
							</select>
						</div>


							<div class="col-md-2 form-group">
							    <label>BOOKING TYPE</label>
							    <select class="form-control" name="status" required="required">
							              <option value="">-- Select --</option>
									      <option value="0">Counter Booking</option>
									      <option value="1">Advance Booking</option>
							     </select>
							</div>


							<div class="col-md-3 form-group">
							    <label>BANK NAME</label>
							    <select class="form-control" id="payname" name="bankname" required="required">
							           <option value="">-- Select --</option>
									   <option value="State_Bank_of_India_Reodar">State Bank of India Reodar </option>
									   <option value="Bank_of_Baroda_Reodar">Bank of Baroda Reodar </option>
									   <option value="Union_Bank_Ahemdabad">Union Bank Ahemdabad </option>
									   <option value="Bank_of_Maharashtra_Mumbai"> Bank of Maharashtra Mumbai </option>
									   <option value="ICICI_Bank">ICICI Bank</option>
							     </select>
							</div>

							 <div class="col-md-2 form-group">
                                <label>VOUCHER NO</label>
                                <input type="text" class="form-control" name="vou_number">
                            </div>

                            <div class="col-md-2 form-group">
                                <label>TOTAL DEPOSIT</label>
								<input type="text" class="form-control" onkeypress="return isNumber(event)" name="amt" onkeyup="document.getElementById('amt').value=convertNumberToWords(this.value)" required="required"> 
							</div>

							<div class="col-md-5 form-group">
                                <label>TOTAL DEPOSIT <sup>(In Words)</sup></label>
    							<input type="text" class="form-control" id="amt" name="amtws" readonly="readonly">
 							</div>

<div class="col-md-2 offset-md-5 form-group text-right">
<label style="visibility:  hidden;" > Bhavan Room Booking </label>
 <button type="submit" class="btn btn-primary"> CONFIRM <i class="fa fa-check-circle" aria-hidden="true"></i> </button>
</div>




                        </div>
                    </div>
                    </div> 
                    <div class="text-right">
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div> 
<script type="text/javascript">

function convertNumberToWords(amount) {
	var words = new Array();
	words[0] = '';
	words[1] = 'One';
	words[2] = 'Two';
	words[3] = 'Three';
	words[4] = 'Four';
	words[5] = 'Five';
	words[6] = 'Six';
	words[7] = 'Seven';
	words[8] = 'Eight';
	words[9] = 'Nine';
	words[10] = 'Ten';
	words[11] = 'Eleven';
	words[12] = 'Twelve';
	words[13] = 'Thirteen';
	words[14] = 'Fourteen';
	words[15] = 'Fifteen';
	words[16] = 'Sixteen';
	words[17] = 'Seventeen';
	words[18] = 'Eighteen';
	words[19] = 'Nineteen';
	words[20] = 'Twenty';
	words[30] = 'Thirty';
	words[40] = 'Forty';
	words[50] = 'Fifty';
	words[60] = 'Sixty';
	words[70] = 'Seventy';
	words[80] = 'Eighty';
	words[90] = 'Ninety';
		amount = amount.toString();
		var atemp = amount.split(".");
		var number = atemp[0].split(",").join("");
		var n_length = number.length;
		var words_string = "";
		if (n_length <= 9) {
		var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
		var received_n_array = new Array();
		for (var i = 0; i < n_length; i++) {
		received_n_array[i] = number.substr(i, 1);
		}
		for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
		n_array[i] = received_n_array[j];
		}
		for (var i = 0, j = 1; i < 9; i++, j++) {
		if (i == 0 || i == 2 || i == 4 || i == 7) {
		if (n_array[i] == 1) {
		n_array[j] = 10 + parseInt(n_array[j]);
		n_array[i] = 0;
		}
		}
		}
		value = "";
		for (var i = 0; i < 9; i++) {
		if (i == 0 || i == 2 || i == 4 || i == 7) {
		value = n_array[i] * 10;
		} else {
		value = n_array[i];
		}
		if (value != 0) {
		words_string += words[value] + " ";
		}
		if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Crores ";
		}
		if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Lakhs ";
		}
		if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Thousand ";
		}
		if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
		words_string += "Hundred and ";
		} else if (i == 6 && value != 0) {
		words_string += "Hundred ";
		}
		}
		words_string = words_string.split("  ").join(" ");
		}
	return words_string;
}

window.onbeforeunload = function () {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == "button" || inputs[i].type == "submit") {
            inputs[i].disabled = true;
        }
    }
};

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$('input[id$=checkout]').datepicker({ 
	minDate: '0D',
    maxDate: '+1D',
    dateFormat: 'dd-mm-yy'
});

$("input[id$=checkout]").keypress(function (evt) {
    evt.preventDefault();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>