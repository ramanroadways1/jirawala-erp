<?php 
 
require "_session.php";

// if($_SESSION["dep"]!="rep")
// {
  
// echo "<script type=\"text/javascript\">
//          window.location = \"main.php\";    
// </script>";

// }

$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$Reception_name=$conn->real_escape_string(htmlspecialchars($_POST["name"]));
$mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["make"]));
$enter_amount=$conn->real_escape_string(htmlspecialchars($_POST["rece"]));
$username=$conn->real_escape_string(htmlspecialchars($_POST["username"]));
$dep=$conn->real_escape_string(htmlspecialchars($_POST["dep"]));
$recdate=$conn->real_escape_string(htmlspecialchars($_POST["recdate"]));
$all=$conn->real_escape_string(htmlspecialchars($_POST["all"]));
$last12=$conn->real_escape_string(htmlspecialchars($_POST["blance"]));
$pedi=$conn->real_escape_string(htmlspecialchars($_POST["pedibalance"]));
$reclost=$conn->real_escape_string(htmlspecialchars($_POST["reclost"]));

$Reception_cash = (float)$last12; 
$entry_amount_in_main_office = (float)$enter_amount;
$Remaing_cash_in_reception =  ((float)$Reception_cash - (float)$entry_amount_in_main_office); 

$main_office_amt = (float)$pedi;
$main_office_amount_added = (float)$main_office_amt + (float)$entry_amount_in_main_office; 

$des1=$reclost." submitted all amount in main office to ".$username ;
$des =$username." receive amount form reception by user ".$reclost ;

try {

    $conn->query("START TRANSACTION"); 
          
        $sql="UPDATE balance set Balance='$Remaing_cash_in_reception' WHERE branch ='reception'";
        if($conn->query($sql) === FALSE) {
            throw new Exception("Code 001 : ".mysqli_error($conn));             
        } 

        $sql="UPDATE balance set Balance='$main_office_amount_added' WHERE branch = 'pedi'";
        if($conn->query($sql) === FALSE) {
            throw new Exception("Code 002 : ".mysqli_error($conn));             
        } 

        $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$username','$Reception_name','$recdate','$mysqldate','-','$reclost','$entry_amount_in_main_office','0','$Remaing_cash_in_reception','Submit_main_office','$des1','0')";
        if($conn->query($sql) === FALSE) {
            throw new Exception("Code 003 : ".mysqli_error($conn));             
        } 

        $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$username','$dep','$recdate','$mysqldate','-','$reclost','0','$entry_amount_in_main_office','$main_office_amount_added','Receive_Form_Reception','$des','0')";
        if($conn->query($sql) === FALSE) {
            throw new Exception("Code 004 : ".mysqli_error($conn));             
        } 

    $conn->query("COMMIT");

    echo "<script type=\"text/javascript\">
    window.alert('Amount Received !');
    window.location = \"acc_AdminCashj.php\";    
    </script>";


} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "<script type=\"text/javascript\">
            window.alert('$content');
            window.location = \"adm_updateamt.php\";    
            </script>";
} 

$conn->close();
?>