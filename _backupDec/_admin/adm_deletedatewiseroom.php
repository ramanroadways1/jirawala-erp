<?php
//done
require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$id=$conn->real_escape_string(htmlspecialchars($_GET["id"]));

try
 {
    $conn->query("START TRANSACTION"); 

    $sql="delete from onlinerooms where id = '$id'";
    $res=$conn->query($sql);
    if($res===FALSE)
    {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
    }

    $conn->query("COMMIT");

    echo "<script type=\"text/javascript\">
    window.alert(' Deleted Successfully !');
    window.location = \"adm_changestatus.php\";    
    </script>";

} catch(Exception $e) { 

        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

        if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo "<script type=\"text/javascript\">
        window.alert('$content');
        window.location = \"adm_changestatus.php\";    
        </script>";
} 

$conn->close();

?>