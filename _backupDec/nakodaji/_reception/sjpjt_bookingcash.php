<?php
//done
    require "_session.php";
    require "_header.php";
 
	$name=$_SESSION["username"];
	date_default_timezone_set('Asia/Calcutta'); 
	$Check=date("d-m-Y");
	$check11=date("Y-m-d");

	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>

<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title">Room Booking Cash Collection {<?php echo $Check; ?>} </h4>
	        </div>
	    </div>

	    <div class="row">

<?php

	$sql="select * from admin group by username";
	$book=$conn->query($sql);

	if($book===FALSE)
	{
	throw new Exception("Code 001 : ".mysqli_error($conn));   
	}
 
	while($row=mysqli_fetch_array($book))
	{
	$sys=$row["username"];
	;
	$sql="SELECT * from overallcash where username='$sys' and Branch = 'Booking'";

	$book1=$conn->query($sql);

	if($book1===FALSE)
	{
	throw new Exception("Code 002 : ".mysqli_error($conn));   
	} 

	while($row=mysqli_fetch_array($book1))
	{
	$rs=$row["amount"];

$AdvanceDepositamount = 0;
$ReceptionRefundedamount = 0;
$BankRefundedamount = 0;
$CancelBookingamount = 0;
  ?>

	        <div class="col-md-4">
	            <div class="dash-widget clearfix card-box">
					<form class="" action="Foodcashupdate.php" method="POST" >
					<input type="hidden" name="sys" value="<?php echo $sys; ?>"> 

	                <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
	                <div class="dash-widget-info">
	                    <h3><?php echo $rs; ?></h3>
	                    <span  style="font-size:18px; font-weight: bold; text-transform: uppercase; "><?php echo $sys; ?></span>
	                </div> 
					<input type="hidden" value="<?php echo $rs; ?>" name="totalamt"> 
					<input type="hidden" value="<?php echo $rs; ?>" name="overall">
					<input type="hidden" name="rec" value="">
					<div class="user-analytics">
					    <div class="row">

						<?php 
						$sql="SELECT * from overallcash where username='$sys' and Branch = 'Advance_Booking'"; 
						$book2=$conn->query($sql); 
						if($book2===FALSE)
						{
						throw new Exception("Code 003 : ".mysqli_error($conn));   
						} 
						if($row=mysqli_fetch_array($book2))
						{
						?>
						<?php $AdvanceDepositamount = $row["amount"]; ?>
						<input type="hidden" value="<?php echo $row["amount"]; ?>" name="advanc">
						<?php
						}

						$sql="SELECT * from overallcash where username='$sys' and Branch = 'Room_Booking_Desposit_Refunded'";
						$book3=$conn->query($sql);
						if($book3===FALSE)
						{
						throw new Exception("Code 004 : ".mysqli_error($conn));   
						}
						while($row=mysqli_fetch_array($book3))
						{
						?>
						<?php $ReceptionRefundedamount = $row["amount"]; ?>
						<input type="hidden" value="<?php echo $row["amount"]; ?>" name="refundamt">
						<?php
						}

						$sql="SELECT * from overallcash where username='$sys' and Branch = 'Room_Booking_Desposit_Refunded_by_Bank'";
						$book4=$conn->query($sql);
						if($book4===FALSE)
						{
						throw new Exception("Code 005 : ".mysqli_error($conn));   
						}
						while($row=mysqli_fetch_array($book4))
						{
						?>
						<?php $BankRefundedamount = $row["amount"]; ?>
						<input type="hidden" value="<?php echo $row["amount"]; ?>" name="refundamt1">
						<?php
						}

						$sql="SELECT * from overallcash where username='$sys' and Branch = 'Booking_Cancel'";
						$book5=$conn->query($sql);
						if($book5===FALSE)
						{
						throw new Exception("Code 006 : ".mysqli_error($conn));   
						}
						if($row=mysqli_fetch_array($book5))
						{
						?>
						<?php $CancelBookingamount = $row["amount"]; ?>
						<input type="hidden" value="<?php echo $row["amount"]; ?>" name="cancelamt">
						<?php 
						} 
						if($rs=='0')
						{
						?> 
						<input type="hidden" name="amt" class="form-control"  value ="0">
						<?php
						}
						else
						{
						?>
						<input type="hidden" name="amt" class="form-control"  value ="<?php echo $rs; ?>" onkeypress="return isNumber(event)" placeholder="Enter Received Amount">
						<?php
						}
						$sql="SELECT * from balance where Branch='reception'";
						$book7=$conn->query($sql);
						if($book7===FALSE)
						{
						throw new Exception("Code 007 : ".mysqli_error($conn));   
						}

						while($row=mysqli_fetch_array($book7))
						{
						$val=$row["Balance"];
							if($val>0)
							{
							?>
							<input type="hidden" name="amt1" value="<?php echo $val;  ?>" class="form-control" placeholder="Enter Received Amount">
							<?php
							}
						}
						?>

					<input type="hidden" name="usename" value="<?php echo $name; ?>">
					<input type="hidden" name="dep" value="Reception">
					<input type="hidden" name="refuned" value="Refund">
					<input type="hidden" name="recdate" value="<?php echo $Check; ?>">
					<input type="hidden" name="mysqldate" value="<?php echo $check11; ?>">
					<input type="hidden" name="bhojanshala" value="Booking_Rep">
					<input type="hidden" name="vo" value="Booking">
					<input type="hidden" name="des" value="Fooding Amount of date : <?php echo $Check; ?> Collected by <?php echo $name; ?>  from the <?php echo $sys; ?> ">
  
	        				<div class="col-sm-6 col-6 border-right">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $AdvanceDepositamount; ?></h5>
					                <span class="analytics-title">Advance Deposit Amount</span>
					            </div>
					        </div>
					        <div class="col-sm-6 col-6 ">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $ReceptionRefundedamount; ?></h5>
					                <span class="analytics-title">Reception Refund Amount</span>
					            </div>
					        </div>
					        <div class="col-sm-6 col-6 border-right">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $BankRefundedamount; ?></h5>
					                <span class="analytics-title">Bank Refunded Amount </span>
					            </div>
					        </div>
					        <div class="col-sm-6 col-6 ">
					            <div class="analytics-desc">
					                <h5 class="analytics-count"><?php echo $CancelBookingamount; ?></h5>
					                <span class="analytics-title">Cancel Booking Amount</span>
					            </div>
					        </div>
					    </div>
					</div> 
	                <div style="float: right;"> 
	                	<button type="submit" style="color : #fff; " id="btnSubmit" name="btnSubmit" class="btn btn-warning btn-sm m-t-10"  /> <i class='fa fa-check-square'> </i> Confirm </button> 
	                </div> 
	            </form>
	            </div>
	        </div>
		<?php }} ?>
		<script type="text/javascript">
		    window.onbeforeunload = function () {
		        var inputs = document.getElementsByTagName("input");
		        for (var i = 0; i < inputs.length; i++) {
		            if (inputs[i].type == "button" || inputs[i].type == "submit") {
		                inputs[i].disabled = true;
		            }
		        }
		    };
		</script>  
	    </div>
	</div>
</div>

<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
        if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
        } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        }


        echo "
        <script>
        swal({
        title: \"Error !\",
        text: \"$content\",
        icon: \"error\",
        button: \"OK\",
        });
        </script>";    
} 

$conn->close();

require "_footer.php"; ?>