<?php 

require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$lst=$conn->real_escape_string(htmlspecialchars($_POST["lst"]));
$branch ="";
$branch=$conn->real_escape_string(htmlspecialchars($_POST["branch"]));
$name=$conn->real_escape_string(htmlspecialchars($_POST["name"]));
$city=$conn->real_escape_string(htmlspecialchars($_POST["city"]));
$number=$conn->real_escape_string(htmlspecialchars($_POST["number"]));
$k1=$conn->real_escape_string(htmlspecialchars($_POST["k1"]));
$k2=$conn->real_escape_string(htmlspecialchars($_POST["k2"]));
$k3=$conn->real_escape_string(htmlspecialchars($_POST["k3"]));
$k4=$conn->real_escape_string(htmlspecialchars($_POST["k4"]));
$k5=$conn->real_escape_string(htmlspecialchars($_POST["k5"]));
$k6=$conn->real_escape_string(htmlspecialchars($_POST["k6"]));
$k7=$conn->real_escape_string(htmlspecialchars($_POST["k7"]));
$k8=$conn->real_escape_string(htmlspecialchars($_POST["k8"]));
$k9=$conn->real_escape_string(htmlspecialchars($_POST["k9"]));
$k10=$conn->real_escape_string(htmlspecialchars($_POST["k10"]));
$k11=$conn->real_escape_string(htmlspecialchars($_POST["k11"]));
$k12=$conn->real_escape_string(htmlspecialchars($_POST["k12"]));
$k13=$conn->real_escape_string(htmlspecialchars($_POST["k13"]));
$k14=$conn->real_escape_string(htmlspecialchars($_POST["k14"]));
$k15=$conn->real_escape_string(htmlspecialchars($_POST["k15"]));
$k16=$conn->real_escape_string(htmlspecialchars($_POST["k16"]));
$k17=$conn->real_escape_string(htmlspecialchars($_POST["k17"]));
$k18=$conn->real_escape_string(htmlspecialchars($_POST["k18"]));
$k19=$conn->real_escape_string(htmlspecialchars($_POST["k19"]));
$k20=$conn->real_escape_string(htmlspecialchars($_POST["k20"]));
$k21=$conn->real_escape_string(htmlspecialchars($_POST["k21"]));
$k22=$conn->real_escape_string(htmlspecialchars($_POST["k22"]));
$k23=$conn->real_escape_string(htmlspecialchars($_POST["k23"]));
$k24=$conn->real_escape_string(htmlspecialchars($_POST["k24"]));
$k25=$conn->real_escape_string(htmlspecialchars($_POST["k25"]));
$k26=$conn->real_escape_string(htmlspecialchars($_POST["k26"]));
$k27=$conn->real_escape_string(htmlspecialchars($_POST["k27"]));
$k28=$conn->real_escape_string(htmlspecialchars($_POST["k28"]));
$k29=$conn->real_escape_string(htmlspecialchars($_POST["k29"]));
$k30=$conn->real_escape_string(htmlspecialchars($_POST["k30"]));
$k31=$conn->real_escape_string(htmlspecialchars($_POST["k31"]));
$k32=$conn->real_escape_string(htmlspecialchars($_POST["k32"]));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
$date=$conn->real_escape_string(htmlspecialchars($_POST["recdate"]));
$rectime=$conn->real_escape_string(htmlspecialchars($_POST["rectime"]));
$sta=$conn->real_escape_string(htmlspecialchars($_POST["sta"]));
$paymethod=$conn->real_escape_string(htmlspecialchars($_POST["paytype"]));
$payno=$conn->real_escape_string(htmlspecialchars($_POST["payno"]));
$payname=$conn->real_escape_string(htmlspecialchars($_POST["payname"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["amtws"]));
$real=$conn->real_escape_string(htmlspecialchars($_POST["real"]));
$split=$conn->real_escape_string(htmlspecialchars($_POST["split"]));
$payno=$conn->real_escape_string(htmlspecialchars($_POST["payno"]));
$payname=$conn->real_escape_string(htmlspecialchars($_POST["payname"]));

$name = strtoupper($name);
$city = strtoupper($city);

try {
    $conn->query("START TRANSACTION"); 

        $sql="SELECT COUNT(rec_id) as value FROM donation";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
        }

        while($row=mysqli_fetch_array($res))
        {
            $id_old=$row["value"];
            $id1=(int)$id_old+1;
            if($paymethod=="CASH")
            {
                $sql="SELECT * from balance where Branch='reception'";
                $res=$conn->query($sql);
                if($res===FALSE)
                {
                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                }

                if($row1=mysqli_fetch_array($res))
                {
                    $fetch_amount_donation_reception =$row1["Balance"];
                    $entry_amount_donation =$amt;
                    $total_amount = $fetch_amount_donation_reception + $entry_amount_donation;

                    $sql="SELECT * from overallcash where username='$lst' and Branch='$branch'";
                    $res=$conn->query($sql);
                    if($res===FALSE)
                    {
                        throw new Exception("Code 003 : ".mysqli_error($conn));   
                    }

                    if($row2=mysqli_fetch_array($res))
                    {
                        $amt22 =$amt;
                        $amt1 =$row2["amount"];
                        $amt2 = $amt22+$amt1;
                        $sql="UPDATE overallcash set amount='$amt2' WHERE Branch = 'Donation' and username='$lst'";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 004 : ".mysqli_error($conn));             
                        } 
 
                        $des = "Made donation slip and its Donation R. No. ".$id1;
                        $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$real','$date','$payname','-','0','$amt','$total_amount','Donation','$des','0')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 005 : ".mysqli_error($conn));             
                        } 

                        $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 006 : ".mysqli_error($conn));             
                        } 
                    }
                    else
                    {
                        $des = "Made donation slip and its Donation R. No. ".$id1;
                        $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$real','$date','$payname','-','0','$amt','$total_amount','Donation','$des','0')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 007 : ".mysqli_error($conn));             
                        } 

                        $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 008 : ".mysqli_error($conn));             
                        }

                        $sql="insert into overallcash (username,Branch,Amount) values ('$lst','$branch','$amt')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 009 : ".mysqli_error($conn));             
                        }
                    }
                } 
            }

            if($paymethod=="CHEQUE")
            {
                $sql="SELECT * from balance where Branch='Bank'";
                $res=$conn->query($sql);
                if($res===FALSE)
                {
                    throw new Exception("Code 0010 : ".mysqli_error($conn));   
                }

                while($row1=mysqli_fetch_array($res))
                {
                    $fetch_amount_donation_reception =$row1["Balance"];
                    $entry_amount_donation =$amt;
                    $total_amount = $fetch_amount_donation_reception + $entry_amount_donation;
                    
                    $sql="SELECT * from overallcash where username='$lst' and Branch='Donation_cheque'";
                    $res=$conn->query($sql);
                    if($res===FALSE)
                    {
                        throw new Exception("Code 0011 : ".mysqli_error($conn));   
                    }

                    if($row2=mysqli_fetch_array($res))
                    {
                        $amt22 =$amt;
                        $amt1 =$row2["amount"];
                        $amt2 = $amt22+$amt1;

                        $sql="UPDATE overallcash set amount='$amt2' WHERE Branch = 'Donation_cheque' and username='$lst'";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0012 : ".mysqli_error($conn));             
                        }

                        $des = "Made donation slip and its Donation R. No.".$id1;
                        $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$lst','Reception','$real','$date','$payname','$payno','0','$amt','$total_amount','Donation','$des','$id1')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0013 : ".mysqli_error($conn));             
                        }

                        $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'Bank'";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0014 : ".mysqli_error($conn));             
                        }
                    }
                    else
                    {
                        $des = "Made donation slip and its Donation R. No.".$id1;
                        $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$lst','Reception','$real','$date','$payname','$payno','0','$amt','$total_amount','Donation','$des','$id1')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0015 : ".mysqli_error($conn));             
                        }

                        $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'Bank'";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0014 : ".mysqli_error($conn));             
                        }

                        $sql="insert into overallcash (username,Branch,Amount) values ('$lst','Donation_cheque','$amt')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0015 : ".mysqli_error($conn));             
                        }
                    }
                } 

            }

                    $sql="insert into donation (rec_id,rec_time,date,amount,amountwrd,name,city,number,k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16,k17,k18,k19,k20,k21,k22,k23,k24,k25,k26,k27,k28,k29,k30,k31,k32,userid,paymentype,payno,payname,status,Bookingid,random,email) values ('$id1','$rectime','$date','$amt','$inword','$name','$city','$number','$k1','$k2','$k3','$k4','$k5','$k6','$k7','$k8','$k9', '$k10' ,'$k11','$k12','$k13' ,'$k14','$k15','$k16','$k17','$k18','$k19','$k20','$k21','$k22','$k23','$k24','$k25','$k26','$k27','$k28','$k29','$k30','$k31','$k32','$sys','$paymethod','$payno','$payname','$sta','0', '$id1' , '0') ";
                    if($conn->query($sql) ===TRUE) {
                        // echo "<script type=\"text/javascript\">
                        // window.location = \"donationSlip.php?id=$id1\";    
                        // </script>"; 
                    } 
                    else
                    {
                        // echo "<script type=\"text/javascript\">
                        // window.location = \"NewFile1.php?msg= data is not inserted \";    
                        // </script>";
                        throw new Exception("Code 0016 : ".mysqli_error($conn));    
                    }    
    }

    $conn->query("COMMIT");

    echo ("<script LANGUAGE='JavaScript'>
    window.alert('Succesfully Updated');
    window.location.href = \"donationSlip.php?id=$id1\";
    </script>");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo ("<script LANGUAGE='JavaScript'>
            window.alert('Error: $content');
            window.location.href = \"sjpjt_newfile1.php\";
            </script>");   

            // echo "
            // <script>
            // swal({
            // title: \"Error !\",
            // text: \"$content\",
            // icon: \"error\",
            // button: \"OK\",
            // });
            // </script>";    
} 

$conn->close();

?>