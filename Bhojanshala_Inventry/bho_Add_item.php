
<?php

require "_session.php";
include "../_header.php";
include "left.php";

  if(!empty($_GET["msg"]))
 {
  if($_GET["msg"]=="Success")
 {
 echo "
    <script>
    swal({
    title: \"Good job!\",
    text: \"Data inserted successfully\",
    icon: \"success\",
    button: \"OK\",
    });
    </script>";
}
 }
try
 {
    $conn->query("START TRANSACTION"); 
        
                 
      date_default_timezone_set('Asia/Calcutta'); 
    $Check=date("d-m-Y");
    $Checkout=date("Y-m-d");
    $hh=date("h:i:A");
       
?>
       <div class="page-wrapper">
        <div class="content">

	   
<div class="row">

<div class="col-md-12">

    <div class="col-lg-12">
      
      <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                            <h3 class="panel-title">Item Master</h3>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-8" >
                       
                       
                       
                       
  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a align="right" href="#myModal" class="btn btn-primary" data-toggle="modal">
        <i class="fa fa-plus"></i>
    ADD Item</a>
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
        <form  action="add_item_insert.php" method="post">
            <div class="modal-content">
                <div class="modal-header">
                
                    <h4 class="modal-title">Add Item </h4>
                </div>
                <div class="modal-body">
                    
                       <div class="row">
                       
                         
                       
                       
               
                        
                       
                        
                        
                        
                       <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName"> Item Name :</label>
                            <input type="text"  name="item_name" class="form-control" required="">
                        
                        </div>
                        </div>
                        
                        
                         <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName"> Company Name :</label>
                            <input type="text"  name="company_name" class="form-control" required="">
                        
                        </div>
                        </div>
                        
                           <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Company Location :</label>
                            <input type="text"  name="company_location" class="form-control" required="">
                        
                        </div>
                        </div>
                        
                        
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary"> <i class="fa fa-check"></i> Save</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>                          
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                        </div>
                    </div>
                </div> 

        <div class="row">
            <div class="col-md-6">
                <div class="table-responsive">
                  <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                    <thead>
                <tr>
                <th>ID</th>
                <th>Item Name</th>
                </tr>
                   </thead>
             <?php
             $sql="SELECT * from jirawala_store.bhojanshala_item_name";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001: ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            {
             ?>
              <tr>
              <th><?php echo $row["id"]; ?></th>
              <th><?php echo $row["item_name"]; ?></th>
              </tr>
            <?php  } ?>
         
                  </table>
                </div>
            </div>
            <div class="col-md-4">
                
            </div>
            <div class="col-md-4">
                
            </div>
            

        </div>
                
                
               
                
                
                </div>
            </div>
        </div>
    </div>

</div>




<script>
$(document).ready(function(){
    $("#myModal").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });
});
</script>
 
	   
	   <?php 
	    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


  
     echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");  
} 

$conn->close();

require "../_footer.php";

	   ?>
