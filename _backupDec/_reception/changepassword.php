<?php
//done
    require "_session.php";
    require "_header.php";



try
 {
    $conn->query("START TRANSACTION"); 
?>


<div class="page-wrapper">
	<div class="content">

                <div class="row">
                	<div class="col-md-4"></div>
                    <div class="col-md-4">
                    	<h4 class="page-title" align="center">Change  Password</h4>
                        <form action="changepasswordupdate.php" method="POST">
                            <div class="form-group">
                                <label>Old Password <span class="text-danger">*</span></label>
                                <input class="form-control" name="old_password" type="text" placeholder="Enter Old Password">
                            </div>
                            <div class="form-group">
                                <label>New Password <span class="text-danger"></span></label>
                                <div class="cal-icon">
                                    <input class="form-control" name="password" type="text" placeholder="Enter New Password">
                                </div>
                            </div>
                            <input type="hidden" name="username" value="<?php echo $username;  ?>">
                             <input type="hidden" name="dep" value="rep">
                            <div class="m-t-20 text-center">
                                <button class="btn btn-primary submit-btn">Update</button>
                            </div>
                        </form>
                    </div>



	    </div>
	</div>

	<script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Rejected_Bookings.xls"
                });
            });
        });
    </script>
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>