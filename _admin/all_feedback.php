<?php
//done
    require "_session.php";
    require "_header.php";
 
try
 {
    $conn->query("START TRANSACTION"); 
?>

<div class="page-wrapper">
  <div class="content">
      
          <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title" style="text-transform: uppercase;">Users Feedback</h4>
        </div>
    </div>

    <div class="row">
          <div class="col-md-12" style="margin-top:10px;">
            <div class="table-responsive">
              <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                <thead>

                  <tr>
                    <th>  Id </th>
                    <th>Booking Id</th>
                    <th>Full Name</th>
                    <th>Mobile No.</th>
                    <th>Feedback</th>
                     <th>Date</th>
                    <th>Status </th>
                   
                  </tr>
                </thead>
                <tbody>
                <?php

                $sql="SELECT * FROM `user_feedback` ORDER BY `id` DESC";
                  $res=$conn->query($sql);
                  if($res===FALSE)
                  {
                  throw new Exception("Code 001 : ".mysqli_error($conn));   
                  }
                  if ($res->num_rows > 0) {
                  while($row=mysqli_fetch_array($res))
                  {
                  ?>
 
                    <tr>
                    <td><?php echo $row["id"]; ?></td>
                    <td><?php echo $row["BookingId"]; ?></td>
                    <td><?php echo ucwords(strtolower($row["fullName"])); ?></td>
                    <td><?php echo $row["mobile_no"]; ?> </td>
                    <td><?php echo $row["feed_back"]; ?> </td>
                    <td><?php echo $row["time"]; ?> </td>
                    <th>
                      <?php 
                      $status=$row["status"];

                      if($status=="0")
                      {
                        ?>
                      <form action="read_feedback.php" method="post" onsubmit="return confirm('Are you sure you want to Read Feedback - <?php echo $row["id"]; ?>  ?');">
                    <input type="hidden" name="id" value="<?php echo $row["id"]; ?> ">
                    <button  class="btn btn-warning btn-sm " type="submit" ><span> <i class="fa fa-edit" aria-hidden="true" ></i>UnRead</span></button>
                    </form>
                      <?php
                      }
                      else
                      {
                        echo '<button  class="btn btn-success btn-sm " type="submit" ><span> <i class="fa fa-check" aria-hidden="true"></i>Read</span></button>';

                      }



                        ?>
                    

                    </th>

                  
                    </tr>

                <?php  } } else {

                  echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
                }?>
                </tbody>
              </table>
            </div>
          </div>
                </div>

      </div>
  </div>
 
<!--     <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Online_RoomBooking.xls"
                });
            });
        });
    </script> -->
     
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>