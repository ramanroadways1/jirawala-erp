<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

if(isset($_POST["starttime"])){
	$dated = $dat=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
	$dat= str_replace("/","-",$dat);
	$d1= date("Y-m-d", strtotime($dat)); 
}

try
 {
    $conn->query("START TRANSACTION"); 
?>

<style>
tbody th
{
	color: #444;
}
	</style>

<div class="page-wrapper">
	<div class="content">
	    <div class="row" style="margin-bottom:10px;">
	        <div class="col-sm-12">
	            <h4 class="page-title" > <span >   <a onclick="window.history.go(-1)" class="btn btn-app btn-danger btn-sm ">
									<i style="color:#fff;" class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a>  Online Room Booking of <?php echo $dated; ?></span>   <button type="button" class="btn btn-success btn-rounded"  id="btnExport"> <i class="fa fa-download" aria-hidden="true"></i></button> </h4>
	        </div>
	    </div>
 


						 

		<div class="row">
					<div class="col-md-12" style="margin-top:10px;">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
								<thead>
									<tr>
										<th>  Id </th>
										<th> Full Name </th>
										<th> Address </th>
										<th> Mobile </th>
										<th> No. of Room </th>
										<th> Room <br> Type </th>
										<th> Paid Amount </th>
										<th> Check-In <br> DateTime  </th>
										<th> Check-Out <br> DateTime</th>
										<th> Room Booking  </th>
										<th> Payment Details  </th>
										<th> Reject Booking </th>
									</tr>
								</thead>
								<tbody>
								<?php

										$sql="select * from bookonline where status2='0' and booking_pay='E000'";
									if(isset($_POST["starttime"])){
										$sql="select * from bookonline where status2='0' and mysqlin='$d1' and booking_pay='E000'";
									}
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
									if ($res->num_rows > 0) {
									while($row=mysqli_fetch_array($res))
									{

									$status2=$row["status2"];
							  	?>
 
				 						<tr>
										<th><a href="Adv_Slip.php?id=<?php echo $row["bookid"]; ?>"><?php echo $row["bookid"]; ?></a> </th>
										<th><?php echo ucwords(strtolower($row["name"])); ?></th>
										<th><?php echo ucwords(strtolower($row["city"])); ?></th>
										<th><?php echo $row["Phone"]; ?></th>
										<th><?php echo $row["noofroom"]; ?> </th>
										<th><?php echo $row["rtype"]; ?></th>
										<th><?php echo $row["remaing"]; ?></th>
										<th><?php echo $row["arrive"]; ?> (<?php echo str_replace("null","",str_replace(":null","",$row["intime"])); ?>)  </th>
										<th><?php echo $row["depart"]; ?> (<?php echo $row["outtime"]; ?>) </th>

										<th>
										<form action="sjpjt_onlineroomalot.php" method="post" >
										<input type="hidden" name="id" value="<?php echo $row["bookid"]; ?>">
										<button  class="btn btn-warning btn-sm " type="submit" ><span> <i class="fa fa-calendar-check-o" aria-hidden="true"></i> Book   </span></button>
										</form>
										</th>
										<th>
										<a href="sjpjt_paidstatement.php?id=<?php echo $row["bookid"]; ?>" style="color:white;">
										<button  class="btn btn-success btn-sm "><span> <i class="fa fa-list-ol" aria-hidden="true"></i> View  </span></button></a> </th>

										<th>

										<form action="DeleteBookOnline.php" method="post" onsubmit="return confirm('Are you sure you want to reject this booking - <?php echo $row["bookid"]; ?> ?');">
										<input type="hidden" name="id" value="<?php echo $row["bookid"]; ?>">
										<button  class="btn btn-danger btn-sm " type="submit" ><span> <i class="fa fa-ban" aria-hidden="true"></i> Reject </span></button>
										</form>
										</th>

										</tr>

								<?php  } } else {

									echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
								}?>
								</tbody>
							</table>
						</div>
					</div>
                </div>

	    </div>
	</div>

	    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Online_RoomBookingbydate.xls"
                });
            });
        });
    </script>
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>