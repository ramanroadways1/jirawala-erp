<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

if(isset($_POST["starttime"])){
	$dat=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
	$dat= str_replace("/","-",$dat);
	$d1= date("Y-m-d", strtotime($dat)); 
}

try
 {
    $conn->query("START TRANSACTION"); 
?>

<style>
tbody th
{
	color: #444;
}
	</style>

<div class="page-wrapper">
	<div class="content">
	    <div class="row" style="margin-bottom:10px;">
	        <div class="col-sm-12">
	            <h4 class="page-title" > <span style="cursor: pointer;" onclick="location.href='sjpjt_onlinebookingreg.php'" > Manage Online Room Booking </span>  <button type="button" class="btn btn-success btn-rounded"  id="btnExport"> <i class="fa fa-download" aria-hidden="true"></i></button> </h4>
	        </div>
	    </div>
 

 

						<div class="row filter-row">

						<div class="col-md-2">
						<form action="sjpjt_onlineroomalot.php" method="post">
						    <div class="form-group form-focus">
						        <label class="focus-label">Booking ID</label>
						        <input type="text" name="id"  required="required" class="form-control floating">
						    </div>
						</div>
						<div class="col-md-1">
						    <button href="#" class="btn btn-primary btn-block"> <i class="fa fa-search" aria-hidden="true"></i> </button>
						</div>
						</form>

						<div class="col-md-3">
							<form action="sjpjt_failed.php" method="post" >
						    <div class="form-group form-focus select-focus">
						        <label class="focus-label">Payment Status</label>
						        <select class="select floating" name="fail"  required="required" >
										<option value="" > -- select --  </option>
										<option value="E000" > Successful </option>
										<option value="Not_paid" > Not Paid </option>
										<option value="E0801" > Fail </option>
										<option value="E0802" > User Dropped  </option>
										<option value="E0803" > Canceled by user </option>
										<option value="E001" > Unauthorized Payment Mode </option>
										<option value="E002" > Unauthorized Key </option>
										<option value="E003" > Unauthorized Packet </option>
										<option value="E004" > Unauthorized Merchant </option>
										<option value="E005" > Unauthorized Return UR </option>
										<option value="E006" > Transaction is already paid </option>
										<option value="E007" > Transaction Failed </option>
										<option value="E008" > Failure from Third Party due to Technical Error </option>
										<option value="E009" > Bill Already Expired </option>
										<option value="E0031" > Mandatory fields coming from merchant are empty </option>
										<option value="E0032" > Mandatory fields coming from database are empty </option>
										<option value="E0033" > Payment mode coming from merchant is empty </option>
										<option value="E0034" > PG Reference number coming from merchant is empty </option>
										<option value="E0035" > Sub merchant id coming from merchant is empty </option>
										<option value="E0036" > Transaction amount coming from merchant is empty </option>
										<option value="E0037" > Payment mode coming from merchant is other than 0 to 9 </option>
										<option value="E0038" > Transaction amount coming from merchant is more than 9 digit length </option>
										<option value="E0039" > Mandatory value Email in wrong format </option>
										<option value="E00310" > Mandatory value mobile number in wrong format </option>
										<option value="E00311" > Mandatory value amount in wrong format </option>
										<option value="E00312" > Mandatory value Pan card in wrong format </option>
										<option value="E00313" > Mandatory value Date in wrong format </option>
										<option value="E00314" > Mandatory value String in wrong format </option>
										<option value="E00315" > Optional value Email in wrong format </option>
										<option value="E00316" > Optional value mobile number in wrong format </option>
										<option value="E00317" > Optional value amount in wrong format </option>
										<option value="E00318" > Optional value pan card number in wrong format </option>
										<option value="E00319" > Optional value date in wrong format </option>
										<option value="E00320" > Optional value string in wrong format </option>
										<option value="E00321" >Request packet mandatory columns is not equal to mandatory columns.</option>
										<option value="E00322" > Reference Number Blank </option>
										<option value="E00323" > Mandatory Columns are Blank </option>
										<option value="E00324" > Merchant Reference Number and Mandatory Columns are Blank </option>
										<option value="E00325" > Merchant Reference Number Duplicate </option>
										<option value="E00326" > Sub merchant id coming from merchant is non numeric </option>
										<option value="E00327" > Cash Challan Generated </option>
										<option value="E00328" > Cheque Challan Generated </option>
										<option value="E00329" > NEFT Challan Generated </option>
										<option value="E00330" > Transaction Amount and Mandatory Transaction Amount mismatch in Request URL </option>
										<option value="E00331" > UPI Transaction Initiated Please Accept or Reject the Transaction </option>
										<option value="E00332" > Challan Already Generated, Please re-initiate with unique reference number </option>
										<option value="E00333" > Referer is null/invalid Referer  </option>
										<option value="E00334" > Mandatory Parameters Reference No and Request Reference No parameter values are not matched </option>
										<option value="E00335" > Transaction Cancelled By User </option>
										<option value="E0801" > FAIL </option>
										<option value="E0802" > User Dropped  </option>
										<option value="E0803" > Canceled by user </option>
										<option value="E0804" > User Request arrived but card brand not supported </option>
										<option value="E0805" >  Checkout page rendered Card function not supported </option>
										<option value="E0806" >  Forwarded / Exceeds withdrawal amount limit </option>
										<option value="E0807" > PG Fwd Fail / Issuer Authentication Server failure </option>
										<option value="E0808" > Session expiry / Failed Initiate Check, Card BIN not present </option>
										<option value="E0809" > Reversed / Expired Card </option>
										<option value="E0810" > Unable to Authorize </option>
										<option value="E0811" > Invalid Response Code or Guide received from Issue </option>
										<option value="E0812" > Do not honor </option>
										<option value="E0813" > Invalid transaction </option>
										<option value="E0814" > Not Matched with the entered amount </option>
										<option value="E0815" > Not sufficient funds </option>
										<option value="E0816" > No Match with the card number </option>
										<option value="E0817" > General Error </option>
										<option value="E0818" > Suspected fraud </option>
										<option value="E0819" > User Inactive </option>
										<option value="E0820" > ECI 1 and ECI6 Error for Debit Cards and Credit Cards </option>
										<option value="E0821" > ECI 7 for Debit Cards and Credit Cards </option>
										<option value="E0822" >  System error. Could not process transaction </option>
										<option value="E0823" > Invalid 3D Secure values </option>
										<option value="E0824" > Bad Track Data </option>
										<option value="E0825" >  Transaction not permitted to cardholder </option>
										<option value="E0826" > Rupay timeout from issuing bank </option>
										<option value="E0827" > OCEAN for Debit Cards and Credit Cards </option>
										<option value="E0828" >  E-commerce decline </option>
										<option value="E0829" > This transaction is already in process or already processed </option>
										<option value="E0830" > Issuer or switch is inoperative </option>
										<option value="E0831" > Exceeds withdrawal frequency limit </option>
										<option value="E0832" > Restricted card </option>
										<option value="E0833" > Lost card </option>
										<option value="E0834" > Communication Error with NPCI </option>
										<option value="E0835" > The order already exists in the database </option>
										<option value="E0836" > General Error Rejected by NPCI </option>
										<option value="E0837" > Invalid credit card number </option>
										<option value="E0838" > Invalid amount </option>
										<option value="E0839" > Duplicate Data Posted </option>
										<option value="E0840" > Format error </option>
										<option value="E0841" > SYSTEM ERROR </option>
										<option value="E0842" > Invalid expiration date </option>
										<option value="E0843" > Session expired for this transaction  </option>
										<option value="E0844" > FRAUD - Purchase limit exceeded  </option>
										<option value="E0845" > Verification decline </option>
										<option value="E0846" > Compliance error code for issuer     </option>
										<option value="E0847" > Caught ERROR of type:[ System.Xml.XmlException ] . strXML is not a valid XML string   Failed in Authorize - I </option>
										<option value="E0848" > Incorrect personal identification number  </option>
										<option value="E0849" > Stolen card   </option>
										<option value="E0850" > Transaction timed out, please retry   </option>
										<option value="E0851" > Failed in Authorize - PE </option>
										<option value="E0852" > Cardholder did not return from Rupay </option>
										<option value="E0853" > Missing Mandatory Field(s)The field card_number has exceeded the maximum length of 19 </option>
										<option value="E0854" > Exception in CheckEnrollmentStatus: Data at the root level is invalid. Line 1, position 1. </option>
										<option value="E0855" > CAF status = 0 or 9 </option>
										<option value="E0856" > 412 </option>
										<option value="E0857" > Allowable number of PIN tries exceeded   </option>
										<option value="E0858" > No such issuer  </option>
										<option value="E0859" > Invalid Data Posted </option>
										<option value="E0860" > PREVIOUSLY AUTHORIZED </option>
										<option value="E0861" > Cardholder did not return from ACS </option>
										<option value="E0862" > Duplicate transmission </option>
										<option value="E0863" > Wrong transaction state </option>
										<option value="E0864" > Card acceptor contact acquirer </option>
						        </select>
						    </div>
						</div>
						<div class="col-md-1">
						    <button href="#" class="btn btn-primary btn-block"> <i class="fa fa-search" aria-hidden="true"></i> </button>
						</div>
						</form>

						<div class="col-md-2">
							<form action="sjpjt_onlinebookingreg_bydate.php" method="post" >
						    <div class="form-group form-focus">
						        <label class="focus-label">Date</label>
						        <div class="cal-icon">
						            <input name="starttime"  class="form-control floating datetimepicker" type="text">
						        </div>
						    </div>
						</div>
						<div class="col-md-1">
						    <button href="#" class="btn btn-primary btn-block"> <i class="fa fa-search" aria-hidden="true"></i> </button>
						</div>
						</form>

						<div class="col-md-2">
						<a style="text-transform: none;" href="sjpjt_rejectedbooking.php" class="btn btn-danger btn-block"> <i class="fa fa-ban" aria-hidden="true"></i> 
						Rejected Bookings</a>
						</div>

						</div>

		<div class="row">
					<div class="col-md-12" style="margin-top:10px;">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
								<thead>
									<tr>
										<th>  Id </th>
										<th> Full Name </th>
										<th> Address </th>
										<th> Mobile </th>
										<th> No. of Room </th>
										<th> Room <br> Type </th>
										<th> Paid Amount </th>
										<th> Check-In <br> DateTime  </th>
										<th> Check-Out <br> DateTime</th>
										<th> Room Booking  </th>
										<th> Payment Details  </th>
										<th> Reject Booking </th>
									</tr>
								</thead>
								<tbody>
								<?php

										$sql="select * from bookonline where status2='0' and booking_pay='E000'";
									if(isset($_POST["starttime"])){
										$sql="select * from bookonline where status2='0' and mysqlin='$d1' and booking_pay='E000'";
									}
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
									if ($res->num_rows > 0) {
									while($row=mysqli_fetch_array($res))
									{

									$status2=$row["status2"];
							  	?>
 
				 						<tr>
										<th><a href="Adv_Slip.php?id=<?php echo $row["bookid"]; ?>"><?php echo $row["bookid"]; ?></a> </th>
										<th><?php echo ucwords(strtolower($row["name"])); ?></th>
										<th><?php echo ucwords(strtolower($row["city"])); ?></th>
										<th><?php echo $row["Phone"]; ?></th>
										<th><?php echo $row["noofroom"]; ?> </th>
										<th><?php echo $row["rtype"]; ?></th>
										<th><?php echo $row["remaing"]; ?></th>
										<th><?php echo $row["arrive"]; ?> (<?php echo str_replace("null","",str_replace(":null","",$row["intime"])); ?>)  </th>
										<th><?php echo $row["depart"]; ?> (<?php echo $row["outtime"]; ?>) </th>

										<th>
										<form action="sjpjt_onlineroomalot.php" method="post" >
										<input type="hidden" name="id" value="<?php echo $row["bookid"]; ?>">
										<button  class="btn btn-warning btn-sm " type="submit" ><span> <i class="fa fa-calendar-check-o" aria-hidden="true"></i> Book   </span></button>
										</form>
										</th>
										<th>
										<a href="sjpjt_paidstatement.php?id=<?php echo $row["bookid"]; ?>" style="color:white;">
										<button  class="btn btn-success btn-sm "><span> <i class="fa fa-list-ol" aria-hidden="true"></i> View  </span></button></a> </th>

										<th>

										<form action="DeleteBookOnline.php" method="post" onsubmit="return confirm('Are you sure you want to reject this booking - <?php echo $row["bookid"]; ?> ?');">
										<input type="hidden" name="id" value="<?php echo $row["bookid"]; ?>">
										<button  class="btn btn-danger btn-sm " type="submit" ><span> <i class="fa fa-ban" aria-hidden="true"></i> Reject </span></button>
										</form>
										</th>

										</tr>

								<?php  } } else {

									echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
								}?>
								</tbody>
							</table>
						</div>
					</div>
                </div>

	    </div>
	</div>
 
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Online_RoomBooking.xls"
                });
            });
        });
    </script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>