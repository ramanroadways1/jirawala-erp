<?php
//done
    require "_session.php";
    require "_header.php";
 
  $name=$_SESSION["username"];
  date_default_timezone_set('Asia/Calcutta'); 
  $Check=date("d-m-Y");
  $check11=date("Y-m-d");

  $username1=$_SESSION["username"];
  $file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
 
  $sum1=0;
  $sum2=0;
  date_default_timezone_set('Asia/Calcutta'); 
  $date=date("d-m-Y");
  $make=date("Y-m-d");
  $prevDate=date('Y-m-d', strtotime('-1 day', strtotime($date)));
  
  $sql="SELECT SUM(amount) as amount from donation where  status='0' and date between '$prevDate' and '$prevDate'";
  $res=$conn->query($sql);
  if($res===FALSE)
  {
    throw new Exception("Code 001 : ".mysqli_error($conn));   
  }

  while ($row=mysqli_fetch_array($res))
  {  
    $sum1=$row["amount"];
  }
  $sql="SELECT SUM(Deposit) as deposit from  bookroom where allstatus='1' and mysqlin between '$prevDate' and '$prevDate'";
  $res=$conn->query($sql);
  if($res===FALSE)
  {
    throw new Exception("Code 002 : ".mysqli_error($conn));   
  }

  while ($row=mysqli_fetch_array($res))
  { 
    $sum2=$row["deposit"];
  }
?>


  <div class="page-wrapper">
  <div class="content">
      <div class="row">
          <div class="col-sm-12">
              <h4 class="page-title"> Cash Collection </h4>
          </div>
      </div>

      <div class="row">

      <?php
     
               
      ?>
          <div class=" col-md-3">
              <div class="dash-widget clearfix card-box">
                  <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
                  <div class="dash-widget-info">
                      <h3><?php echo $sum1=='' ? '0.00' : $sum1; ?></h3>
                      <span style="font-size:16px; ">Yesterday Donation  </span>
                  </div>  
              </div>
          </div>
          <div class=" col-md-3">
              <div class="dash-widget clearfix card-box">
                  <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
                  <div class="dash-widget-info">
                      <h3><?php echo $sum2=='' ? '0.00' : $sum2; ?></h3>
                      <span style="font-size:16px; ">  Yesterday Booking  </span>
                  </div>  
              </div>
          </div>
    <?php 
     
    ?>  
          <div class=" col-md-6 "></div>
          <div class=" col-md-6 ">
              <div class="dash-widget clearfix card-box">
                  <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
                  <div class="dash-widget-info">

                    <form  action="Foodcashadminup.php" method="post" name="myform" onsubmit="return validateform()">

                    <?php
                    date_default_timezone_set('Asia/Calcutta'); 
                    $date=date("d-m-Y");
                    $make=date("Y-m-d");

                    $sql="SELECT * FROM repcash where vou_type = 'Day_Closing' and mysqldate='$make'";
                    // $sql="SELECT * FROM repcash where vou_type = 'Day_Closing' and mysqldate='2020-05-13' ";
                    $res=$conn->query($sql);
                    if($res===FALSE)
                    {
                    throw new Exception("Code 001 : ".mysqli_error($conn));   
                    }
                    while ($row=mysqli_fetch_array($res))
                    { 
                    $sql="SELECT * from balance where Branch='reception'";
                    $res1=$conn->query($sql);
                    if($res1===FALSE)
                    {
                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                    }
                    while ($row1=mysqli_fetch_array($res1))
                    { 
                    ?>

                        <h3 style="margin-bottom: 2px;"><?php echo $row1["Balance"]; ?></h3>
                        <span style="font-size:14px;"><?php echo $row["username"]; ?> has close the day, please collect all the reception cash amount. </span>
                        <input type="hidden"  value="<?php echo $row["id"]; ?>" name="final">
                        <input type="hidden"  value="<?php echo $row1["Balance"]; ?>" name="blance">
                        <input type="hidden"  value="<?php echo $row["username"]; ?>" name="reclost">
                        <input type="text" class="form-control" id="i1" min="1" style=" margin-top: 8px; max-width: 250px;" name="rece" placeholder="Enter amount" required="">

                        <?php
                        $sql="SELECT * from balance where Branch='pedi'";
                        $res2=$conn->query($sql);
                        if($res2===FALSE)
                        {
                          throw new Exception("Code 003 : ".mysqli_error($conn));   
                        }
                        if($row2=mysqli_fetch_array($res2))
                        { 
                        ?>
                          <input type="hidden" value="<?php echo $row2["Balance"];  ?>" name="pedibalance" /> 
                        <?php
                        }
                        ?>

                        <input type="hidden" value="Reception" name="name" />
                        <input type="hidden" value="<?php echo $make;  ?>" name="make" />
                        <input type="hidden" value="<?php echo $username;  ?>" name="username" />
                        <input type="hidden" value="main_office" name="dep" />
                        <input type="hidden" value="<?php echo $date;  ?>" name="recdate" />
                        <input type="hidden" value="Day_closing" name="all" />
                        <button type="submit" id="btnSubmit" class="btn btn-primary pull-right"> <i class='fa fa-check-circle'> </i> Received </button>
                        </div>
                        </div>
                        </div>

                      <?php
                        }
                        }

                        if($res->num_rows == 0) {
                          echo '<span style="font-size:14px; color:red;"> <br> No one has close the day yet ! </span>';
                        }
                    ?>
                    </div>
                    </form>   

<?php  
 
    $conn->query("COMMIT");

    } catch(Exception $e) { 
 
        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
        
        if ($conn->query($sql) === FALSE) { 
        echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo "
        <script>
        swal({
        title: \"Error !\",
        text: \"$content\",
        icon: \"error\",
        button: \"OK\",
        });
        </script>";    
  }  

  $conn->close();

?>

            </div>
           </div>
        </div>

<?php require "_footer.php"; ?>