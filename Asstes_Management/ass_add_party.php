
<?php


require "_session.php";
include "../_header.php";
include "left.php";





date_default_timezone_set('Asia/Calcutta'); 
    $Check=date("d-m-Y");
    $Checkout=date("Y-m-d");
    $hh=date("h:i:A");   

 if(!empty($_GET["msg"]))
 {
  if($_GET["msg"]=="Success")
 {
 echo "
    <script>
    swal({
    title: \"Good job!\",
    text: \"Employee Added successfully\",
    icon: \"success\",
    button: \"OK\",
    });
    </script>";
}
 }



try
 {
    $conn->query("START TRANSACTION"); 
        

?>
        <div class="page-wrapper">
        <div class="content">
  


<div class="col-md-12" >

    <div class="col-lg-12">
      
      <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                            <h3 class="panel-title">Party Master</h3>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-8" >
                       
                       
                       
                       
  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a align="right" href="#myModal" class="btn btn-sm btn-primary" data-toggle="modal">ADD New Party</a>
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-lg">
        <form  action="add_party_insert.php" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="modal-content">
                <div class="modal-header">
    
                    <h4 class="modal-title">Add New Party  (Supplier)</h4>
                </div>
                <div class="modal-body">
                    
                       <div class="row">
                       
                          
                       
                       
               <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Type  :</label>
                      <select name="department" class="form-control ">
                      <option value=""> -- select -- </option>
                      <option value="IT-ASSETS">ASSETS </option>
                      <option value="VEHICLE-ASSTES">VEHICLE</option>
                      </select>     
                        </div>
                        
                   </div>      
                         <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Party Name :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="s_name" class="form-control" id="inputName" required="required">
                           
                        </div>
                        </div>
                        
                        
                        
                       <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Date & Time :</label>
                            <input type="text" value="<?php echo $Check; ?>(<?php echo $hh;   ?>)" name="date" class="form-control" id="inputName" readonly="readonly">
                            <input type="hidden" value=" <?php echo $Checkout; ?>" name="mysqldate" class="form-control" id="inputName" readonly="readonly">
                        
                        </div>
                        </div>
                        
                       <div class="col-md-3">
                        <div class="form-group">
                            <label for="inputName">Mobile :  </label>
                           <input type="text" value="" minlength="0" maxlength="10"  pattern="[0-9]+" name="s_number" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        
                        
                           <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Address :</label>
                           <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="s_city" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        
                           <div class="col-md-3">
                        <div class="form-group">
                            <label for="inputName">Pin Code : </label>
                           <input type="text" value="" minlength="0" maxlength="6" pattern="[0-9]+" name="s_pincode" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        <!-- new added -->
                        
                             <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Pan No :</label>
                           <input type="text" value="" pattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}" name="pan_num" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                             <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">GSTIN Applicable :</label>
                             <Select name="gstin_app" class="form-control "> 
                           <option value="">---Select Option---</option>
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                      
                          </Select>
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                             <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">GSTIN No :</label>
                           <input type="text" value=""  name="gstin_num" class="form-control" id="inputName" >
                         
                          
                      
                          
                          </div>
                        </div>
                        
                      
                     
                      
       <h4 class="col-md-12" align="center" style="border-bottom: 1px solid #ccc;">Account Details</h4>


      <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Bank Name :</label>
                           <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="bank" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>   
                        
                        <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Branch Name :</label>
                           <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="branch" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>



                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Account Number:</label>
                           <input type="text" value="" pattern="[0-9]+" name="account_num" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>



         <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Account Holder Name :</label>
                           <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$"  name="account_hold" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>



                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">IfSC Code :</label>
                           <input type="text" value=""  pattern="[a-zA-Z0-9]+" name="IFSC" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Cheque Upload :</label>
                           <input type="file" value="" name="cheque" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        </div>
                        </div>
                        
                         <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" > <i class="fa fa-check"></i> Save</button>
                </div>
                        </div>
                        </form>
                        </div>
                        
                        </div>
                        </div>
                        

<input type="hidden" name="username" value="<?php echo $username;  ?>">



                       
                        
                        
                        
                       
                    </div>
                    
                </div>
               
     
        </div>
        
         <div class="panel-body">
                  <table  class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                    <thead>
              <tr>
                <th>Id</th>
                <th>Date & Time</th>
                
                <th>Party Name</th>
                
                <th>Contact</th>
                <th>City</th>
                <th>Pincode</th>
                <th> Cheque</th>
              
                      
              </tr>
              </thead>
             <?php
 $sql="SELECT * from jirawala_store.asstes_party where status='0'";
  $res=$conn->query($sql);
        if($res===FALSE)
        {
          throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
       
           while ($row=mysqli_fetch_array($res))
            { 

             ?>
              <tr>
              <th><?php echo $row["id"]; ?></th>
              <th><?php echo $row["date"]; ?></th>
              <th><?php echo $row["supplier_name"]; ?></th>
              <th><?php echo $row["contact"]; ?></th>
              <th><?php echo $row["city"]; ?></th>
              <th><?php echo $row["pincode"]; ?></th>
              <th><a target="_blank" href="../party_chq1/<?php echo $row["imagepath"]; ?>"> Image </a>   
              </tr>

              <?php
}
              ?>
            
                  </table>
                </div>
            </div>
        </div>
    </div>

                         
     <script>
$(document).ready(function(){
    $("#myModal").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });
});
</script>  






       <?php 
      $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


 echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");   

} 

$conn->close();

require "../_footer.php";

      ?>

   
