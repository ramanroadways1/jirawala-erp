<?php

$x = 5;

function one(){
	 global $x;
	 echo $x;
}

one();

$item = "";

switch($item){

	case "apple":
		echo "this is fruit";
	break;

	case "tomato":
		echo "this is vegetable";
	break;

	case "bmw":
		echo "this is car";
	break;

	default:
		echo "this is default";
	break;
}



$diesel = array("bpcl"=> 100, "hpcl"=> 300, "iocl"=> 250);

// print_r($diesel);
arsort($diesel);
foreach($diesel as $key => $value){
	//echo $key." | ".$value."<br>";
}

// $name = "amar";
// echo $Name;

/*

php is a server side scripting language, loosely typed, partial case sensitive

core of biggest blogging system - wordpress 
deep enough to run large social networks

Constant is a data item whose value cannot be change, the value is fixed

define('greet', "gud mrng");
define("cars",["bmw","ferari"]);

Variable is a data item whose value can be change during execution, the value can vary

type casting - (int)
NaN - not a number

datatype is classification of data:
String: sequence of characters
Integer: Non decimal numbers
Float: floating point number
Boolean: represents two possible states true or false
Array: stores multiple values in one single variable
Null: special type of data type, variable without value have default value NULL

echo has no return value and print return value of 1, echo takes multiple arguments and print can have only one.

variable scope: local, global, static.
the global keyword is used to access global variable from within function.

normally when a function is completed, all of variables are deleted, sometimes we want a local variable not to be deleted, we have to use static keyword 

*/


/*
function is a block of statement

php is partially case sensitive, function and class are case-insensitive and variables are case-sensitive.

php is a loosely type language, automatically associates a data type to variable depending on its value.

php7 type declaration added, adding string declaration throw fatal error if data type mismatch.
declare(strict_types=1);

when a function argument is passed by reference, the & operator is used.

function declaration - arguments
function definition - parameter

call by value - copies value of actual arguments to formal parameter
call by reference - copies address actual arguments to formal parameter, changes made to parameter will affect argument.


 */


/*

Loops are used to execute the same block of code as long as specified condition is true.

for - number of iteration is known
while - number of iteration is not known
dowhile - loops block of code once, repeats until condition is true
foreach - loops through for each element in array

break statement used to jump out of loop
continue breaks one iteration and continue with next iteration

operators: arithmetic, assignment, comparison, inc/dec, logical, string, array, conditional

<=> spaceship (smaller -1 0 +1)
=== identical
++$i pre increment, increment then returns
$i++ post increment, returns then increment
xor either x or y is true
.= concatenation assignment

$x = exp1 ? true : false (ternary operator)
$x = exp1 ?? exp2 (null coalescing)

*/


/*

	regexp - a regular expression is a sequence of characters that forms a search pattern.
	used to perform all types of text search and text replace operations.

	preg_match($pattern, $string) - case insensitive search returns 1 or 0
	preg_match_all($pattern, $string) - case sensitive search returns n or 0
	preg_replace($pattern, "", $string) - case insensitive replacement and returns updated string


 */

/*
	Superglobals:
	$GLOBALS, $_SERVER, $_REQUEST, $_POST, $_GET, $_FILES, $_ENV, $_COOKIE, $_SESSION

 */

/*

	array is a special variable which can hold more than one value at a time, 
	values can be access by referring to an index number.
	
	indexed arrays - arrays with a numeric index
	associative arrays - arrays with named keys
	multidimensional arrays - aarays containing one or more keys
	
	$cars = array("bmw", "toyota", "maruti");
	$cars[0] = "bmw"; $cars[1] = "toyota"; $cars[2] = "maruti";
	
	$age = array("Peter"=>"10", "John"=>"30");
	$age["Peter"] = "10";
	$age["John"] = "30";
	
	$cars = array(
		array("Volvo", 20)
		array("BMW", 30)
	);
	
	sort() - sort arrays in asc order
	rsort() - sort arrays in desc order
	asort() - sort assoc arrays in asc order by value
	ksort() - sort assoc arrays in asc order by key
	arsort() - sort assoc arrays in desc order by value
	krsort() - sort assoc arrays in desc order by key

 */


/*

Object oriented programming is about creating objects that contain both data and functions, classes and obects are two main aspects of oops, advantages:

faster and easier to execute
clear structure for the program
dry - dont repeat yourself, easier to maintain, modify and debug
oops make possible to create full reusable applications with less code and shorter development time

Class is a template for the objects and object is an instance of the class

when the individual objects are created, they inherit all the properties and behaviors from the class, but each object will have different values for the properties.

lets assume we have a class named fruit, a fruit can have properties like name, color, weight and we can define variables to hold values for the same.

constructor allows to initialize a object properties upon creation of objects, php will automatically call this function when you create an object  from a class.

destructor is called when the object is destructed or the script is stopped, php will automatically call this function at the end of the function.

properties and methods have access modifiers which control where they can be accessed.

public - property or method can be accessed from anyhere by default.
protected - property or method can be accessed within class and derived class.
private - property or method can be accessed within the class.

Inheritance - when a class derives from another class, the child class will inherit all the public and protected properties and methods from the parent class, and in addition it can have its own properties and methods.

the final keyword can be used to prevent class inheritance or to prevent method overriding.

Constants cannot be changed once it is declared, to access constant outside class we have to use scoper resolution operator and with the class we have to use self keyword.

abract class and methods are when the parent class has a named method, but need its child classes to fill out tasks.

inheriting from an abstract class the child class method must be defined with the same name and the same or less access modifier.

number of arguments must be same however child class may have optional arguments in addition.

interface allows to specify what method a class should implements, multilple classes using a same interface referred as polymorphism.

interface cannot have properties while abstract classes can have, and methods should be public


traits - php only supports single inheritance, traits are used to implement multiple inheritance.

static methods and properties can be called directly without creating instance of object.


namespaces are used to group classes and allows sname name can be used for more than one class, 
for example you have a set of classes which describe HTML table such as table row cell and another set of classes whoch describe furniture table chair bed, namespaces can be used to organize classes into different groups, preventing two classes of table mixing up.

iterable is any value which can be looped through, the iterable keyword can be use as a argument of function and return type of a function.

all arrays are iterables, any object that implement iterator can be used as argument of function.

an iterator contains list of items an provides methods to loop through, it keeps a pointer to one of its element, each item in list should have key which can be used to find them.

current() key() next() rewind() valid()
	 
*/



/*
date('d-m-y h:i:s')

include will produce warning and script will continue (optional)
require will show fatal error and exits (mandatory)

require_once or include_once will check if file already included it wont include then

php - hypertext pre processor
html - hypertext markup language
css - cascading style sheet
sql - structured query language
ajax - asynchronous javascript and xml 
svg - scalar vector graphics
xml - extensible markup language

fopen() fgets() fread() fwrite() fclose() readfile() feof() fgetc()

cookie is often used to identify the user, created with setcookie() function. a file server embeds on user computer

session, temporary storage to hold information. unlike cookie information is not stored on user computer

Filters are used to validate (determine if data is in proper form) and sanitize (remove any illegal characters) 
$email = filter_var($email, FILTER_SANITIZE_EMAIL);

Callback function is a function which is passed as a argument into another function

function callback($val){
	return strlen($val);
}
$str = ["apple", "mango"];
$len = array_map("callback", $str);

JSON stands for Javascript Object Notation, text based format for storing and exchanging data
json_encode() - index array to json array / assoc array to json object
json_decode() vice versa

an exception is a problem that arises during the excution of program, exception handling built upon try, catch and throw

try{
	code that throws an exception
} catch(Exception $e){
	code that runs when an exception is caught
} finally{
	code that always runs regardless of whether an exception is caught
}


Types of error:
parse error or syntax error (unexpected end of file)
fatal error (undeclared function)
warning error (missing include file)
notice error (undefined variable)


to perform any operations on xml document, will need a xml parser

tree based 
- SimpleXML
- DOM

Event based
- XML Reader
- XML Expat Parser


Ajax allow web pages to be update asynchronously by exchanging small amounts of data with the server behind the scenes, means updating parts of webpage without reloading the web page
*/




// karsan bhai patel 50yrs net worth 4 billion and byju 10 year net worth 2 billion ease of operations autopilot app based
// mehnat karne se kuch nahi hota dimag lagana padta h
// fault taulrence / ease of operations 
// 

// abstract class Tyre{

// 	abstract function size($name); 

// }

// class jk extends Tyre{

// 	function size($name, $type = "New"){
// 		echo "$name and $type";
// 	}

// }

// $object = new jk();
// $object->size("123");

// abstract class Car{

// 	function __construct($name){
// 		$this->name = $name;
// 	}

// 	abstract function intro() : string;
// }

// class audi extends Car{

// 	function intro() : string{

// 	}
// }

// class Reliance{
// 	const KEY = '7434';

// 	function display(){

// 		echo self::KEY;
// 	}
// }

// // $psu = new Reliance();
// // $psu->display();
// // echo Reliance::KEY;

// abstract class rrpl {

// 	abstract protected function prefix($name);
// }


// // class child extends rrpl{

// // 	// function 
// // }

// class diesel{ 
// //final
// 	 function __construct($name, $color){
// 		$this->name = $name;
// 		$this->color = $color;
// 	}

// 	protected function showall(){
// 		echo "$this->name / $this->color / $this->weight"."<br>";
// 	}
// }

// class fastag extends diesel{

// 	function __construct($name, $color, $weight){
// 		$this->name = $name;
// 		$this->color = $color;
// 		$this->weight = $weight;
// 	}

// 	function viewall(){

// 		$this->showall();
// 		echo "this is the fastag class";
// 	}
// }

// $truck = new Fastag("2925", "red", "25");
// // $truck->showall();
// $truck->viewall();

// class Fruit{
// 	// private $name;

// 	function diesel(){
// 		echo $this->name = 'demo';
// 	}
// }

// class Vegis extends Fruit{

// 	parrent::diesel

// 	function bpcl(){
// 		echo  $this->name = 'test';
// 	}
// }

// $apple = new Vegis();
// $apple->diesel();
// $apple->name = 'demo';

// class Fruit{

// 	public $name;
// 	protected $color;
// 	private $size;

// }

// $apple = new Fruit();
// // $apple->name = 'abc';
// // $apple->color = 'abc';
// $apple->size = 'abc';

// class Fruit{

// 	public $name;
// 	public $color;

// 	function __construct($name, $color){
// 		$this->name = $name;
// 		$this->color = $color;
// 	}

// 	function __destruct(){
// 		echo "$this->name and $this->color";
// 	}


// }

//  $apple = new Fruit("Apple","Red");


// Class Fruit{

// 	public $name;
// 	public $color;

// 	function __construct($name, $color){
// 		$this->name = $name;
// 		$this->color = $color;
// 	}

// 	function getData(){
// 		return $this->name." || ".$this->color;
// 	}

// }

// $tata = new Fruit("Apple", "Red");
// echo $tata->getData();

// Class Truck{
	
// 	public $regno;
// 	public $modelno;

// 	function set_truck($regno,$modelno){
// 		$this->regno = $regno;
// 		$this->modelno = $modelno;
// 	}

// 	function get_truck(){
// 		return "Regno is ".$this->regno." and Model no is ".$this->modelno;
// 	}

// }

// $tata = new Truck();
// $tata->set_truck('GJ18BT26925','3118');
// $tata->modelno = '4018';
// echo $tata->get_truck();

// var_dump($tata instanceof Truck);