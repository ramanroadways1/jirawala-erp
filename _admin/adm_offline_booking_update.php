<?php
//done
    require "_session.php";
    require "_header.php";
 
  $username1=$_SESSION["username"];
  $file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>

<div class="page-wrapper">
  <div class="content">
      
          <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title"> Update/Delete {Bhavan Room Offline Booking} </h4>
        </div>
    </div>

    <div class="row">
          <div class="col-md-12" style="margin-top:10px;">
            <div class="table-responsive">
              <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                <thead>

                  <tr>
                    <th>  Id </th>
                    <th> Full Name </th>
                    <th> Asddress </th>
                    <th> Mobile </th>
                    <th>Checkin</th>
                    <th>Checked</th>
                    <th> Action </th>
                    <th> Delete </th>
                  </tr>
                </thead>
                <tbody>
                <?php

                    $sql="SELECT * FROM `all_book_dea` ORDER BY `id` DESC";
                  $res=$conn->query($sql);
                  if($res===FALSE)
                  {
                  throw new Exception("Code 001 : ".mysqli_error($conn));   
                  }
                  if ($res->num_rows > 0) {
                  while($row=mysqli_fetch_array($res))
                  {
                  ?>
 
                    <tr>
                    <td><?php echo $row["id"]; ?></td>
                    <th><?php echo ucwords(strtolower($row["fullname"])); ?></th>
                    <td><?php echo ucwords(strtolower($row["address"])); ?></td>
                    <td><?php echo $row["mnumber"]; ?></td>
                    <td><?php echo $row["checkin"]; ?> </td>
                    <td><?php echo $row["checkout"]; ?></td>
                    <th>
                    <a href="adm_offline_booking_update1.php?id=<?php echo $row["id"]; ?>" style="color:white;">
                    <button  class="btn btn-primary btn-sm "><span> <i class="fa fa-edit" aria-hidden="true"></i> Update  </span></button></a> </th>

                    <th>

                    <form action="adm_offline_booking_delete.php" method="post" onsubmit="return confirm('Are you sure you want to reject this booking - <?php echo $row["id"]; ?>  ?');">
                    <input type="hidden" name="id" value="<?php echo $row["id"]; ?> ">
                    <button  class="btn btn-danger btn-sm " type="submit" ><span> <i class="fa fa-ban" aria-hidden="true"></i> Reject </span></button>
                    </form>
                    </th>

                  
                    </tr>

                <?php  } } else {

                  echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
                }?>
                </tbody>
              </table>
            </div>
          </div>
                </div>

      </div>
  </div>
 
<!--     <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Online_RoomBooking.xls"
                });
            });
        });
    </script> -->
     
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>