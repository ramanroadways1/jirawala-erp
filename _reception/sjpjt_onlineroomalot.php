<?php
//done
    require "_session.php";
    require "_header.php";
 
	include "numbertoword.php";
	$id = $conn->real_escape_string(htmlspecialchars($_POST["id"]));
	$name = $_SESSION["username"];
	$username1 = $_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>

 <link rel="stylesheet" type="text/css" href="../assets/css/bootstarp-multi.css">
<script type="text/javascript" src="../assets/js/multi-js.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#multiselect').multiselect({    
     buttonWidth : '250px',
     includeSelectAllOption : true,
     maxHeight: '280',
      nonSelectedText: 'Select an Option'
    });
});

function getSelectedValues() {
var selectedVal = $("#multiselect").val();
for(var i=0; i<selectedVal.length; i++){
  function innerFunc(i) {
    setTimeout(function() {
      location.href = selectedVal[i];
    }, i*200000000000000000000);
  }
  innerFunc(i);
}
}
 
$(document).ready(function() {
  $('#multiselect1').multiselect({
  buttonWidth : '250px',
    includeSelectAllOption : true,
    maxHeight: '280',
    nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect1").val();
  for(var i=0; i<selectedVal.length; i++){
    function innerFunc(i) {
      setTimeout(function() {
        location.href = selectedVal[i];
      }, i*2000);
    }
    innerFunc(i);
  }
}

$(document).ready(function() {
$('#multiselect2').multiselect({
  buttonWidth : '250px',
  includeSelectAllOption : true,
  maxHeight: '300',
  nonSelectedText: 'Select an Option'
});
});

function getSelectedValues() {
var selectedVal = $("#multiselect2").val();
for(var i=0; i<selectedVal.length; i++){
  function innerFunc(i) {
    setTimeout(function() {
      location.href = selectedVal[i];
    }, i*2000);
  }
  innerFunc(i);
}
}

$(document).ready(function() {
  $('#multiselect3').multiselect({
    buttonWidth : '250px',
    includeSelectAllOption : true,
    maxHeight: '300',
    nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect3").val();
  for(var i=0; i<selectedVal.length; i++){
    function innerFunc(i) {
      setTimeout(function() {
        location.href = selectedVal[i];
      }, i*2000);
    }
    innerFunc(i);
  }
}
</script>
<style>
	label{
		font-weight: bold;
		/*color: #444;*/
	}
	input{
		font-weight: bold;
		color: #000;
	}
</style>
<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title">   <a onclick="window.history.go(-1)" class="btn btn-app btn-danger btn-sm ">
									<i style="color:#fff;" class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a> Bhavan Room Booking </h4> 
	        </div>
	    </div>

		<div class="row">
        <div class="col-md-12">
            <div class="card-box">
				<?php  
				$sql="select * from bookonline WHERE booking_pay='E000' and status2='0' and bookid =$id";
				$res=$conn->query($sql);
				if($res===FALSE)
				{
					throw new Exception("Code 001 : ".mysqli_error($conn));   
				}
				if($row=mysqli_fetch_array($res))
				{
				?>
             <!--    <h4 class="card-title">Two Column Horizontal Form</h4>
                <h4 class="card-title">Personal Information</h4> -->
				<form action="Online_Room_Booking_Insert.php" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="row">
                        	<div class="col-md-2 form-group">
                                <label>BOOKING ID</label>
                                <input type="text" class="form-control" value="<?php echo $row["bookid"]; ?>" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>FULL NAME</label>
                                <input type="text" class="form-control" value="<?php echo  ucwords(strtolower($row["name"]));  ?>" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label> ADDRESS</label>
                                <input type="text" class="form-control" value="<?php echo  ucwords(strtolower($row["city"]));  ?>" readonly>
                            </div> 
                            <div class="col-md-2 form-group">
                                <label> ADVANCE R. NO.</label>
                                <input type="text" class="form-control" value="<?php echo $row["bookid"];  ?>" readonly>
                            </div> 
                            <div class="col-md-2 form-group">
                                <label>MOBILE NUMBER</label>
                                <input type="text" class="form-control" value="<?php echo $row["Phone"];  ?>" readonly>
                            </div>
 
                            <div class="col-md-2 form-group">
                                <label><?php echo $row["prove"]; ?></label>
                                <input type="text" class="form-control" value="<?php echo $row["snumber"]; ?>" readonly>
                            </div>
                             <div class="col-md-1 form-group">
                                <label>ADULT</label>
                                <input type="text" class="form-control" value="<?php echo $row["male"]; ?>" readonly>
                            </div>
                             <div class="col-md-1 form-group">
                                <label>CHILD</label>
                                <input type="text" class="form-control" value="<?php echo $row["child"]; ?>" readonly>
                            </div>


                            <div class="col-md-3 form-group">
                                <label>CHECK IN </label>
                                <input type="text" value="<?php echo $row["arrive"]; ?> (<?php echo str_replace("null","",str_replace(":null","",$row["intime"])); ?>)" class="form-control" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>CHECK OUT </label>
                                <input type="text" value="<?php echo $row["depart"]; ?> (<?php echo $row["outtime"]; ?>)" class="form-control" readonly>
                            </div>

                            <div class="col-md-2 form-group">
                                <label>NUMBER OF ROOM </label>
                                <input type="text" value="<?php echo $row["noofroom"]; ?>" class="form-control" readonly>
                            </div>

                            <div class="col-md-2 form-group">
                                <label>TYPE OF ROOM</label>
                                <input type="text" value="<?php echo $row["rtype"]; ?>" class="form-control" readonly>
                            </div>
 
                            <div class="col-md-2 form-group">
                                <label>TOTAL DAYS STAY</label>
                                <input type="text" value="<?php echo $row["days"]; ?>" class="form-control" readonly>
                            </div>

                            <div class="col-md-2 form-group">
                                <label>TOTAL DEPOSIT AMOUNT</label>
                                <input type="text" value="<?php echo $row["make"]; ?>" class="form-control" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>PAID DEPOSIT AMOUNT ONLINE</label>
                                <input type="text" value="<?php echo $row["collection"]; ?>" class="form-control" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>REMAINING DEPOSIT AMOUNT</label>
                                <input type="text" value="<?php echo $row["remaing"]; ?>" class="form-control" readonly>
                            </div>
 

 
<!--   <link  href="css/bootstarp-multi.css" rel="stylesheet" type="text/css" />
  <script src="js/multi-js.js"></script> -->
						<?php
						$sql="select * from admin where username = '$name'";
						$res2=$conn->query($sql);
						if($res2===FALSE)
						{
							throw new Exception("Code 002 : ".mysqli_error($conn));   
						}
							while($row1=mysqli_fetch_array($res2))
							{
								$id=$row1["id"];
								?>
								<input type="hidden" name="lst" value="<?php echo $name; ?>">
								<input type="hidden" name="sys" value="<?php echo $id; ?> ">
								<?php
							}
						?>

						<div class="col-md-3 form-group">
						    <label>YATRIK BHAVAN </label>
							<select id="multiselect"  name="multiselect[]" multiple="multiple" >
							<?php
							$sql="select * from bigdharmshala where status='0'";
							$res=$conn->query($sql);
							if($res===FALSE)
							{
							throw new Exception("Code 003: ".mysqli_error($conn));   
							}
							while($row2=mysqli_fetch_array($res))
							{
							?>
							<option value="<?php echo $row2["id"]; ?>" ><?php echo $row2["id"]; ?> &nbsp;<?php echo $row2["big"]; ?>  </option>
							<?php
							}
							?>
							</select>
						</div>

  
						<div class="col-md-3 form-group">
						    <label>VIP BHAVAN </label>
							<select id="multiselect1"   name="multiselect1[]" multiple="multiple" >
							<?php
							$sql="select * from bigdharmshala where status_vid='2'";
							$res2=$conn->query($sql);
							if($res2===FALSE)
							{
							throw new Exception("Code 004 : ".mysqli_error($conn));   
							}
							while($row2=mysqli_fetch_array($res2))
							{
							?>
							<option value="<?php echo $row2["vid"]; ?>"  >  <?php echo $row2["vid"]; ?> &nbsp;<?php echo $row2["vtype"]; ?> </option>
							<?php } ?>
							</select>
						</div>


						<div class="col-md-3 form-group">
						    <label>ATHITI BHAVAN </label>
							<select id="multiselect2" name="multiselect2[]" multiple="multiple"  >
								<option value="1001">(UPPER HALL)</option>
								<option value="14">(SB HALL)</option>
								<option value="115">(M-HALL)</option>
                                 <option value="215" >(P HALL)</option>
                                  <option value="230" >(Store)</option>
							<?php
							// $sql="select * from bigdharmshala where sstatus='4'";
							$sql="select * from bigdharmshala where sstatus='4' AND NOT(sid='14' OR sid='115' OR sid='215' OR sid='230' OR sid='1001')";
							$res3=$conn->query($sql);
							if($res3===FALSE)
							{
							throw new Exception("Code 005 : ".mysqli_error($conn));   
							}
							while($row3=mysqli_fetch_array($res3))
							{
							?>
							<option value="<?php echo $row3["sid"]; ?>"  >  <?php echo $row3["sid"]; ?> &nbsp;<?php echo $row3["stype"]; ?> </option>
							<?php } ?>
							</select>
						</div>

						<input type="hidden" name="name" value="<?php echo $row["name"]; ?>">
						<input type="hidden" name="address" value="<?php echo $row["city"]; ?>">
						<input type="hidden" name="adv" value="<?php echo $row["bookid"]; ?>">
						<input type="hidden" name="mnumber" value="<?php echo $row["Phone"]; ?>">
						<input type="hidden" name="identity" value="<?php echo $row["prove"]; ?>">
						<input type="hidden" name="snumber" value="<?php echo $row["snumber"]; ?>">
						<input type="hidden" name="checkin" value="<?php echo $row["arrive"]; ?>">
						<input type="hidden" name="checkout" value="<?php echo $row["depart"]; ?>">
						<input type="hidden" name="checkinm" value="<?php echo $row["mysqlin"]; ?>">
						<input type="hidden" name="checkoutm" value="<?php echo $row["mysqlout"]; ?>">
						<input type="hidden" name="intime" value="<?php echo $row["intime"]; ?>">
						<input type="hidden" name="outtime" value="<?php echo $row["outtime"]; ?>">
						<input type="hidden" name="male" value="<?php echo $row["male"]; ?>">
						<input type="hidden" name="female" value="<?php echo $row["female"]; ?>">
						<input type="hidden" name="child" value="<?php echo $row["child"]; ?>">
						<input type="hidden" name="driver" value="<?php echo $row["driver"]; ?>">
						<input type="hidden" name="total" value="<?php echo $row["total"]; ?>">
						<input type="hidden" name="apm" value="<?php echo $row["days"]; ?>">
						<input type="hidden" name="mode" value="Online">
						<input type="hidden" name="Remaining" value="<?php echo $row["remaing"]; ?>">
						<input type="hidden" name="amt"  value="<?php echo $row["make"]; ?>">
						<?php
							$make=$row["make"]; 
							$atm=getIndianCurrency($make);
							date_default_timezone_set('Asia/Calcutta'); 
							$date=date("d-m-Y");
							$Check1=date("Y-m-d");
							$time=date("h:i:A");
						?>
						<input type="hidden" class="form-control"  name="amtws" value="<?php echo $atm; ?>">
						<input type="hidden" name="guestname" value="<?php echo $row["guestname"]; ?>">
						<input type="hidden" name="guesttype" value="<?php echo $row["guesttype"]; ?>">
						<input type="hidden" name="guestage" value="<?php echo $row["guestage"]; ?>">
						<input type="hidden" name="membernum" value="<?php echo $row["guestnumber"]; ?>">
						<input type="hidden" name="email21" value="<?php echo $row["email"]; ?>">
						<input type="hidden" name="noofroom" value="<?php echo $row["noofroom"]; ?>">
						<input type="hidden" name="roomtype" value="<?php echo $row["rtype"]; ?>">
      					<input type="hidden" id="checkout" class="" name="BookingTime" value="<?php echo $date;  ?> <?php echo $time;  ?>" >
      					<input type="hidden" name="status" value="1">

                            <div class="col-md-2 form-group" style="padding-top:25px;"> 

                                <input type="checkbox" style="width: 20px; min-height: 15px;" required="required"> <b> <i>  Notice: Please Collect Security Deposit (₹ <?php echo $row["remaing"]; ?>) </i> </b>
                            </div>

                        </div>
                    </div>
                    </div> 
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary"> CONFIRM <i class="fa fa-check-circle" aria-hidden="true"></i> </button>
                    </div>
                </form>

				<?php 
				} 
				else
				{
					    throw new Exception("Booking not available or room already allotted !");   
				}
				?>

            </div>
        </div>
    </div>
    </div>
</div> 
<script type="text/javascript">

function convertNumberToWords(amount) {
	var words = new Array();
	words[0] = '';
	words[1] = 'One';
	words[2] = 'Two';
	words[3] = 'Three';
	words[4] = 'Four';
	words[5] = 'Five';
	words[6] = 'Six';
	words[7] = 'Seven';
	words[8] = 'Eight';
	words[9] = 'Nine';
	words[10] = 'Ten';
	words[11] = 'Eleven';
	words[12] = 'Twelve';
	words[13] = 'Thirteen';
	words[14] = 'Fourteen';
	words[15] = 'Fifteen';
	words[16] = 'Sixteen';
	words[17] = 'Seventeen';
	words[18] = 'Eighteen';
	words[19] = 'Nineteen';
	words[20] = 'Twenty';
	words[30] = 'Thirty';
	words[40] = 'Forty';
	words[50] = 'Fifty';
	words[60] = 'Sixty';
	words[70] = 'Seventy';
	words[80] = 'Eighty';
	words[90] = 'Ninety';
		amount = amount.toString();
		var atemp = amount.split(".");
		var number = atemp[0].split(",").join("");
		var n_length = number.length;
		var words_string = "";
		if (n_length <= 9) {
		var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
		var received_n_array = new Array();
		for (var i = 0; i < n_length; i++) {
		received_n_array[i] = number.substr(i, 1);
		}
		for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
		n_array[i] = received_n_array[j];
		}
		for (var i = 0, j = 1; i < 9; i++, j++) {
		if (i == 0 || i == 2 || i == 4 || i == 7) {
		if (n_array[i] == 1) {
		n_array[j] = 10 + parseInt(n_array[j]);
		n_array[i] = 0;
		}
		}
		}
		value = "";
		for (var i = 0; i < 9; i++) {
		if (i == 0 || i == 2 || i == 4 || i == 7) {
		value = n_array[i] * 10;
		} else {
		value = n_array[i];
		}
		if (value != 0) {
		words_string += words[value] + " ";
		}
		if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Crores ";
		}
		if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Lakhs ";
		}
		if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Thousand ";
		}
		if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
		words_string += "Hundred and ";
		} else if (i == 6 && value != 0) {
		words_string += "Hundred ";
		}
		}
		words_string = words_string.split("  ").join(" ");
		}
	return words_string;
}

window.onbeforeunload = function () {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == "button" || inputs[i].type == "submit") {
            inputs[i].disabled = true;
        }
    }
};

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$('input[id$=checkout]').datepicker({ 
	minDate: '0D',
    maxDate: '+1D',
    dateFormat: 'dd-mm-yy'
});

$("input[id$=checkout]").keypress(function (evt) {
    evt.preventDefault();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>