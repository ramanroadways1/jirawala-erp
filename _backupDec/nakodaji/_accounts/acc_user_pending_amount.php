<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

if(isset($_POST["starttime"])){
	$dat=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
	$dat= str_replace("/","-",$dat);
	$d1= date("Y-m-d", strtotime($dat)); 
}

try
 {
    $conn->query("START TRANSACTION"); 
?>

<style>
tbody th
{
	color: #444;
}
	</style>

<div class="page-wrapper">
	<div class="content">
	   
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title"> All Operator's Cash Pending Amount </h4>
	        </div>
	    </div>

		<div class="row">
					<div class="col-md-8" style="margin-top:10px;">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
								<thead>
									<tr>
										<th>  Id </th>
										<th>Username</th>
										<th>Branch</th>
										<th>Amount </th>

										
									</tr>
								</thead>
								<tbody>
								<?php

									$sql="SELECT * from overallcash";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
									if ($res->num_rows > 0) {
									while($row=mysqli_fetch_array($res))
									{

									
							  	?>
 
				 						<tr>

        <th ><?php echo $row["id"]; ?></th>
		<td><?php echo $row["username"]; ?></td>
		<td><?php echo $row["Branch"]; ?></td>
		<td> <?php echo $row["amount"]; ?></td>
										</tr>

								<?php  } } else {

									echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
								}?>
								</tbody>
							</table>
						</div>
					</div>
                </div>

	    </div>
	</div>
 
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Online_RoomBooking.xls"
                });
            });
        });
    </script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>