<?php

require "_session.php";
require "_header.php"; 

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
 
?>

  <div class="page-wrapper">
  <div class="content">

  <div class="row">
  <?php

  try {
          $conn->query("START TRANSACTION"); 

          $result1=0;
          $result2=0;
          $result3=0;

          $sql="SELECT * from balance where Branch='pedi'";
          $res1=$conn->query($sql);
          if($res1===FALSE)
          {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
          }

          while($row=mysqli_fetch_array($res1))
              {
                $result1=$row["Balance"];
              }

          $sql="SELECT * from balance where Branch='reception'";
          $res1=$conn->query($sql);
          if($res1===FALSE)
          {
            throw new Exception("Code 002 : ".mysqli_error($conn));   
          }

          while($row=mysqli_fetch_array($res1))
          {
            $result2=$row["Balance"];
          }

          $sql=" SELECT * from balance where Branch='expense'";
          $res1=$conn->query($sql);
          if($res1===FALSE)
          {
            throw new Exception("Code 003 : ".mysqli_error($conn));   
          }

          while($row=mysqli_fetch_array($res1))
          {
            $result3=$row["Balance"];
          }

  ?>



<div class=" col-md-4">
    <div class="dash-widget clearfix card-box">
        <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
        <div class="dash-widget-info">
            <h3><?php echo $result1=='' ? '0.00' : $result1; ?></h3>
            <span style="font-size:16px; ">  Total amount in main office <br> {Final Cash}</span>
        </div>  
    </div>
</div>

<div class=" col-md-4">
    <div class="dash-widget clearfix card-box">
        <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
        <div class="dash-widget-info">
            <h3><?php echo $result2=='' ? '0.00' : $result2; ?></h3>
            <span style="font-size:16px; ">  Pending amount from reception <br> <br> </span>
        </div>  
    </div>
</div>

<div class=" col-md-4">
    <div class="dash-widget clearfix card-box">
        <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
        <div class="dash-widget-info">
            <h3><?php echo $result3=='' ? '0.00' : $result3; ?></h3>
            <span style="font-size:16px; ">  Total expense amount <br> <br> </span>
        </div>  
    </div>
</div>

<div class=" col-md-4">
    <div class="dash-widget clearfix card-box">
        <span class="dash-widget-icon"><i class="fa fa-book"></i></span>
        <div class="dash-widget-info">
            <h3 style="font-size: 20px; line-height: 30px;">Confirm Today's <br> Overall Cash</h3>
        </div>  
        <a style="float: right;" class="btn btn-primary" href="acc_Cash_Main_Office_Confirm.php"> <i class="fa fa-check-circle"></i> Cash Confirm </a>
    </div>
</div>

 
 







                <!-- ################### ################### REPORT 1 ################### ###################  -->
                <div class="col-md-4">
                <div class="card-box"> 
                    <form action="last_cash.php" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="">
                                <div class="col-md-12">
                                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> Overall Cash Statement</h4>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>START DATE</label>
                                    <input type="text" class="form-control" id="dt1" name="starttime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>END DATE</label>
                                    <input type="text" class="form-control" id="dt2" name="endtime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>Report :  </label>


                                    <select class="form-control" name="sys"> 
                                    <option value="0">ALL</option>
                                    <option value="Reception">Reception</option> 
                                    <option value="main_office">Main_Office</option> 
                                    </select>



                                </div>
                                     
                            </div>
                            <div class="text-right col-md-12">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
                            </div>
                        </div>
                        </div> 
                    </form>
                </div>

      <!-- <br><br> -->
<!--  <div class="card bg-light mb-3" style="max-width: 20rem;" id="log">
  <div class="card-header"><h5 align="center">Today's Overall Cash Confirm</h5></div>
  <div class="card-body">
  
<a href="acc_Cash_Main_Office_Confirm.php" > 
<button class="btn btn-primary"><span>  Cash Confirm </span></button>
</a>
  </div>
</div> -->



                </div>

            <!-- ################### ################### REPORT 2 ################### ###################  -->
                <div class="col-md-4">
                <div class="card-box"> 
                    <form action="Bank_Satement.php" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="">
                                <div class="col-md-12">
                                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Overall Bank Statement</h4>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>START DATE</label>
                                    <input type="text" class="form-control" id="dt3" name="starttime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>END DATE</label>
                                    <input type="text" class="form-control" id="dt4" name="endtime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>Report :</label>
                                    <select class="form-control" name="sys"> 
                                    <option value="0">ALL</option>

                                    <?php
                                    $sql="SELECT DISTINCT formtheuser as val FROM repcash_chq";
                                    $res=$conn->query($sql);
                                    if($res===FALSE)
                                    {
                                    throw new Exception("Code 004 : ".mysqli_error($conn));   
                                    }

                                    while($row=mysqli_fetch_array($res))
                                    {
                                    ?>
                                    <option value="<?php echo $row["val"]; ?>"><?php echo $row["val"];  ?></option> 
                                    <?php } ?>
                                    </select>
                                </div>
                                     
                            </div>
                            <div class="text-right col-md-12">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
                            </div>
                        </div>
                        </div> 
                    </form>
                </div>
                </div>

            











 <?php
 $conn->query("COMMIT");


} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 


$conn->close();


?>

 
 
 
 
 
 <script type="text/javascript">

function addtotal(){
        var male = document.getElementById("i1").value;
        var femals = document.getElementById("i2").value;
        var child = document.getElementById("i3").value;
        var dri = document.getElementById("i4").value;
        
        document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
    }

$(document).ready(function () {

    // ############ start ############
    $("#dt1").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt2');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt2').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############
    
    // ############ start ############
    $("#dt3").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt4');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt4').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    
    // ############ start ############
    $("#dt5").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt6');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt6').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    
    // ############ start ############
    $("#dt7").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt8');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt8').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    
    // 

    // ############ start ############
    $("#dt9").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt10');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt10').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    

  




   
    // ############ end ############    


});



function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//  var name=document.myform.name.value; 
    if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
            {
            alert("Full name is not valid !");
            document.myform.fullname.focus() ;

            return false;
            }
    if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
    {
    alert("Address is not valid !");
    document.myform.address.focus() ;

    return false;
    }

    if( document.myform.mnumber.value == "" ||
            isNaN( document.myform.mnumber.value) ||
            document.myform.mnumber.value.length != 10 )
            {
            alert("Mobile number is not valid !");
            document.myform.mnumber.focus() ;

            return false;
            }
    return true;
}

function validatedate(inputText)
  {
    // alert(inputText);
      var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
      // Match the date format through regular expression
      if(inputText.value.match(dateformat))
      {
      document.form1.text1.focus();
      //Test which seperator is used '/' or '-'
      var opera1 = inputText.value.split('/');
      var opera2 = inputText.value.split('-');
      lopera1 = opera1.length;
      lopera2 = opera2.length;
      // Extract the string into month, date and year
      if (lopera1>1)
      {
      var pdate = inputText.value.split('/');
      }
      else if (lopera2>1)
      {
      var pdate = inputText.value.split('-');
      }
      var dd = parseInt(pdate[0]);
      var mm  = parseInt(pdate[1]);
      var yy = parseInt(pdate[2]);
      // Create list of days of a month [assume there is no leap year by default]
      var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
      if (mm==1 || mm>2)
      {
      if (dd>ListofDays[mm-1])
      {
          alert('Invalid date format!');
          return false;
      }
      }
      if (mm==2)
      {
      var lyear = false;
      if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
      {
      lyear = true;
      }
      if ((lyear==false) && (dd>=29))
      {
          alert('Invalid date format!');
          return false;
      }
      if ((lyear==true) && (dd>29))
      {
          alert('Invalid date format!');
          return false;
      }
      }
      }
      else
      {
          alert("Invalid date format !");
          document.form1.text1.focus();
          return false;
      }
  }
</script>
 
 
 
 
 
 
 

 </div>
   	


	
  



	   
	   <?php 
	   
	   require "_footer.php";  ?>
