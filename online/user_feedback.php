
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container jumbotron">


    <form  action="feedback_insert.php" method="POST">
        <div class="card-box">
        <div class="row">
        <div class="col-md-3"></div> 
        <div class="col-md-6">
        <div class="row"> 
            <center> <h4 class="card-title">Please Fill Feedback</h4></center>
        <div class="col-md-8 form-group">
        <label>Full Name</label>
        <input type="text" class="form-control" id="fullname" placeholder="Full Name" name="fullname" required="required">
        </div>
        <div class="col-md-8 form-group">
        <label>Mobile No.</label>
        <input type="text" maxlength="10" minlength="10" class="form-control" placeholder="Mobile Number" id="mobileNo" name="mobileNo" required="required">
        </div>
        <div class="col-md-8 form-group">
        <label>Booking Id(Optional)</label>
        <input type="text" class="form-control" name="bookingid" placeholder="Booking Id">
        </div>
        <div class="col-md-8 form-group">
        <label>FeedBack</label>
         <textarea rows="5" cols="5" class="form-control" name="FeedBack" placeholder="Enter message" required="required"></textarea>
        </div>
        </div>
         <div class="text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
         </div>
        
        </div>

      </div>

    </form>


</div>

</body>
</html>

