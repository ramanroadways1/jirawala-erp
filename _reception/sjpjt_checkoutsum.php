<?php
//done
    require "_session.php";
    require "_header.php";
 
	$starttime9=$conn->real_escape_string(htmlspecialchars($_POST["starttime9"]));
	$endtime9=$conn->real_escape_string(htmlspecialchars($_POST["endtime10"]));
	$starttime= date("Y-m-d", strtotime($starttime9));
	$endtime= date("Y-m-d", strtotime($endtime9));
	$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>

<style>
tbody th
{
	color: #444;
}


 .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
       background-color: #FFF9C4;
   }
	</style>

<div class="page-wrapper">
	<div class="content">
	    <div class="row" style="">
	        <div class="col-sm-12">
	            <h4 class="page-title" > <a onclick="window.history.go(-1)" class="btn btn-app btn-danger btn-sm ">
									<i style="color:#fff;" class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a> Checkout Report (<?php echo $starttime9; ?>&nbsp;to&nbsp;<?php echo $endtime9; ?>) <button type="button" class="btn btn-success btn-rounded"  id="btnExport"> <i class="fa fa-download" aria-hidden="true"></i></button> </h4>
	        </div>
	    </div>
  
		<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">

				<table class="table table-hover table-border table-bordered custom-table datatable m-b-0" id="BookTable" >
				<thead>
				<tr>
				<th> Slip </th>
				<th> UserId </th>
				<th> Name </th>
				<th> Member </th>
				<th> Mobile </th>
				<th> City </th>
				<th> CheckIn </th>
				<th> CheckOut </th>
				<th> Yatrik Bhavan </th>
				<th> VIP Bhavan </th>
				<th> Atithi Bhavan </th>
				<th> Yatrik Room </th>
				<th> VIP Room </th>
				<th> Atithi Room </th>
				<th> Amount </th>
				<th> DonationId </th>
				<th> Booking </th>
				</tr>
				</thead>
				<tbody>
				<?php   
				$res=0;
				$res1=0;
				if($sys=="0")
				{
				$sql="SELECT * from bookroom where allstatus='3' and  mysqlout1  between '$starttime' and '$endtime'";
				$res=$conn->query($sql);
				if($res===FALSE)
				{
					throw new Exception("Code 001 : ".mysqli_error($conn));   
				}

				$sql=" SELECT SUM(Deposit) as deposit ,SUM(total)as total from bookroom where method1 = 'checkout' and allstatus='3' and  mysqlout1 between '$starttime' and '$endtime'";
				$res1=$conn->query($sql);
				if($res1===FALSE)
				{
					throw new Exception("Code 002 : ".mysqli_error($conn));   
				}
				}
				else
				{

				$sql="SELECT * from bookroom WHERE checkoutby1= '$sys' and allstatus='3'  and mysqlout1 between '$starttime' and '$endtime' ";
				$res=$conn->query($sql);
				if($res===FALSE)
				{
					throw new Exception("Code 003 : ".mysqli_error($conn));   
				}

				$sql="SELECT SUM(Deposit) as deposit,SUM(total) as total from bookroom where method1='checkout' and checkoutby1='$sys' and allstatus='3'  and  mysqlout1 between '$starttime' and '$endtime' ";
				$res1=$conn->query($sql);
				if($res1===FALSE)
				{
					throw new Exception("Code 004 : ".mysqli_error($conn));   
				}

				}
				if ($res->num_rows > 0) {
				while($row=mysqli_fetch_array($res))
				{
					$Bookid=$row["Bookid"];
					$checkout=$row["method1"];
					if($checkout=="checkout")
					{
					?>
					<tr >
					<?php
					}
					else
					{
					?>
					<tr style="background-color: #ffecec !important;">
					<?php
					}

					?>

					<th><a target="_blank" href="Checkout.php?id=<?php echo $row["Bookid"]; ?>"><?php echo $row["Bookid"]; ?></a> </th>
					<th><?php echo $row["userid"]; ?></th>
					<th><?php echo ucwords(strtolower($row["Name"])); ?></th>
					<th><?php echo $row["total"]; ?></th>
					<th><?php echo $row["Mobilenumber"]; ?></th>
					<th><?php echo ucwords(strtolower($row["address"])); ?></th>
					<th><?php echo $row["checkindate"]; ?> (<?php echo trim(str_replace("null","",str_replace(":null","",$row["intime"]))); ?>)  </th>
					<th><?php echo $row["checkoutdate"]; ?> (<?php echo $row["outtime"]; ?>) </th> 
					<th><?php echo $row["BigDharmshala"]; ?></th> 
					<th><?php echo $row["VishistAtithiti"]; ?></th> 
					<th><?php echo $row["SmallDhrm"]; ?></th> 

					<?php

					$string1 = $row["BigDharmshala"];
					$count1=0;
					$token1 = strtok($string1, ",");
					while ($token1 !== false)
					{ 
						$count1++;
						$token1 = strtok(",");
					}

					$string2 =$row["VishistAtithiti"];
					$count2=0;
					$token2 = strtok($string2, ",");
					while ($token2 !== false)
					{ 
						$count2++;
						$token2 = strtok(",");
					}

					$string3 =$row["SmallDhrm"];
					$count3=0;
					$token3 = strtok($string3, ",");
					while ($token3 !== false)
					{ 
						$count3++;
						$token3 = strtok(",");
					}
					?>
					<th> <?php echo $count1; ?></th>
					<th> <?php echo $count2; ?></th>
					<th><?php echo $count3; ?> </th> 
					<th><?php echo $row["Deposit"]; ?> </th> 
					<th>
					<?php 
					$res3=mysqli_query($conn,"SELECT * from donation where Bookingid='$Bookid'");
					if($row1=mysqli_fetch_array($res3))
					{
					?>
					<a target="_blank" href="donationSlip.php?id=<?php echo $row1["rec_id"]; ?> "><?php echo $row1["rec_id"]; ?></a></th> 
					<?php 
					} 
					$Atatusid=$row["Atatusid"];
					if($Atatusid=="1")
					{
					?>
					<th>Online</th>
					<?php
					}
					else
					{
					?>
					<th>Offline</th>
					<?php
					}
					?>
					</tr>  
					<?php 
				} 

				while($row1=mysqli_fetch_array($res1))
				{
				?>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th> Total: <br><?php echo $row1["total"];   ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th> Total: <br><?php echo $row1["deposit"]; ?> </th>
						<th></th>
						<th></th>
					</tr>
				<?php
				}  

				} else {
					echo "<tr> <td colspan=17> No booking data available ! </td> </tr>";
				}

 
				?> 
				</tbody>
				</table>

							<!-- <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
								<thead>
									<tr>
										<th> Slip</th>
										<th> User </th>
										<th> Name </th>
										<th> Member </th>
										<th> Number</th>
										<th> City </th>
										<th> Check-in</th>
										<th> Check-out </th>
										<th> Yatrik Bhavan </th>
										<th> VIP Bhavan</th>
										<th> Atithi Bhavan </th>
										<th> C Yatrik </th>
										<th> C VIP </th>
										<th> C Atithi </th>
										<th> Amount </th>
										<th> Donation Id </th>
										<th> Booking </th>
									</tr>
								</thead>
								<tbody>
								<?php

									$sql="select * from bookonline where status2='1'";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
									if ($res->num_rows > 0) {
									while($row=mysqli_fetch_array($res))
									{

									$status2=$row["status2"];
							  	?>
 
				 						<tr>
												<th><?php echo $row["bookid"]; ?> </th>
												<th><?php echo  ucwords(strtolower($row["name"])); ?> </th>
												<th><?php echo  ucwords(strtolower($row["city"])); ?></th>
												<th><?php echo $row["Phone"]; ?> </th>
												<th><?php echo $row["noofroom"]; ?></th>
												<th><?php echo $row["rtype"]; ?></th>
												<th><?php echo $row["remaing"]; ?></th>
												<th><?php echo $row["arrive"]; ?> </th>
												<th><?php echo $row["depart"]; ?> <br>  </th>
												<th>Rejected</th>
										</tr>

								<?php  } } else {

									echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
								}?>
								</tbody>
							</table> -->
						</div>
					</div>
                </div>

	    </div>
	</div>

	<script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Checkout_Report.xls"
                });
            });
        });
    </script>
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>