
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
    <title> Shri Jirawala Parshwanath Jain Tirth </title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/morris/morris.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">


    <!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.min.js"></script>
		<script src="../assets/js/respond.min.js"></script>
	<![endif]-->
    <script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="../assets/sweetalert.min.js"></script>
    <script src="../assets/table2excel.js"></script>
    <style>
        input[required], select[required] {
        background-image: url('../assets/require.png');
        background-repeat: no-repeat;
        background-position-x: right;
        }

        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #FFF9C4;
        }
    </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="index.php" class="logo">
                    <img src="../assets/logo.png" width="135" alt="">
                </a>
            </div>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars"></i></a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-danger float-right">0</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                               <!--  <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">
												<img alt="John Doe" src="../assets/img/user.jpg" class="img-fluid">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
												<p class="noti-time"><span class="notification-time">4 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">V</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
												<p class="noti-time"><span class="notification-time">6 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="#">View all Notifications</a>
                            <!-- activities.html -->
                        </div>
                    </div>
                </li>
                <!-- <li class="nav-item dropdown d-none d-sm-block">
                    <a href="javascript:void(0);" id="open_msg_box" class="hasnotifications nav-link"><i class="fa fa-comment-o"></i> <span class="badge badge-pill bg-danger float-right">8</span></a>
                </li> -->
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
							<img class="rounded-circle" src="../assets/img/user.jpg" width="24" alt="Admin">
							<span class="status online"></span>
						</span>
						<span><?php echo strtolower($_SESSION["username"]); ?> </span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">My Profile</a>
						<a class="dropdown-item" href="changepassword.php">Change password</a>
						<a class="dropdown-item" href="logout.php">Logout</a>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="changepassword.php">Change password</a>
                    <a class="dropdown-item" href="logout.php">Logout</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Reception</li>
                        <li class="">
                            <a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
						<li>
                            <a href="sjpjt_empty.php"><i class="fa fa-building-o"></i> Available Rooms</a>
                        </li> 

                        <li>
                            <a href="sjpjt_bookroom.php"><i class="fa fa-calendar-check-o"></i> Room Booking</a>
                        </li> 
                        <li>
                            <a href="sjpjt_cleaning.php"><i class="fa fa-shower"></i> Room Cleaning</a>
                        </li> 
                        <li>
                            <a href="sjpjt_onlinebookingreg.php"><i class="fa fa-desktop"></i> Online  Booking</a>
                        </li> 
                        <li>
                            <a href="sjpjt_newfile1.php"><i class="fa fa-file-text-o"></i> Donation Receipt </a>
                        </li> 
                        

                        <li class="submenu">
                            <a href="#"><i class="fa fa-address-book-o"></i> <span> Cash Book </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="sjpjt_cashbook5.php">Reception Cash</a></li>
                                <li><a href="sjpjt_foodingcash.php">Bhojanshala </a></li>
                                <li><a href="sjpjt_bookingcash.php">Room Booking </a></li>
                                <li><a href="sjpjt_donationcash.php">Donation </a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="sjpjt_receiptreprt.php"><i class="fa fa-area-chart"></i> Receipt Report </a>
                        </li>


                        <li class="submenu">
                            <a href="#"><i class="fa fa-window-restore"></i> <span> Advance Booking </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="sjpjt_compus_adv.php"> Reports </a></li>
                                <li><a href="sjpjt_compus_adv_off.php"> Offline Booking </a></li> 
                            </ul>
                        </li>
<!-- 
                        <li>
                            <a href="sjpjt_compus_adv.php"><i class="fa fa-window-restore"></i> Advance Booking</a>
                        </li>   -->
                        <!-- <li class="menu-title">Admin</li>

                         <li>
                            <a href="adm_admin2.php"><i class="fa  fa-file-excel-o"></i> Cancel Donation </a>
                        </li>
                        <li>
                            <a href="adm_addusers.php"><i class="fa fa-users"></i> Manage Users </a>
                        </li>
                         <li>
                            <a href="adm_updateamt.php"><i class="fa fa-inr"></i> Price Manager </a>
                        </li>
                         <li>
                            <a href="adm_blockroom.php"><i class="fa fa-ban"></i> Block Rooms </a>
                        </li>
                         <li>
                            <a href="adm_changestatus.php"><i class="fa fa-hotel"></i> Online Rooms </a>
                        </li>

                         <li>
                            <a href="adm_offline_booking_update.php"><i class="fa fa-trash"></i> Delete Booking </a>
                        </li>


                          <li class="menu-title">Account</li>
                
                        <li>
                            <a href="sjpjt_newfile1.php"><i class="fa fa-file-text-o"></i> Donation Receipt </a>
                        </li> 
                        
                         <li>
                            <a href="acc_AdminCashj.php"><i class="fa  fa-money"></i> Collect Cash</a>
                        </li>
                         <li>
                            <a href="acc_user_pending_amount.php"><i class="fa fa-book"></i> Pending Cash  </a>
                        </li>
                        <li>
                            <a href="acc_pesonalcashbook.php"><i class="fa fa-inr"></i> Overall Cash  </a>
                        </li>
                        

                          <li class="submenu">
                            <a href="#"><i class="fa fa-files-o"></i> <span>Voucher  </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="acc_expense_voucher.php">Expense Voucher </a></li>
                                <li><a href="acc_canara_voucher.php">Contra Voucher </a></li> 
                                <li><a href="acc_deposite_voucher.php">Deposit Voucher </a></li> 
                                <li><a href="acc_Investment_voucher.php">Investment Voucher </a></li> 
                                <li><a href="acc_Fetch_Donation.php">Donation Deposit </a></li> 
                            </ul>
                        </li>
                         <li class="submenu">
                            <a href="#"><i class="fa  fa-line-chart"></i> <span> Reports </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="acc_Donation_Report.php"> Donation Report </a></li>
                                <li><a href="acc_Booking_Report.php"> Booking Report</a></li> 
                                <li><a href="acc_Expense_report.php"> Expense Report</a></li> 
                            </ul>
                        </li> -->

                    </ul>
                </div>
            </div>
        </div>