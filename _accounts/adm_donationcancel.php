<?php

require "_session.php";
// include "numbertoword.php";
$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
$id1= $conn->real_escape_string(htmlspecialchars($_POST['cancel']));
$make_Donation_slip= $conn->real_escape_string(htmlspecialchars($_POST['amt']));
$branch= $conn->real_escape_string(htmlspecialchars($_POST['sete']));
$totalamt= $conn->real_escape_string(htmlspecialchars($_POST['totalamt']));
$Department= $conn->real_escape_string(htmlspecialchars($_POST['Department']));
$Date= $conn->real_escape_string(htmlspecialchars($_POST['real']));
$Mysqldate= $conn->real_escape_string(htmlspecialchars($_POST['recdate']));
$lst= $conn->real_escape_string(htmlspecialchars($_POST['username']));

try {
    $conn->query("START TRANSACTION"); 

if($Department=="reception")
{
      $sql="SELECT * from overallcash where username='$make_Donation_slip' and Branch='$branch'";
      $res=$conn->query($sql);
      if($res===FALSE)
      {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
      }

      $sql="SELECT * from balance where Branch='reception'";
      $res1=$conn->query($sql);
      if($res1===FALSE)
      {
        throw new Exception("Code 002 : ".mysqli_error($conn));   
      }

      while($row1=mysqli_fetch_array($res1))
      { 
          $fetch_amount_donation_reception =$row1["Balance"];
          $entry_amount_donation =$totalamt;
          $total_amount = $fetch_amount_donation_reception - $entry_amount_donation;

          if($row=mysqli_fetch_array($res))
          { 
              $amt22 =$totalamt;
              $amt1 =$row["amount"];
              $amt2 = $amt22+$amt1;

              $sql="UPDATE overallcash set amount='$amt2' WHERE Branch = 'Donation_Cancel' and username='$make_Donation_slip'";
              if($conn->query($sql) === FALSE) 
              {
                throw new Exception("Code 003 : ".mysqli_error($conn));             
              } 
              $des =  $lst." Cancel donation slip and its donation id is ".$id1;


              $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$Date','$Mysqldate','-','-','$totalamt','0','$total_amount','Donation_Cancel','$des','0')";
              if($conn->query($sql) === FALSE) 
              {
                throw new Exception("Code 004 : ".mysqli_error($conn));             
              } 
              $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
              if($conn->query($sql) === FALSE) 
              {
                throw new Exception("Code 005 : ".mysqli_error($conn));             
              } 
          }
          else
          {

              $des =  $lst." Cancel donation slip and its donation id is ".$id1;

              $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$Date','$Mysqldate','-','-','$totalamt','0','$total_amount','Donation_Cancel','$des','0')";
              if($conn->query($sql) === FALSE) 
              {
                throw new Exception("Code 006 : ".mysqli_error($conn));             
              } 

              $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
              if($conn->query($sql) === FALSE) 
              {
                throw new Exception("Code 007 : ".mysqli_error($conn));             
              } 

              $sql="insert into overallcash (username,Branch,Amount) values ('$make_Donation_slip','$branch','$totalamt')";
              if($conn->query($sql) === FALSE) 
              {
                throw new Exception("Code 008 : ".mysqli_error($conn));             
              } 
          }
      }

        $sql="UPDATE donation set status='1'  WHERE rec_id ='$id1'";
        if($conn->query($sql) === FALSE) 
        {
          throw new Exception("Code 009 : ".mysqli_error($conn));             
        }  
}

if($Department=="main_office")
{
      $sql="SELECT * from main_office_overall_cash where username='$make_Donation_slip' and Branch='$branch'";
      $res=$conn->query($sql);
      if($res===FALSE)
      {
        throw new Exception("Code 0010 : ".mysqli_error($conn));   
      }

      $sql="SELECT * from balance where Branch='pedi'";
      $res1=$conn->query($sql);
      if($res1===FALSE)
      {
        throw new Exception("Code 0011 : ".mysqli_error($conn));   
      }

      while($row1=mysqli_fetch_array($res1))
      {
          $fetch_amount_donation_reception=$row1["Balance"];;
          $entry_amount_donation =$totalamt;
          $total_amount = $fetch_amount_donation_reception - $entry_amount_donation;
          if($row=mysqli_fetch_array($res))
          {
          $amt22 =$totalamt;
          $amt1 =$row["amount"];
          $amt2 = $amt22+$amt1;
          $sql="UPDATE main_office_overall_cash set amount='$amt2' WHERE Branch = 'Donation_Cancel' and username='$make_Donation_slip'";
          if($conn->query($sql) === FALSE) 
          {
            throw new Exception("Code 0012 : ".mysqli_error($conn));             
          } 
          $des =  $lst." Cancel donation slip and its donation id is ".$id1;

          $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','main_office','$Date','$Mysqldate','-','-','$totalamt','0','$total_amount','Donation_Cancel','$des','0')";

          if($conn->query($sql) === FALSE) 
          {
            throw new Exception("Code 0013 : ".mysqli_error($conn));             
          } 

          $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'pedi'";
          if($conn->query($sql) === FALSE) 
          {
            throw new Exception("Code 0014 : ".mysqli_error($conn));             
          } 

          }
          else
          {
            $des =  $lst." Cancel donation slip and its donation id is ".$id1;

            $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','main_office','$Date','$Mysqldate','-','-','$totalamt','0','$total_amount','Donation_Cancel','$des','0')";

            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 0015: ".mysqli_error($conn));             
            } 

            $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'pedi'";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 0016 : ".mysqli_error($conn));             
            } 
            $sql="insert into main_office_overall_cash (username,Branch,Amount) values ('$make_Donation_slip','$branch','$totalamt')";

            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 0017 : ".mysqli_error($conn));             
            }

          }
      }

      $sql="UPDATE donation set status='1'  WHERE rec_id ='$id1'";
      if($conn->query($sql) === FALSE) 
      {
        throw new Exception("Code 0018 : ".mysqli_error($conn));             
      }  

}

$conn->query("COMMIT");

echo "<script type=\"text/javascript\">
window.alert('Receipt Rejected !');
window.location = \"cancel_donation.php\";    
</script>";

//     echo "
//     <script>
//    swal({
//   title: \"Good job!\",
//   text: \"You clicked the button!\",
//   icon: \"success\",
//   button: \"OK\",
// });
//     </script>";

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = htmlspecialchars($e->getMessage());
    $content = htmlentities($conn->real_escape_string($content));

    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
    if ($conn->query($sql) === TRUE) {
    // echo "New record created successfully";
    } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }

    echo "<script type=\"text/javascript\">
    window.alert('$content');
    window.location = \"cancel_donation.php\";    
    </script>";

}

$conn->close();

?>