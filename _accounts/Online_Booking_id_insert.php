<?php

require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$bookid=$conn->real_escape_string(htmlspecialchars($_POST["bookid"]));
$name=$conn->real_escape_string(htmlspecialchars($_POST["name"]));
$city=$conn->real_escape_string(htmlspecialchars($_POST["city"]));
$ph_no=$conn->real_escape_string(htmlspecialchars($_POST["ph_no"]));
$r_type=$conn->real_escape_string(htmlspecialchars($_POST["r_type"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$tr_date=$conn->real_escape_string(htmlspecialchars($_POST["tr_date"]));
$mysqlin=$conn->real_escape_string(htmlspecialchars($_POST["mysqlin"]));
$status=$conn->real_escape_string(htmlspecialchars($_POST["status"]));
$tr_id=$conn->real_escape_string(htmlspecialchars($_POST["tr_id"]));
$mode=$conn->real_escape_string(htmlspecialchars($_POST["mode"]));
$amt1 = $amt;

try {

    $conn->query("START TRANSACTION"); 

    $sql = "select * from bankpayment where bookind_id='$bookid'";
    $res = $conn->query($sql);
    if($res->num_rows > 0){
    throw new Exception(" Bank payment entry already exist ! ");             
    }

    $sql="insert into bankpayment(name,address,room_no_type,tr_id,service_tax,prr_fee,t_amt,tr_amt,tr_date,inter_value,tdr,payment,submerchantid,bookind_id,id,response,mysqldate) values ('$name','$city','$r_type','$tr_id','0.00','0.00','$amt1','$amt1','$tr_date',' ',' ','$mode','11','$bookid','252553','$status','$mysqlin')";
    if($conn->query($sql) === FALSE) {
    throw new Exception("Code 001 : ".mysqli_error($conn));             
    }

    $sql="UPDATE bookonline set tr_id='$tr_id',booking_pay='E000' WHERE bookid = '$bookid'";
    if($conn->query($sql) === FALSE) {
    throw new Exception("Code 002 : ".mysqli_error($conn));             
    }


    // echo "<script type=\"text/javascript\">
    //          window.location = \"acc_Booking_Report.php\";    
    // </script>";
    $conn->query("COMMIT");

    //     echo "
    //     <script>
    //    swal({
    //   title: \"Good job!\",
    //   text: \"You clicked the button!\",
    //   icon: \"success\",
    //   button: \"OK\",
    // });
    //     </script>";

    echo "<script type=\"text/javascript\">
    window.alert('Updated successfully !');
    window.location = \"acc_paidstatement.php?id=$bookid\";    
    </script>";
    

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = htmlspecialchars($e->getMessage());
    $content = htmlentities($conn->real_escape_string($content));

    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

    if ($conn->query($sql) === TRUE) {
    // echo "New record created successfully";
    } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }

    echo "<script type=\"text/javascript\">
    window.alert('$content');
    window.location = \"acc_Booking_Report.php\";    
    </script>"; 
}
$conn->close();

?>