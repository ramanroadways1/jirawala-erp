<?php
//done
    require "_session.php";
    require "_header.php";
 
    $username1=$_SESSION["username"];
    $file_name = basename($_SERVER['PHP_SELF']);

    date_default_timezone_set('Asia/Calcutta'); 
    $date=date("d-m-Y");
    $time=date("h:i");
    $intime=date("h:i:A");
    $time1=date("A");


try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
            <div class="page-wrapper">
                <div class="content"> 



                    <div class="row">

                <!-- ################### ################### REPORT 1 ################### ###################  -->
                <div class="col-md-3">
                <div class="card-box"> 
                    <form action="Total_expense.php" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="">
                                <div class="col-md-12">
                                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> All Expenses
                                    <br> <sup>{ User Wise }</sup>
                                </h4>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>START DATE</label>
                                    <input type="text" class="form-control" id="dt1" name="starttime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>END DATE</label>
                                    <input type="text" class="form-control" id="dt2" name="endtime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>Type  </label>
                                    <select class="form-control select" name="sys" required=""> 
                                    <option value="0"> -- ALL -- </option>

                                    <?php
                                    $sql="select * from admin where fullname='admin'";
                                    $res=$conn->query($sql);

                                    if($res===FALSE)
                                    {
                                    throw new Exception("Code 001 : ".mysqli_error($conn));   
                                    }
                                    while($row=mysqli_fetch_array($res))
                                    {
                                    ?>
                                    <option value="<?php echo $row["username"]  ?>"><?php echo $row["username"]  ?></option> 
                                    <?php } 
                                    ?>

                                    </select>
                                </div>
                                     
                            </div>
                            <div class="text-right col-md-12">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
                            </div>
                        </div>
                        </div> 
                    </form>
                </div>
                </div>

            <!-- ################### ################### REPORT 2 ################### ###################  -->
                <div class="col-md-3">
                <div class="card-box"> 
                    <form action="Expense_deatils_by_Payment_mode.php" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="">
                                <div class="col-md-12">
                                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> All Expenses
                                    <br> <sup>{ Payment Mode }</sup>
                                </h4>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>START DATE</label>
                                    <input type="text" class="form-control" id="dt3" name="starttime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>END DATE</label>
                                    <input type="text" class="form-control" id="dt4" name="endtime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>Type</label>
                                    <select class="form-control select" name="sys" required=""> 
                                    <option value=""> -- Select -- </option>
                                    <option value="Cash">Cash </option>
                                    <option value="Cheque">Cheque </option>
                                    <option value="NEFT">NEFT </option>

                                    </select>
 



                                </div>
                                     
                            </div>
                            <div class="text-right col-md-12">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
                            </div>
                        </div>
                        </div> 
                    </form>
                </div>
                </div>

            

            


<!-- ################### ################### REPORT 3 ################### ###################  -->
                <div class="col-md-3">
                <div class="card-box"> 
                    <form action="Expense_deatils_by_Account.php" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="">
                                <div class="col-md-12">
                                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> All Expenses
                                    <br> <sup>{ Head Wise }</sup>
                                </h4>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>START DATE</label>
                                    <input type="text" class="form-control" id="dt5" name="starttime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>END DATE</label>
                                    <input type="text" class="form-control" id="dt6" name="endtime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>Type </label>

                                                                        
                                               
                                    <select class="form-control select" name="sys" required=""> 
                                    <option value="" > -- Select -- </option>

                                    <?php
                                    $sql="SELECT * from heads";
                                    $res=$conn->query($sql);

                                    if($res===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    while($row=mysqli_fetch_array($res))
                                    {
                                    ?>
                                    <option value="<?php echo $row["expense_vouchar"]; ?>"><?php echo $row["expense_vouchar"];  ?></option> 
                                    <?php } 
                                    ?>
                                    </select>

                                </div>
                                     
                            </div>
                            <div class="text-right col-md-12">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
                            </div>
                        </div>
                        </div> 
                    </form>
                </div>
                </div>



<!-- ################### ################### REPORT 4 ################### ###################  -->
                <div class="col-md-3">
                <div class="card-box"> 
                    <form action="Confirm_r_d_c.php" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="">
                                <div class="col-md-12">

                                    <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> Confirm Voucher
                                    <br> <sup>{ Refund / Deposit / Contra }</sup>
                                </h4>

                                </div>
                                <div class="col-md-12 form-group">
                                    <label>START DATE</label>
                                    <input type="text" class="form-control" id="dt7" name="starttime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>END DATE</label>
                                    <input type="text" class="form-control" id="dt8" name="endtime" required="required" placeholder="dd-mm-yyyy" >
                                </div>

                                <div class="col-md-12 form-group">
                                    <label>Type </label>
                                                            
                                    <select class="form-control select" name="sys" required=""> 
                                    <option value=""> -- Select --</option>

                                    <option  value="Room_Booking_deposit">Room_Booking_deposit</option> 
                                    <option  value="Room_Booking_Desposit_Refunded">Room_Booking_Desposit_Refunded</option> 
                                    <option  value="Contra_voucher">Contra_voucher</option> 
                                    <option  value="FDR_Voucher">FD Voucher</option>  

                                    </select>

                                </div>
                                     
                            </div>
                            <div class="text-right col-md-12">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
                            </div>
                        </div>
                        </div> 
                    </form>
                </div>
                </div>






                </div>




























            </div>
        </div>


<script type="text/javascript">

function addtotal(){
        var male = document.getElementById("i1").value;
        var femals = document.getElementById("i2").value;
        var child = document.getElementById("i3").value;
        var dri = document.getElementById("i4").value;
        
        document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
    }

$(document).ready(function () {

    // ############ start ############
    $("#dt1").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt2');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt2').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############
    
    // ############ start ############
    $("#dt3").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt4');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt4').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    
    // ############ start ############
    $("#dt5").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt6');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt6').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    
    // ############ start ############
    $("#dt7").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt8');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt8').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    
    // 

    // ############ start ############
    $("#dt9").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt10');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt10').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    


    // ############ start ############
    $("#dt11").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt11');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt12').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    


    // ############ start ############
    $("#dt13").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt14');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt14').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    


    // ############ start ############
    $("#dt15").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt16');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt16').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    


    // ############ start ############
    $("#dt17").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt18');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt18').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    


    // ############ start ############
    $("#dt19").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function () {
            var dt2 = $('#dt20');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 31);
            if (dt2Date == null || dateDiff < 0) {
                dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 31){
                dt2.datepicker('setDate', startDate);
            }
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });

    $('#dt20').datepicker({
        dateFormat: "dd-mm-yy"
    }); 
    // ############ end ############    










});

// $('input[id$=date1]').datepicker({ 
//  // minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

// $('input[id$=date1]').datepicker({ 
//  // minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//  var name=document.myform.name.value; 
    if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
            {
            alert("Full name is not valid !");
            document.myform.fullname.focus() ;

            return false;
            }
    if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
    {
    alert("Address is not valid !");
    document.myform.address.focus() ;

    return false;
    }

    if( document.myform.mnumber.value == "" ||
            isNaN( document.myform.mnumber.value) ||
            document.myform.mnumber.value.length != 10 )
            {
            alert("Mobile number is not valid !");
            document.myform.mnumber.focus() ;

            return false;
            }
    return true;
}

function validatedate(inputText)
  {
    // alert(inputText);
      var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
      // Match the date format through regular expression
      if(inputText.value.match(dateformat))
      {
      document.form1.text1.focus();
      //Test which seperator is used '/' or '-'
      var opera1 = inputText.value.split('/');
      var opera2 = inputText.value.split('-');
      lopera1 = opera1.length;
      lopera2 = opera2.length;
      // Extract the string into month, date and year
      if (lopera1>1)
      {
      var pdate = inputText.value.split('/');
      }
      else if (lopera2>1)
      {
      var pdate = inputText.value.split('-');
      }
      var dd = parseInt(pdate[0]);
      var mm  = parseInt(pdate[1]);
      var yy = parseInt(pdate[2]);
      // Create list of days of a month [assume there is no leap year by default]
      var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
      if (mm==1 || mm>2)
      {
      if (dd>ListofDays[mm-1])
      {
          alert('Invalid date format!');
          return false;
      }
      }
      if (mm==2)
      {
      var lyear = false;
      if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
      {
      lyear = true;
      }
      if ((lyear==false) && (dd>=29))
      {
          alert('Invalid date format!');
          return false;
      }
      if ((lyear==true) && (dd>29))
      {
          alert('Invalid date format!');
          return false;
      }
      }
      }
      else
      {
          alert("Invalid date format !");
          document.form1.text1.focus();
          return false;
      }
  }
</script>
       
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>