<?php
require "_session.php";
    require "_header.php";

date_default_timezone_set('Asia/Calcutta'); 
    $date=date("d-m-Y");
     $Check1=date("Y-m-d");
   // $time=date("h:i:A");
   $username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

try {
    $conn->query("START TRANSACTION"); 

?>




<div class="page-wrapper">
<div class="content">

<div class="row">

<div class="col-md-4">
</div>


<div class="col-md-4">

 <div class="card text-Black bg-light mb-30" style="max-width: 20rem;" >
  <div class="card-header"><h6 class="widget-title lighter" align="center"><i class="ace-icon fa fa-clone blue"></i>&nbsp;Add Expense Heads </h6>
  </div>
  <div class="card-body">
<form class="" action="add_head_insert.php" method="post" >

Expense  heads : <input type="text" class="form-control" placeholder="type name" name="expense"> <br><br>
 
 
 <button  class="btn btn-primary pull-right"><span>Add </span></button>
</form>

</div>
</div>
</div>
</div>


	   <?php 
	   

 $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 


$conn->close();


	    require "_footer.php";  ?>
