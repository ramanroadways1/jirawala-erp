<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
@$starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
@$endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime= date("Y-m-d", strtotime($starttime1));
$endtime= date("Y-m-d", strtotime($endtime1));
@$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));



try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
			<div class="page-wrapper">
				<div class="content"> 
  					<div class="row">

				<!-- ################### ################### REPORT 1 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="adm_admin5.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">    Add  User {BhojanShala} </h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>Full Name</label>
				                    <input type="text" class="form-control" name="fullname" required="required" placeholder="" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Username</label>
				                    <input type="text" class="form-control" name="adduer" required="required" placeholder="" >
				                </div>


				                <div class="col-md-12 form-group">
				                    <label>Password</label>
				                    <input type="text" class="form-control" name="setpass" required="required" placeholder="" >
				                </div>

				            
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-check" aria-hidden="true"></i> ADD  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>


				<div class="col-md-3">
				<div class="card-box"> 
					<form action="adm_admin6.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">    Add  User {ERP} </h4>
				            	</div>
				                
				                    <div class="col-md-12 form-group">
				                    <label>Department</label>
				                    <select class="select" id="" name="fullname" required="required">
				                            <option value=""> -- Select -- </option>
											<option value="rep">Reception</option>
											<option value="admin">Account</option>
											<option value="admin121">Main Admin</option>
											<option value="bhojanshala">Bhojanshala Stock User</option>
											<option value="plumber">Plumber Stock User</option>
				                     </select>
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Username</label>
				                    <input type="text" class="form-control" name="adduer" required="required" placeholder="" >
				                </div>


				                <div class="col-md-12 form-group">
				                    <label>Password</label>
				                    <input type="text" class="form-control" name="setpass" required="required" placeholder="" >
				                </div>

				            
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-check" aria-hidden="true"></i> ADD  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>


				<div class="col-md-3">
				<div class="card-box"> 
					<form action="adm_admin7.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">    Add  User {Cleaning} </h4>
				            	</div>
				                
				                    <div class="col-md-12 form-group">
				                    <label>Dharmshala</label>
				                    <select class="select" id="" name="fullname" required="required">
				                            <option value=""> -- Select -- </option>
											<option value="Yatrik">Yatrik Dharmshala</option>
											<option value="Atithi">Atithi Dharmshala</option>
											<option value="Vip">Vip Dharmshala</option>
				                     </select>
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Username</label>
				                    <input type="text" class="form-control" name="adduer" required="required" placeholder="" >
				                </div>


				                <div class="col-md-12 form-group">
				                    <label>Password</label>
				                    <input type="text" class="form-control" name="setpass" required="required" placeholder="" >
				                </div>
	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-check" aria-hidden="true"></i> ADD  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>



				<div class="col-md-3">
				<div class="card-box"> 
					<form action="adm_add_expense.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> Add Expense Head </h4>
				            	</div>
				                
				                <div class="col-md-12 form-group">
				                    <label>Head Title</label>
				                    <input type="text" class="form-control" name="expense" required="required" placeholder="" >
				                </div>
	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-check" aria-hidden="true"></i> ADD  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

				<div class="col-md-6">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="left">
										<th>  UserId  	</th>
										<th>  Name 	</th>
										<th>   Department  	</th>
										<th>  Balance 	</th>
										<th style="text-align: center;">Action</th>
									</tr>
								</thead>
								<tbody>

									<?php 
									$sql="SELECT * FROM `admin`";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}

									while ($row=mysqli_fetch_array($res))
									{
									$username=$row["username"];
									$sql="SELECT SUM(amount) as amt from overallcash where username='$username'";
									$res1=$conn->query($sql);
									if($res1===FALSE)
									{
									throw new Exception("Code 002: ".mysqli_error($conn));   
									}
									while ($row1=mysqli_fetch_array($res1))
									{
										?>

										<tr align="left">
										<td><?php echo $row["id"]; ?> </td>
										<td align="left"> <?php echo $row["username"]; ?> </td>
										<td align="left"><?php echo $row["fullname"]; ?></td>
										<td><?php echo $row1["amt"]; ?></td>
										<td>
											<center>
											<form action="Delete_users_admin.php" method="post">
											<input type="hidden" name="nav" value="<?php echo $row["id"]; ?>" >
											<input type="hidden" name="nav1" value="reception" >
											<button  class="btn btn-sm btn-danger" disabled=""><span> <i class="fa fa-close"></i> Delete </span></button>
											</form>
											</center>
										</td>
										</tr>

									<?php } } ?>
 
								</tbody>
							</table>
						</div>
					</div>
				</div>
 




				<div class="col-md-6">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="left">
										<th>  UserId  	</th>
										<th>  Name 	</th>
										<th>   Department  	</th>
										<th>  Balance 	</th>
										<th style="text-align: center;">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									        $sql="select * from foodlogin";
											$res=$conn->query($sql);
											if($res===FALSE)
											{
											throw new Exception("Code 003 : ".mysqli_error($conn));   
											}
											while ($row=mysqli_fetch_array($res))
											{
											$username=$row["username"];
											$sql="SELECT SUM(amount) as total from overallcash where username='$username'";

											$res1=$conn->query($sql);
											if($res1===FALSE)
											{
											throw new Exception("Code 004: ".mysqli_error($conn));   
											}
									       while ($row1=mysqli_fetch_array($res1))
									        {
									?>
									<tr align="left">
											<td><?php echo $row["id"]; ?></td>
											<td align="left"><?php echo $row["username"]; ?></td>
											<td align="left">Bhojanshala</td>
											<td><?php echo $row1["total"]; ?></td>
											<td>
												<center>
												<form action="Delete_users_admin.php" method="post">
												<input type="hidden" name="nav" value="<?php echo $row["id"]; ?>"   >
												<input type="hidden" name="nav1" value="Bhojanshala"   >
												<button  class="btn btn-danger btn-sm " disabled=""> <span> <i class="fa fa-close"></i>  Delete </span></button>
												</form>
												</center>
											</td>

									</tr>
									<?php
									}}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
 
                </div>
	    	</div>
		</div>


<script type="text/javascript">

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$(document).ready(function () {

	// ############ start ############
	$("#dt1").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt2');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt2').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############
	
	// ############ start ############
	$("#dt3").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt4');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt4').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt5").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt6');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt6').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt7").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt8');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt8').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
});

// $('input[id$=date1]').datepicker({ 
// 	// minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

// $('input[id$=date1]').datepicker({ 
// 	// minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>