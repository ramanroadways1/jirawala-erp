
<?php
require "_session.php";
include "../_header.php";
include "left.php";





 
date_default_timezone_set('Asia/Calcutta'); 
    $Check=date("d-m-Y");
    $Checkout=date("Y-m-d");
    $hh=date("h:i:A");   

 if(!empty($_GET["msg"]))
 {
  if($_GET["msg"]=="Success")
 {
 echo "
    <script>
    swal({
    title: \"Good job!\",
    text: \"Employee Added successfully\",
    icon: \"success\",
    button: \"OK\",
    });
    </script>";
}
 }

?>


 <div class="page-wrapper">
        <div class="content">

       




                         <div class="panel-body">
                  
                  
        <form  action="insert_emp.php" method="post" enctype="multipart/form-data" autocomplete="off" >
            <div class="modal-content">
                <div class="modal-header">
                  
                    <h4 class="modal-title">Add New Employee
                    
                   
                    
                     </h4>
                    
                                    </div>
                <div class="modal-body">
                    
                       <div class="row">
                         
                 <div class="col-md-12">
                    <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="">PHOTO :</label>
                             <input type="file" value="" name="emp_photo" class="form-control" id="" required="required">
                        </div>
                    </div>
                
                
                   <div class="col-md-3">
                          <div class="form-group">
                             <label for="">FULL NAME :</label>
                             <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_name" class="form-control " id="" required="required">
                        </div>
                    </div>
                    
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for="">DEPARTMENT :</label>
                          <Select class="form-control" name="emp_branch"  required="required">
              <option value=""> -- select -- </option>
              <option value="RECEPTION">RECEPTION </option>
              <option value="MAIN_OFFICE">MAIN_OFFICE </option>
              <option value="YATRIK_BHAVAN">YATRIK_BHAVAN </option>
              <option value="VIP_BHAVAN">VIP_BHAVAN </option>
              <option value="ATITHI_BHAVAN">ATITHI_BHAVAN </option>
              <option value="TEMPLE">TEMPLE </option>
              
              <option value="BHOJANSHALA">BHOJANSHALA DEPARTMENT </option>
              <option value="ELECTRICAL">ELECTRICAL DEAPARTMENT </option>
              <option value="CONSTRUCTION">CONSTRUCTION DEPARTMENT </option>
              <option value="OTHER">OTHER </option>
                            </Select>                         
                        </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="">DATE OF BIRTH :</label>
                        <input type='text' name="emp_dob"  id='tbDate' pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" placeholder='DD-MM-YYYY' autocomplete="off" class="form-control" required="required"/>    
              </div>
                    </div>
              
               
               </div>
               
               
               <!--  2nd line -->
               
               
               
               
               <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="">AGE :</label>
                            <input type="text" value=""  pattern="[0-9]+" name="emp_age" class="form-control" id="" required="required">                       
                              </div>
                    </div>
                
                
                   <div class="col-md-3">
                          <div class="form-group">
                             <label for="">DATE OF JOINING :</label>
                          <input type='text' name="emp_doj" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" id='tbDate2' placeholder='DD-MM-YYYY' autocomplete="off" class="form-control"/>    
               </div>
                    </div>
                    
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> FATHER NAME :</label>
                          
                             <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_father" class="form-control" id="" required="required">                       
                        </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="">FATHER OCCUPATION :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_focc" class="form-control" id="" required="required">                        
            </div>
                    </div>
              
               
               </div>  
               
               
               <!-- 3rd row -->
               <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="">MOTHER NAME :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_mother" class="form-control" id="" required="required">                        
                              </div>
                    </div>
                
                
                   <div class="col-md-3">
                          <div class="form-group">
                             <label for="">MOTHER OCCUPATION :</label>
                           <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_mocc" class="form-control" id="" required="required">                       
                        </div>
                    </div>
                    
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> AADHAR NO :</label>
                          
                             <input type="text" value="" pattern="[0-9]+" name="emp_aadhar" class="form-control" id="" required="required">                        
                        </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="">AADHAR COPY :</label>
                         <input type="file" value=""     name="emp_addcopy" class="form-control" id="" required="required">
                      </div>
                    </div>
              
               
               </div>  
               
               <!-- 4TH ROW -->
               
                <div class="row">    
                 <div class="col-md-6">
                          <div class="form-group">
                             <label for="">CURRENT ADDRESS :</label>
                           <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_add" class="form-control" id="" required="required">                        
                        </div>
                    </div>
                    
                     <div class="col-md-6">
                          <div class="form-group">
                             <label for="">PERMANENT ADDRESS :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_per" class="form-control" id="" required="required">                       
                              </div>
                    </div>
                
                
                
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> PINCODE :</label>
                          
                             <input type="text" value=""  minlength="0" maxlength="6"  pattern="[0-9]+" name="emp_pin" class="form-control" id="" required="required">                       
                        </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="">NEAREST POLICE STATION :</label>
                              <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_ploice" class="form-control" id="">                        
                        </div>
                    </div>
              
               
       
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="">QUALIFICATION  :</label>
                            <input type="text" value="" name="emp_qua" class="form-control" id="" required="required">                       
                              </div>
                    </div>
                
                
                   <div class="col-md-3">
                          <div class="form-group">
                             <label for="">WORK EXPERIENCE :</label>
                           <input type="text" value="" pattern="[0-9]+" name="emp_work" class="form-control" id="" required="required">                        
                        </div>
                    </div>
                            </div>  
               
               
               <!-- 5TH ROW -->
                <div class="row">    
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> BLOOD GROUP :</label>
                          <Select class="form-control" name="emp_blood"  >
              
                          <option value=""> -- select --  </option>
              <option value="B+">B+ </option>
              <option value="B-">B- </option>
          <option value="A+">A+ </option>
          <option value="A-">A- </option>
          <option value="O+">O+ </option>   
            <option value="O-">O- </option>
          <option value="AB+">AB+ </option>
          <option value="AB-">AB- </option>
          


                            </Select> </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="">MOBILE NO :</label>
                              <input type="text" value="" minlength="0" maxlength="10"  pattern="[0-9]+" name="emp_mobile" class="form-control" id="" required="required">                       
                        </div>
                    </div>
              
               
       
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="">ALTERNATE NO  :</label>
                            <input type="text" value="" minlength="0" maxlength="10"  pattern="[0-9]+" name="emp_alt" class="form-control" id="" >                        
                              </div>
                    </div>
                
                
                   <div class="col-md-3">
                          <div class="form-group">
                             <label for="">EMAIL ID :</label>
                           <input type="text" value="" name="emp_mail" class="form-control" id="">                       
                        </div>
                    </div>
                            </div>  
               
               
               <!-- 6TH ROW -->
               
               
                <div class="row">    
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> EMERGENCY  MOBILE NO :</label>
                   <input type="text" value= ""   minlength="0" maxlength="10"  pattern="[0-9]+" name="emp_emer" class="form-control" id="" >                       
                        
                            </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="">LANGUAGES KNOWN :</label>
                              <input type="text" value=""  name="emp_language" class="form-control" id="" required="required">                       
                        </div>
                    </div>
              
    
        
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="">ANY MAJOR DISEASES :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_md" class="form-control" id="" >                        
                              </div>
                    </div>
                
                
                   <div class="col-md-3">
                       
                    </div>
                    
                       <div class="col-md-3">
                          
                    </div>
                
                 <div class="col-md-3">
                         
                    </div>
              
               
               </div>  
               
 <hr style=" display: block; margin-before: 0.5em; margin-after: 0.5em; margin-start: auto; margin-end: auto; overflow: hidden; border-style: inset; border-width: 1px;"></hr>              
       
       
       
       <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> MARITIAL STATUS :</label>
                                  <Select class="form-control" name="emp_maritial"  required="required">
              
                          <option value=""> -- select -- </option>
              <option value="SINGLE">SINGLE </option>
              <option value="MARRIED">MARRIED </option>

                            </Select>
                            
                                </div>
                    </div>
                
                
                   <div class="col-md-3">
                        <div class="form-group">
                             <label for=""> WIFE NAME :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_wife" class="form-control" id="" >                        
                              </div>
                    </div>
                    
                       <div class="col-md-3">
                           <div class="form-group">
                             <label for=""> WIFE OCCUPATION  :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_occupation" class="form-control" id="" >                        
                              </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> NO OF CHILDREN :</label>
                            <input type="text" value=""    pattern="[0-9]+" name="emp_child" class="form-control" id="" >                       
                              </div>
                    </div>
              
               
               </div>  
                       
               
               
           <hr style=" display: block; margin-before: 0.5em; margin-after: 0.5em; margin-start: auto; margin-end: auto; overflow: hidden; border-style: inset; border-width: 1px;"></hr>              
          
               
               
               
               
               
               
     <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> REFERENCE NAME  :</label>
                              <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_ref" class="form-control" id="" >                       
                                                    
                                </div>
                    </div>
                
                
                   <div class="col-md-3">
                        <div class="form-group">
                             <label for=""> REFERENCE MOBILE :</label>
                            <input type="text" value=""  minlength="0" maxlength="10"  pattern="[0-9]+" name="emp_refmo" class="form-control" id="" >                       
                              </div>
                    </div>
                    
                       <div class="col-md-3">
                           <div class="form-group">
                             <label for=""> REFERENCE ADDRESS  :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="emp_refadd" class="form-control" id="" >                        
                              </div>
                    </div>
                
                 <div class="col-md-3">
                       </div>
              
               
               </div>            
               
               
               
            <hr style=" display: block; margin-before: 0.5em; margin-after: 0.5em; margin-start: auto; margin-end: auto; overflow: hidden; border-style: inset; border-width: 1px;"></hr>              
               
               
               
               
               
               
               
               
               
            <input type="hidden" name="user12" value="self">   
               
               
               
            <input type="hidden" name="mydate12" value="<?php echo $Check; ?>(<?php echo $hh; ?> )">   
                  
            <input type="hidden" name="mysqldate12" value="<?php echo $Checkout; ?>">   
                  
               
               
               
               
               
               
               
               
               
               
                <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for=""> RELATIVE IN COMPANY  :</label>
                                  <Select class="form-control " name="emp_inname"  required="required">
              
                          <option value=""> -- select -- </option>
              <option value="YES"> YES </option>
              <option value="NO">NO </option>
              

                            </Select>
                            
                                </div>
                    </div>
                
                
                   <div class="col-md-3">
                        
                    </div>
                    
                       <div class="col-md-3">
                       
                    </div>
                    </div>
               
               
               
               
               
               
                    </div>
                    </div>
                 </div>
                
               
                 
                <div class="modal-footer">
                    <button style="color: #fff;" class="btn btn-warning btn-lg m-t-10" > <i class="fa fa-check"></i> SAVE</button>
                </div>
            </div>
        </form>
        </div>
                      
      
      
      
      </div>

      
      
      
      
      
  
<script>
    $(document).ready(function () {
        $('input[id$=tbDate]').datepicker({
            dateFormat: 'dd-mm-yy'      // Date Format "dd/mm/yy"
        });
    });




    $(document).ready(function () {
        $('input[id$=tbDate2]').datepicker({
            dateFormat: 'dd-mm-yy'      // Date Format "dd/mm/yy"
        });
    });
</script>


	     <?php 
	   




	     require "../_footer.php"; ?>

	 
