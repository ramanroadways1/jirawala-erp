<?php
//done
    require "_session.php";
    require "_header.php";
 include "numbertoword.php";

$starttime=$conn->real_escape_string(htmlspecialchars($_POST["starttime1"]));
$endtime=$conn->real_escape_string(htmlspecialchars($_POST["endtime1"]));
$starttime1= date("Y-m-d", strtotime($starttime));
$endtime1= date("Y-m-d", strtotime($endtime));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));

try
 {
    $conn->query("START TRANSACTION"); 

	$sql="SELECT sum(case when room_type = 'VIP_Rooms' then Price else 0 end) as VIP_Rooms, sum(case when room_type = 'Ac_Room' then Price else 0 end) as Ac_Room, sum(case when room_type = 'Ac_Hall' then Price else 0 end) as Ac_Hall, sum(case when room_type = 'Non_Ac_Hall' then Price else 0 end) as Non_Ac_Hall, sum(case when room_type = 'Non_Ac_Room' then Price else 0 end) as Non_Ac_Room FROM `roomprice`";

	$res=$conn->query($sql);
	if($res===FALSE)
	{
	throw new Exception("Code 001 : ".mysqli_error($conn));   
	}
	$VIP_Rooms=0;
	$Ac_Room=0;
	$Ac_Hall=0;
	$Non_Ac_Hall=0;
	$Non_Ac_Room=0;

	while($row=mysqli_fetch_array($res))
	{
		$VIP_Rooms=(int)$row["VIP_Rooms"];
		$Ac_Room=(int)$row["Ac_Room"];
		$Ac_Hall=(int)$row["Ac_Hall"];
		$Non_Ac_Hall=(int)$row["Non_Ac_Hall"];
		$Non_Ac_Room=(int)$row["Non_Ac_Room"];
	}


?>

<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	         <div class="col-sm-12">
        <form action="Reconciliation_download.php" id="myform" method="post">
        <input type="hidden" name="sys" value="<?php echo $sys; ?>" >
        <input type="hidden" name="starttime" value="<?php echo $starttime;  ?>" >
        <input type="hidden" name="endtime" value="<?php echo $endtime;  ?>" >
        <h4 class="page-title ">Booking Reconciliation From <?php echo $starttime;  ?> To <?php echo $endtime;  ?>  
        <button type="submit" class="btn btn-success btn-rounded" > <i class="fa fa-download" aria-hidden="true"></i></button>
        </h4>
        </form>

            </div>
	    </div>


		<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Booking Id</th>
										<th>Date</th>
										<th>Name</th>
										<th>Address</th>
										<th>Mobile</th>
										<th>BigDharmshala</th>
										<th>VishistAtithiti</th>
										<th>SmallDhrm</th>
										<th>Actual Price</th>
										<th>Deposit</th>
										<th>Profit/Loss</th>
										
									</tr>
								</thead>
								<tbody>
								<?php
                                  $ActualSum=0;
                                  $DepositeSum=0;
                                  $ProfitSum=0;


									
       $sql="";
		if($sys=="0")
		{
		$sql="SELECT Bookid, mysqlin, Name, address, Mobilenumber, BigDharmshala, VishistAtithiti,SmallDhrm, Deposit FROM `bookroom` WHERE mysqlin BETWEEN '$starttime1' and '$endtime1'";
		}
		else
		{
		$sql="SELECT Bookid, mysqlin, Name, address, Mobilenumber, BigDharmshala, VishistAtithiti,SmallDhrm, Deposit FROM `bookroom` WHERE userid='$sys' and mysqlin BETWEEN '$starttime1' and '$endtime1'";
		}


									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
 

									while($row=mysqli_fetch_array($res))
									  {
									  	$total=0;

										$BigDharmshala=$row["BigDharmshala"];
										$token = strtok($BigDharmshala, ",");
										while ($token !== false)
										{

										$sql="select * from bigdharmshala where id= '$token'";
										$res1=$conn->query($sql);
										if($res1===FALSE)
										{
										throw new Exception("Code 002 : ".mysqli_error($conn));   
										}
										$token = strtok(",");
										while($row1=mysqli_fetch_array($res1))
										{
                                         $typeofroom=$row1["big"];
                                         if($typeofroom=="(AC)")
                                         {
                                          $total=(int)$total+(int)$Ac_Room;

                                         }
                                         if($typeofroom=="(HALL)")
                                         {
                                         $total=(int)$total+(int)$Ac_Hall;
                                         }
                                         
										} } 

										$VishistAtithiti=$row["VishistAtithiti"];
										$token1 = strtok($VishistAtithiti, ",");
										while ($token1 !== false)
										{

										$sql="select * from bigdharmshala where vid= '$token1'";
										$res1=$conn->query($sql);
										if($res1===FALSE)
										{
										throw new Exception("Code 002 : ".mysqli_error($conn));   
										}
										$token1 = strtok(",");
										while($row1=mysqli_fetch_array($res1))
										{
                                         $typeofroom=$row1["vtype"];
                                         if($typeofroom=="(AC)")
                                         {
                                         $total=(int)$total+(int)$VIP_Rooms;

                                         }
                                         
                                         
										} } 


										$SmallDhrm=$row["SmallDhrm"];
										$token2 = strtok($SmallDhrm, ",");
										while ($token2 !== false)
										{

										$sql="select * from bigdharmshala where sid= '$token2'";
										$res1=$conn->query($sql);
										if($res1===FALSE)
										{
										throw new Exception("Code 002 : ".mysqli_error($conn));   
										}
										$token2 = strtok(",");
										while($row1=mysqli_fetch_array($res1))
										{
                                         $typeofroom=$row1["stype"];
                                         if($typeofroom=="(AC)")
                                         {
                                          $total=(int)$total+(int)$Ac_Room;

                                         }
                                        else if($typeofroom=="(NON-AC)")
                                         {
                                        $total=(int)$total+(int)$Non_Ac_Room;
                                         }
                                         else
                                         {
                                        $total=(int)$total+(int)$Non_Ac_Hall;
                                         }
                                         
										} }
					$deposit=(int)$row["Deposit"];
					$profit=$deposit-$total;
					$ActualSum=$ActualSum+$total;
					$DepositeSum=$DepositeSum+$deposit;
					$ProfitSum=$ProfitSum+$profit;

$date1=$row["mysqlin"];
$date2= date("d-m-Y", strtotime($date1));
                ?>


<tr>
<td><a href="BookSlip.php?id=<?php echo $row["Bookid"]; ?>"><?php echo $row["Bookid"]; ?></a></td>
<td><?php echo $date2; ?></td>
<td><?php echo $row["Name"]; ?></td>
<td><?php echo $row["address"]; ?></td>
<td><?php echo $row["Mobilenumber"]; ?></td>
<td><?php echo $row["BigDharmshala"]; ?></td>
<td><?php echo $row["VishistAtithiti"]; ?></td>
<td><?php echo $row["SmallDhrm"]; ?></td>
<td><?php echo $total; ?></td>
<td><?php echo $deposit; ?></td>
<td><?php echo $profit; ?></td>
</tr>



                <?php

							}




                                  $ActualSumword="Zero";
                                    $DepositeSumword="Zero";
                                    $ProfitSumword="Zero";


			if($ActualSum>0)
			{
			$ActualSumword=getIndianCurrency((int)$ActualSum);
			}
			if($DepositeSum>0)
			{
			$DepositeSumword=getIndianCurrency((int)$DepositeSum);
			}
			if($ProfitSum>0)
			{
			$ProfitSumword=getIndianCurrency((int)$ProfitSum);
			}






							  ?>
								</tbody>
							</table>

							 <table  class="table table-border table-striped custom-table datatable m-b-0">
                           
                    <tr>
                     <td>Total Actual Amount</td>   
                     <td><?php echo $ActualSum; ?></td> 
                     <td><?php echo ucwords(strtolower($ActualSumword)); ?></td> 
                    </tr>
                    <tr>
                     <td>Total Deposite Amount</td>   
                     <td><?php echo $DepositeSum; ?></</td> 
                     <td><?php echo ucwords(strtolower($DepositeSumword)); ?></td> 
                    </tr>
                    <tr>
                     <td>Total Profit/Loss Amount</td>   
                     <td><?php echo $ProfitSum; ?></</td> 
                     <td><?php echo ucwords(strtolower($ProfitSumword)); ?></td> 
                    </tr>



                       </table>
                   
						</div>
					</div>
                </div>

	    </div>
	</div>

	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>