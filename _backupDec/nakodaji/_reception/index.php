<?php
    require "_session.php";
    require "_header.php";
     

    $username1=$_SESSION["username"];
    $file_name = basename($_SERVER['PHP_SELF']);
     
try
 {
    $conn->query("START TRANSACTION"); 
 
    $Bhojan=0;
    $booking=0;
    $donation=0;
    $total=0;
    $reception=0;
    $sql="SELECT * from balance where Branch='reception'";
    $res=$conn->query($sql);
    if($res===FALSE)
    {
    throw new Exception("Code 001 : ".mysqli_error($conn));   
    }
 
    if($row=mysqli_fetch_array($res))
    {
    $reception=$row["Balance"];
    }

    $sql="SELECT SUM(amount) as amt from overallcash where Branch='Bhojanshala'";
    $bho=$conn->query($sql);
    if($bho===FALSE)
    {
    throw new Exception("Code 002 : ".mysqli_error($conn));   
    }

    if($row=mysqli_fetch_array($bho))
    {
    $Bhojan=$row["amt"];
    }
    $sql="SELECT SUM(amount) as amt from overallcash where Branch='Booking'";
    $book=$conn->query($sql);

    if($book===FALSE)
    {
    throw new Exception("Code 003 : ".mysqli_error($conn));   
    }

    if($row=mysqli_fetch_array($book))
    {
    $booking=$row["amt"];
    }

    $sql="SELECT SUM(amount) as amt from overallcash where Branch='Donation'";
    $dona=$conn->query($sql);
    if($dona===FALSE)
    {
    throw new Exception("Code 004 : ".mysqli_error($conn));   
    }

    if($row=mysqli_fetch_array($dona))
    {
    $donation=$row["amt"];
    }

    $total=$Bhojan+$booking+$donation;
?>
    <div class="page-wrapper">
        <div class="content">
   <div class="row">

    <div class="col-md-12">
    <div style="text-align: center; padding-bottom:20px;"> <img src="../assets/logo-head.png" width='500'> </div>
    </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                    <div class="dash-widget">
                <span class="dash-widget-bg1"><i class="fa fa-bank" aria-hidden="true"></i></span>
                <div class="dash-widget-info text-right">
                  <h3><?php echo (int)$reception; ?></h3>
                 
                  <span style="cursor: pointer;" onclick="location.href='sjpjt_cashbook5.php'" class="widget-title1">Reception Cash <i class="fa fa-rupee" aria-hidden="true"></i></span>
                  </a>
                </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                    <div class="dash-widget">
                        <span class="dash-widget-bg1"><i class="fa fa-cutlery"></i></span>
                        <div class="dash-widget-info text-right">
                            <h3><?php echo $Bhojan; ?></h3>
                            <span  style="cursor: pointer;" onclick="location.href='sjpjt_foodingcash.php'" class="widget-title1">Bhojanshala  <i class="fa fa-rupee" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                    <div class="dash-widget">
                        <span class="dash-widget-bg1"><i class="fa fa-hotel" aria-hidden="true"></i></span>
                        <div class="dash-widget-info text-right">
                            <h3><?php echo $booking; ?></h3>
                            <span  style="cursor: pointer;" onclick="location.href='sjpjt_bookingcash.php'" class="widget-title1">Booking <i class="fa fa-rupee" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                    <div class="dash-widget">
                        <span class="dash-widget-bg1"><i class="fa fa-envelope-open-o" aria-hidden="true"></i></span>
                        <div class="dash-widget-info text-right">
                            <h3><?php echo $donation; ?></h3>
                            <span  style="cursor: pointer;" onclick="location.href='sjpjt_donationcash.php'" class="widget-title1">Donation  <i class="fa fa-rupee" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
            </div>

<?php

      $yatrikinform1=0;
      $yatrikinform2=0;
      $yatrikinform3=0;
      $yatrikinform4=0;
      $sql="SELECT COUNT(*) as amt FROM bigdharmshala WHERE status='1' and big='(AC)'";
      $yatrik1=$conn->query($sql);
      if($yatrik1===FALSE)
      {
      throw new Exception("Code 005 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($yatrik1))
      {
      $yatrikinform1=$row["amt"];
      }

      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE STATUS='1' and big='(HALL)'";
      $yatrik2=$conn->query($sql);

      if($yatrik2===FALSE)
      {
      throw new Exception("Code 006 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($yatrik2))
      {
      $yatrikinform4=$row["amt"];
      }
      $sql="SELECT COUNT(*) as amt FROM bigdharmshala WHERE status='7'";

      $yatrik3=$conn->query($sql);
      if($yatrik3===FALSE)
      {
      throw new Exception("Code 007 : ".mysqli_error($conn));   
      }
 
      if($row=mysqli_fetch_array($yatrik3))
      {
      $yatrikinform3=$row["amt"];
      }
      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE status='0'";
      $yatrik4=$conn->query($sql);

      if($yatrik4===FALSE)
      {
      throw new Exception("Code 008 : ".mysqli_error($conn));   
      }
 
      if($row=mysqli_fetch_array($yatrik4))
      {
      $yatrikinform4=$row["amt"];
      }
 
?>
    <div class='row'>
    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-4">
    <div class="profile-widget profile-widget3">
    <div class="blur-new"> <a href="sjpjt_yatrik.php" > <img src='../assets/i1.jpg' width="400" height="180"> </a> </div>
    <div>
        
        <div class="user-info" style="padding-left:20px; padding-top:10px;">
            <div class="username" style="text-align: center;">
                <a href="sjpjt_yatrik.php" style="color: #444444d1;">Booked Room Status of <span style="color: #ffaf0e;"> Yatrik Bhavan </span></a>
            </div>
            <span>
                    <!-- <a href="#">@<span>johndoe</span></a> -->
            </span>
        </div>
        <div class="user-analytics">
            <div class="row">
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $yatrikinform1;  ?></h5>
                        <span class="analytics-title">  AC Rooms</span>
                    </div>
                </div>
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title">  Non-AC Rooms</span>
                    </div>
                </div>
                <div class="col-sm-4 col-4">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $yatrikinform2;  ?></h5>
                        <span class="analytics-title">  Halls</span>
                    </div>
                </div>

                 

                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title"> <sub>Member Stay <b>Offline</b></sub>  </span>
                    </div>
                </div>
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title">  <sub>Member Stay <b>Online</b></sub></span>
                    </div>
                </div>
                <div class="col-sm-4 col-4">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title"> <sub>Member Stay <b>Total</b></sub></span>
                    </div>
                </div>

                <div class="col-sm-6 col-6 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $yatrikinform3;  ?></h5>
                        <span class="analytics-title"> Rooms in Cleaning</span>
                    </div>
                </div>
                <div class="col-sm-6 col-6">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $yatrikinform4;  ?></h5>
                        <span class="analytics-title"> Free Rooms</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

<?php
      $vip1=0;
      $vip2=0;
      $vip3=0;
      $vip4=0;
      $vip5=0;
      $vip6=0;

      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE status_vid='3'";
      $vipbhawan1=$conn->query($sql);
      if($vipbhawan1===FALSE)
      {
      throw new Exception("Code 009 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($vipbhawan1))
      {
      $vip1=$row["amt"];
      }

      $sql="SELECT COUNT(*) as amt FROM bigdharmshala WHERE status_vid='8'";
      $vipbhawan2=$conn->query($sql);
      if($vipbhawan2===FALSE)
      {
      throw new Exception("Code 0010 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($vipbhawan2))
      {
      $vip2=$row["amt"];
      }
      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE status_vid='2'";
      $vipbhawan3=$conn->query($sql);
      if($vipbhawan3===FALSE)
      {
      throw new Exception("Code 0011 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($vipbhawan3))
      {
      $vip3=$row["amt"];
      }

      $sql="SELECT COALESCE(SUM(total),0) as amt from bookroom where Atatusid='0' and allstatus='1'";
      $vipbhawan4=$conn->query($sql);
      if($vipbhawan4===FALSE)
      {
      throw new Exception("Code 0012 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($vipbhawan4))
      {
      $vip4=$row["amt"];
      }

      $sql="SELECT COALESCE(SUM(total),0) as amt from bookroom where Atatusid='1' and allstatus='1'";
      $vipbhawan5=$conn->query($sql);
      if($vipbhawan5===FALSE)
      {
      throw new Exception("Code 0014 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($vipbhawan5))
      {
      $vip5=$row["amt"];
      }

      $vip6=$vip5+$vip4;
?>

    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-4">
    <div class="profile-widget profile-widget3">
    <div class='blur-new'>  <a href="sjpjt_vip.php"> <img src='../assets/i2.jpg' width="400" height="180"> </a> </div>
    <div>
        
        <div class="user-info" style="padding-left:20px; padding-top:10px;">
            <div class="username" style="text-align: center;">
                <a href="sjpjt_vip.php" style="color: #444444d1;">Booked Room Status of <span style="color: #ffaf0e;">  VIP Bhavan </span></a>
            </div>
            <span>
                    <!-- <a href="#">@<span>johndoe</span></a> -->
            </span>
        </div>
        <div class="user-analytics">
            <div class="row">
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $vip1;  ?></h5>
                        <span class="analytics-title">  AC Rooms</span>
                    </div>
                </div>
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title">  Non-AC Rooms</span>
                    </div>
                </div>
                <div class="col-sm-4 col-4">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title">  Halls</span>
                    </div>
                </div>

                 

                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $vip4;  ?></h5>
                        <span class="analytics-title"> <sub>Member Stay <b>Offline</b></sub>  </span>
                    </div>
                </div>
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $vip5;  ?></h5>
                        <span class="analytics-title">  <sub>Member Stay <b>Online</b></sub></span>
                    </div>
                </div>
                <div class="col-sm-4 col-4">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $vip6;  ?></h5>
                        <span class="analytics-title"> <sub>Member Stay <b>Total</b></sub></span>
                    </div>
                </div>

                <div class="col-sm-6 col-6 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $vip2;  ?></h5>
                        <span class="analytics-title"> Rooms in Cleaning</span>
                    </div>
                </div>
                <div class="col-sm-6 col-6">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $vip3;  ?></h5>
                        <span class="analytics-title"> Free Rooms</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

<?php
      $atithiNo1=0;
      $atithiNo2=0;
      $atithiNo3=0;
      $atithiNo4=0;
      $atithiNo5=0;

      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE sstatus='5' and stype='(AC)'";
      $atithi=$conn->query($sql);
      if($atithi===FALSE)
      {
      throw new Exception("Code 0015 : ".mysqli_error($conn));   
      }

      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE sstatus='5' and stype='(AC)'";
      $atithi=$conn->query($sql);
      if($atithi===FALSE)
      {
      throw new Exception("Code 0016 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($atithi))
      {
      $atithiNo1=$row["amt"];
      }
      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE sstatus='5' and stype='(NON-AC)'";
      $atithi2=$conn->query($sql);
      if($atithi2===FALSE)
      {
      throw new Exception("Code 0017 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($atithi2))
      {
      $atithiNo2=$row["amt"];
      }
      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE sstatus='5' and stype='(HALL)'";
      $atithi3=$conn->query($sql);
      if($atithi3===FALSE)
      {
      throw new Exception("Code 0018: ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($atithi3))
      {
      $atithiNo3=$row["amt"];
      }

      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE sstatus='9'";
      $atithi4=$conn->query($sql);
      if($atithi4===FALSE)
      {
      throw new Exception("Code 0019 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($atithi4))
      {
      $atithiNo4=$row["amt"];
      }
      $sql="SELECT COUNT( * ) as amt FROM bigdharmshala WHERE sstatus='4'";
      $atithi5=$conn->query($sql);

      if($atithi5===FALSE)
      {
      throw new Exception("Code 0020 : ".mysqli_error($conn));   
      }

      if($row=mysqli_fetch_array($atithi5))
      {
      $atithiNo5=$row["amt"];
      }
 
?>
    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-4">
    <div class="profile-widget profile-widget3">
    <div class="blur-new">   <a href="sjpjt_athiti.php"> <img src='../assets/i5.jpg' width="400" height="180"> </a> </div>
    <div>
        
        <div class="user-info" style="padding-left:20px; padding-top:10px;">
            <div class="username" style="text-align: center;">
                <a href="sjpjt_athiti.php" style="color: #444444d1;">Booked Room Status of <span style="color: #ffaf0e;"> Athiti Bhavan </span></a>
            </div>
            <span>
                    <!-- <a href="#">@<span>johndoe</span></a> -->
            </span>
        </div>
        <div class="user-analytics">
            <div class="row">
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $atithiNo1;  ?></h5>
                        <span class="analytics-title">  AC Rooms</span>
                    </div>
                </div>
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $atithiNo2;  ?></h5>
                        <span class="analytics-title">  Non-AC Rooms</span>
                    </div>
                </div>
                <div class="col-sm-4 col-4">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $atithiNo3;  ?></h5>
                        <span class="analytics-title">  Halls</span>
                    </div>
                </div>

                 

                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title"> <sub>Member Stay <b>Offline</b></sub>  </span>
                    </div>
                </div>
                <div class="col-sm-4 col-4 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title">  <sub>Member Stay <b>Online</b></sub></span>
                    </div>
                </div>
                <div class="col-sm-4 col-4">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><i><sub>NA</sub></i></h5>
                        <span class="analytics-title"> <sub>Member Stay <b>Total</b></sub></span>
                    </div>
                </div>

                <div class="col-sm-6 col-6 border-right">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $atithiNo4;  ?></h5>
                        <span class="analytics-title"> Rooms in Cleaning</span>
                    </div>
                </div>
                <div class="col-sm-6 col-6">
                    <div class="analytics-desc">
                        <h5 class="analytics-count"><?php echo $atithiNo5;  ?></h5>
                        <span class="analytics-title"> Free Rooms</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
 
    </div>
    </div>
    <?php 


      $conn->query("COMMIT");
 

      } catch(Exception $e) { 


      $conn->query("ROLLBACK"); 
      $content = htmlspecialchars($e->getMessage());
      $content = htmlentities($conn->real_escape_string($content));

      $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
      if ($conn->query($sql) === FALSE) { 
      echo "Error: " . $sql . "<br>" . $conn->error;
      }


      echo "
      <script>
      swal({
      title: \"Error !\",
      text: \"$content\",
      icon: \"error\",
      button: \"OK\",
      });
      </script>";    
      } 
 
      $conn->close();

      require "_footer.php"; ?>