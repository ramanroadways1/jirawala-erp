<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");


try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
			<div class="page-wrapper">
				<div class="content"> 
  					<div class="row">

				<!-- ################### ################### REPORT 1 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="all_room_satus.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Booked & Available Rooms</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt1" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt2" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    <select class="select" id="" name="sys" required="required">
				                            <option value=""> -- Select -- </option>
 											<option value="List">List</option>
											<option value="Org" disabled="">Org</option>
											<option value="Grid" disabled="">Grid</option>
				                     </select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

				<!-- ################### ################### REPORT 11 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="Account_trn_success1.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">All Transaction Report</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt3" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt4" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    <select class="select" id="" name="sys" required="required">
				                            <option value=""> -- Select -- </option>
											<option value="E000" > Successful </option>
											<option value="Not_paid" > Not Paid </option>
											<option value="E0801" > Fail </option>
											<option value="E0802" > User Dropped  </option>
											<option value="E0803" > Canceled by user </option>
											<option value="E001" > Unauthorized Payment Mode </option>
											<option value="E002" > Unauthorized Key </option>
											<option value="E003" > Unauthorized Packet </option>
											<option value="E004" > Unauthorized Merchant </option>
											<option value="E005" > Unauthorized Return UR </option>
											<option value="E006" > Transaction is already paid </option>
											<option value="E007" > Transaction Failed </option>
											<option value="E008" > Failure from Third Party due to Technical Error </option>
											<option value="E009" > Bill Already Expired </option>
											<option value="E0031" > Mandatory fields coming from merchant are empty </option>
											<option value="E0032" > Mandatory fields coming from database are empty </option>
											<option value="E0033" > Payment mode coming from merchant is empty </option>
											<option value="E0034" > PG Reference number coming from merchant is empty </option>
											<option value="E0035" > Sub merchant id coming from merchant is empty </option>
											<option value="E0036" > Transaction amount coming from merchant is empty </option>
											<option value="E0037" > Payment mode coming from merchant is other than 0 to 9 </option>
											<option value="E0038" > Transaction amount coming from merchant is more than 9 digit length </option>
											<option value="E0039" > Mandatory value Email in wrong format </option>
											<option value="E00310" > Mandatory value mobile number in wrong format </option>
											<option value="E00311" > Mandatory value amount in wrong format </option>
											<option value="E00312" > Mandatory value Pan card in wrong format </option>
											<option value="E00313" > Mandatory value Date in wrong format </option>
											<option value="E00314" > Mandatory value String in wrong format </option>
											<option value="E00315" > Optional value Email in wrong format </option>
											<option value="E00316" > Optional value mobile number in wrong format </option>
											<option value="E00317" > Optional value amount in wrong format </option>
											<option value="E00318" > Optional value pan card number in wrong format </option>
											<option value="E00319" > Optional value date in wrong format </option>
											<option value="E00320" > Optional value string in wrong format </option>
											<option value="E00321" >Request packet mandatory columns is not equal to mandatory columns.</option>
											<option value="E00322" > Reference Number Blank </option>
											<option value="E00323" > Mandatory Columns are Blank </option>
											<option value="E00324" > Merchant Reference Number and Mandatory Columns are Blank </option>
											<option value="E00325" > Merchant Reference Number Duplicate </option>
											<option value="E00326" > Sub merchant id coming from merchant is non numeric </option>
											<option value="E00327" > Cash Challan Generated </option>
											<option value="E00328" > Cheque Challan Generated </option>
											<option value="E00329" > NEFT Challan Generated </option>
											<option value="E00330" > Transaction Amount and Mandatory Transaction Amount mismatch in Request URL </option>
											<option value="E00331" > UPI Transaction Initiated Please Accept or Reject the Transaction </option>
											<option value="E00332" > Challan Already Generated, Please re-initiate with unique reference number </option>
											<option value="E00333" > Referer is null/invalid Referer  </option>
											<option value="E00334" > Mandatory Parameters Reference No and Request Reference No parameter values are not matched </option>
											<option value="E00335" > Transaction Cancelled By User </option>
											<option value="E0801" > FAIL </option>
											<option value="E0802" > User Dropped  </option>
											<option value="E0803" > Canceled by user </option>
											<option value="E0804" > User Request arrived but card brand not supported </option>
											<option value="E0805" >  Checkout page rendered Card function not supported </option>
											<option value="E0806" >  Forwarded / Exceeds withdrawal amount limit </option>
											<option value="E0807" > PG Fwd Fail / Issuer Authentication Server failure </option>
											<option value="E0808" > Session expiry / Failed Initiate Check, Card BIN not present </option>
											<option value="E0809" > Reversed / Expired Card </option>
											<option value="E0810" > Unable to Authorize </option>
											<option value="E0811" > Invalid Response Code or Guide received from Issue </option>
											<option value="E0812" > Do not honor </option>
											<option value="E0813" > Invalid transaction </option>
											<option value="E0814" > Not Matched with the entered amount </option>
											<option value="E0815" > Not sufficient funds </option>
											<option value="E0816" > No Match with the card number </option>
											<option value="E0817" > General Error </option>
											<option value="E0818" > Suspected fraud </option>
											<option value="E0819" > User Inactive </option>
											<option value="E0820" > ECI 1 and ECI6 Error for Debit Cards and Credit Cards </option>
											<option value="E0821" > ECI 7 for Debit Cards and Credit Cards </option>
											<option value="E0822" >  System error. Could not process transaction </option>
											<option value="E0823" > Invalid 3D Secure values </option>
											<option value="E0824" > Bad Track Data </option>
											<option value="E0825" >  Transaction not permitted to cardholder </option>
											<option value="E0826" > Rupay timeout from issuing bank </option>
											<option value="E0827" > OCEAN for Debit Cards and Credit Cards </option>
											<option value="E0828" >  E-commerce decline </option>
											<option value="E0829" > This transaction is already in process or already processed </option>
											<option value="E0830" > Issuer or switch is inoperative </option>
											<option value="E0831" > Exceeds withdrawal frequency limit </option>
											<option value="E0832" > Restricted card </option>
											<option value="E0833" > Lost card </option>
											<option value="E0834" > Communication Error with NPCI </option>
											<option value="E0835" > The order already exists in the database </option>
											<option value="E0836" > General Error Rejected by NPCI </option>
											<option value="E0837" > Invalid credit card number </option>
											<option value="E0838" > Invalid amount </option>
											<option value="E0839" > Duplicate Data Posted </option>
											<option value="E0840" > Format error </option>
											<option value="E0841" > SYSTEM ERROR </option>
											<option value="E0842" > Invalid expiration date </option>
											<option value="E0843" > Session expired for this transaction  </option>
											<option value="E0844" > FRAUD - Purchase limit exceeded  </option>
											<option value="E0845" > Verification decline </option>
											<option value="E0846" > Compliance error code for issuer     </option>
											<option value="E0847" > Caught ERROR of type:[ System.Xml.XmlException ] . strXML is not a valid XML string   Failed in Authorize - I </option>
											<option value="E0848" > Incorrect personal identification number  </option>
											<option value="E0849" > Stolen card   </option>
											<option value="E0850" > Transaction timed out, please retry   </option>
											<option value="E0851" > Failed in Authorize - PE </option>
											<option value="E0852" > Cardholder did not return from Rupay </option>
											<option value="E0853" > Missing Mandatory Field(s)The field card_number has exceeded the maximum length of 19 </option>
											<option value="E0854" > Exception in CheckEnrollmentStatus: Data at the root level is invalid. Line 1, position 1. </option>
											<option value="E0855" > CAF status = 0 or 9 </option>
											<option value="E0856" > 412 </option>
											<option value="E0857" > Allowable number of PIN tries exceeded   </option>
											<option value="E0858" > No such issuer  </option>
											<option value="E0859" > Invalid Data Posted </option>
											<option value="E0860" > PREVIOUSLY AUTHORIZED </option>
											<option value="E0861" > Cardholder did not return from ACS </option>
											<option value="E0862" > Duplicate transmission </option>
											<option value="E0863" > Wrong transaction state </option>
											<option value="E0864" > Card acceptor contact acquirer </option>
				                     </select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>
				<!-- ################### ################### REPORT 12 ################### ###################  -->
				<!-- ################### ################### REPORT 1 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="off_on.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">offline & online Report</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt5" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt6" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    <select class="select" id="" name="sys" required="required">
				                            <option value=""> -- Select -- </option>
											<option value="BOTH" > BOTH </option>
											<option value="OFFLINE" > OFFLINE </option>
											<option value="ONLINE" > ONLINE </option>
				                     </select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>


				<div class="col-md-3">
				<div class="card-box"> 
					<form action="accountdonation_old.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Donation Register <sup>{2020-21}</sup></h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt21" name="starttime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt22" name="endtime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    <select class="select" id="" name="sys" required="required">
				                            <option value="0"> -- ALL -- </option>
											<?php
											$sql="select * from admin group by username ";
											$rep1=$conn->query($sql);
											if($rep1===FALSE)
											{
												throw new Exception("Code 001 : ".mysqli_error($conn));   
											}
											while($row=mysqli_fetch_array($rep1))
											{
											?>
												<option value="<?php echo $row["id"]  ?>"><?php echo strtolower($row["username"]);  ?></option> 
											<?php } ?>
				                     </select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>


				<div class="col-md-3">
				<div class="card-box"> 
					<form action="room_booked_available.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Booked & Available Rooms</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>Enter Date</label>
				                    <input type="text" class="form-control" id="dt9" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				              

				               
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

                </div>
	    	</div>
		</div>


<script type="text/javascript">

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$(document).ready(function () {

	// ############ start ############
	$("#dt1").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt2');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt2').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############
	
	// ############ start ############
	$("#dt3").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt4');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt4').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt5").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt6');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt6').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt7").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt8');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt8').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// 

	// ############ start ############
	$("#dt21").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt22');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt22').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
});
// ############ start ############
	$("#dt9").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt9');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt10').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 




// $('input[id$=date1]').datepicker({ 
// 	// minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

// $('input[id$=date1]').datepicker({ 
// 	// minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>