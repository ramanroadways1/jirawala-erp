<?php

require "_session.php";

$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate"]));
$date=$conn->real_escape_string(htmlspecialchars($_POST["date"]));
$username=$conn->real_escape_string(htmlspecialchars($_POST["username"]));
$usname=$conn->real_escape_string(htmlspecialchars($_POST["usname"]));
$type=$conn->real_escape_string(htmlspecialchars($_POST["type"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["inword"]));
$expense=$conn->real_escape_string(htmlspecialchars($_POST["expense"]));
$comment=$conn->real_escape_string(htmlspecialchars($_POST["comment"]));
$bkname=$conn->real_escape_string(htmlspecialchars($_POST["bkname"]));
$CB=$conn->real_escape_string(htmlspecialchars($_POST["CB"]));


try {
    $conn->query("START TRANSACTION"); 
          
        if($CB=="CASH")
        {
          $sql="SELECT * from balance where Branch='pedi'";
          $res=$conn->query($sql);
          if($res===FALSE)
          {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
          }
          while($row=mysqli_fetch_array($res))
          {
              $fetct_amount_pedi =$row["Balance"];
              $enter_amt =$amt;
              if($fetct_amount_pedi < $enter_amt)
              {

                throw new Exception(" Enter amount is $amt is less then your total amount $etct_amount_pedi ");   

                //   echo "<script type=\"text/javascript\">
                // window.location = \"acc_canara_voucher.php?msg= Enter amount is.$amt. is less then your total amount >$etct_amount_pedi\";    
                // </script>";
              }
              else
              { 
                $sql="SELECT COUNT(id) as total FROM expense";
                $res=$conn->query($sql);
                if($res===FALSE)
                {
                  throw new Exception("Code 002 : ".mysqli_error($conn));   
                }
                while($row1=mysqli_fetch_array($res))
                {
                  $d_b_id=(int)$row1["total"]+1;
                  $update_pedi_cash =$fetct_amount_pedi -$enter_amt;
                  $sql="UPDATE balance set Balance='$update_pedi_cash' WHERE Branch='pedi'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 003 : ".mysqli_error($conn));             
                    }
                    $sql="SELECT * from balance where Branch='Bank'";
                    $res2=$conn->query($sql);
                    if($res2===FALSE)
                    {
                      throw new Exception("Code 004 : ".mysqli_error($conn));   
                    }
                    while($row2=mysqli_fetch_array($res2))
                    {
                      $sql="SELECT * from bank_name_with_amount where Bank_Name='$expense'";
                      $res3=$conn->query($sql);
                      if($res3===FALSE)
                      {
                        throw new Exception("Code 005 : ".mysqli_error($conn));   
                      }
                    while($row3=mysqli_fetch_array($res3))
                    {
                      $balance_bank =$row2["Balance"]; // bank balance from where bhanch is bank
                      $bank_name_with_amount =$row3["Amount"]; // blanace with bank
                      $bank_with_balance =$balance_bank +$enter_amt;
                      $bank_with_balance_particular =$bank_name_with_amount + $enter_amt;

                          $sql="UPDATE balance set Balance='$bank_with_balance' WHERE Branch='Bank'";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 006 : ".mysqli_error($conn));             
                          }

                          $sql="UPDATE bank_name_with_amount set Amount='$bank_with_balance_particular' WHERE Bank_Name='$expense'";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 007 : ".mysqli_error($conn));             
                          }

                          $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$username','$usname','$expense','$amt','$inword','$comment','$date','$mysqldate','$type','','$expense','','','','','0','0','0','0','Contra_voucher','0')";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 008 : ".mysqli_error($conn));             
                          }


                          $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username','main_office','$date','$mysqldate','$expense','','0','$amt','$bank_with_balance','$type','$comment','$d_b_id')";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 009 : ".mysqli_error($conn));             
                          }

                          $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$username','main_office','$date','$mysqldate','$expense','','$amt','0','$update_pedi_cash','$type','$comment','$d_b_id')";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0010 : ".mysqli_error($conn));             
                          }

               


                    }
                    }

                    // echo "<script type=\"text/javascript\">
                    // window.location = \"expense_Slip.php?id=$d_b_id\";    
                    // </script>";

                }

              }

          }

        }

        if($CB=="BANK")
        {
          $sql="SELECT COUNT(id) as total FROM expense";
          $res=$conn->query($sql);
          if($res===FALSE)
          {
          throw new Exception("Code 0011 : ".mysqli_error($conn));   
          }
          while($row=mysqli_fetch_array($res))
          {
              $d_b_id=(int)$row["total"]+1;
              $sql="SELECT * from balance where Branch='Bank'";
              $res2=$conn->query($sql);
              if($res2===FALSE)
              {
                throw new Exception("Code 0012 : ".mysqli_error($conn));   
              }
              while($row2=mysqli_fetch_array($res2))
              {
                $sql="SELECT * from bank_name_with_amount where Bank_Name='$expense'";
                $res3=$conn->query($sql);
                if($res3===FALSE)
                {
                  throw new Exception("Code 0013 : ".mysqli_error($conn));   
                }

                $sql="SELECT * from bank_name_with_amount where Bank_Name='$bkname'";

                $res4=$conn->query($sql);
                if($res4===FALSE)
                {
                  throw new Exception("Code 0014 : ".mysqli_error($conn));   
                }

              while($row3=mysqli_fetch_array($res3))
              {
                  while($row4=mysqli_fetch_array($res4))
                  {
                    $enter_amt =$amt;
                    $debit_amount =$row4["Amount"]; //bknae
                    $credit_amount =$row3["Amount"];//bnk
                    $update_debit_amount =$debit_amount - $enter_amt; // bkname
                    $update_credit_amount = $credit_amount + $enter_amt; // bnk
                    $bank_amount =$row2["Balance"];
                    $bank_debit = $bank_amount - $enter_amt; // bank_debit
                    $bank_credit = $bank_amount + $enter_amt; // bank_credit

                    $sql="UPDATE bank_name_with_amount set Amount='$update_debit_amount' WHERE Bank_Name='$bkname'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0015 : ".mysqli_error($conn));             
                    }

                    $sql ="UPDATE bank_name_with_amount set Amount='$update_credit_amount' WHERE Bank_Name='$expense'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0016 : ".mysqli_error($conn));             
                    }

                    $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$username','$bkname','$expense','$amt','$inword','$comment','$date','$mysqldate','$type','','$expense','','','','','0','0','0','0','Contra_voucher','0')";

                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0017 : ".mysqli_error($conn));             
                    }
                    //debit
                    $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username','main_office','$date','$mysqldate','$expense','','$enter_amt','0','$bank_debit','$type','$comment','$d_b_id')";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0018 : ".mysqli_error($conn));             
                    }

                    $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username','main_office','$date','$mysqldate','$expense','','0','$enter_amt','$bank_amount','$type','$comment','$d_b_id')";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0019 : ".mysqli_error($conn));             
                    }
                }
              }

              }
             // echo "<script type=\"text/javascript\">
             //        window.location = \"expense_Slip.php?id=$d_b_id\";    
             //        </script>";
          }


        }

    $conn->query("COMMIT");
    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

    echo "<script type=\"text/javascript\">
    window.alert('Voucher created successfully !');
    window.location = \"expense_Slip.php?id=$d_b_id\";    
    </script>";
    
} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
            echo "<script type=\"text/javascript\">
            window.alert('$content');
            window.location = \"acc_canara_voucher.php\";    
            </script>";
            // echo "
            // <script>
            // swal({
            // title: \"Error !\",
            // text: \"$content\",
            // icon: \"error\",
            // button: \"OK\",
            // });
            // </script>";    
} 

$conn->close();
?>