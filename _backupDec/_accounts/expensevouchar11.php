<?php

require "_session.php";
  
$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$name=$conn->real_escape_string(htmlspecialchars($_POST["username"]));
$expense=$conn->real_escape_string(htmlspecialchars($_POST["expense"]));
$narration=$conn->real_escape_string(htmlspecialchars($_POST["comment"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["inword"]));
$date=$conn->real_escape_string(htmlspecialchars($_POST["date"]));
$mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate"]));
$usname=$conn->real_escape_string(htmlspecialchars($_POST["usname"]));
$type=$conn->real_escape_string(htmlspecialchars($_POST["type"]));
$cheque=$conn->real_escape_string(htmlspecialchars($_POST["cheque"]));
$chequetype=$conn->real_escape_string(htmlspecialchars($_POST["bank"]));
$nefttype=$conn->real_escape_string(htmlspecialchars($_POST["bank11"]));
$pan=$conn->real_escape_string(htmlspecialchars($_POST["pan"]));
$achol=$conn->real_escape_string(htmlspecialchars($_POST["achol"]));
$ifsc=$conn->real_escape_string(htmlspecialchars($_POST["ifsc"]));
$accc=$conn->real_escape_string(htmlspecialchars($_POST["accc"]));
$bank_name=$conn->real_escape_string(htmlspecialchars($_POST["bank_name"]));


$vo="";
$des = "Expense of ".$expense." Date :".$date;
$form="expense_vouchar";


try {
      $conn->query("START TRANSACTION"); 

      $sql="SELECT COUNT(id) as count FROM expense";
      $res=$conn->query($sql);

      if($res===FALSE)
      {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
      }
      while($row=mysqli_fetch_array($res))
      {
        $d_b_id=(int)$row["count"]+1;

      if($type=="CASH")
      {
         $sql="SELECT * from balance where Branch='pedi'";
          $res=$conn->query($sql);

          if($res===FALSE)
          {
            throw new Exception("Code 002 : ".mysqli_error($conn));   
          }
          while($row=mysqli_fetch_array($res))
          {
             $fetch_cash_total_amt =$row["Balance"];
             $enter_amount =$amt;
             if($enter_amount >= $fetch_cash_total_amt)
             {
                throw new Exception("Enter amount $amt is greater then your total amount $fetch_cash_total_amt");  
                // echo "<script type=\"text/javascript\">
                //      window.location = \"acc_expense_voucher.php?msg= Enter amount is .$amt. is greater then your total amount .$fetch_cash_total_amt\";    
                //      </script>";
             
             }
             else
             {
                $amountupdatepedi = $fetch_cash_total_amt - $enter_amount;
                $sql="SELECT * from balance where Branch='expense'";
                $res=$conn->query($sql);
                if($res===FALSE)
                {
                  throw new Exception("Code 003 : ".mysqli_error($conn));   
                }
                while($row1=mysqli_fetch_array($res))
                {
                    $expense_fetch =$row1["Balance"];
                    $last_expense =$expense_fetch + $enter_amount; // expense update
                    $sql="SELECT * from heads where expense_vouchar='$expense'";
                    $res=$conn->query($sql);
                    if($res===FALSE)
                    {
                      throw new Exception("Code 004 : ".mysqli_error($conn));   
                    }

                while($row2=mysqli_fetch_array($res))
                {
                    $heads_fetch =$row2["opening_balance"];
                    $heads_sum =$heads_fetch +$enter_amount;
                    $sql="UPDATE heads set opening_balance='$heads_sum' WHERE expense_vouchar='$expense'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 005 : ".mysqli_error($conn));             
                    } 

                }
                    $sql="UPDATE balance set Balance='$amountupdatepedi' WHERE Branch='pedi'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 006 : ".mysqli_error($conn));             
                    } 

                    $sql="UPDATE balance set Balance='$last_expense' WHERE Branch='expense'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 007 : ".mysqli_error($conn));             
                    } 

                }

                  $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$usname','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque',' ','$pan','$achol','$ifsc','$accc','0','0','0','0','Expense_voucher','0')";

                  if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 008 : ".mysqli_error($conn));             
                  } 
           
                  $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$name','main_office','$date','$mysqldate','','$expense','$amt','0','$amountupdatepedi','$form','$narration','$d_b_id')";

                  if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 009 : ".mysqli_error($conn));             
                  } 
              }
          }
         //  echo "<script type=\"text/javascript\">
         // window.location = \"Expense_vocuher_slip.php?id=$d_b_id\";    
         //    </script>";
     }
     if($type=="CHEQUE")
     {
          $enter_amount = $amt;
          $sql="SELECT * from balance where Branch='Bank'";
          $res=$conn->query($sql);
          if($res===FALSE)
          {
            throw new Exception("Code 0010 : ".mysqli_error($conn));   
          }
          $sql="SELECT * from bank_name_with_amount where Bank_Name='$bank_name'";
          $res2=$conn->query($sql);
          if($res2===FALSE)
          {
            throw new Exception("Code 0011 : ".mysqli_error($conn));   
          }

          while($row=mysqli_fetch_array($res))
          {
          if($row2=mysqli_fetch_array($res2))
          {
            $fetchdata =$row2["Amount"]; /// Bank Amount of table bank_name_with_amount
             $fetch_bank_data =$row["Balance"];// total Bank Amount of balance
            if($enter_amount >= $fetchdata)
             {
                throw new Exception("Enter amount $amt is greater then your total amount $fetchdata");  

                // echo "<script type=\"text/javascript\">
                //      window.location = \"acc_expense_voucher.php?msg= Enter amount is .$amt. is greater then your total amount .$fetchdata\";    
                //      </script>";
             }
             else
             {
                $sql="SELECT * from balance where Branch='expense'";
                $res3=$conn->query($sql);
                if($res3===FALSE)
                {
                throw new Exception("Code 0012 : ".mysqli_error($conn));   
                }

                while($row3=mysqli_fetch_array($res3))
                {
                    $expense_fetch =$row3["Balance"];
                    $last_expense = $expense_fetch + $enter_amount; // expense update
                    $sql="UPDATE balance set Balance='$last_expense' WHERE Branch='expense'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0013 : ".mysqli_error($conn));             
                    } 
                }
                $update_totoal_bank_amount =$fetch_bank_data - $enter_amount; // update total bank amount
                $update_bank_amount =$fetchdata -$enter_amount;   // update particular bank Data
             
                $sql="UPDATE balance set Balance='$update_totoal_bank_amount' WHERE Branch='Bank'";
                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0014 : ".mysqli_error($conn));             
                } 
                $sql="UPDATE bank_name_with_amount set Amount='$update_bank_amount' WHERE Bank_Name='$bank_name'";
                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0015 : ".mysqli_error($conn));             
                } 

                $sql="SELECT * from heads where expense_vouchar='$expense'";
                $res4=$conn->query($sql);
                if($res4===FALSE)
                {
                  throw new Exception("Code 0016 : ".mysqli_error($conn));   
                }
                while($row4=mysqli_fetch_array($res4))
                {
                    $heads_fetch =$row4["opening_balance"];
                    $heads_sum = $heads_fetch + $enter_amount;
                    $sql="UPDATE heads set opening_balance='$heads_sum' WHERE expense_vouchar='$expense'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0017 : ".mysqli_error($conn));             
                    }
                }
                $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$usname','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque','$chequetype','$pan','$achol','$ifsc','$accc','0','0','0','$bank_name','Expense_voucher','0')";

                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0018 : ".mysqli_error($conn));             
                }
                       
                $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$name','main_office','$date','$mysqldate','$bank_name','$expense','$amt','0','$update_totoal_bank_amount','$form','$des','$d_b_id')";

                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0019 : ".mysqli_error($conn));             
                }

                  //    echo "<script type=\"text/javascript\">
                  //   window.location = \"Expense_vocuher_slip.php?id=$d_b_id\";    
                  // </script>";

             }
          }
          else
          {
              throw new Exception("Please select bank");  

            // echo "<script type=\"text/javascript\">
            //         window.location = \"acc_expense_voucher.php?msg= Bank is not Selected\";    
            //       </script>";
          }

          }


     }
     if($type=="NEFT")
     {

          $enter_amount = $amt;
          $sql="SELECT * from balance where Branch='Bank'";
          $res=$conn->query($sql);
          if($res===FALSE)
          {
            throw new Exception("Code 0020 : ".mysqli_error($conn));   
          }
          $sql="SELECT * from bank_name_with_amount where Bank_Name='$bank_name'";
          $res2=$conn->query($sql);
          if($res2===FALSE)
          {
            throw new Exception("Code 0021 : ".mysqli_error($conn));   
          }

          while($row=mysqli_fetch_array($res))
          {
          if($row2=mysqli_fetch_array($res2))
          {
             $fetchdata =$row2["Amount"]; // Bank Amount
             $fetch_bank_data =$row["Balance"];// total Bank Amount
            
                if($enter_amount >= $fetchdata)
                {
                      throw new Exception("Enter amount $amt is greater then your total amount $fetchdata");  
                      // echo "<script type=\"text/javascript\">
                      // window.location = \"acc_expense_voucher.php?msg= Enter amount is .$amt. is greater then your total amount .$fetchdata\";    
                      // </script>";
                }
                else
                {
                    $sql="SELECT * from balance where Branch='expense'";
                    $res3=$conn->query($sql);
                    if($res3===FALSE)
                    {
                      throw new Exception("Code 0022 : ".mysqli_error($conn));   
                    }
                    while($row3=mysqli_fetch_array($res3))
                    {
                      $expense_fetch =$row3["Balance"];
                      $last_expense =$expense_fetch +$enter_amount; // expense update
                      $sql="UPDATE balance set Balance='$last_expense' WHERE Branch='expense'";

                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0023 : ".mysqli_error($conn));             
                      }

                    }
                    $update_totoal_bank_amount =$fetch_bank_data - $enter_amount; // update total bank amount
                    $update_bank_amount =$fetchdata -$enter_amount;   // update particular bank Data

                    $sql="UPDATE balance set Balance='$update_totoal_bank_amount' WHERE Branch='Bank'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0024 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE bank_name_with_amount set Amount='$update_bank_amount' WHERE Bank_Name='$bank_name'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0025 : ".mysqli_error($conn));             
                    }


                    $sql="SELECT * from heads where expense_vouchar='$expense'";
                    $res4=$conn->query($sql);
                    if($res4===FALSE)
                    {
                    throw new Exception("Code 0026 : ".mysqli_error($conn));   
                    }
                    while($row4=mysqli_fetch_array($res4))
                    {
                    $heads_fetch =$row4["opening_balance"];
                    $heads_sum =$heads_fetch + $enter_amount;

                    $sql="UPDATE heads set opening_balance='$heads_sum' WHERE expense_vouchar='$expense'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0027 : ".mysqli_error($conn));             
                    }

                    }


                    $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$usname','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque','$nefttype','$pan','$achol','$ifsc','$accc','0','0','0','$bank_name','Expense_voucher','0')";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0028 : ".mysqli_error($conn));             
                    }

                    $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$name','main_office','$date','$mysqldate','$bank_name','$expense','$amt','0','$update_totoal_bank_amount','$form','$des','$d_b_id')";

                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0029 : ".mysqli_error($conn));             
                    }
                }
             // echo "<script type=\"text/javascript\">
             //        window.location = \"Expense_vocuher_slip.php?id=$d_b_id\";    
             //      </script>";
          }
          else
          {

            throw new Exception("Please select bank");  
            // echo "<script type=\"text/javascript\">
            //         window.location = \"acc_expense_voucher.php?msg= Bank is not Selected\";    
            //       </script>";

          }
          }   
     }

      if($type=="TRANSFER")
      {

        throw new Exception("Not allowed !");  

        // echo "<script type=\"text/javascript\">
        //             window.location = \"acc_expense_voucher.php?msg= under Working\";    
        //           </script>";
      }
      
   }


    $conn->query("COMMIT");

    echo "<script type=\"text/javascript\">
    window.alert('Voucher created successfully !');
    window.location = \"Expense_vocuher_slip.php?id=$d_b_id\";    
    </script>";

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
              echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "<script type=\"text/javascript\">
            window.alert('$content');
            window.location = \"acc_expense_voucher.php\";    
            </script>";
            // echo "
            // <script>
            // swal({
            // title: \"Error !\",
            // text: \"$content\",
            // icon: \"error\",
            // button: \"OK\",
            // });
            // </script>";    
} 


$conn->close();
?>