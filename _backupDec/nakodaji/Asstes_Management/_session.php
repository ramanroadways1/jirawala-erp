
<?php

	session_start();
     if((time() - $_SESSION['last_login_timestamp']) > 1800) // 1800= 15 * 60  
           {  
                header("location:../logout.php");  
           }  

	if(!isset($_SESSION["username"])&&$_SESSION["dep"]!="it") {
		header('Location: ../index.php');
		exit();
	}

	require('../connection.php');
	$username=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);