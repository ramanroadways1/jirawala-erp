<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");

$mnumber=$conn->real_escape_string(htmlspecialchars($_POST["mnumber"]));

$name="";
$identity="";
$address="";
$snumber="";

        $sql="SELECT * FROM `bookroom` WHERE Mobilenumber='$mnumber'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
        if($row=mysqli_fetch_array($res))
        { 
			$name=$row["Name"];
			$identity=$row["identity"];
			$address=$row["address"];
			$snumber=$row["snumber"];
			$male=$row["male"];
            $female=$row["female"];
            $child=$row["child"];
            $driver=$row["driver"];
            $total=$row["total"];

        }
        else
        {
        	  echo ("<script LANGUAGE='JavaScript'>
    window.alert('Mobile number does not exist !');
    window.location.href = \"sjpjt_bookroom.php\";
    </script>");

        }


try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-6">
	            <h4 class="page-title"> Bhavan Room Booking </h4>
	        </div>
	        <div class="col-sm-6">
				
				
	        </div>

	    </div>

  <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                         <!--    <h4 class="card-title">Two Column Horizontal Form</h4>
                            <h4 class="card-title">Personal Information</h4> -->
  						<form action="sjpjt_bookroom1.php" method="POST" id="myform" name="myform" onsubmit="return validateform()" >
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-3 form-group">
                                            <label>FULL NAME</label>
                                            <input type="text" class="form-control" value="<?php echo $name; ?>" id="fullname" name="name" required="required">
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <label>MOBILE NUMBER </label>
                                            <input type="text" class="form-control" value="<?php echo $mnumber; ?>" id="mnumber" size="10" maxlength="10" name="mnumber" required="required">
                                        </div>

                                        
                                        <div class="col-md-4 form-group">
                                            <label>STREET ADDRESS</label>
                                            <input type="text" class="form-control" id="address" value="<?php echo $address; ?>" name="address" required="required">
                                        </div>

                                        <div class="col-md-2 form-group">
                                            <label>STATE</label>
                                            <select class="form-control select" id="state" name="state" required="required">
                                                <option value="">...select...</option>
												<option value="Andhra Pradesh">Andhra Pradesh</option>
												<option value="Andaman and Nicobar Islands">Andaman & Nicobar </option>
												<option value="Arunachal Pradesh">Arunachal Pradesh</option>
												<option value="Assam">Assam</option>
												<option value="Bihar">Bihar</option>
												<option value="Chandigarh">Chandigarh</option>
												<option value="Chhattisgarh">Chhattisgarh</option>
												<option value="Dadar and Nagar Haveli">Dadar & Nagar Haveli</option>
												<option value="Daman and Diu">Daman and Diu</option>
												<option value="Delhi">Delhi</option>
												<option value="Lakshadweep">Lakshadweep</option>
												<option value="Puducherry">Puducherry</option>
												<option value="Goa">Goa</option>
												<option value="Gujarat">Gujarat</option>
												<option value="Haryana">Haryana</option>
												<option value="Himachal Pradesh">Himachal Pradesh</option>
												<option value="Jammu and Kashmir">Jammu and Kashmir</option>
												<option value="Jharkhand">Jharkhand</option>
												<option value="Karnataka">Karnataka</option>
												<option value="Kerala">Kerala</option>
												<option value="Madhya Pradesh">Madhya Pradesh</option>
												<option value="Maharashtra">Maharashtra</option>
												<option value="Manipur">Manipur</option>
												<option value="Meghalaya">Meghalaya</option>
												<option value="Mizoram">Mizoram</option>
												<option value="Nagaland">Nagaland</option>
												<option value="Odisha">Odisha</option>
												<option value="Punjab">Punjab</option>
												<option value="Rajasthan">Rajasthan</option>
												<option value="Sikkim">Sikkim</option>
												<option value="Tamil Nadu">Tamil Nadu</option>
												<option value="Telangana">Telangana</option>
												<option value="Tripura">Tripura</option>
												<option value="Uttar Pradesh">Uttar Pradesh</option>
												<option value="Uttarakhand">Uttarakhand</option>
												<option value="West Bengal">West Bengal</option>
                                             </select>
                                        </div>

                                         <div class="col-md-1 form-group">
                                            <label>MALE</label>
                                            <input type="text" class="form-control" onkeypress="return isNumber(event)"  id="i1" maxlength="3"  name="male"  size="3" onkeyup="addtotal()" required="required">
                                        </div>

                                         <div class="col-md-1 form-group">
                                            <label>FEMALE</label>
                                            <input type="text" class="form-control" onkeypress="return isNumber(event)"  id="i2" maxlength="3" name="female" size="3" onkeyup="addtotal()" required="required">
                                        </div>

                                         <div class="col-md-1 form-group">
                                            <label>CHILD</label>
                                            <input type="text" class="form-control" onkeypress="return isNumber(event)"  id="i3" maxlength="3" name="child" size="3" onkeyup="addtotal()" required="required">
                                        </div>

                                         <div class="col-md-1 form-group">
                                            <label>DRIVER</label>
                                            <input type="text" class="form-control"  onkeypress="return isNumber(event)" id="i4" maxlength="3" name="driver" size="3" onkeyup="addtotal()" required="required">
                                        </div>

                                         <div class="col-md-1 form-group">
                                            <label>TOTAL</label>
                                            <input type="text" class="form-control" id="i5" name="total" readonly="readonly" >
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <label>ID Card</label>
                                            <select class="select" id="Rno" name="identity" required="required">
                             <option>---select---</option>
                             <option value="AadharID" <?php echo ($identity=='AadharID') ? 'selected' : ''; ?>>Aadhar Card</option>
                              <option value="DriverID" <?php echo ($identity=='DriverID') ? 'selected' : ''; ?>>Driving license</option>                                              

							<!-- <?php 
							if($identity=="AadharID")
							{
							?>
							<option value="AadharID">Aadhar Card</option>
							<option value="DriverID">Driving License</option>

							<?php

							}
							if($identity=="DriverID")
							{
							
							?>
							<option value="DriverID">Driving License</option>
							<option value="AadharID">Aadhar Card</option>
							<?php
							}
							?> -->


													
                                             </select>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>ID Card Number</label>
                                            <input type="text" class="form-control" value="<?php echo $snumber;  ?>" id="number" name="snumber" required="required">
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <label>CHECK IN DATE</label>
                                            <input type="text" value="<?php echo $date; ?>" class="form-control" name="checkin" readonly="readonly">
                                        </div>


                                        <div class="col-md-3 form-group">
                                            <label>CHECK IN TIME</label>
                                            <div class="row">
                                            <div class="col-md-6">
                                            <input type="text" value="<?php echo $time; ?>" class="form-control" id="R" name="intime" readonly="readonly" >
                                        	</div>
                                        	<div class="col-md-6">
                                            <input type="text" value="<?php echo $time1; ?>" class="form-control" id="Rno" name="kkk" readonly="readonly">
                                        	</div>
                                        	</div>
                                        </div>



                                        <div class="col-md-3 form-group">
                                            <label>CHECK OUT DATE</label>
                                            <input type="text" class="form-control" id="checkout" name="checkout" required="required">
                                        </div>


                                         <div class="col-md-3 form-group">
                                            <label>CHECK OUT TIME</label>
                                            <div class="row">
                                            <div class="col-md-6">
                                            	<select class="select" id="R" name="outtime" required="required">
													<option value="">-- Select --</option>
													<option value="01:00">01:00</option>
													<option value="02:00">02:00</option>
													<option value="03:00">03:00</option>
													<option value="04:00">04:00</option>
													<option value="05:00">05:00</option>
													<option value="06:00">06:00</option>
													<option value="07:00">07:00</option>
													<option value="08:00">08:00</option>
													<option value="09:00">09:00</option>
													<option value="10:00">10:00</option>
													<option value="11:00">11:00</option>
													<option value="12:00">12:00</option>
                                             	</select>
                                        	</div>
                                        	<div class="col-md-6">
                                            	<select class="select" id="Rno" name="apm" required="required">
													<option value="">-- Select --</option>
													<option value="AM">AM</option>
													<option value="PM">PM</option>
                                             	</select>
                                        	</div>
                                        	</div>
                                        </div> 
                                    </div>
                                    
                                </div>
                                </div> 
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary"> NEXT <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

	    </div>
	</div>


<script type="text/javascript">

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$('input[id$=checkout]').datepicker({ 
	minDate: '0D',
    maxDate: '+1D',
    dateFormat: 'dd-mm-yy'
});

$("input[id$=checkout]").keypress(function (evt) {
    evt.preventDefault();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>