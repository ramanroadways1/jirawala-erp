<?php

require "_session.php";

$name=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$usname= $conn->real_escape_string(htmlspecialchars($_POST['usname']));
$dep= $conn->real_escape_string(htmlspecialchars($_POST['dep']));
$recdate= $conn->real_escape_string(htmlspecialchars($_POST['recdate']));
$mysqldate= $conn->real_escape_string(htmlspecialchars($_POST['mysqldate']));
$formthe= $conn->real_escape_string(htmlspecialchars($_POST['bhojanshala']));
$debit= $conn->real_escape_string(htmlspecialchars($_POST['debit']));
$vo= $conn->real_escape_string(htmlspecialchars($_POST['vo']));
$sys= $conn->real_escape_string(htmlspecialchars($_POST['sys']));
$totalamt= $conn->real_escape_string(htmlspecialchars($_POST['totalamt']));
$cancelamt= $conn->real_escape_string(htmlspecialchars($_POST['cancelamt']));
$b1= $conn->real_escape_string(htmlspecialchars($_POST['b1']));
$b2= $conn->real_escape_string(htmlspecialchars($_POST['b2']));

$cash1 = "Donation Amount Confirmation of ".$sys." , Donation Amount -> ".$totalamt." , Cancel Amount -> ".$cancelamt;
$cheque1 = "Donation Amount Confirmation of ".$sys." , Donation Amount -> ".$b1." , Cancel Amount -> ".$b2;


try {

    $conn->query("START TRANSACTION"); 

    $sql="SELECT * from repcash where username='$usname' and mysqldate='$mysqldate' and formthe='$sys' and vou_type='Confirm_Donation_Amount'";
    $res=$conn->query($sql);
    if($res===FALSE)
    {
      throw new Exception("Code 001 : ".mysqli_error($conn));   
    }

    if($row=mysqli_fetch_array($res))
          {
            throw new Exception("Please try after 24 hours today's confirmation already done !");  
          }
          else
          {
            $sql="UPDATE main_office_overall_cash set amount='0' WHERE username = '$sys' and Branch='Donation'";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 002 : ".mysqli_error($conn));  
            }

            $sql="UPDATE main_office_overall_cash set amount='0' WHERE username = '$sys' and Branch='Donation'";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 003 : ".mysqli_error($conn));  
            }

            $sql="UPDATE main_office_overall_cash set amount='0' WHERE username = '$sys' and Branch='Donation_cheque'";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 004 : ".mysqli_error($conn));  
            }
            // $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Donation_Cancel'";
            // i chenge table name overallcash to main- office-overallcash

            $sql="UPDATE main_office_overall_cash set amount='0' WHERE username = '$sys' and Branch='Donation_Cancel'";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 005 : ".mysqli_error($conn));  
            }

            $sql="UPDATE main_office_overall_cash set amount='0' WHERE username = '$sys' and Branch='Donation_cheque_cancel'";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 006 : ".mysqli_error($conn));  
            }

            $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$sys','main_office','$recdate','$mysqldate','-','-','0','0','0','Confirm_Donation_Status','$cash1','0')";

            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 007 : ".mysqli_error($conn));  
            }

            $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$sys','main_office','$recdate','$mysqldate','-','-','0','0','0','Bank_Donation_Status','$cheque1','0')";
            if($conn->query($sql) === FALSE) 
            {
              throw new Exception("Code 008 : ".mysqli_error($conn));  
            } 

          }

          echo ("<script LANGUAGE='JavaScript'>
          window.alert('Successfully Confirmed. ');
          window.location.href='acc_Cash_Main_Office_Confirm.php';
          </script>");

          $conn->query("COMMIT");
    
} catch(Exception $e) { 

        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
        if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
        } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo ("<script LANGUAGE='JavaScript'>
        window.alert('$content');
        window.location.href='acc_Cash_Main_Office_Confirm.php';
        </script>");
 
} 

$conn->close();

?>
