<?php
include "_session.php";



$user12=$conn->real_escape_string(htmlspecialchars($_POST["user12"]));
$mydate12=$conn->real_escape_string(htmlspecialchars($_POST["mydate12"]));
$mysqldate12=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate12"]));
$ass_name=$conn->real_escape_string(htmlspecialchars($_POST["ass_name"]));
$ass_cat=$conn->real_escape_string(htmlspecialchars($_POST["ass_cat"]));
$ass_dop=$conn->real_escape_string(htmlspecialchars($_POST["ass_dop"]));
$ass_company=$conn->real_escape_string(htmlspecialchars($_POST["ass_company"]));
$ass_mod=$conn->real_escape_string(htmlspecialchars($_POST["ass_mod"]));
$ass_amt=$conn->real_escape_string(htmlspecialchars($_POST["ass_amt"]));
$ass_gst=$conn->real_escape_string(htmlspecialchars($_POST["ass_gst"]));
$ass_perc=$conn->real_escape_string(htmlspecialchars($_POST["ass_perc"]));
$ass_gstamot=$conn->real_escape_string(htmlspecialchars($_POST["ass_gstamot"]));
$ass_tamt=$conn->real_escape_string(htmlspecialchars($_POST["ass_tamt"]));
$ass_party_name=$conn->real_escape_string(htmlspecialchars($_POST["ass_party_name"]));
$ass_mode=$conn->real_escape_string(htmlspecialchars($_POST["ass_mode"]));
$cheque=$conn->real_escape_string(htmlspecialchars($_POST["cheque"]));
$ba=$conn->real_escape_string(htmlspecialchars($_POST["bank"]));
$pan=$conn->real_escape_string(htmlspecialchars($_POST["pan"]));
$achol=$conn->real_escape_string(htmlspecialchars($_POST["achol"]));
$accc=$conn->real_escape_string(htmlspecialchars($_POST["accc"]));
$bank11=$conn->real_escape_string(htmlspecialchars($_POST["bank11"]));
$ifsc=$conn->real_escape_string(htmlspecialchars($_POST["ifsc"]));
$bank="";

$d1= date("Y-m-d", strtotime($ass_dop));

if($ba=="")
{
$bank=$conn->real_escape_string(htmlspecialchars($_POST["bank11"]));
}
if($bank11=="")
{
  $bank=$conn->real_escape_string(htmlspecialchars($_POST["bank"]));
}

$it= "";
date_default_timezone_set('Asia/Calcutta'); 
$Check=date("dmY"); 
$it_id= "IT/".$Check; 


try
 {
    $conn->query("START TRANSACTION"); 

  $sql="SELECT * FROM jirawala_store.assets_new";
  $res=$conn->query($sql);
  if($res===FALSE)
  {
  throw new Exception("Code 001 : ".mysqli_error($conn));   
  }
  while($row=mysqli_fetch_array($res))
  {
    $make_id2=$row["make_id"];
    $lst=substr($make_id2,0,11);
    if($it_id==$lst)
    {
      $id1=substr($make_id2,12);
      $id2=(int)$id1+1;
      $it=$lst."/".$id2;

    }
    else
    {
      $it= "IT/".$Check."/1";
    }

  }


$sql="INSERT INTO jirawala_store.assets_new(make_id,user12,mydate12,mysqldate12,ass_name,ass_cat,ass_dop,ass_dop_mysql,ass_company,ass_mod,ass_amt,ass_gst,ass_perc,ass_gstamot,ass_tamt,ass_party_name,ass_mode,cheque,bank,pan,achol,accc,bank11,ifsc,data,data1) values ('$it','$user12','$mydate12','$mysqldate12','$ass_name','$ass_cat','$ass_dop','$d1','$ass_company','$ass_mod','$ass_amt','$ass_gst','$ass_perc','$ass_gstamot','$ass_tamt','$ass_party_name','$ass_mode','$cheque','$bank','$pan','$achol','$accc','0','$ifsc','0','0')";
  


     if($conn->query($sql) === FALSE) {
    throw new Exception("Code 001 : ".mysqli_error($conn));             
    }  

// echo "<script type=\"text/javascript\">
//          window.location = \"ass_add_it_asstes.php?msg=Success\";    
//           </script>";



    $conn->query("COMMIT");


            echo ("<script LANGUAGE='JavaScript'>
            window.alert('Asset added successfully');
            window.location.href='ass_add_it_asstes.php';
            </script>");
    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }



            echo ("<script LANGUAGE='JavaScript'>
            window.alert('Error: $content');
            window.location.href='ass_add_it_asstes.php';
            </script>");

 
} 



$conn->close();


?>