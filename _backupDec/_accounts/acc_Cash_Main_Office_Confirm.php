<?php

	require "_session.php";
	require "_header.php";

	$username=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);


	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$Check1=date("Y-m-d");

?>



<div class="page-wrapper">
<div class="content">

 <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title">Donation Cash Collection  {<?php echo $date; ?>} </h4>
	        </div>
	    </div>

<div class="card" style="background-color: transparent; box-shadow: none;">
<!-- <div class="card-header">
	<h2 class="widget-title lighter" align="center">Donation Cash Collection <?php echo $date; ?>  </h2>
</div>
 -->
<div class="row">

 
<?php  
try {
    $conn->query("START TRANSACTION"); 
    $sql="select * from admin";
    $res=$conn->query($sql);
    if($res===FALSE)
    {
    throw new Exception("Code 001 : ".mysqli_error($conn));   
    }
    while($row=mysqli_fetch_array($res))
            {

           $sys=$row["username"];
			$sql="SELECT * from main_office_overall_cash where username='$sys' and Branch = 'Donation'";
			$res2=$conn->query($sql);
			if($res2===FALSE)
			{
			throw new Exception("Code 002: ".mysqli_error($conn));   
			}
         while($row2=mysqli_fetch_array($res2))
            {
                    $donation1=0;
                    $donation2=0;
                    $donation3=0;
					$sql="SELECT * from main_office_overall_cash where username='$sys' and Branch = 'Donation_cheque'";
					$res3=$conn->query($sql);
					if($res3===FALSE)
					{
					throw new Exception("Code 003: ".mysqli_error($conn));   
					}
					while($row3=mysqli_fetch_array($res3))
					{
                    $donation1=$row3["amount"];
					}


					$sql="SELECT * from main_office_overall_cash where username='$sys' and Branch = 'Donation_cheque_cancel'";
					$res3=$conn->query($sql);
					if($res3===FALSE)
					{
					throw new Exception("Code 004 : ".mysqli_error($conn));   
					}
					while($row3=mysqli_fetch_array($res3))
					{
                      $donation2=$row3["amount"];
					}



					$sql="SELECT * from main_office_overall_cash where username='$sys' and Branch = 'Donation_Cancel'";
					$res3=$conn->query($sql);
					if($res3===FALSE)
					{
					throw new Exception("Code 005 : ".mysqli_error($conn));   
					}
					while($row3=mysqli_fetch_array($res3))
					{
                  $donation3=$row3["amount"];
					}


?>

<!-- <div class="col-md-4">
<form action="update_overall_main_cash.php" method="post" >
			<input type="hidden" name="sys" value="<?php echo $sys; ?>">	
			<div class="card   mb-3" style="max-width: 20rem; background-color:#e67e22; color:white;  border-style: solid; border-color:Black;" id="log">
			<div class="card-header " style="border-color:Black;"><h5 align="center"><?php echo $sys; ?></h5></div>
			<div class="card-body">
			<h4 class="card-title"> <h6 align="center">Total Collection of :-  <?php echo $sys; ?></h6></h4>
			<p class="card-text" align="center"  >  Rs. <?php echo $row2["amount"]; ?> <b></b></p>
			<input type="hidden" value="<?php echo $row2["amount"]; ?>" name="totalamt">
			<h4 class="card-title"> <h6 align="center"> Bank  amount is  :-  <?php echo $donation1; ?></h6></h4>
			<input type="hidden" value="<?php echo $donation1; ?>" name="b1">
 	    	<h4 class="card-title"> <h6 align="center"> Donation cheuqe cancel amount is  :- <?php echo $donation2; ?></h6></h4>
 	    	  <input type="hidden" value="<?php echo $donation2; ?>" name="b2">
 	    	<h4 class="card-title"> <h6 align="center">Donation Cancel  amount is of :- <?php echo $donation3; ?></h6></h4>
 	    	<input type="hidden" value="<?php echo $donation3; ?>" name="cancelamt">

			<input type="hidden" name="usname" value="<?php echo $username; ?>">
			<input type="hidden" name="dep" value="main_office">
			<input type="hidden" name="debit" value="0">
			<input type="hidden" name="recdate" value="<?php echo $date; ?>">
			<input type="hidden" name="mysqldate" value="<?php echo $Check1; ?>">
			<input type="hidden" name="bhojanshala" value="Donation">
			<input type="hidden" name="vo" value="Donation">
			<input type="submit" id="btnSubmit" class="button btn-primary pull-right" value="Confirm" />
			
			</div>
			</div>  
    </form>	
  </div> -->

<div class="col-md-4">
<div class="dash-widget clearfix card-box" style="margin-bottom: 0px;">
	<form class="" action="update_overall_main_cash.php" method="POST" >

    <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
    <div class="dash-widget-info">
        <h3><?php echo $row2["amount"]; ?></h3>
        <span style="font-size:18px; font-weight: bold; text-transform: uppercase; "><?php echo $sys; ?></span>
    </div> 

	<div class="user-analytics">
	    <div class="row">

			<input type="hidden" name="sys" value="<?php echo $sys; ?>">	
			<input type="hidden" value="<?php echo $row2["amount"]; ?>" name="totalamt">
			<input type="hidden" value="<?php echo $donation1; ?>" name="b1">
			<input type="hidden" value="<?php echo $donation2; ?>" name="b2">
			<input type="hidden" value="<?php echo $donation3; ?>" name="cancelamt">
			<input type="hidden" name="usname" value="<?php echo $username; ?>">
			<input type="hidden" name="dep" value="main_office">
			<input type="hidden" name="debit" value="0">
			<input type="hidden" name="recdate" value="<?php echo $date; ?>">
			<input type="hidden" name="mysqldate" value="<?php echo $Check1; ?>">
			<input type="hidden" name="bhojanshala" value="Donation">
			<input type="hidden" name="vo" value="Donation">
			<!-- <input type="submit" id="btnSubmit" class="button btn-primary pull-right" value="Confirm" /> -->

	        <div class="col-sm-6 col-6 border-right">
	            <div class="analytics-desc">
	                <h5 class="analytics-count"><?php echo $donation1; ?></h5>
	                <span class="analytics-title"> Bank amount</span>
	            </div>
	        </div>
	        <div class="col-sm-6 col-6 border-right">
	            <div class="analytics-desc">
	                <h5 class="analytics-count"><?php echo $donation2; ?></h5>
	                <span class="analytics-title"> Cheque cancel amount</span>
	            </div>
	        </div>
	        <div class="col-sm-6 col-6 border-right">
	            <div class="analytics-desc">
	                <h5 class="analytics-count"><?php echo $donation3; ?></h5>
	                <span class="analytics-title"> Cash Cancel amount </span>
	            </div>
	        </div> 

	    </div>
	</div> 
    <div style="float: right;"> 
    	<button type="submit" style="color : #fff; " id="btnSubmit" name="btnSubmit" class="btn btn-warning btn-sm m-t-10"  /> <i class='fa fa-check-square'> </i> Confirm </button> 
    </div> 
</form>
</div>
</div>

<?php
            } }
?>


		</div>

		</div>

	   <?php

 $conn->query("COMMIT");
} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 


$conn->close();
require "_footer.php"; ?>
