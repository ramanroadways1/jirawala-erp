<?php

require "_session.php";
include "numbertoword.php";

$starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
$endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime= date("Y-m-d", strtotime($starttime1));
$endtime= date("Y-m-d", strtotime($endtime1));
$code = $sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);


if($code=="E000") { $status = "Successful"; }
else if($code=="Not_paid") { $status = "Not Paid"; }
else if($code=="E0801") { $status = "Fail"; }
else if($code=="E0802") { $status = "User Dropped "; }
else if($code=="E0803") { $status = "Canceled by user"; }
else if($code=="E001") { $status = "Unauthorized Payment Mode"; }
else if($code=="E002") { $status = "Unauthorized Key"; }
else if($code=="E003") { $status = "Unauthorized Packet"; }
else if($code=="E004") { $status = "Unauthorized Merchant"; }
else if($code=="E005") { $status = "Unauthorized Return UR"; }
else if($code=="E006") { $status = "Transaction is already paid"; }
else if($code=="E007") { $status = "Transaction Failed"; }
else if($code=="E008") { $status = "Failure from Third Party due to Technical Error"; }
else if($code=="E009") { $status = "Bill Already Expired"; }
else if($code=="E0031") { $status = "Mandatory fields coming from merchant are empty"; }
else if($code=="E0032") { $status = "Mandatory fields coming from database are empty"; }
else if($code=="E0033") { $status = "Payment mode coming from merchant is empty"; }
else if($code=="E0034") { $status = "PG Reference number coming from merchant is empty"; }
else if($code=="E0035") { $status = "Sub merchant id coming from merchant is empty"; }
else if($code=="E0036") { $status = "Transaction amount coming from merchant is empty"; }
else if($code=="E0037") { $status = "Payment mode coming from merchant is other than 0 to 9"; }
else if($code=="E0038") { $status = "Transaction amount coming from merchant is more than 9 digit length"; }
else if($code=="E0039") { $status = "Mandatory value Email in wrong format"; }
else if($code=="E00310") { $status = "Mandatory value mobile number in wrong format"; }
else if($code=="E00311") { $status = "Mandatory value amount in wrong format"; }
else if($code=="E00312") { $status = "Mandatory value Pan card in wrong format"; }
else if($code=="E00313") { $status = "Mandatory value Date in wrong format"; }
else if($code=="E00314") { $status = "Mandatory value String in wrong format"; }
else if($code=="E00315") { $status = "Optional value Email in wrong format"; }
else if($code=="E00316") { $status = "Optional value mobile number in wrong format"; }
else if($code=="E00317") { $status = "Optional value amount in wrong format"; }
else if($code=="E00318") { $status = "Optional value pan card number in wrong format"; }
else if($code=="E00319") { $status = "Optional value date in wrong format"; }
else if($code=="E00320") { $status = "Optional value string in wrong format"; }
else if($code=="E00321") { $status = "Request packet mandatory columns is not equal to mandatory columns"; }
else if($code=="E00322") { $status = "Reference Number Blank"; }
else if($code=="E00323") { $status = "Mandatory Columns are Blank"; }
else if($code=="E00324") { $status = "Merchant Reference Number and Mandatory Columns are Blank"; }
else if($code=="E00325") { $status = "Merchant Reference Number Duplicate"; }
else if($code=="E00326") { $status = "Sub merchant id coming from merchant is non numeric"; }
else if($code=="E00327") { $status = "Cash Challan Generated"; }
else if($code=="E00328") { $status = "Cheque Challan Generated"; }
else if($code=="E00329") { $status = "NEFT Challan Generated"; }
else if($code=="E00330") { $status = "Transaction Amount and Mandatory Transaction Amount mismatch in Request URL"; }
else if($code=="E00331") { $status = "UPI Transaction Initiated Please Accept or Reject the Transaction"; }
else if($code=="E00332") { $status = "Challan Already Generated, Please re-initiate with unique reference number"; }
else if($code=="E00333") { $status = "Referer is null/invalid Referer "; }
else if($code=="E00334") { $status = "Mandatory Parameters Reference No and Request Reference No parameter values are not matched"; }
else if($code=="E00335") { $status = "Transaction Cancelled By User"; }
else if($code=="E0801") { $status = "FAIL"; }
else if($code=="E0802") { $status = "User Dropped "; }
else if($code=="E0803") { $status = "Canceled by user"; }
else if($code=="E0804") { $status = "User Request arrived but card brand not supported"; }
else if($code=="E0805") { $status = " Checkout page rendered Card function not supported"; }
else if($code=="E0806") { $status = " Forwarded / Exceeds withdrawal amount limit"; }
else if($code=="E0807") { $status = "PG Fwd Fail / Issuer Authentication Server failure"; }
else if($code=="E0808") { $status = "Session expiry / Failed Initiate Check, Card BIN not present"; }
else if($code=="E0809") { $status = "Reversed / Expired Card"; }
else if($code=="E0810") { $status = "Unable to Authorize"; }
else if($code=="E0811") { $status = "Invalid Response Code or Guide received from Issue"; }
else if($code=="E0812") { $status = "Do not honor"; }
else if($code=="E0813") { $status = "Invalid transaction"; }
else if($code=="E0814") { $status = "Not Matched with the entered amount"; }
else if($code=="E0815") { $status = "Not sufficient funds"; }
else if($code=="E0816") { $status = "No Match with the card number"; }
else if($code=="E0817") { $status = "General Error"; }
else if($code=="E0818") { $status = "Suspected fraud"; }
else if($code=="E0819") { $status = "User Inactive"; }
else if($code=="E0820") { $status = "ECI 1 and ECI6 Error for Debit Cards and Credit Cards"; }
else if($code=="E0821") { $status = "ECI 7 for Debit Cards and Credit Cards"; }
else if($code=="E0822") { $status = " System error. Could not process transaction"; }
else if($code=="E0823") { $status = "Invalid 3D Secure values"; }
else if($code=="E0824") { $status = "Bad Track Data"; }
else if($code=="E0825") { $status = " Transaction not permitted to cardholder"; }
else if($code=="E0826") { $status = "Rupay timeout from issuing bank"; }
else if($code=="E0827") { $status = "OCEAN for Debit Cards and Credit Cards"; }
else if($code=="E0828") { $status = " E-commerce decline"; }
else if($code=="E0829") { $status = "This transaction is already in process or already processed"; }
else if($code=="E0830") { $status = "Issuer or switch is inoperative"; }
else if($code=="E0831") { $status = "Exceeds withdrawal frequency limit"; }
else if($code=="E0832") { $status = "Restricted card"; }
else if($code=="E0833") { $status = "Lost card"; }
else if($code=="E0834") { $status = "Communication Error with NPCI"; }
else if($code=="E0835") { $status = "The order already exists in the database"; }
else if($code=="E0836") { $status = "General Error Rejected by NPCI"; }
else if($code=="E0837") { $status = "Invalid credit card number"; }
else if($code=="E0838") { $status = "Invalid amount"; }
else if($code=="E0839") { $status = "Duplicate Data Posted"; }
else if($code=="E0840") { $status = "Format error"; }
else if($code=="E0841") { $status = "SYSTEM ERROR"; }
else if($code=="E0842") { $status = "Invalid expiration date"; }
else if($code=="E0843") { $status = "Session expired for this transaction "; }
else if($code=="E0844") { $status = "FRAUD - Purchase limit exceeded "; }
else if($code=="E0845") { $status = "Verification decline"; }
else if($code=="E0846") { $status = "Compliance error code for issuer    "; }
else if($code=="E0847") { $status = "Caught ERROR of type:[ System.Xml.XmlException ] . strXML is not a valid XML string   Failed in Authorize - I"; }
else if($code=="E0848") { $status = "Incorrect personal identification number "; }
else if($code=="E0849") { $status = "Stolen card  "; }
else if($code=="E0850") { $status = "Transaction timed out, please retry  "; }
else if($code=="E0851") { $status = "Failed in Authorize - PE"; }
else if($code=="E0852") { $status = "Cardholder did not return from Rupay"; }
else if($code=="E0853") { $status = "Missing Mandatory Field(s)The field card_number has exceeded the maximum length of 19"; }
else if($code=="E0854") { $status = "Exception in CheckEnrollmentStatus: Data at the root level is invalid. Line 1, position 1."; }
else if($code=="E0855") { $status = "CAF status = 0 or 9"; }
else if($code=="E0856") { $status = "412"; }
else if($code=="E0857") { $status = "Allowable number of PIN tries exceeded  "; }
else if($code=="E0858") { $status = "No such issuer "; }
else if($code=="E0859") { $status = "Invalid Data Posted"; }
else if($code=="E0860") { $status = "PREVIOUSLY AUTHORIZED"; }
else if($code=="E0861") { $status = "Cardholder did not return from ACS"; }
else if($code=="E0862") { $status = "Duplicate transmission"; }
else if($code=="E0863") { $status = "Wrong transaction state"; }
else if($code=="E0864") { $status = "Card acceptor contact acquirer"; }
try
 {
    $conn->query("START TRANSACTION"); 

         
?>

   <script src="../assets/sweetalert.min.js"></script>

    <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
    <title> Shri Jirawala Parshwanath Jain Tirth </title>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">


#sidebar{
display: none; 
}

.main-content{
width: 350mm !important;
padding: 0px !important;
margin: auto !important;
background: white !important;
}
#navbar{
display: none !important;
visibility: hidden !important;
}

@media print {
  a[href]:after {
    content: none !important;
  }
}
</style>
<style type="text/css">
.inpsty{
  max-width: 60px !important; 

 }  
 .table>tbody>tr>td {
  vertical-align: middle !important;
  padding-bottom: 6px !important;
 }

@media print
   {
    .inpsty{
    border: none !important;
    } 
    
 
   }

 .table>thead>tr>th, .table-bordered>thead>tr>th,  
 .table-bordered>tbody>tr>td{
  border-color: #000;
 }
</style>
</head>
<body>


<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>


<div class="main-content">
<div class="main-content-inner">


<div class="page-content">

<style type="text/css">
.form-group{
margin-bottom: 10px;
}
label{
color: #000 !important;
}
@media print
   {
      .hideborder {
        border: 1px solid #e5e5e5;
      }
   }
</style>

 
          <div class="row">
             <div class="col-xs-12">
                <div class="panel panel-default" style="border-color: #000 !important;">
                     <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important;">
                            <div class="row">

                              <div class="col-xs-12" style=" ">
                              <center><img src="../images/jp.jpg" width="500px" >
                               <hr style="margin: 5px !important; padding-bottom: 10px !important;"> </center> 
                              </div> 
<div class="col-xs-12" style="padding-bottom: 10px !important;">
 
<div class="col-xs-6" >
<div class="form-group" style="margin-bottom: 5px !important;">
 <label style="font-size: 12px !important;"> PAYMENT CODE: <?php echo $sys;  ?> </label>



</div>

    </div>  


<div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;">PAYMENT STATUS: <?php echo ucwords(strtolower($status));  ?></label>
    &nbsp;&nbsp;&nbsp;
    </div>
</div>

</div>
 <div class="col-xs-12">
  <table class="table table-bordered" id="tblCustomers">
   <tr align="center">
    <th style="font-size: 14px; text-align: center; vertical-align: middle;">Book Id</th>
       <th style="font-size: 14px; text-align: center; vertical-align: middle;">Name </th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Address </th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Room No/type</th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Total Amount  </th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Date & Time</th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Transaction Id</th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle; width: 100px;">CheckIn  </th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle; width: 100px;">CheckOut  </th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Response</th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;">Payment Mode</th>
       <th  style="font-size: 14px; text-align: center; vertical-align: middle;"> Voucher</th>
  
        </tr>

        <?php
if($sys=="Not_paid")
{

    $sql="SELECT * from bookonline where  booking_pay='$sys' and mysqlin between '$starttime' and '$endtime'";
    $res=$conn->query($sql);
    if($res===FALSE)
    {
    throw new Exception("Code 001 : ".mysqli_error($conn));   
    }


    $sql="SELECT SUM(remaing) as total from bookonline where booking_pay='$sys' and mysqlin between '$starttime' and '$endtime'";

    $res1=$conn->query($sql);
    if($res1===FALSE)
    {
    throw new Exception("Code 002 : ".mysqli_error($conn));   
    }

while($row=mysqli_fetch_array($res))
            { 
?>
      <tr>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["bookid"];  ?></th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo ucwords(strtolower($row["name"]));  ?></th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo ucwords(strtolower($row["city"]));  ?></th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo ucwords(strtolower($row["rtype"]));  ?> ( <?php echo $row["noofroom"]; ?> )</th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["collection"];  ?></th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["Bookingtime"];  ?></th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["tr_id"];  ?> </th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;">0 </th>
      <th style="color: #444; vertical-align: middle; font-size: 13px;">0</th> 
      <th style="color: #444; vertical-align: middle; font-size: 13px;">0 </th> 
      <th style="color: #444; vertical-align: middle; font-size: 13px;">0</th> 
      <th style="color: #444; vertical-align: middle; font-size: 13px;">0</th>
      </tr>
   
   <?php } ?>
     </table>
 
    <div class="form-group" style="padding: 0px !important; ">
   
  <div class="col-xs-12" style="border: 1px solid #999; ">
  
  <div class="col-xs-4" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
 

   <label></label>
  &nbsp;                 
  </div>
  </div>
  <div class="col-xs-8" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
 <?php
while($row1=mysqli_fetch_array($res1))
            { 
 ?>
  <label class="pull-right">Total Deposit:&nbsp; ₹ <?php echo $row1["total"];  
   $total=$row1["total"];
    if($total>0){
   echo " / ".ucwords(strtolower(getIndianCurrency($total)));
      }
  ?> 
   </label>
  <?php
}
  ?>
<!--    <label class="pull-right"></label>
  &nbsp;  -->                
  </div>
  </div>
  
<!--   <div class="col-xs-8" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
  <label  >  </label>
  &nbsp;  
  </div>  
  </div>
 -->


<?php

}
else
{
$sql="SELECT * from bankpayment where response='$sys' and mysqldate between '$starttime' and '$endtime'";

 $res=$conn->query($sql);
    if($res===FALSE)
    {
    throw new Exception("Code 003 : ".mysqli_error($conn));   
    }

$sql="SELECT SUM(t_amt) as total from bankpayment where response='$sys' and mysqldate between '$starttime' and '$endtime'";
 $res1=$conn->query($sql);
    if($res1===FALSE)
    {
    throw new Exception("Code 004 : ".mysqli_error($conn));   
    }

while($row=mysqli_fetch_array($res))
            {
           $bookid=$row["bookind_id"];
           $booking_pay=$row["response"];

$sql="SELECT * from bookonline where bookid='$bookid' and booking_pay='$booking_pay'";
$bank=$conn->query($sql);
    if($bank===FALSE)
    {
    throw new Exception("Code 005 : ".mysqli_error($conn));   
    }

if($row1=mysqli_fetch_array($bank))
            {
?>
        <tr>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["bookind_id"];  ?></th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo ucwords(strtolower($row["name"]));  ?> </th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo ucwords(strtolower($row["address"]));  ?> </th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo ucwords(strtolower($row["room_no_type"]));  ?> </th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["t_amt"];  ?></th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["tr_date"];  ?></th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["tr_id"];  ?> </th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row1["arrive"];  ?></th>
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row1["depart"];  ?></th> 
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["response"];  ?> </th> 
        <th style="color: #444; vertical-align: middle; font-size: 13px;"><?php echo $row["payment"];  ?></th> 


<?php

$adv =$row["bookind_id"];

$sql="SELECT * from expense where d_b_id='$adv'";
$res2=$conn->query($sql);
if($res2===FALSE)
{
throw new Exception("Code 006 : ".mysqli_error($conn));   
}

if($row2=mysqli_fetch_array($res2))
{
?>
        <th style="color: #444; vertical-align: middle; font-size: 13px;" color="red"><p style="color:red;"> Already added </p></th>  
<?php
}
else
{
?>

        <th style="color: #444; vertical-align: middle; font-size: 13px;"><button data-toggle="modal" data-target="#view-modal" id="<?php echo $row["b_id"]; ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-edit"></i> Update</button></th>

<?php
}
?>
        </tr>
   
   <?php
}
}
   ?>
   
  </table>

  <div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    
    <div class="modal-dialog"> 
        <div class="modal-content"> 
                  
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h4 class="modal-title">
                        <i class="glyphicon glyphicon-list"></i> Update Voucher
                    </h4> 
            </div> 
                      
            <div class="modal-body">   
            <!-- all data will be show here -->                          
                <div id="show-data"></div>
            </div> 
                      
           <!--  <div class="modal-footer"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
            </div>  -->
                        
        </div> 
    </div>
  
</div>



  <div class="form-group" style="padding: 0px !important; ">
 
<div class="col-xs-12" style="border: 1px solid #999;">
  <div class="col-xs-4" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
 

   <label> </label>
  &nbsp;                 
  </div>
  </div>
  
  
  <div class="col-xs-8" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
  <?php
while($row1=mysqli_fetch_array($res1))
            { 
 ?>
  <label class="pull-right">Total Deposit:&nbsp; ₹ <?php echo $row1["total"];  
   $total=$row1["total"];
    if($total>0){
   echo " / ".ucwords(strtolower(getIndianCurrency($total)));
      }
  ?> 
   </label>

<?php } ?>
  <!-- <label class="pull-right"></label> -->
  <!-- &nbsp;                  -->
  </div>
  </div>

<!-- <div class="col-xs-8" style="padding-top: 8px;">
<div class="form-group" style="margin-bottom: 5px !important;">
<label  >  </label>
&nbsp;  
</div>  
</div> -->

</div>
 <!-- <div> &nbsp; </div> -->


</div>  </div>
    </div>
    
</div>
</div>


<?php
}

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}

 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 
$conn->close();

?>
 


</div><script type="text/javascript">
function myFunction() { 
window.print();
}
</script>
          </div>
          </div>
          </div>
          </div>

                          <center class="hidden-print">
      <a href="sjpjt_compus_adv.php" class="btn btn-app btn-danger">
      <i class="ace-icon fa fa-remove bigger-160"></i>
      Exit
      </a>
      &nbsp; &nbsp; 
      <button onclick="myFunction()" class="btn btn-app btn-primary">
      <i class="ace-icon fa fa-print bigger-160"></i>
      Print
      </button>
&nbsp; &nbsp;

      <button class="btn btn-app btn-success" id="btnExport">
      <i class="ace-icon fa fa-download bigger-160"></i>
      Export
      </button>

       <!-- <input type="submit" class="btn btn-app btn-success"   value="Export" /> -->

  </center>

<br>


 
          </div><!-- /.page-content -->
        </div>      




<!--   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
      </a>
    </div>





    


<center class="hidden-print">

<a href="ReceiptReprt.php" class="btn btn-app btn-danger  btn-xs">
<i class="ace-icon fa fa-remove bigger-160"></i>
Exit
</a>
 &nbsp; &nbsp; &nbsp; &nbsp;
<button onclick="myFunction()" class="btn btn-app btn-primary  btn-xs">
<i class="ace-icon fa fa-print bigger-160"></i>
Print
</button>
<input type="button" class="btn btn-app btn-success  btn-xs" id="btnExport" value="Export" />

</center> -->

</div>  
      
      
      
      
    
      
      
      
      
      
      
      
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="table2excel.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "Transaction_Report.xls"
                });
            });
        });
    </script>
      
      
      
      
      
            
<script type="text/javascript">
    $(document).ready(function(){
       $('.btn-warning').click(function(){
           var id=$(this).attr("id");

           $.ajax({
              url:"Create_trn1.php",
              type:"post",
              data:"uid="+id,
              success:function(data){

                $("#show-data").html(data);
                 
              }
           });
       });
    });
</script> 
      
      <!-- /.main-content -->
 
    <!-- /.main-container -->

</body>
</html>