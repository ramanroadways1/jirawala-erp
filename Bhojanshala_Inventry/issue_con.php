
<?php
include "_session.php";



$starttime=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
$endtime=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));

$d1=date("Y-m-d", strtotime($starttime));
$d3=date("Y-m-d", strtotime($endtime));

try
 {
    $conn->query("START TRANSACTION"); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title><?php  echo $starttime;  ?> to <?php echo $endtime;  ?> Daily Issue Consumption  Report </title>

<!-- <title>Shri Jirawala Parshwanath Jain Tirth </title> -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">

 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
    @media print {
    a[href]:after {
    content: none !important;
    }
    }
.table>thead>tr>th{
      border:1px solid black;
    padding: 4px !important;
    background-color:#D3D3D3;
}

.table>tbody>tr>td {
    padding: 4px !important;
}

 body{
    font-color: #000 !important;
 }
.row1, .row2, .row3, .row4, .row5, .row6, .row7 {
  font-size: 12px !important;
}

@media print{
 /* .dtrec {
    transform: scale(.5);
    
  }*/
   
  @page {margin:0; margin-top: 5px !important; }

    .table-bordered>tbody>tr>td, .table>thead>tr>th{
    border:1px solid #000 !important ;
   }
 
}
 
#sidebar{
display: none; 
}

.main-content2{
 
width: 240mm !important; 
padding: 0px !important;
margin: auto !important;
float: center; 
background: white !important; 

}

.main-content{
 
width: 145mm !important; 
padding: 0px !important;
margin: auto !important;
float: left; 
background: white !important; 

}
#navbar{
display: none !important;
visibility: hidden !important;
}

/*@media print {
  a[href]:after {
    content: none !important;
  }
}*/
</style>
<style type="text/css">
.form-group{
margin-bottom: 10px;
}
label{
color: #000 !important;
}
/*@media print
   {
      .hideborder { 
        border: 1px solid #e5e5e5;
      }
   }*/


   .row1, .row2, .row3, .row5 , .row6 , .row7 {
    border:1px solid #000  ;
   }

</style>

<body class="no-skin"  style="margin-top: 10px;">
<script type="text/javascript">        
function DisableBackButton() {
     window.history.forward()
 }
 DisableBackButton();
 window.onload = DisableBackButton;
 window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
 window.onunload = function () { void (0) }
</script>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

       <div class="main-content2">
    <div class="main-content-inner">

    <div class="page-content">

    <div class="dtrec">
    <div class="col-xs-12">
    <div class="panel panel-default" style="border-color: #000 !important; border-width: 2.5px !important;">
    <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important;  padding-bottom: 4px !important;">
    <div class="row">

    <div class="col-xs-12" style=" ">
    <center><img src="../images/jp.jpg"width="490px" height="90px" >
    <hr style="border-color: #000; margin: 5px !important; padding-bottom: 2px !important;"> </center> 
    </div> 
    <div class="col-xs-12" style="padding-bottom: 02px !important;">

    <div class="col-xs-6" >
    <div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">






    <label style="font-size: 12px !important;"> Daily Issue Consumption Report</label>
   
    </div>

    </div>
    <!-- /.col-lg-6 (nested) -->
    <div class="col-xs-6">
  
 <div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;">Date:</label> <?php  echo $starttime;  ?> to <?php  echo $endtime;  ?>
    </div>
</div>
    </div>

        <div class="col-xs-12">
      <table class="table" 
      style="margin-bottom: 0px !important; ">
      <thead style="border:1px solid black;">
      <tr style="border:1px solid black;">
      <th style="font-size: 12px; text-align: center; ">S. NO.  </th>
      <th style="font-size: 12px; text-align: center;  ">ITEM NAME </th>
      <th style="font-size: 12px; text-align: center;  ">QUANTITY  </th>
      <th style="font-size: 12px; text-align: center; "> AMOUNT  </th>
 <th style="font-size: 12px; text-align: center; ">TOTAL AMOUNT  </th> 
    
    
      </tr>
      </thead>
      
   
      
      
      
      
      
      
      
      <tbody style="border:1px solid black;" >
    <?php

    $sql="SELECT * from jirawala_store.bhojanshala_item_name";
    $res=$conn->query($sql);
    if($res===FALSE)
    {
    throw new Exception("Code 001 : ".mysqli_error($conn));   
    }

           while ($row=mysqli_fetch_array($res))
            { 

            $item_name=$row["item_name"];
              $sql="SELECT SUM(issue_qty) as k1, SUM(u_price) as k2, SUM(total) as k3 from jirawala_store.issue_deta where item_name='$item_name' and  mysqldate between '$d1' and '$d3'";
              $res2=$conn->query($sql);
              if($res2===FALSE)
              {
              throw new Exception("Code 002 : ".mysqli_error($conn));   
              }
              while ($row2=mysqli_fetch_array($res2))
            {
              $k1=(int)$row2["k1"];
               $k2=(int)$row2["k2"];
               $k3=(int)$row2["k3"];

             
              if($k1==0)
              { }
            else
            {

         $tpt=$k3/$k1;
        $tpt1=$k1*$tpt;


    ?>


    
      <tr>
      <td  class="row1"> <?php  echo $row["id"];  ?> </td>
      <td  class="row1"> <?php  echo $row["item_name"];  ?> </td>
      <td  class="row1"><?php  echo $k1;  ?> </td>
      <td  class="row1"> <?php  echo $tpt;  ?> </td>
      <td  class="row1"> <?php  echo $tpt1;  ?> </td>
      </tr>
      

<?php
 }
}}

  $sql="SELECT SUM(total) as total from jirawala_store.issue_deta where mysqldate between '$d1' and '$d3'";
  $res3=$conn->query($sql);
  if($res3===FALSE)
  {
  throw new Exception("Code 003 : ".mysqli_error($conn));   
  }

  $sql="SELECT SUM(salary) as salary from jirawala_store.add_member where mysql between '$d1' and '$d3'";
  $res4=$conn->query($sql);
  if($res4===FALSE)
  {
  throw new Exception("Code 004 : ".mysqli_error($conn));   
  }

  $sql="SELECT SUM(b1) as b1,SUM(b2) as b2,SUM(b3) as b3,SUM(b4) as b4,SUM(b5) as b5,SUM(b6) as b6,SUM(b7) as b7,SUM(k1) as b8,SUM(l1) as b9,SUM(l2) as b10,SUM(l3) as b11,SUM(l4) as b12,SUM(l5) as b13,SUM(l6) as b14,SUM(l7) as b15,SUM(k2) as b16,SUM(d1) as b17,SUM(d2) as b18,SUM(d3) as b19,SUM(d4) as b20,SUM(d5) as b21,SUM(d6) as b22,SUM(d7) as b23,SUM(k3) as b24,SUM(tot1) as b25,SUM(tot2) as b26,SUM(tot3) as b27,SUM(tot4) as b28,SUM(tot5) as b29,SUM(tot6) as b30,SUM(tot7) as b31,SUM(k4) as b32 from jirawala_store.add_member where mysql between '$d1' and '$d3'";

  $res5=$conn->query($sql);
  if($res5===FALSE)
  {
  throw new Exception("Code 005 : ".mysqli_error($conn));   
  }

 if($row3=mysqli_fetch_array($res3))
            { 
               if($row4=mysqli_fetch_array($res4))
            { 
               if($row5=mysqli_fetch_array($res5))
            { 

 $f_amount =(int)$row3["total"]+(int)$row4["salary"];

//    $f_amount = rst2.getFloat(1) + rst3.getFloat(1);
?>





       <tr>
    <td class="row1">  Amount </td>
     <td class="row1" colspan="3"> </td>
    <td class="row1"> <?php echo (int)$row3["total"]; ?></td>
    
    </tr>
    <tr>
       <td class="row1">Salary</td>
     <td class="row1" colspan="3"> </td>
 
     <td class="row1"><?php echo (int)$row4["salary"]; ?> </td>
     </tr>
     
     
     <tr>
       <td class="row1">Total Amount</td>
     <td class="row1" colspan="3"> </td>
 
     <td class="row1"> <?php echo $f_amount; ?></td>
     </tr>
     
     
     
     <tr>
     
     <td class="row1" colspan="5"> </td> 
     
     </tr>
     
     
     
        <tr>
                          <td class="row1" style="font-size: 12px; text-align: center;  ">विवरण </td>
                   <td class="row1" style="font-size: 12px; text-align: center;  " colspan="3">संख्या  </td>
                  <td  class="row1" style="font-size: 12px; text-align: center; ">कुल </td> 
        
        </tr>
        <tr>
        
             <td class="row1"></td>
                       <td class="row3"align="center" >नाश्ता</td>
                       <td class="row3"align="center" > दोपहर</td>
                       <td class="row3"align="center" >शाम</td>
                       <td class="row1"></td>
        </tr>
        
       
     <tr>   
  <td class="row1"> यात्री संख्या</td>
        <td class="row3" align="left"><?php echo (int)$row5["b1"]; ?> </td>
        <td class="row3" align="left"><?php echo (int)$row5["b9"]; ?> </td>
        <td class="row3" align="left"><?php echo (int)$row5["b17"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b25"]; ?></td>
   </tr>  <tr>
          <td class="row1">मेहमान संख्या</td>
           <td class="row3" align="left"><?php echo (int)$row5["b2"]; ?> </td>
        <td class="row3" align="left"><?php echo (int)$row5["b10"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b18"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b26"]; ?></td>
     
    </tr> 

 <tr>
     <td class="row1">साधु  साध्वी जी</td>
      
        <td class="row3" align="left"><?php echo (int)$row5["b3"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b11"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b19"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b27"]; ?></td>


  </tr>



    <tr>
       <td class="row1">दीक्षार्थी</td>
             <td class="row3" align="left"><?php echo (int)$row5["b4"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b12"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b20"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b28"]; ?></td>
     
</tr>

<tr>
<td class="row1">आयम्बिल संख्या</td>
       <td class="row3" align="left"><?php echo (int)$row5["b5"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b13"]; ?> </td>
        <td class="row3" align="left"><?php echo (int)$row5["b21"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b29"]; ?></td>
     
</tr><tr>
       <td class="row1">स्टाफ संख्या</td>
              <td class="row3" align="left"><?php echo (int)$row5["b6"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b14"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b22"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b30"]; ?></td>
      </tr>  <tr>
        <td class="row1">सेवक</td>
          <td class="row3" align="left"><?php echo (int)$row5["b7"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b15"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b23"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b31"]; ?></td>
    </tr>  <tr>  
      <td class="row1">कुल संख्या</td>
        <td class="row3" align="left"><?php echo (int)$row5["b8"]; ?> </td>
        <td class="row3" align="left"><?php echo (int)$row5["b16"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b24"]; ?></td>
        <td class="row3" align="left"><?php echo (int)$row5["b32"]; ?></td>
     
     
   </tr> 


  <tr>
   <td class="row1">पड़तल</td>
  <?php
  $val=(int)$row5["b32"];
  if($val==0)
  {
    $total_amt=0;
  }
  else
  {
    $total_amt=$f_amount/(int)$row5["b32"];
  }



  ?>
   
   <td class="row3" colspan="3"></td>
   <td class="row1"><?php echo $total_amt; ?></td>

  
        
    </tr>  
     <?php
}}}
     ?>
     
     
      </tbody>
      </table>       
      </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div> 
   



<script type="text/javascript">
function myFunction() { 
window.print();
}
</script>

       <center class="hidden-print" style="margin-bottom: 10px;">
      <a href="bho_reports.php" class="btn btn-app btn-danger">
      <i class="ace-icon fa fa-remove bigger-160"></i>
      Exit
      </a>
      &nbsp; &nbsp; 
      <button onclick="myFunction()" class="btn btn-app btn-primary">
      <i class="ace-icon fa fa-print bigger-160"></i>
      Print
      </button>

    </center>
&nbsp; &nbsp;

      



 </div><!-- /.page-content -->
    </div>
    </div><!-- /.main-content --> 



<?php

 $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


             
               
 echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");  

} 



$conn->close();




?>




</body>
</html>
  