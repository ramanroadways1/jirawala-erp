 </div>
    <div class="sidebar-overlay" data-reff=""></div>
	<script type="text/javascript" src="../assets/js/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="../assets/js/slidemenu.html"></script>
    <script type="text/javascript" src="../assets/js/select2.min.js"></script>

    <script type="text/javascript" src="../assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/js/dataTables.bootstrap4.min.js"></script>
    
    <script type="text/javascript" src="../assets/js/moment.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="../assets/js/Chart.bundle.js"></script>
    <script type="text/javascript" src="../assets/js/app.js"></script>

</body> 
</html>