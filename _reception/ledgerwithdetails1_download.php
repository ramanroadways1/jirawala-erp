<?php
  require "_session.php";
 
$starttime=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
$endtime=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime1= date("Y-m-d", strtotime($starttime));
$endtime1= date("Y-m-d", strtotime($endtime));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
$title=$conn->real_escape_string(htmlspecialchars($_POST["title"]));
$valueofk=$conn->real_escape_string(htmlspecialchars($_POST["valueofk"]));


try
 {
    $conn->query("START TRANSACTION"); 

          $sql="";
            if($sys=="0")
            {
            $sql="SELECT d.rec_id,d.date,d.name,d.city,d.number,d.$valueofk, d.amount, d.Bookingid, b.Deposit FROM donation as d LEFT JOIN bookroom as b ON d.Bookingid = b.Bookid WHERE d.date BETWEEN '$starttime1' and '$endtime1' and status='0'and $valueofk!='' ORDER BY `d`.`rec_id` DESC";
            }
            else
            {
            $sql="SELECT d.rec_id,d.date,d.name,d.city,d.number,d.$valueofk, d.amount, d.Bookingid, b.Deposit FROM donation as d LEFT JOIN bookroom as b ON d.Bookingid = b.Bookid WHERE d.userid= '$sys' and d.date BETWEEN '$starttime1' and '$endtime1' and status='0'and $valueofk!='' ORDER BY `d`.`rec_id` DESC";
            }



$res=$conn->query($sql);
if($res===FALSE)
{
throw new Exception("Code 001 : ".mysqli_error($conn));   
}

$output="";
  // while($row=mysqli_fetch_array($res))                                  

if(mysqli_num_rows($res) == 0)
{
  echo "<script type='text/javascript'>
    alert('No result found !');
    window.location.href='sjpjt_receiptreprt.php';
    </script>";
    exit();
}
 $output .= '
   <table border="1">  

   <tr>

      <th style=" text-align: center;  color:#444;"> Donation Id </th>
      <th style=" text-align: center;  color:#444;"> Date </th>
      <th style=" text-align: center;  color:#444;">Name </th>
      <th style=" text-align: center;  color:#444;">City</th>
      <th style=" text-align: center;  color:#444;">Mobile No. </th>
      <th style=" text-align: center;  color:#444;">Particular Amount</th>
      <th style=" text-align: center;  color:#444;">Total Amount</th>
      <th style=" text-align: center;  color:#444;">Booking Id</th>
      <th style=" text-align: center;  color:#444;">Booking Amount</th>

  </tr>
  ';
  while($row = mysqli_fetch_array($res))
  {
    $date1=$row["date"];

    $date2=date("d-m-Y", strtotime($date1));
  
   $output .= '
      <tr> 
      <td>'.$row["rec_id"].'</td>
      <td>'.$date2.'</td> 
      <td>'.$row["name"].'</td>
      <td>'.$row["city"].'</td> 
      <td>'.$row["number"].'</td>
      <td>'.$row[$valueofk].'</td> 
      <td>'.$row["amount"].'</td> 
      <td>'.$row["Bookingid"].'</td>
      <td>'.$row["Deposit"].'</td>
      </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  $name = "ledger".$starttime."to".$endtime.".xls";
  header('Content-Disposition: attachment; filename='.$name.'');
  echo $output;
  exit();


   $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();

?>