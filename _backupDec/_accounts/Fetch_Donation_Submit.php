<?php

require "_session.php";

$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);


$name=$conn->real_escape_string(htmlspecialchars($_POST["username"]));
$expense=$conn->real_escape_string(htmlspecialchars($_POST["expense"]));
$narration=$conn->real_escape_string(htmlspecialchars($_POST["comment"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["inword"]));
$date=$conn->real_escape_string(htmlspecialchars($_POST["date"]));
$mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate"]));
$usname=$conn->real_escape_string(htmlspecialchars($_POST["usname"]));
$type=$conn->real_escape_string(htmlspecialchars($_POST["type"]));

$chequetype= "";//$conn->real_escape_string(htmlspecialchars($_POST["bank"]));
$cheque=""; //$conn->real_escape_string(htmlspecialchars($_POST["cheque"]));
$pan=""; //$conn->real_escape_string(htmlspecialchars($_POST["pan"]));
$achol="";//$conn->real_escape_string(htmlspecialchars($_POST["achol"]));
$ifsc="";//$conn->real_escape_string(htmlspecialchars($_POST["ifsc"]));
$accc="";//$conn->real_escape_string(htmlspecialchars($_POST["accc"]));

$bank_name=$conn->real_escape_string(htmlspecialchars($_POST["bank_name"]));


$vo="";
$des = "Expense of ".$expense ." Date :".$date;
$form = "expense_vouchar";



try {
    $conn->query("START TRANSACTION"); 
    $sql="SELECT COUNT(id) as total FROM expense";
     $res=$conn->query($sql);
      if($res===FALSE)
      {
      throw new Exception("Code 001 : ".mysqli_error($conn));   
      }
      while($row=mysqli_fetch_array($res))
      {
      $d_b_id=(int)$row["total"]+1;
    
 $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$usname','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque','$chequetype','$pan','$achol','$ifsc','$accc','0','0','0','$bank_name','Expense_voucher','0')";

      if($conn->query($sql) === FALSE) {
      throw new Exception("Code 002 : ".mysqli_error($conn));             
      } 
     

// echo "<script type=\"text/javascript\">
//          window.location = \"Expense_vocuher_slip.php?id=$d_b_id\";    
// </script>";


      }

       

    $conn->query("COMMIT");

        echo "<script type=\"text/javascript\">
    window.alert('Voucher updated successfully !');
    window.location = \"Expense_vocuher_slip2.php?id=$d_b_id\";    
    </script>";

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

                  echo "<script type=\"text/javascript\">
            window.alert('$content');
            window.location = \"acc_canara_voucher.php\";    
            </script>";
            // echo "
            // <script>
            // swal({
            // title: \"Error !\",
            // text: \"$content\",
            // icon: \"error\",
            // button: \"OK\",
            // });
            // </script>";    
} 


$conn->close();
?>