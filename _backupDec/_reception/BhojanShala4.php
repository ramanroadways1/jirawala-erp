<?php

require "_session.php";

$starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
$endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime= date("Y-m-d", strtotime($starttime1));
$endtime= date("Y-m-d", strtotime($endtime1));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));

$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

  <script src="../assets/sweetalert.min.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
  <title> Shri Jirawala Parshwanath Jain Tirth </title>

   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
#sidebar{
display: none; 
}

.main-content{


width: 220mm !important;

padding: 0px !important;
margin: auto !important;
background: white !important;

}
#navbar{
display: none !important;
visibility: hidden !important;
}

@media print {
  a[href]:after {
    content: none !important;
  }
}
</style>
<style type="text/css">
.inpsty{
  max-width: 60px !important; 

 }  
 .table>tbody>tr>td {
  vertical-align: middle !important;
  padding-bottom: 6px !important;
 }


@media print
   {
    .inpsty{
    border: none !important;
    } 
    

 
   }

 .table>thead>tr>th, .table-bordered>thead>tr>th,  
 .table-bordered>tbody>tr>td{
  border-color: #000;
 }
</style>
</head>
<body>
<script  >        
function DisableBackButton() {
     window.history.forward()
 }
 DisableBackButton();
 window.onload = DisableBackButton;
 window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
 window.onunload = function () { void (0) }
</script>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>


<div class="main-content">
<div class="main-content-inner">


<div class="page-content">

<style type="text/css">
.form-group{
margin-bottom: 10px;
}
label{
color: #000 !important;
}
@media print
   {
      .hideborder {
    
        border: 1px solid #e5e5e5;
      }
   }
</style>

 

          <div class="row">
             <div class="col-xs-12">
                <div class="panel panel-default" style="border-color: #000 !important;">
                     <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important;">
                            <div class="row">

                              <div class="col-xs-12" style=" ">
                              <center><img src="../images/jp.jpg" width="490px" height="90px" >
                               <hr style="margin: 5px !important; padding-bottom: 10px !important;"> </center> 
                              </div> 
<div class="col-xs-12" style="padding-bottom: 10px !important;">
 
<div class="col-xs-6" >
<div class="form-group" style="margin-bottom: 5px !important;">
 <label style="font-size: 12px !important;">FOOD REPORT</label>



</div>

    </div>  


<div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;">DATE:
    &nbsp;<?php  echo $starttime1; ?>&nbsp;to&nbsp;<?php  echo $endtime1; ?></label>
    </div>
</div>


 

</div>
 <div class="col-xs-12">
  <table class="table table-bordered">
   <tr>
    <th>S. No. </th>
    <th>Username </th>
       <th>Date & Time</th>
       <th>Food Type </th>
       <th>Total Member </th>
        <th>Amount</th>
    
		
	      </tr>

<?php   

$res=0;
$res1=0;
$sql="SELECT * from food WHERE Ftype= '$sys'  and sqldate between '$starttime' and '$endtime'";
$res=$conn->query($sql);
if($res===FALSE)
{
throw new Exception("Code 001 : ".mysqli_error($conn));   
}
                     

$sql="SELECT SUM(Amt) as amt, SUM(totalm) as totalm from food where Ftype= '$sys' and status='0' and sqldate between '$starttime' and '$endtime' ";
$res1=$conn->query($sql);
if($res1===FALSE)
{
throw new Exception("Code 002 : ".mysqli_error($conn));   
}
     



while($row=mysqli_fetch_array($res))
            {
        $status=$row["status"];

             if($status=='0')
             {
?>
<tr>

<?php
             }
             else
             {
?>


        <tr style="background-color: #ffecec;">
<?php
             }

              ?>

        
       
		<th  style="font-weight: normal; font-size: 14px;"><?php echo $row["id"];  ?></th>
	    <th  style="font-weight: normal; font-size: 14px;"><?php echo ucwords(strtolower($row["username"]));  ?></th>

		<th  style="font-weight: normal; font-size: 14px;">
		<?php echo $row["fdate"];  ?>&nbsp; (<?php echo $row["ftime"];  ?>)</th>
		<th  style="font-weight: normal; font-size: 14px;"><?php echo ucwords(strtolower($row["Ftype"]));  ?> </th>
		<th  style="font-weight: normal; font-size: 14px;"><?php echo $row["totalm"];  ?></th>
		<th  style="font-weight: normal; font-size: 14px;"><?php echo $row["Amt"];  ?></th>
	    
    </tr>
   
    <?php } ?>
	   </table>
	
    <div class="form-group" style="padding: 0px !important; ">
   <?php  

while($row1=mysqli_fetch_array($res1))
            {

?>
  <div class="col-xs-12" style="border: 1px solid #999;">
  <div class="col-xs-4" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
  <label>Total Deposit : &nbsp; ₹ <?php echo $row1["amt"];  ?></label>
  &nbsp;                 
  </div>
  </div>

  <div class="col-xs-8" style="padding-top: 8px;">
  <div class="form-group" style="margin-bottom: 5px !important;">
  <label  >Total Member:&nbsp; <?php echo $row1["totalm"];  ?> </label>
  &nbsp;  
  </div>  
  </div>
  <?php
}


    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 



$conn->close();


  ?>

  </table>


</div>
 <div> &nbsp; </div>


</div>  </div>
    </div>
</div><script type="text/javascript">
function myFunction() { 
window.print();
}
</script>
          </div>
          </div>
          </div>
          </div>

              <center class="hidden-print">
      <a href="sjpjt_receiptreprt.php" class="btn btn-app btn-danger">
      <i class="ace-icon fa fa-remove bigger-160"></i>
      Exit
      </a>
      &nbsp; &nbsp; 
      <button onclick="myFunction()" class="btn btn-app btn-primary">
      <i class="ace-icon fa fa-print bigger-160"></i>
      Print
      </button>
&nbsp; &nbsp;

      <button class="btn btn-app btn-success" id="btnExport">
      <i class="ace-icon fa fa-download bigger-160"></i>
      Export
      </button>

       <!-- <input type="submit" class="btn btn-app btn-success"   value="Export" /> -->

  </center>

 
					</div><!-- /.page-content -->
				</div>      



 



 
</div>  </div>
    </div>
</div>
          </div>
          </div>
			
			</div>
			
			</div>
			
			</div>
			
			

</body>
</html>