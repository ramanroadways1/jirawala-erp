<?php

require "_session.php";

$branch = "Booking"; 
$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
$lst=$conn->real_escape_string(htmlspecialchars($_POST["lst"]));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
$name=$conn->real_escape_string(htmlspecialchars($_POST["name"]));
$address=$conn->real_escape_string(htmlspecialchars($_POST["address"]));
$state=$conn->real_escape_string(htmlspecialchars($_POST["state"])); 
$Adv=$conn->real_escape_string(htmlspecialchars($_POST["adv"]));
$mnumber=$conn->real_escape_string(htmlspecialchars($_POST["mnumber"]));
$identity=$conn->real_escape_string(htmlspecialchars($_POST["identity"]));
$snumber=$conn->real_escape_string(htmlspecialchars($_POST["snumber"]));
$checkin=$conn->real_escape_string(htmlspecialchars($_POST["checkin"]));
$checkout=$conn->real_escape_string(htmlspecialchars($_POST["checkout"]));
$intime=$conn->real_escape_string(htmlspecialchars($_POST["intime"]));
$outtime=$conn->real_escape_string(htmlspecialchars($_POST["outtime"]));
$male=$conn->real_escape_string(htmlspecialchars($_POST["male"]));
$female=$conn->real_escape_string(htmlspecialchars($_POST["female"]));
$child=$conn->real_escape_string(htmlspecialchars($_POST["child"]));
$driver=$conn->real_escape_string(htmlspecialchars($_POST["driver"]));
$total=$conn->real_escape_string(htmlspecialchars($_POST["total"]));
$amount=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$By_book=$conn->real_escape_string(htmlspecialchars($_POST["mode"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["amtws"]));
$status=$conn->real_escape_string(htmlspecialchars($_POST["status"]));
$email21=$conn->real_escape_string(htmlspecialchars($_POST["email21"]));
$guestname=$conn->real_escape_string(htmlspecialchars($_POST["guestname"]));
$guesttype=$conn->real_escape_string(htmlspecialchars($_POST["guesttype"]));
$guestage=$conn->real_escape_string(htmlspecialchars($_POST["guestage"]));
$membernum=$conn->real_escape_string(htmlspecialchars($_POST["membernum"]));
$noofroom=$conn->real_escape_string(htmlspecialchars($_POST["noofroom"]));
$roomtype=$conn->real_escape_string(htmlspecialchars($_POST["roomtype"]));
$BookingTime=$conn->real_escape_string(htmlspecialchars($_POST["BookingTime"]));
@$bankname=$conn->real_escape_string(htmlspecialchars($_POST["bankname"]));
$vou_number=$conn->real_escape_string(htmlspecialchars($_POST["vou_number"]));
$apm=$conn->real_escape_string(htmlspecialchars($_POST["apm"]));

$allstatus = 1;
$checkinm = date("Y-m-d", strtotime($checkin));  
$checkoutm = date("Y-m-d", strtotime($checkout));
$d1=$checkinm;
$d2=$checkoutm;
$str1="";
$str2="";
$str3="";
$str4="0";

$id = 0;

try
 {
            $conn->query("START TRANSACTION"); 
 
        if(empty($_POST["multiselect"]) && (empty($_POST["multiselect1"])) && (empty($_POST["multiselect2"])))
            {
                throw new Exception("Please select at least single room");   
            }
 
            $sql="SELECT COUNT(Bookid) as totalid FROM bookroom";
            $res2=$conn->query($sql);
            if($res2===FALSE)
            {
                throw new Exception("Code 001: ".mysqli_error($conn));   
            }


            $fetch_old_id_maine_bookingid="";
            while($row=mysqli_fetch_array($res2))
            {
                $fetch_old_id_maine_bookingid=$row["totalid"];
            }

            if($status=="0")
             {

                    if(!empty($_POST["multiselect"]))
                    {
                        $firstempty=$_POST["multiselect"];
                        $str1=implode(",", $firstempty);
                        foreach ($_POST["multiselect"] as $room)
                        {
                            $sql="UPDATE bigdharmshala set status='1'  WHERE id=$room";
                            if($conn->query($sql) === FALSE) {
                                throw new Exception("Code 002 : ".mysqli_error($conn));             
                            }
                        }
                    }

                    if(!empty($_POST["multiselect1"]))
                    {
                        $firstempty2=$_POST["multiselect1"];
                        $str2=implode(",", $firstempty2);
                        foreach ($_POST["multiselect1"] as $room1)
                            {
                            $sql="UPDATE bigdharmshala set status_vid='3'  WHERE vid ='$room1'";
                            if($conn->query($sql) === FALSE) {
                                throw new Exception("Code 003 : ".mysqli_error($conn));             
                            }
                        }
                    }


                    if(!empty($_POST["multiselect2"]))
                    {
                        $firstempty3=$_POST["multiselect2"];
                        $str3=implode(",", $firstempty3);
                        foreach ($_POST["multiselect2"] as $room2)
                        {
                            $sql="UPDATE bigdharmshala set sstatus='5'  WHERE sid ='$room2'";
                            if($conn->query($sql) === FALSE) {
                                throw new Exception("Code 004 : ".mysqli_error($conn));             
                            }   
                        }
                    }

                    $id=(int)$fetch_old_id_maine_bookingid + 1;
                    $sql="SELECT * from overallcash where username='$lst' and Branch='$branch'";
                    $res3=$conn->query($sql);
                    if($res3===FALSE)
                    {
                        throw new Exception("Code 005 : ".mysqli_error($conn));   
                    }

                    $sql="SELECT * from balance where Branch='reception'";
                    $res4=$conn->query($sql);
                    if($res4===FALSE)
                    {
                        throw new Exception("Code 006 : ".mysqli_error($conn));   
                    }


                        while($row4=mysqli_fetch_array($res4))
                        {
                            $fetch_amount_balance_reception=$row4["Balance"];
                            $total_amount=$fetch_amount_balance_reception+$amount;

                            if($row3=mysqli_fetch_array($res3))
                            {
                                $amt=$amount;
                                $amt1=$row3['amount'];

                                $amt2 = (int)$amount+(int)$amt1;
                                $debit = "0";
                                $des = "Received room booking deposit and its deposit Receipt No. is ".$id;
                                $sql ="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$checkin','$d1','-','-','$debit','$amt','$total_amount','Room_Booking','$des','$id')";
                                if($conn->query($sql) === FALSE) {
                                    throw new Exception("Code 007 : ".mysqli_error($conn));             
                                }

                                $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
                                if($conn->query($sql) === FALSE) {
                                    throw new Exception("Code 008 : ".mysqli_error($conn));             
                                } 

                                $sql="UPDATE overallcash set amount='$amt2' WHERE Branch = 'Booking' and username='$lst'";
                                if($conn->query($sql) === FALSE) {
                                    throw new Exception("Code 009 : ".mysqli_error($conn));             
                                } 
                            } else {

                                $des = "Received room booking deposit and its deposit Receipt No. is ".$id;
                                $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$checkin','$d1','-','-','0','$amount','$total_amount','Room_Booking','$des','$id')";
                                if($conn->query($sql) === FALSE) {
                                    throw new Exception("Code 0010 : ".mysqli_error($conn));             
                                }   

                                $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
                                if($conn->query($sql) === FALSE) {
                                    throw new Exception("Code 0011 : ".mysqli_error($conn));             
                                }

                                $sql="insert into overallcash (username,Branch,Amount) values ('$lst','$branch','$amount')";
                                if($conn->query($sql) === FALSE) {
                                    throw new Exception("Code 0012 : ".mysqli_error($conn));             
                                } 

                             }



                        }


                    // $sql="insert into bookroom (Bookid,userid,mysqlin,mysqlout,Name,address,AdvanceBookingNumber,Mobilenumber,identity,snumber,checkindate,checkoutdate,intime,outtime,days,male,female,child,driver,total,BigDharmshala,VishistAtithiti,SmallDhrm,Food,Deposit,Depositinword,ramdom,Atatusid,email,noofroom,roomtype,guestname,guesttype,guestage,guestnumber,Bookingtime,allstatus) values ('$id','$sys','$d1','$d2','$name','$address','$Adv','$mnumber','$identity','$snumber','$checkin','$checkout','$intime','$outtime','$apm','$male','$female','$child','$driver','$total','$str1','$str2','$str3','$str4','$amount','$inword','$id','$status','$vou_number','$noofroom','$roomtype','$guestname','$guesttype','$guestage','$membernum','$BookingTime','$allstatus')";


 $sql="insert into bookroom (Bookid,userid,mysqlin,mysqlout,Name,address,state,AdvanceBookingNumber,Mobilenumber,identity,snumber,checkindate,checkoutdate,intime,outtime,days,male,female,child,driver,total,BigDharmshala,VishistAtithiti,SmallDhrm,Food,Deposit,Depositinword,ramdom,Atatusid,email,noofroom,roomtype,guestname,guesttype,guestage,guestnumber,Bookingtime,allstatus) values ('$id','$sys','$d1','$d2','$name','$address','$state','$Adv','$mnumber','$identity','$snumber','$checkin','$checkout','$intime','$outtime','$apm','$male','$female','$child','$driver','$total','$str1','$str2','$str3','$str4','$amount','$inword','$id','$status','$vou_number','$noofroom','$roomtype','$guestname','$guesttype','$guestage','$membernum','$BookingTime','$allstatus')";

                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0013: ".mysqli_error($conn)); 
                        // echo " <script type=\"text/javascript\">
                        // window.location = \"BookRoom.php?msg= data is not inserted\";
                        // </script>";

                    } else {

                        $sql="insert into status_checkout (Bookid,status,book_By,gave_key,recevie_key) values ('$id','1','$lst','0','0')";
                        if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0014: ".mysqli_error($conn));             
                        }

                        // echo " <script type=\"text/javascript\">
                        // window.location = \"BookSlip.php?id=".$id."\";
                        // </script>";
                    }

            }

            if($status=="1")
            {
                throw new Exception("Not in use - Error ! ");             
                // echo "<script type=\"text/javascript\">
                // window.location =\"BookRoom.php?msg='not used'\";
                // </script>";
            }

    if($status=="2")
    {
        //***********actully this code used when status is 1 but currently not used so i change status.***********
        if($vou_number=="")
        {
                throw new Exception("Fill voucher number while booking is online - Error ! ");             
        // echo "<script type=\"text/javascript\">
        //  window.location =\"BookRoom.php?msg='Fill_Voucher_Number_While_Booking_is_Online'\";
        //    </script>";

        } else {

            $id=$fetch_old_id_maine_bookingid + 1;
            $sql="SELECT * FROM expense where id ='$vou_number'";
            $res4=$conn->query($sql);
            if($res4===FALSE)
            {
                throw new Exception("Code 0015 : ".mysqli_error($conn));   
            }

            if($row4=mysqli_fetch_array($res4))
            {

            $voucher_narration=$row4["narration"];
            $update_narration = $voucher_narration." Room Deposit R. No. -> ".$id; 
            $expense_amount =$row4["amt"];
            $sql="SELECT * from overallcash where username='$lst' and Branch='Advance_Booking'";
            $res5=$conn->query($sql);
            if($res5===FALSE)
            {
                throw new Exception("Code 0016 : ".mysqli_error($conn));   
            }

            if($row5=mysqli_fetch_array($res5))
            {
                    $add =$row5["amount"];
                    $add_id = (int)$add+(int)$expense_amount;
                    $sql ="UPDATE overallcash set Amount='$add_id' WHERE username='$lst' and Branch='Advance_Booking'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0017 : ".mysqli_error($conn));             
                    }

                    if($expense_amount==$amount)
                    {
                    $sql ="UPDATE expense set narration='$update_narration',d_b_id='$id' where id ='$vou_number'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0018 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE repcash_chq set Des='$update_narration' where d_b_id ='$vou_number'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0019 : ".mysqli_error($conn));             
                    }
                    
            if(!empty($_POST["multiselect"]))
            {
                $firstempty=$_POST["multiselect"];
                $str1=implode(",", $firstempty);

                foreach ($_POST["multiselect"] as $room)
                {
                    $sql="UPDATE bigdharmshala set status='1'  WHERE id = '$room'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0020 : ".mysqli_error($conn));             
                    }
                }
            }
            if(!empty($_POST["multiselect1"]))
            {
                $firstempty2=$_POST["multiselect1"];
                $str2=implode(",", $firstempty2);

                foreach ($_POST["multiselect1"] as $room1)
                {
                    $sql="UPDATE bigdharmshala set status_vid='3'  WHERE vid ='$room1'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0021 : ".mysqli_error($conn));             
                    }
                }
            }
            if(!empty($_POST["multiselect2"]))
            {
                $firstempty3=$_POST["multiselect2"];
                $str3=implode(",", $firstempty3);

                foreach ($_POST["multiselect2"] as $room2)
                {
                    $sql="UPDATE bigdharmshala set sstatus='5'  WHERE sid ='$room2'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0022 : ".mysqli_error($conn));             
                    }   
                }
            }


                $sql="insert into bookroom (Bookid,userid,mysqlin,mysqlout,Name,address,AdvanceBookingNumber,Mobilenumber,identity,snumber,checkindate,checkoutdate,intime,outtime,days,male,female,child,driver,total,BigDharmshala,VishistAtithiti,SmallDhrm,Food,Deposit,Depositinword,ramdom,Atatusid,email,noofroom,roomtype,guestname,guesttype,guestage,guestnumber,Bookingtime,allstatus) values ('$id','$sys','$d1','$d2','$name','$address','$Adv','$mnumber','$identity','$snumber','$checkin','$checkout','$intime','$outtime','$apm','$male','$female','$child','$driver','$total','$str1','$str2','$str3','$str4','$amount','$inword','$id','$status','$vou_number','$noofroom','$roomtype','$guestname','$guesttype','$guestage','$membernum','$BookingTime','$allstatus')";

                if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0023 : ".mysqli_error($conn));  
                    // echo " <script type=\"text/javascript\">
                    // window.location = \"BookRoom.php?msg= data is not inserted\";
                    // </script>";
                }
                else
                {
                    $sql="insert into status_checkout (Bookid,status,book_By,gave_key,recevie_key) values ('$id','1','$lst','0','0')";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0024 : ".mysqli_error($conn));             
                    }
                    // echo "<script type=\"text/javascript\">
                    // window.location = \"BookSlip.php?id=$id\";
                    // </script>";
                    
                }
            }
            else
            {
                throw new Exception("Amount is greater then voucher amount - Error !");           
            // echo "<script type=\"text/javascript\">
            // window.location = \"BookRoom.php?msg=Amoount_is_greater_then_voucher_amount\";
            // </script>";
            }
        }
        else
        {
                $sql ="insert into overallcash (username,Branch,Amount) values ('$lst','Advance_Booking','$expense_amount')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0025 : ".mysqli_error($conn));             
                }   
                if($expense_amount==$amount)
                {
                    $sql ="UPDATE expense set narration='$update_narration',d_b_id='$id' where id ='$vou_number'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0026 : ".mysqli_error($conn));             
                    }
                    $sql="UPDATE repcash_chq set Des='$update_narration' where d_b_id ='$vou_number'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0027 : ".mysqli_error($conn));             
                    }

                    if(!empty($_POST["multiselect"]))
                    {

                        $firstempty=$_POST["multiselect"];
                        $str1=implode(",", $firstempty);

                        foreach ($_POST["multiselect"] as $room)
                        {
                            $sql="UPDATE bigdharmshala set status='1'  WHERE id = '$room'";
                            if($conn->query($sql) === FALSE) {
                                throw new Exception("Code 0028 : ".mysqli_error($conn));             
                            }
                        }
                    }

                    if(!empty($_POST["multiselect1"]))
                    {
                        $firstempty2=$_POST["multiselect1"];
                        $str2=implode(",", $firstempty2);
                        foreach ($_POST["multiselect1"] as $room1)
                        {
                            $sql="UPDATE bigdharmshala set status_vid='3'  WHERE vid ='$room1'";
                            if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0029 : ".mysqli_error($conn));             
                            }
                        }
                    }

                    if(!empty($_POST["multiselect2"]))
                    {
                        $firstempty3=$_POST["multiselect2"];
                        $str3=implode(",", $firstempty3);

                        foreach ($_POST["multiselect2"] as $room2)
                        {
                            $sql="UPDATE bigdharmshala set sstatus='5'  WHERE sid ='$room2'";
                            if($conn->query($sql) === FALSE) {
                                throw new Exception("Code 0030: ".mysqli_error($conn));             
                            }   
                        }
                    }


                    $sql="insert into bookroom (Bookid,userid,mysqlin,mysqlout,Name,address,AdvanceBookingNumber,Mobilenumber,identity,snumber,checkindate,checkoutdate,intime,outtime,days,male,female,child,driver,total,BigDharmshala,VishistAtithiti,SmallDhrm,Food,Deposit,Depositinword,ramdom,Atatusid,email,noofroom,roomtype,guestname,guesttype,guestage,guestnumber,Bookingtime,allstatus) values ('$id','$sys','$d1','$d2','$name','$address','$Adv','$mnumber','$identity','$snumber','$checkin','$checkout','$intime','$outtime','$apm','$male','$female','$child','$driver','$total','$str1','$str2','$str3','$str4','$amount','$inword','$id','$status','$vou_number','$noofroom','$roomtype','$guestname','$guesttype','$guestage','$membernum','$BookingTime','$allstatus')";

                    if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0031 : ".mysqli_error($conn));   
                            // echo "<script type=\"text/javascript\">
                            // window.location = \"BookRoom.php?msg= data is not inserted\";
                            // </script>";

                    }
                    else
                    {
                        // echo "<script type=\"text/javascript\">
                        // window.location = \"BookSlip.php?id=$id\";
                        // </script>";
                        $sql="insert into status_checkout (Bookid,status,book_By,gave_key,recevie_key) values ('$id','1','$lst','0','0')";
                        if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 0032 : ".mysqli_error($conn));             
                        }
                    }
                    }
                    else
                    {   

                        throw new Exception("Amount is greater then voucher amount - Error !");           

                        // echo "<script type=\"text/javascript\">
                        // window.location = \"BookRoom.php?msg=Amoount_is_greater_then_voucher_amount\";
                        // </script>";
                    }      
                }
            }
        }
    }

    $conn->query("COMMIT");
 
    echo ("<script LANGUAGE='JavaScript'>
    window.alert('Succesfully Updated');
    window.location.href = \"BookSlip.php?id=$id\";
    </script>");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
        if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
        } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo ("<script LANGUAGE='JavaScript'>
        window.alert('Error: $content');
        window.location.href = \"sjpjt_bookroom.php\";
        </script>");   
} 

$conn->close();

?>