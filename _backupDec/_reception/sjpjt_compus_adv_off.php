<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");


try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title"> Bhavan Room - OFFLINE Booking </h4>
	        </div>
	    </div>

  <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                         <!--    <h4 class="card-title">Two Column Horizontal Form</h4>
                            <h4 class="card-title">Personal Information</h4> -->
  						<form action="offline_book.php" method="POST" name="myform" onsubmit="return validateform()" autocomplete="off">
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-3 form-group">
                                            <label>Name of Party</label>
                                            <input type="text" class="form-control" id="fullname" name="fullname" required="required">
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>Party Address</label>
                                            <input type="text" class="form-control" id="address" name="address">
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <label>Party Mobile </label>
                                            <input type="text" class="form-control" onkeypress="return isNumber(event)" id="mnumber" size="10" maxlength="10" name="mnumber">
                                        </div>

                                       
                                         <div class="col-md-2 form-group">
                                            <label>Total Member</label>
                                            <input type="text" class="form-control" onkeypress="return isNumber(event)" id="" maxlength="3"  name="total"  size="3" required="required">
                                        </div>

                                       
                                       
                                        <!-- <div class="col-md-2 form-group">
                                            <label>Reference Name</label>
                                            <input type="text" class="form-control" id="" name="reference">


                                        </div> -->
                                         <div class="col-md-3 form-group">
                                            <label>Bhojanshala</label>
											<select class="select" name="bhojanshala" required="">
											<option value=""> -- Select -- </option>
											<option value="ALL"> BREAKFAST, LUNCH, DINNER </option>
											<option value="BREAKFAST_LUNCH"> BREAKFAST, LUNCH </option>
											<option value="LUNCH_DINNER"> LUNCH, DINNER </option>
											<option value="BREAKFAST_DINNER"> BREAKFAST, DINNER </option>
											<option value="BREAKFAST "> BREAKFAST </option>
											<option value="LUNCH">  LUNCH </option>
											<option value="DINNER">DINNER </option>
											<option value="NONE"> NONE </option>
											</select>
                                        </div> 


									

									

                                        <div class="col-md-2 form-group">
                                            <label>Booking Date</label>
                                            <input type="text" value="<?php echo $date; ?>" class="form-control" name="bdate" readonly="readonly">
                                        </div>


										<div class="col-md-2 form-group">
										<label style="">VIP Room</label>
										<input placeholder="Room" type="text" class="form-control"  onkeypress="return isNumber(event)" id="r1" maxlength="3"  name="vip"  size="3" onkeyup="addtotal1()" required="required" />
										</div>

										<div class="col-md-1 form-group">
										<label style="">AC Room</label>
										<input placeholder="Room" type="text" class="form-control" onkeypress="return isNumber(event)" id="r2" maxlength="3" name="ac" size="3" onkeyup="addtotal1()" required="required"   />
										</div>
										<div class="col-md-1 form-group">
										<label style="">Non-AC </label>
										<input placeholder="Room" type="text" class="form-control" onkeypress="return isNumber(event)" id="r3" maxlength="3" name="nac" size="3" onkeyup="addtotal1()" required="required"   />
										</div>
										<div class="col-md-1 form-group">
										<label style="">AC Hall</label>
										<input placeholder="Hall" type="text" class="form-control" onkeypress="return isNumber(event)" id="r4" maxlength="3" name="ach" size="3" onkeyup="addtotal1()" required="required"   />
										</div>
										<div class="col-md-1 form-group">
										<label style="">Non-AC </label>
										<input placeholder="Hall" type="text" class="form-control" id="r5" maxlength="" name="nah"  size="1" required="required"  onkeyup="addtotal1()"  />
										</div>
										<div class="col-md-1 form-group">
										<label style="">Total </label>
										<input type="text" class="form-control" id="r6" maxlength="" name="totalr"  size="1" readonly="readonly"/>
										</div>

 										

 
                                        <div class="col-md-2 form-group">
                                            <label>Days</label>
											<select class="select" name="days" required="">
												<option value=""> -- Select -- </option>
												<option value="One Day"> ONE DAY </option>
												<option value="Two Day"> TWO DAY </option>
											</select>
                                        </div> 


<!-- 
                                        <div class="col-md-3 form-group">
                                            <label>CHECK IN TIME</label>
                                            <div class="row">
                                            <div class="col-md-6">
                                            <input type="text" value="<?php echo $time; ?>" class="form-control" id="R" name="intime" readonly="readonly" >
                                        	</div>
                                        	<div class="col-md-6">
                                            <input type="text" value="<?php echo $time1; ?>" class="form-control" id="Rno" name="kkk" readonly="readonly">
                                        	</div>
                                        	</div>
                                        </div> -->



                                        <div class="col-md-2 form-group">
                                            <label>CHECK IN DATE</label>
                                            <input type="text" class="form-control" id="dt1" name="checkin" required="required">
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <label>CHECK IN TIME</label>
                                            <div class="row">
                                            <div class="col-md-6">
                                            	<select class="select" id="" name="intime" required="required">
													<option value="">-- Select --</option>
													<option value="01:00">01:00</option>
													<option value="02:00">02:00</option>
													<option value="03:00">03:00</option>
													<option value="04:00">04:00</option>
													<option value="05:00">05:00</option>
													<option value="06:00">06:00</option>
													<option value="07:00">07:00</option>
													<option value="08:00">08:00</option>
													<option value="09:00">09:00</option>
													<option value="10:00">10:00</option>
													<option value="11:00">11:00</option>
													<option value="12:00">12:00</option>
                                             	</select>
                                        	</div>
                                        	<div class="col-md-6">
                                            	<select class="select" id="" name="chp" required="required">
													<option value="">-- Select --</option>
													<option value="AM">AM</option>
													<option value="PM">PM</option>
                                             	</select>
                                        	</div>
                                        	</div>
                                        </div> 
 
                                        <div class="col-md-2 form-group">
                                            <label>CHECK OUT DATE</label>
                                            <input type="text" class="form-control" id="dt2" name="checkout" required="required">
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <label>CHECK OUT TIME</label>
                                            <div class="row">
                                            <div class="col-md-6">
                                            	<select class="select" id="" name="outtime" required="required">
													<option value="">-- Select --</option>
													<option value="01:00">01:00</option>
													<option value="02:00">02:00</option>
													<option value="03:00">03:00</option>
													<option value="04:00">04:00</option>
													<option value="05:00">05:00</option>
													<option value="06:00">06:00</option>
													<option value="07:00">07:00</option>
													<option value="08:00">08:00</option>
													<option value="09:00">09:00</option>
													<option value="10:00">10:00</option>
													<option value="11:00">11:00</option>
													<option value="12:00">12:00</option>
                                             	</select>
                                        	</div>
                                        	<div class="col-md-6">
                                            	<select class="select" id="" name="apm" required="required">
													<option value="">-- Select --</option>
													<option value="AM">AM</option>
													<option value="PM">PM</option>
                                             	</select>
                                        	</div>
                                        	</div>
                                        </div> 
                                        <div class="col-md-5">
									<div class="form-group">
									<label for="inputName">Trustee Name :</label>
									<Select  name="reference" class="form-control select" id="form-field-select-1"  required>
										<option value="">... Select ....</option>
									

                   <?php 

				$sql="SELECT * FROM `trustee_detail`";
				$res=$conn->query($sql);
				if($res===FALSE)
				{
				throw new Exception("Code 0010 : ".mysqli_error($conn));   
				}

				while($row1=mysqli_fetch_array($res))
				{
				?>
				<option value="<?php  echo $row1["id"]; ?>"><?php  echo $row1["fullName"]." (".$row1["mobile_no"].")"; ?> </option>
				<?php
				}

                   ?>

									</Select>                         
									</div>
									</div>
										<div class="col-md-7 form-group">
										    <label>Narration</label>
										    <input type="text" class="form-control" id="" name="nrr">
										</div>
										<div class="col-md-12 form-group text-right">
										    <label></label> <br>
										    <button type="submit" class="btn btn-primary"> SAVE & CONFIRM <i class="fa fa-check-circle" aria-hidden="true"></i> </button>
										</div>
                                    </div>
                                </div>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>

	    </div>
	</div>


<script type="text/javascript">

function addtotal1(){
	var vip = document.getElementById("r1").value;
	var ac = document.getElementById("r2").value;
	var nonac = document.getElementById("r3").value;
	var achall = document.getElementById("r4").value;
	var nonachall = document.getElementById("r5").value;
	document.getElementById("r6").value = Number(vip) +  Number(ac) +  Number(nonac) +  Number(achall) + Number(nonachall) ;
}
      

$(document).ready(function(){
	// ############ start ############
	$("#dt1").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt2');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 2);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 2){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt2').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############
});
	function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$('input[id$=checkout]').datepicker({ 
	minDate: '0D',
    maxDate: '+1D',
    dateFormat: 'dd-mm-yy'
});

$("input[id$=checkout]").keypress(function (evt) {
    evt.preventDefault();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   


  <script>
    function yesnoCheck(that) {
        if (that.value == "emp_name") { 
            document.getElementById("ifYes").style.display = "block";
            document.getElementById("q").required = true;
          
            
        } else {
            document.getElementById("ifYes").style.display = "none";
          document.getElementById("q").required = false;
          document.getElementById('q').value= "" ;
          
        }
        
        
        if (that.value == "emp_id") { 
            document.getElementById("ifYes2").style.display = "block";
            document.getElementById("e").required = true;
        
           
        } else {
            document.getElementById("ifYes2").style.display = "none";
          document.getElementById("e").required = false;
   
          document.getElementById('e').value= "" ;
          
        
        }   
    }
</script> 



<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>