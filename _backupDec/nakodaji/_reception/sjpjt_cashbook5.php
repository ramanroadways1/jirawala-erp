<?php
//done
    require "_session.php";
    require "_header.php";
 
	$name=$_SESSION["username"];
	date_default_timezone_set('Asia/Calcutta'); 
	$Check=date("d-m-Y");
	$check11=date("Y-m-d");

	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 
?>

<div class="page-wrapper">
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Reception Final Cash Collection {<?php echo $Check; ?>} </h4>
        </div>
    </div>
 
    <div class="row">

			<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
			<form class="" action="Foodcashupdate1.php" method="POST" >
            <div class="dash-widget clearfix card-box">

		<?php
			$sql=" SELECT * from balance where Branch='reception'";
			$your=$conn->query($sql);
			if($your===FALSE)
			{
			throw new Exception("Code 001 : ".mysqli_error($conn));   
			}

			while($row=mysqli_fetch_array($your))
			{
		?>
                <span class="dash-widget-icon"><i class="fa fa-rupee"></i></span>
                <div class="dash-widget-info">
                    <h3><?php echo  $row["Balance"];  ?></h3>
                    <span style="text-transform: lowercase;" >Notice :- After Click on Close day please Submit this Amount in main office.</span>
                </div>
      			<input type="hidden" value="<?php echo  $row["Balance"];  ?>" name="amt1">

		<?php

			}

			//$sql=" SELECT * from repcash where username='$name' and mysqldate='$check11' and formtheuser='day_closing' and formthe='day_closing'";

			$sql="SELECT * from repcash where mysqldate='2021-09-01' and formtheuser='day_closing' and formthe='day_closing'";
				$your1=$conn->query($sql);
				if($your1===FALSE)
				{
				throw new Exception("Code 002 : ".mysqli_error($conn));   
				}

			if($row=mysqli_fetch_array($your1))
			{
				$btnstat = "disabled";
			}
			else
			{ 
				$btnstat = "";
			}
		?>
                <div style="float: right;"> 
                	<button type="submit" style="color : #fff; " id="btnSubmit" name="btnSubmit" class="btn btn-warning btn-sm m-t-10" <?php echo $btnstat; ?> /> <i class='fa fa-check-circle'> </i> Close Day </button> 
                </div>
               
				<input type="hidden" name="usename" value="<?php echo $name; ?>">
				<input type="hidden" name="dep" value="Reception">
				<input type="hidden" name="debit" value="0">
				<input type="hidden" name="recdate" value="<?php echo $Check; ?>">
				<input type="hidden" name="mysqldate" value="<?php echo $check11; ?>">
				<input type="hidden" name="bhojanshala" value="Day_closing">
				<input type="hidden" name="vo" value="Day_closing">
				<input type="hidden" name="des" value="<?php echo $name; ?> close the day please collect this amount">  
            </div>
			</form>
        	</div>

    </div>
</div>
</div>

<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

	} catch(Exception $e) { 

                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }

                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 

$conn->close();

require "_footer.php"; ?>
