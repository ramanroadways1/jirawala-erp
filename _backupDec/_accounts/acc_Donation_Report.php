<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");


try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  text-transform: uppercase !important;
  }
</style>
			<div class="page-wrapper">
				<div class="content"> 



  					<div class="row">










				<!-- ################### ################### REPORT 1 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="ledgermain1.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Ledger Report</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt1" name="starttime1" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt2" name="endtime1" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Report :  </label>
									<select class="form-control select" name="sys" required=""> 
									<option value="0">ALL</option>

									<?php

											$sql="select * from admin group by username ";
									$rep1=$conn->query($sql);
									if($rep1===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
									while($row=mysqli_fetch_array($rep1))
									{
									?>
									<option value="<?php echo $row["id"]  ?>"><?php echo $row["username"]  ?></option> 
									<?php } ?>

									</select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

			<!-- ################### ################### REPORT 2 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="donationRecipt.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Receipt Report</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt3" name="starttime2" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt4" name="endtime2" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                   <select class="form-control select" name="sys" required=""> 
										<option value="0">ALL</option>

										<?php

											$sql="select * from admin group by username ";
										$rep1=$conn->query($sql);
										if($rep1===FALSE)
										{
										throw new Exception("Code 002 : ".mysqli_error($conn));   
										}
										while($row=mysqli_fetch_array($rep1))
										{
										?>
										<option value="<?php echo $row["id"]  ?>"><?php echo $row["username"]  ?></option> 
										<?php } ?>
										</select>
				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

			

			


<!-- ################### ################### REPORT 3 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="accountdonation.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Donation Register</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt5" name="starttime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt6" name="endtime3" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Report : </label>

									                    
								<select class="form-control select" name="sys" required=""> 
								<option value="0">ALL</option>

								<?php
											$sql="select * from admin group by username ";
								$rep1=$conn->query($sql);
								if($rep1===FALSE)
								{
								throw new Exception("Code 003 : ".mysqli_error($conn));   
								}
								while($row=mysqli_fetch_array($rep1))
								{
								?>
								<option value="<?php echo $row["id"]  ?>"><?php echo $row["username"]  ?></option> 
								<?php } ?>
								</select>
 

				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>



<!-- ################### ################### REPORT 4 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="Account_details_wise.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Account Wise</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt7" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt8" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Report : </label>
								<select class="form-control select" name="sys" required=""> 
								<option value="0">ALL</option>

								<option value="k1">श्री जिर्णोधार खाते</option> 
								<option value="k2">श्री शुभ साधारण खाते</option> 
								<option value="k3">श्री देवद्रव्य खाते </option> 
								<option value="k4">श्री शुभ साधारण </option> 
								<option value="k5">श्री भण्डार खाते</option> 
								<option value="k6">श्री धर्मशाला खाते</option> 
								<option value="k7">श्री धीरत चढ़ावा खाते</option> 
								<option value="k8">श्री भोजनशाला खाते</option> 
								<option value="k9">श्री देवद्रव्य साधारण खाते </option> 
								<option value="k10">श्री भाता खाते </option> 
								<option value="k11">श्री स्नात्र पूजा खाते</option> 
								<option value="k12">श्री आयंबिल खाते</option> 
								<option value="k13">श्री अखण्ड दीपक खाते </option> 
								<option value="k14">श्री गर्म पानी खाते</option> 
								<option value="k15">श्री पक्षाल खाते</option> 
								<option value="k16">श्री स्वामी वात्सल्य </option> 
								<option value="k17">श्री आंगी खाते</option> 
								<option value="k18">श्री तीर्थ विकास भक्ति स्थायी फण्ड</option> 
								<option value="k19">श्री पूजा खाते</option> 
								<option value="k20">श्री साधना संकुलन विकास स्थायी फण्ड</option> 
								<option value="k21">श्री केसर खाते </option> 
								<option value="k22">श्री देवद्रव्य जीणोद्धार स्थायी फण्ड</option> 
								<option value="k23">श्री बगीचा फूल खाते</option> 
								<option value="k24">श्री साधारण चढ़ावे खाते</option> 
								<option value="k25">वार्षिक अष्टप्रकारी पूजा  </option> 
								<option value="k26">श्री देवद्रव्य चढ़ावे  खाते</option> 
								<option value="k27">श्री वैयावच्छ खाते </option> 
								<option value="k28">सात   क्षेत्र</option> 
								<option value="k29">श्री ज्ञान खाते </option> 
								<option value="k30">अन्य</option> 
								<option value="k31">श्री जीवदया खाते</option> 

								</select>





				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>






                </div>

<div class="row">

				<!-- ################### ################### REPORT 5 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="BhojanShala3.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Bhojanshala Report</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt9" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt10" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Report</label>
				                    <select class="form-control select" name="sys" required=""> 
										<option value="0">ALL</option>

										<?php

											$sql="select * from admin group by username ";
										$rep1=$conn->query($sql);
										if($rep1===FALSE)
										{
										throw new Exception("Code 004 : ".mysqli_error($conn));   
										}

										while($row=mysqli_fetch_array($rep1))
										{
										?>
										<option value="<?php echo $row["id"]  ?>"><?php echo $row["username"]  ?></option> 
										<?php } ?>

										</select>

				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

			<!-- ################### ################### REPORT 6 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="BhojanShala4.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Bhojanshala Type</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt11" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt12" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Report : </label>
							<select class="form-control select" name="sys"> 
								<option value=""> -- select -- </option>
							<option value="Navkarshi">Navkarshi</option> 
							<option value="Lunch">Lunch</option> 
							<option value="Dinner">Dinner</option> 
							</select>
                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

			

			


<!-- ################### ################### REPORT 7 ################### ###################  -->
				<div class="col-md-3">
				<div class="card-box"> 
					<form action="Cash_cheque_deatils.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Donation Report <br> <sup>(Cash / Cheque)</sup> </h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>START DATE</label>
				                    <input type="text" class="form-control" id="dt13" name="starttime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>END DATE</label>
				                    <input type="text" class="form-control" id="dt14" name="endtime" required="required" placeholder="dd-mm-yyyy" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>TYPE</label>
				                    
									<select class="form-control select" name="sys"> 

									<option value="0">All</option> 
									<option value="CASH">CASH</option> 
									<option value="CHEQUE">CHEQUE</option> 
									</select>

				                </div>
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>



<!-- ################### ################### REPORT 8 ################### ###################  -->

<div class="col-md-3">
				<div class="card-box"> 
					<form action="sequance.php" method="POST" autocomplete="off">
				        <div class="row">
				            <div class="col-md-12">
				            <div class="">
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Sequence Report</h4>
				            	</div>
				                <div class="col-md-12 form-group">
				                    <label>Starting id:</label>
				                    <input type="text" class="form-control"  name="starttime" required="required" placeholder="Enter start id" >
				                </div>

				                <div class="col-md-12 form-group">
				                    <label>Ending id :</label>
				                    <input type="text" class="form-control"  name="endtime" required="required" placeholder="Enter end id" >
				                </div>

				               
				                	 
				            </div>
							<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> SEARCH  </button>
							</div>
				        </div>
				        </div> 
				    </form>
				</div>
				</div>

				


				

                </div>













	    	</div>
		</div>


<script type="text/javascript">

function addtotal(){
		var male = document.getElementById("i1").value;
		var femals = document.getElementById("i2").value;
		var child = document.getElementById("i3").value;
		var dri = document.getElementById("i4").value;
		
		document.getElementById("i5").value = Number(male) +  Number(femals) +  Number(child) +  Number(dri) ;
	}

$(document).ready(function () {

	// ############ start ############
	$("#dt1").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt2');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt2').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############
	
	// ############ start ############
	$("#dt3").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt4');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt4').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt5").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt6');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt6').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// ############ start ############
	$("#dt7").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt8');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt8').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	
	// 

	// ############ start ############
	$("#dt9").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt10');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt10').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


	// ############ start ############
	$("#dt11").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt12');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt12').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


	// ############ start ############
	$("#dt13").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt14');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt14').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


	// ############ start ############
	$("#dt15").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt16');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt16').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


	// ############ start ############
	$("#dt17").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt18');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt18').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	


	// ############ start ############
	$("#dt19").datepicker({
	    dateFormat: "dd-mm-yy",
	    onSelect: function () {
	        var dt2 = $('#dt20');
	        var startDate = $(this).datepicker('getDate');
	        var minDate = $(this).datepicker('getDate');
	        var dt2Date = dt2.datepicker('getDate');
	        var dateDiff = (dt2Date - minDate)/(86400 * 1000);
	        startDate.setDate(startDate.getDate() + 31);
	        if (dt2Date == null || dateDiff < 0) {
	            dt2.datepicker('setDate', minDate);
	        }
	        else if (dateDiff > 31){
	            dt2.datepicker('setDate', startDate);
	        }
	        dt2.datepicker('option', 'maxDate', startDate);
	        dt2.datepicker('option', 'minDate', minDate);
		}
	});

	$('#dt20').datepicker({
	    dateFormat: "dd-mm-yy"
	}); 
	// ############ end ############	










});

// $('input[id$=date1]').datepicker({ 
// 	// minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

// $('input[id$=date1]').datepicker({ 
// 	// minDate: '0D',
//  //    maxDate: '+1D',
//     dateFormat: 'dd-mm-yy'
// });

// $("input[id$=date1]").keypress(function (evt) {
//     evt.preventDefault();
// });

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function validateform(){  
//	var name=document.myform.name.value; 
	if( document.myform.fullname.value == "" || !isNaN( document.myform.fullname.value) )
			{
			alert("Full name is not valid !");
			document.myform.fullname.focus() ;

			return false;
			}
	if( document.myform.address.value == "" || !isNaN( document.myform.address.value) )
	{
	alert("Address is not valid !");
	document.myform.address.focus() ;

	return false;
	}

	if( document.myform.mnumber.value == "" ||
			isNaN( document.myform.mnumber.value) ||
			document.myform.mnumber.value.length != 10 )
			{
			alert("Mobile number is not valid !");
			document.myform.mnumber.focus() ;

			return false;
			}
	return true;
}

function validatedate(inputText)
  {
  	// alert(inputText);
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	  // Match the date format through regular expression
	  if(inputText.value.match(dateformat))
	  {
	  document.form1.text1.focus();
	  //Test which seperator is used '/' or '-'
	  var opera1 = inputText.value.split('/');
	  var opera2 = inputText.value.split('-');
	  lopera1 = opera1.length;
	  lopera2 = opera2.length;
	  // Extract the string into month, date and year
	  if (lopera1>1)
	  {
	  var pdate = inputText.value.split('/');
	  }
	  else if (lopera2>1)
	  {
	  var pdate = inputText.value.split('-');
	  }
	  var dd = parseInt(pdate[0]);
	  var mm  = parseInt(pdate[1]);
	  var yy = parseInt(pdate[2]);
	  // Create list of days of a month [assume there is no leap year by default]
	  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
	  if (mm==1 || mm>2)
	  {
	  if (dd>ListofDays[mm-1])
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  if (mm==2)
	  {
	  var lyear = false;
	  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
	  {
	  lyear = true;
	  }
	  if ((lyear==false) && (dd>=29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  if ((lyear==true) && (dd>29))
	  {
		  alert('Invalid date format!');
		  return false;
	  }
	  }
	  }
	  else
	  {
		  alert("Invalid date format !");
		  document.form1.text1.focus();
		  return false;
	  }
  }
</script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>