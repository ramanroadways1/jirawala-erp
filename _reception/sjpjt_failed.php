<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	$code = $sys=$conn->real_escape_string(htmlspecialchars($_POST["fail"]));

if($code=="E000") { $status = "Successful"; }
else if($code=="Not_paid") { $status = "Not Paid"; }
else if($code=="E0801") { $status = "Fail"; }
else if($code=="E0802") { $status = "User Dropped "; }
else if($code=="E0803") { $status = "Canceled by user"; }
else if($code=="E001") { $status = "Unauthorized Payment Mode"; }
else if($code=="E002") { $status = "Unauthorized Key"; }
else if($code=="E003") { $status = "Unauthorized Packet"; }
else if($code=="E004") { $status = "Unauthorized Merchant"; }
else if($code=="E005") { $status = "Unauthorized Return UR"; }
else if($code=="E006") { $status = "Transaction is already paid"; }
else if($code=="E007") { $status = "Transaction Failed"; }
else if($code=="E008") { $status = "Failure from Third Party due to Technical Error"; }
else if($code=="E009") { $status = "Bill Already Expired"; }
else if($code=="E0031") { $status = "Mandatory fields coming from merchant are empty"; }
else if($code=="E0032") { $status = "Mandatory fields coming from database are empty"; }
else if($code=="E0033") { $status = "Payment mode coming from merchant is empty"; }
else if($code=="E0034") { $status = "PG Reference number coming from merchant is empty"; }
else if($code=="E0035") { $status = "Sub merchant id coming from merchant is empty"; }
else if($code=="E0036") { $status = "Transaction amount coming from merchant is empty"; }
else if($code=="E0037") { $status = "Payment mode coming from merchant is other than 0 to 9"; }
else if($code=="E0038") { $status = "Transaction amount coming from merchant is more than 9 digit length"; }
else if($code=="E0039") { $status = "Mandatory value Email in wrong format"; }
else if($code=="E00310") { $status = "Mandatory value mobile number in wrong format"; }
else if($code=="E00311") { $status = "Mandatory value amount in wrong format"; }
else if($code=="E00312") { $status = "Mandatory value Pan card in wrong format"; }
else if($code=="E00313") { $status = "Mandatory value Date in wrong format"; }
else if($code=="E00314") { $status = "Mandatory value String in wrong format"; }
else if($code=="E00315") { $status = "Optional value Email in wrong format"; }
else if($code=="E00316") { $status = "Optional value mobile number in wrong format"; }
else if($code=="E00317") { $status = "Optional value amount in wrong format"; }
else if($code=="E00318") { $status = "Optional value pan card number in wrong format"; }
else if($code=="E00319") { $status = "Optional value date in wrong format"; }
else if($code=="E00320") { $status = "Optional value string in wrong format"; }
else if($code=="E00321") { $status = "Request packet mandatory columns is not equal to mandatory columns"; }
else if($code=="E00322") { $status = "Reference Number Blank"; }
else if($code=="E00323") { $status = "Mandatory Columns are Blank"; }
else if($code=="E00324") { $status = "Merchant Reference Number and Mandatory Columns are Blank"; }
else if($code=="E00325") { $status = "Merchant Reference Number Duplicate"; }
else if($code=="E00326") { $status = "Sub merchant id coming from merchant is non numeric"; }
else if($code=="E00327") { $status = "Cash Challan Generated"; }
else if($code=="E00328") { $status = "Cheque Challan Generated"; }
else if($code=="E00329") { $status = "NEFT Challan Generated"; }
else if($code=="E00330") { $status = "Transaction Amount and Mandatory Transaction Amount mismatch in Request URL"; }
else if($code=="E00331") { $status = "UPI Transaction Initiated Please Accept or Reject the Transaction"; }
else if($code=="E00332") { $status = "Challan Already Generated, Please re-initiate with unique reference number"; }
else if($code=="E00333") { $status = "Referer is null/invalid Referer "; }
else if($code=="E00334") { $status = "Mandatory Parameters Reference No and Request Reference No parameter values are not matched"; }
else if($code=="E00335") { $status = "Transaction Cancelled By User"; }
else if($code=="E0801") { $status = "FAIL"; }
else if($code=="E0802") { $status = "User Dropped "; }
else if($code=="E0803") { $status = "Canceled by user"; }
else if($code=="E0804") { $status = "User Request arrived but card brand not supported"; }
else if($code=="E0805") { $status = " Checkout page rendered Card function not supported"; }
else if($code=="E0806") { $status = " Forwarded / Exceeds withdrawal amount limit"; }
else if($code=="E0807") { $status = "PG Fwd Fail / Issuer Authentication Server failure"; }
else if($code=="E0808") { $status = "Session expiry / Failed Initiate Check, Card BIN not present"; }
else if($code=="E0809") { $status = "Reversed / Expired Card"; }
else if($code=="E0810") { $status = "Unable to Authorize"; }
else if($code=="E0811") { $status = "Invalid Response Code or Guide received from Issue"; }
else if($code=="E0812") { $status = "Do not honor"; }
else if($code=="E0813") { $status = "Invalid transaction"; }
else if($code=="E0814") { $status = "Not Matched with the entered amount"; }
else if($code=="E0815") { $status = "Not sufficient funds"; }
else if($code=="E0816") { $status = "No Match with the card number"; }
else if($code=="E0817") { $status = "General Error"; }
else if($code=="E0818") { $status = "Suspected fraud"; }
else if($code=="E0819") { $status = "User Inactive"; }
else if($code=="E0820") { $status = "ECI 1 and ECI6 Error for Debit Cards and Credit Cards"; }
else if($code=="E0821") { $status = "ECI 7 for Debit Cards and Credit Cards"; }
else if($code=="E0822") { $status = " System error. Could not process transaction"; }
else if($code=="E0823") { $status = "Invalid 3D Secure values"; }
else if($code=="E0824") { $status = "Bad Track Data"; }
else if($code=="E0825") { $status = " Transaction not permitted to cardholder"; }
else if($code=="E0826") { $status = "Rupay timeout from issuing bank"; }
else if($code=="E0827") { $status = "OCEAN for Debit Cards and Credit Cards"; }
else if($code=="E0828") { $status = " E-commerce decline"; }
else if($code=="E0829") { $status = "This transaction is already in process or already processed"; }
else if($code=="E0830") { $status = "Issuer or switch is inoperative"; }
else if($code=="E0831") { $status = "Exceeds withdrawal frequency limit"; }
else if($code=="E0832") { $status = "Restricted card"; }
else if($code=="E0833") { $status = "Lost card"; }
else if($code=="E0834") { $status = "Communication Error with NPCI"; }
else if($code=="E0835") { $status = "The order already exists in the database"; }
else if($code=="E0836") { $status = "General Error Rejected by NPCI"; }
else if($code=="E0837") { $status = "Invalid credit card number"; }
else if($code=="E0838") { $status = "Invalid amount"; }
else if($code=="E0839") { $status = "Duplicate Data Posted"; }
else if($code=="E0840") { $status = "Format error"; }
else if($code=="E0841") { $status = "SYSTEM ERROR"; }
else if($code=="E0842") { $status = "Invalid expiration date"; }
else if($code=="E0843") { $status = "Session expired for this transaction "; }
else if($code=="E0844") { $status = "FRAUD - Purchase limit exceeded "; }
else if($code=="E0845") { $status = "Verification decline"; }
else if($code=="E0846") { $status = "Compliance error code for issuer    "; }
else if($code=="E0847") { $status = "Caught ERROR of type:[ System.Xml.XmlException ] . strXML is not a valid XML string   Failed in Authorize - I"; }
else if($code=="E0848") { $status = "Incorrect personal identification number "; }
else if($code=="E0849") { $status = "Stolen card  "; }
else if($code=="E0850") { $status = "Transaction timed out, please retry  "; }
else if($code=="E0851") { $status = "Failed in Authorize - PE"; }
else if($code=="E0852") { $status = "Cardholder did not return from Rupay"; }
else if($code=="E0853") { $status = "Missing Mandatory Field(s)The field card_number has exceeded the maximum length of 19"; }
else if($code=="E0854") { $status = "Exception in CheckEnrollmentStatus: Data at the root level is invalid. Line 1, position 1."; }
else if($code=="E0855") { $status = "CAF status = 0 or 9"; }
else if($code=="E0856") { $status = "412"; }
else if($code=="E0857") { $status = "Allowable number of PIN tries exceeded  "; }
else if($code=="E0858") { $status = "No such issuer "; }
else if($code=="E0859") { $status = "Invalid Data Posted"; }
else if($code=="E0860") { $status = "PREVIOUSLY AUTHORIZED"; }
else if($code=="E0861") { $status = "Cardholder did not return from ACS"; }
else if($code=="E0862") { $status = "Duplicate transmission"; }
else if($code=="E0863") { $status = "Wrong transaction state"; }
else if($code=="E0864") { $status = "Card acceptor contact acquirer"; }
try
 {
    $conn->query("START TRANSACTION"); 
?>

<style>
tbody th
{
	color: #444;
}
	</style>

<div class="page-wrapper">
	<div class="content">
	    <div class="row" style="">
	        <div class="col-sm-12">
	            <h4 class="page-title" > <a onclick="window.history.go(-1)" class="btn btn-app btn-danger btn-sm ">
									<i style="color:#fff;" class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a> Payment Status: <b> <?php echo ucwords(strtolower($status)); ?> </b> <button type="button" class="btn btn-success btn-rounded"  id="btnExport"> <i class="fa fa-download" aria-hidden="true"></i></button> </h4>
	        </div>
	    </div>
  
		<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
								<thead>
									<tr>
										<th>Booking Id</th>
										<th>Name </th>
										<th>Address </th>
										<th>Room No/Type</th>
										<th>Total Amount  </th>
										<th>Date & Time</th>
										<th>Transaction Id</th>
										<th>Service Tax </th>
										<th>Processing Fee </th>
										<th>Sub Merchant Id</th>
										<th>Response  Code </th>
										<th>Payment   Mode</th>
									</tr>
								</thead>
								<tbody>
								<?php

								$total=0;
								if($sys=="Not_paid")
								{
									$sql="SELECT SUM(remaing) as total from bookonline where booking_pay='$sys'";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
										throw new Exception("Code 001 : ".mysqli_error($conn));   
									}

									while ($row=mysqli_fetch_array($res))
									{  
										$total=$row["total"];
									}

									$sql="SELECT * from bookonline where  booking_pay='$sys'";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
										throw new Exception("Code 002 : ".mysqli_error($conn));   
									}
									while ($row=mysqli_fetch_array($res))
									{       
									?>
									<tr>
										<th><?php echo $row["bookid"]; ?></th>
										<th><?php echo  ucwords(strtolower($row["name"])); ?></th>
										<th><?php echo  ucwords(strtolower($row["city"])); ?> </th>
										<th><?php echo $row["rtype"]; ?> (<?php echo $row["noofroom"]; ?>)</th>
										<th><?php echo $row["collection"]; ?></th>
										<th><?php echo $row["Bookingtime"]; ?></th>
										<th><?php echo $row["tr_id"]; ?></th>
										<th>0 </th>
										<th>0</th> 
										<th>0 </th> 
										<th>0</th> 
										<th>0</th>
									</tr>
									<?php 
									}

								} else {

								$sql="SELECT SUM(t_amt) as total from bankpayment where response='$sys'";
								$res=$conn->query($sql);
								if($res===FALSE)
								{
									throw new Exception("Code 003 : ".mysqli_error($conn));   
								}

								while ($row=mysqli_fetch_array($res))
								{  
									$total=$row["total"];
								}

								$sql="SELECT * from bankpayment where response='$sys'";
								$res=$conn->query($sql);
								if($res===FALSE)
								{
									throw new Exception("Code 004 : ".mysqli_error($conn));   
								}
								while ($row=mysqli_fetch_array($res))
									{       
									?>
									<tr>
										<th><?php echo $row["bookind_id"]; ?></th>
										<th><?php echo  ucwords(strtolower($row["name"])); ?></th>
										<th><?php echo  ucwords(strtolower($row["address"])); ?></th>
										<th><?php echo $row["room_no_type"]; ?></th>
										<th><?php echo $row["t_amt"]; ?></th>
										<th><?php echo $row["tr_date"]; ?></th>
										<th><?php echo $row["tr_id"]; ?></th>
										<th><?php echo $row["service_tax"]; ?></th>
										<th><?php echo $row["prr_fee"]; ?></th> 
										<th><?php echo $row["submerchantid"]; ?></th> 
										<th><?php echo $row["response"]; ?></th> 
										<th><?php echo $row["payment"]; ?></th> 
									</tr>
									<?php
									}
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
                </div>

	    </div>
	</div>
 
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Payment_Status.xls"
                });
            });
        });
    </script>
	   
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>