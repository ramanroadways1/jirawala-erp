
<?php

require "_session.php";
include "../_header.php";
include "left.php";

 if(!empty($_GET["msg"]))
 {
  if($_GET["msg"]=="Success")
 {
 echo "
    <script>
    swal({
    title: \"Good job!\",
    text: \"Data inserted successfully\",
    icon: \"success\",
    button: \"OK\",
    });
    </script>";
}
 }


try
 {
    $conn->query("START TRANSACTION"); 
        
                 
      date_default_timezone_set('Asia/Calcutta'); 
    $Check=date("d-m-Y");
    $Checkout=date("Y-m-d");
    $hh=date("h:i:A");
       
?>
        <div class="page-wrapper">
        <div class="content">
	   
<div class="row">
<div class="col-md-12">

    <div class="col-lg-12">
      
      <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                            <h3 class="panel-title">Party Master</h3>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-8" >
                       
  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a align="right" href="#myModal" class="btn btn-sm btn-primary" data-toggle="modal"> <i class="fa fa-plus"></i> ADD Party</a>
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-lg">
        <form  action="Add_Supplier_insert.php" method="post" autocomplete="off">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Supplier Details</h4>
                </div>
                <div class="modal-body">
                    
                       <div class="row">
                       
                         <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Party Name :</label>
                            <input type="text" value="" name="s_name" class="form-control" id="inputName" required="required">
                           
                        </div>
                        </div>
                        
                        
                        
                       <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Date & Time :</label>
                            <input type="text" value="<?php echo $Check; ?>(<?php echo $hh; ?>)" name="date" class="form-control" id="inputName" readonly="readonly">
                            <input type="hidden" value="<?php echo $Checkout; ?>" name="mysqldate" class="form-control" id="inputName" readonly="readonly">
                        
                        </div>
                        </div>
                        
                       <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Contact Number : </label>
                           <input type="text" value="" name="s_number" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        
                        
                           <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Address :</label>
                           <input type="text" value="" name="s_city" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        
                           <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Pin Code :</label>
                           <input type="text" value="" name="s_pincode" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        <!-- new added -->
                        
                             <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Pan Number :</label>
                           <input type="text" value="" name="pan_num" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                             <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">GSTIN Applicable :</label>
                             <Select name="gstin_app" class="form-control "> 
                           <option value="">---Select Option---</option>
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                      
                          </Select>
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                             <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">GSTIN Number :</label>
                           <input type="text" value="" name="gstin_num" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>
                        
                      
                     
                      
       <h4 class="col-md-12" align="center">Account Details</h4>


      <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Bank Name :</label>
                           <input type="text" value="" name="bank" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>   
                        
                        <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Branch Name :</label>
                           <input type="text" value="" name="branch" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>


                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Account Number :</label>
                           <input type="Number" value="" name="account_num" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>



         <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">Account Holder Name :</label>
                           <input type="text" value="" name="account_hold" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>



                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputName">IfSC Code :</label>
                           <input type="text" value="" name="IFSC" class="form-control" id="inputName" required="required">
                         
                          
                      
                          
                          </div>
                        </div>

<input type="hidden" name="username" value="<?php echo $username; ?>">



                       
                        
                        
                        
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary"> <i class="fa fa-check"></i> Save</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>                          
                             
                        </div>
                    </div>
                </div> 
                <div class="table-responsive">
                  <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                    <thead>
              <tr>
                <th>Id</th>
                <th>Date Time</th>
                <th>Supplier Name</th>
                <th>Contact</th>
                <th>City</th>
                <th>Pincode</th>      
              </tr>
               </thead>
             <?php
       $sql="SELECT * from jirawala_store.supplier_name_bhojanshala";
       $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001: ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            {

             ?>
              <tr>
              <td><?php  echo $row["id"]; ?></td>
              <td><?php  echo $row["date"]; ?></td>
              <td><?php  echo $row["supplier_name"]; ?></td>
              <td><?php  echo $row["contact"]; ?></td>
              <td><?php  echo $row["city"]; ?></td>
              <td><?php  echo $row["pincode"]; ?></td>
              </tr>
           <?php
        }
           ?>
           
                  </table>
                </div>
            </div>
        </div>
    </div>

</div>



	   



<script>
$(document).ready(function(){
    $("#myModal").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });
});
</script>
 
	   
	   <?php 
	    $conn->query("COMMIT");

   
}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }     

        
     echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");  


        
} 

$conn->close();



	  require "../_footer.php"; ?>
