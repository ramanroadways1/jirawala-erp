
<?php

  require "_session.php";
  include "../_header.php";
  include "left.php";
   
      date_default_timezone_set('Asia/Calcutta'); 
      $Check=date("d-m-Y");
      $Checkout=date("Y-m-d");
      $hh=date("h:i:A");  

   if(!empty($_GET["msg"]))
   {
      if($_GET["msg"]=="Success")
      {

      }
   }


try
 {
    $conn->query("START TRANSACTION"); 
        

?>

    <div class="page-wrapper">
    <div class="content">


    <div class="row">

    <div class="col-md-12">


    <div class="wrapper">

    <section>
    <div class="panel-body">
                  
        <form  action="add_ve_insert.php" method="post" autocomplete="off" >
            <div class="modal-content">
                <div class="modal-header">
                   
                    <h4 class="modal-title">Add New Vehicle</h4></div>
                <div class="modal-body">
                    
                       <div class="row" style="margin-bottom: 20px;">
                         
                 <div class="col-md-12">
                    <div class="row">    
              
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">REGISTRATION NO :</label>
                               <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="ve_reg" class="form-control" id="inputName" required="required">
                      </div>
                    </div>
                
                
                   <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName"> CATEGORY :</label>
                          <select name="ve_category" class="form-control " required="required">
                           <option value=""> -- select -- </option>
                          
                               <?php
              $sql="SELECT * from jirawala_store.asstes_category";
              $res=$conn->query($sql);
              if($res===FALSE)
              {
              throw new Exception("Code 001 : ".mysqli_error($conn));   
              }
              while ($row=mysqli_fetch_array($res))
              { 
              
               $ve_category=$row["ve_category"];
    if($ve_category!=" ")
   {
         ?>
<option value="<?php echo $ve_category;  ?>"><?php echo $ve_category;  ?></option>
                  <?php
   }




               
                 
               
              }

                        ?>

                    
                            
                         </select>        
                          
                          
                           </div>
                    </div>
                    
      
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName"> REGISTRATION DATE:</label>
                        <input type='text' name="ve_reg_date"  id='tbDate' placeholder='DD-MM-YYYY' autocomplete="off" class="form-control" required="required"/>    
              </div>
                    </div>
              
              
              <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">OWNER NAME :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="ve_ownwe" class="form-control" id="inputName" required="required">                        
                              </div>
                    </div>
               
               </div>
               
               
               
                 <!--  2nd line -->
               
            <input type="hidden" name="user12" value="<?php echo $username;   ?>">   
            <input type="hidden" name="mydate12" value="<?php echo $Check; ?>(<?php echo $hh; ?> )">   
            <input type="hidden" name="mysqldate12" value="<?php echo $Checkout; ?>">   
                  
               
               


               
               <div class="row">    
              <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">VEHICLE CLASS :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="ve_cls" class="form-control" id="inputName" required="required">                        
                            </div>
                </div>
                     <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">MAKER / MODEL NAME :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="ve_maker" class="form-control" id="inputName" required="required">                        
                            </div>
                </div>
                   <div class="col-md-3">
                     <div class="form-group">
                             <label for="inputName">FUEL TYPE :</label>
                            <Select name="ve_f_type" class="form-control " required="required">
              <option value=""> -- select -- </option>
              <option value="DIESEL">DIESEL</option>
              <option value="PETROL">PETROL </option>
              
              </Select>                             
                            </div>
                     </div>

 <div class="col-md-3">
   <div class="form-group">
                      <label for="inputName">CHASSIS NO :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="ve_chass" class="form-control" id="inputName" required="required">                        
                          
                   
                   </div>
 
 
                 </div>
                 </div>    
                 
                 <div class="row">                 
                       <div class="col-md-3">
                       
                         <label for="inputName">ENGINE NO :</label>
                            <input type="text" value="" pattern="^[a-zA-Z0-9 ]*$" name="ve_engine" class="form-control" id="inputName" required="required">                       
                   
                       
                       
                       </div>
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName"> ASSET AMOUNT :</label>
                          
                             <input type="text" pattern="[0-9]+"  onkeyup="calc1()" id="totalamt"name="ass_amt" class="form-control" required="required">                       
                        </div>
                    </div>
                    
                    
                           <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">GST TYPE :</label>
                             
  <select name="ass_gst" class="form-control " required="required">
  <option value=""> -- select -- </option>
  <option value="SGST">SGST</option>
  <option value="CGST">CGST</option>
  <option value="SGST_CGST">SGST_CGST</option>
  <option value="Other">Other</option>
  </select>    </div>
                    </div>
                       
                       
                       
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">GST %  :</label>
                           <input type="text" value="" pattern="[0-9]+" onkeyup="calc1()"  name="ass_perc" class="form-control" id="gstttt" required="required">                        
                        </div>
                    </div>
                       </div>
                 
                 
                     <div class="row">                 
               
               </div>  
              
               
               <!-- 3rd row -->
               <div class="row">    
              
                
                  
                    
                       <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName"> GST AMOUNT :</label>
                          
                             <input type="text"   name="ass_gstamot" class="form-control" id="gst" required="required" readonly="readonly">                       
                        </div>
                    </div>
                
                 <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">TOTAL AMOUNT  :</label>
                           <input type="text" value="" name="ass_tamt" class="form-control" id="to" readonly="readonly">                        
                      </div>
                    </div>
              
               
               
                <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">PARTY NAME :</label>
                          <select name="ass_party_name" class="form-control " required="required">
                           <option value=""> -- select -- </option>
                        
                           <?php
              $sql="SELECT * from jirawala_store.asstes_party WHERE department='VEHICLE-ASSTES'";
              $res=$conn->query($sql);
              if($res===FALSE)
              {
              throw new Exception("Code 002 : ".mysqli_error($conn));   
              }
              while ($row=mysqli_fetch_array($res))
              { 
                $supplier_name=$row["supplier_name"];
                
                  ?>
<option value="<?php echo $supplier_name;  ?>"><?php echo $supplier_name;  ?></option>
                  <?php
               
              }

                        ?>
                    
            
                          
                      </select>    
                          
                          
                                </div>
                                </div>
                                
                                
                                
                                
                                
                                
                                
                                
                                  <div class="col-md-3">
                          <div class="form-group">
                             <label for="inputName">MODE OF PAYMENT :</label>
                          <Select onchange="yesnoCheck(this);" class="form-control " id="form-field-select-1" name="ass_mode" required="required">
              <option value=""> -- select -- </option>
              <option value="CASH">CASH </option>
              <option value="CHEQUE">CHEQUE </option>
              <option value="NEFT">NEFT </option>
              </Select>
                    </div>
                           </div>
                              </div>
              
              <script>
              
    function yesnoCheck(that) {
        if (that.value == "CHEQUE") { 
            document.getElementById("ifYes").style.display = "block";
            document.getElementById("q").required = true;
            document.getElementById("w").required = true;
            
        } else {
            document.getElementById("ifYes").style.display = "none";
          document.getElementById("q").required = false;
          document.getElementById("w").required = false;
          document.getElementById('q').value= "" ;
          document.getElementById('w').value= "" ;
          
        }
        
        
        if (that.value == "NEFT") { 
            document.getElementById("ifYes2").style.display = "block";
            document.getElementById("e").required = true;
            document.getElementById("r").required = true;
            document.getElementById("t").required = true;
            document.getElementById("y").required = true;
            document.getElementById("u").required = true;
           
        } else {
            document.getElementById("ifYes2").style.display = "none";
          document.getElementById("e").required = false;
          document.getElementById("r").required = false;
          document.getElementById("t").required = false;
          document.getElementById("y").required = false;
          document.getElementById("u").required = false;
       
          document.getElementById('e').value= "" ;
          document.getElementById('r').value= "" ;
          document.getElementById('t').value= "" ;
          document.getElementById('y').value= "" ;
          document.getElementById('u').value= "" ;
       
        
        }


        if (that.value == "TRANSFER") { 
            document.getElementById("TRANSFER").style.display = "block";
            document.getElementById("Z").required = true;
                       
        } else {
            document.getElementById("TRANSFER").style.display = "none";
            document.getElementById("z").required = false;
          document.getElementById('Z').value= "" ;
             
        
        }        
    }


    
</script>
                   
                    
                    
                    <div class="row">

<div class="col-md-12">

<div id="ifYes" style="display: none;">
                    <div class="row">
<div class="col-md-3">
<label >Cheque Number : </label>
<input type="text" name="cheque" onkeypress="return isNumber(event)" id="q"  placeholder="Enter Cheque Number"  class="form-control" />
</div>

<div class="col-md-3">
<label >Bank Name : </label>
<input type="text" name="bank"  aria-describedby="name-format" id="w" placeholder="Enter Bank Name"   class="form-control"  />
</div>
</div>

</div>                      
</div>                      

               <!-- 4TH ROW -->
<div class="col-md-12">
                <div id="ifYes2" style="display: none;">
                    <div class="row">
<div class="col-md-3">
     <label >Pan Number : </label>
      <input type="text" name="pan" id="e"  placeholder="Enter Pan Card Number"  class="form-control" />
  </div><div class="col-md-3">
       <label >A/C Holder Name : </label>
      <input type="text" name="achol" id="r"   aria-describedby="name-format"  placeholder="A/C Holder Name"   class="form-control"  />
   </div><div class="col-md-3">
       <label >Account Number  : </label>
      <input type="text" name="accc"   id="t" onkeypress="return isNumber(event)"  placeholder="Account number"   class="form-control"  />
   </div><div class="col-md-3">
   
   
       <label >Bank Name : </label>
      <input type="text" name="bank11"  id="y" aria-describedby="name-format" placeholder="Enter Bank Name"   class="form-control" />
        </div>


      <div class="col-md-3 "  style="margin-top:20px;">
        <label >IFSC Code : </label>
      <input type="text" name="ifsc"   id="u" placeholder="IFSC Code"  class="form-control"  />
  
      </div>
          </div>
          </div>
          </div>
               
               
               
            
               
               
                    </div>
                    </div>
                 </div>
                
                
                 
                <div class="modal-footer">
                    <button style="color: #fff;" class="btn btn-warning btn-lg m-t-10"> <i class="fa fa-check"></i> SAVE</button>
                </div>
            </div>
        </form>
        </div>
                      
      
      
      
      
      
      
      
      
      
      
    </section>

  </div>
  </div>
  </div>
  
  
    <script>
        function calc1(){
             var textValue1 = Number(document.getElementById('totalamt').value);
             var textValue2 = Number(document.getElementById('gstttt').value);
            
             document.getElementById('gst').value = Number((textValue1 * textValue2) / 100); 

          var textvalue4=  Number(document.getElementById('gst').value);
             
              document.getElementById('to').value = Number(textvalue4 + textValue1); 
                 
                 //Number(textValue1 + textValue2); 
          }
        </script>
<script>
    $(document).ready(function () {
        $('input[id$=tbDate]').datepicker({
            dateFormat: 'dd-mm-yy'      // Date Format "dd/mm/yy"
        });
    });




    $(document).ready(function () {
        $('input[id$=tbDate2]').datepicker({
            dateFormat: 'dd-mm-yy'      // Date Format "dd/mm/yy"
        });
    });
</script>

	     <?php 
	    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


  echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");   
 
} 

$conn->close();



	   require "../_footer.php"; ?>

	 
