
<body>


    <div class="main-wrapper">
        <div class="header">

            <div class="header-left">
                <a href="index.php" class="logo">
                    <img src="../assets/logo.png" width="135" alt="">
                </a>
            </div>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars"></i></a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-danger float-right">0</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                               
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="#">View all Notifications</a>
                            
                        </div>
                    </div>
                </li>
                
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
							<img class="rounded-circle" src="../assets/img/user.jpg" width="24" alt="Admin">
							<span class="status online"></span>
						</span>
						<span><?php echo strtolower($_SESSION["username"]); ?> </span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">My Profile</a>
						<a class="dropdown-item" href="#">Settings</a>
						<a class="dropdown-item" href="../logout.php">Logout</a>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="../logout.php">Logout</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="">
                            <a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
						<li>
                            <a href="bho_Grn_Po.php"><i class="fa fa-building-o"></i>GRN + PO</a>
                        </li> 
                 
                        <li class="submenu">
                            <a href="#"><i class="fa fa-address-book-o"></i> <span>Add Master </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="bho_Add_Supplier.php">Party Master</a></li>
                                <li><a href="bho_Add_item.php">ITEM Master</a></li>
                               
                            </ul>
                        </li>

                        <li>
                            <a href="bho_issue_product.php"><i class="fa fa-calendar-check-o"></i>Issue Details</a>
                        </li> 
                        <li>
                            <a href="bho_REPORTS.php"><i class="fa fa-flag-o"></i>Reports</a>
                        </li> 
                        <li>
                            <a href="bho_add_member_staff.php"><i class="fa fa-user"></i>Add Member</a>
                        </li> 
                       
                        

                    </ul>
                </div>
            </div>
        </div>