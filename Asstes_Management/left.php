

<body>


    <div class="main-wrapper">
        <div class="header">



            <div class="header-left">
                <a href="index.php" class="logo">
                    <img src="../assets/logo.png" width="135" alt="">
                </a>
            </div>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars"></i></a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-danger float-right">0</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="#">View all Notifications</a>
                            <!-- activities.html -->
                        </div>
                    </div>
                </li>
               
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
							<img class="rounded-circle" src="../assets/img/user.jpg" width="24" alt="Admin">
							<span class="status online"></span>
						</span>
						<span><?php echo strtolower($_SESSION["username"]); ?> </span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">My Profile</a>
						<a class="dropdown-item" href="#">Settings</a>
						<a class="dropdown-item" href="../logout.php">Logout</a>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="../logout.php">Logout</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                                                <li class="menu-title">Inventory</li>

                         <li>
                            <a href="index.php"><i class="fa fa-dashboard"></i>DashBoard </a>
                        </li>
                        <li>
                            <a href="ass_issue_asstes.php"><i class="fa fa-desktop"></i>Issue Asset </a>
                        </li>

                         <li class="submenu">
                            <a href="#"><i class="fa fa-list"></i> Item Master 
                             <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="ass_Add_Employee.php">Employee</a></li>
                                <li><a href="ass_add_it_asstes.php">IT Asset</a></li> 
                                <li><a href="ass_add_ve_asstes.php">Vehicle</a></li> 
                                <li><a href="ass_add_Category.php">Category </a></li> 
                                <li><a href="ass_add_party.php">Party</a></li> 
                            </ul>
                        </li>

                         <li class="submenu">
                            <a href="#"><i class="fa fa-edit"></i> <span>Update Item </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="ass_Update_Add_Employee.php">Employee</a></li>
                                <li><a href="ass_update_add_it_asstes.php">IT Asset  </a></li> 
                                <li><a href="ass_update_add_ve_asstes.php">Vehicle </a></li> 
                                
                            </ul>
                        </li>

                         <li class="submenu">
                            <a href="#"><i class="fa fa-trash-o"></i> Remove Item <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="ass_lost_damage_it.php">Lost IT Asset</a></li>
                                <li><a href="ass_lost_damage_vec.php">Lost Vehicle Asset  </a></li> 
                                <li><a href="ass_resign_emp.php">Resign Employee </a></li> 
                                  <li><a href="ass_remove_emp_from_asstes.php">Detach Asset </a></li> 
                                
                            </ul>
                        </li>


                    

                    </ul>
                </div>
            </div>
        </div>