<?php
include "_session.php";


$id=$conn->real_escape_string(htmlspecialchars($_GET["id"]));

try
 {
    $conn->query("START TRANSACTION"); 

$Grn="";

           


?>


<!DOCTYPE html>
<html>
<head>
    <title>Convert HTML To PDF</title>
   <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/css/bootstrap.css'>
   
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
</head>


<body>
    <input type="button" id="create_pdf" value="Download PDf">
<input type="button" class="btn btn-app btn-success  btn-xs" id="btnExport" value="Export" />

    <form class="form" style="max-width: none; width: 1005px;">
<div class="container">
  <div class="card">
<div class="card-header">
<strong><img src="../images/final-06.png" alt="Girl in a jacket" width="300" height="100"></strong> 
  <span class="float-right"> <strong>Address :</strong> 
  <br>
  Post. Jirawala, Via Reodar,<br>
  Dist. Sirohi – 307514 (Rajasthan)<br>
  Mo. : 9413000351 / 8890878325<br>
  Email :sjpjtj@gmail.com
  
  
  </span>

</div>
<div class="card-body">


<div class="row mb-4">
<div class="col-sm-6">
<h6 class="mb-3">From:</h6>
<div>

    <?php
 $sql="SELECT * from jirawala_store.create_purcahse_order_bhojanshala where Po_id='$id'";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001: ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            { 
            $suppiler_name=$row["suppiler_name"];
          $sql="SELECT * from jirawala_store.supplier_name_bhojanshala where supplier_name='$suppiler_name'";
            $res1=$conn->query($sql);
            if($res1===FALSE)
            {
            throw new Exception("Code 002: ".mysqli_error($conn));   
            }
            while($row1=mysqli_fetch_array($res1))
            { 
           ?>

          
<strong>Supplier Name : </strong><?php echo $row["suppiler_name"];  ?></div>
<div><strong>Address : </strong><?php echo $row1["city"];  ?></div>
<div><strong>Phone No : </strong><?php echo $row1["contact"];  ?></div>

</div>
<div class="col-sm-1"></div>
<div class="col-sm-5">
<h6 class="mb-3">Bill Details:</h6>
<div><strong>P.O. ID : </strong><?php echo $id;  ?></div>
<div><strong>DATE : </strong><?php echo $row["dt"];  ?></div>


</div>

           <?php

            }
            }

    ?>






</div>

<div class="table-responsive-sm">
<table class="table table-striped" id="tblCustomers">
<thead>

<tr>
<th class="center">PO. No.</th>
<th>Item Name</th>

<th class="center">Qty</th>
<th class="center">Unit</th>
<th class="center">Received</th>
<th class="right">Per Pcs. Amt.</th>
  
<th class="right">Total</th>
</tr>


</thead>
<tbody>

<?php
$sql="SELECT * from jirawala_store.Bhojan_po_item_deatils_rec where po_id='$id'";
  $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 003: ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            {

?>

<tr>
<td class="center">*</td>
<td class="left strong"><?php echo $row["po_item_name"];  ?></td>
<td class="right"><?php echo $row["po_quantity"];  ?></td>
<th class="center"><?php echo $row["po_unit"];  ?></th>
<th class="center"><?php echo $row["receive"];  ?></th>
<th class="center"><?php echo $row["per_qty_amt"];  ?></th>
<th class="center"><?php echo $row["total_amt"];  ?></th>
</tr>
<?php } ?>
</tbody>
</table>
</div>
<div class="row">
<div class="col-lg-4 col-sm-5">

</div>

<div class="col-lg-4 col-sm-5 ml-auto">
<table class="table table-clear">
<tbody>
 <?php
 $sql="SELECT SUM(totalamt) as totalamt,SUM(gst_amt) as gst_amt,SUM(gst_interset) as gst_interset from jirawala_store.create_purcahse_order_bhojanshala where Po_id='$id'";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001: ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            { 
           
            $sql="SELECT SUM(total_amt) as total_amt from jirawala_store.Bhojan_po_item_deatils_rec where Po_id='$id'";
            $res1=$conn->query($sql);
            if($res1===FALSE)
            {
            throw new Exception("Code 002: ".mysqli_error($conn));   
            }
            while($row1=mysqli_fetch_array($res1))
            { 

                
                $a=(int)$row1["total_amt"]+(int)$row["gst_amt"]; 
           ?>


<tr>

<td class="left"><strong>Subtotal</strong></td>
<td class="right">
<?php echo $row1["total_amt"]; ?>
</td>
</tr>
<tr>
<td class="left">
<strong>Rate of % </strong>
</td>
<td class="right"><?php echo $row["gst_interset"]; ?></td>

</tr>
<tr>
<td class="left">
 <strong>GST Amount </strong>
</td>
<td class="right"><?php echo $row["gst_amt"]; ?></td>

</tr>
<tr>
<td class="left">
<strong>Total Amount</strong>
</td>
<td class="right">
<strong>
   <?php echo $a; ?>
 </strong>
</td>
</tr>

<?php  } } ?>
</tbody>
</table>

</div>

</div>

</div>
</div>
</div>


    </form>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="table2excel.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "OP.xls"
                });
            });
        });
    </script>

<script>
    (function () {
        var
         form = $('.form'),
         cache_width = form.width(),
         a4 = [595.28, 841.89]; // for a4 size paper width and height

        $('#create_pdf').on('click', function () {
            $('body').scrollTop(0);
            createPDF();
        });
        //create pdf
        function createPDF() {
            getCanvas().then(function (canvas) {
                var
                 img = canvas.toDataURL("image/png"),
                 doc = new jsPDF({
                     unit: 'px',
                     format: 'a4'
                 });
                doc.addImage(img, 'JPEG', 20, 20);
                doc.save('bhavdip-html-to-pdf.pdf');
                form.width(cache_width);
            });
        }

        // create canvas object
        function getCanvas() {
            form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
            return html2canvas(form, {
                imageTimeout: 2000,
                removeContainer: true
            });
        }

    }());
</script>

<script>
    /*
 * jQuery helper plugin for examples and tests
 */
    (function ($) {
        $.fn.html2canvas = function (options) {
            var date = new Date(),
            $message = null,
            timeoutTimer = false,
            timer = date.getTime();
            html2canvas.logging = options && options.logging;
            html2canvas.Preload(this[0], $.extend({
                complete: function (images) {
                    var queue = html2canvas.Parse(this[0], images, options),
                    $canvas = $(html2canvas.Renderer(queue, options)),
                    finishTime = new Date();

                    $canvas.css({ position: 'absolute', left: 0, top: 0 }).appendTo(document.body);
                    $canvas.siblings().toggle();

                    $(window).click(function () {
                        if (!$canvas.is(':visible')) {
                            $canvas.toggle().siblings().toggle();
                            throwMessage("Canvas Render visible");
                        } else {
                            $canvas.siblings().toggle();
                            $canvas.toggle();
                            throwMessage("Canvas Render hidden");
                        }
                    });
                    throwMessage('Screenshot created in ' + ((finishTime.getTime() - timer) / 1000) + " seconds<br />", 4000);
                }
            }, options));

            function throwMessage(msg, duration) {
                window.clearTimeout(timeoutTimer);
                timeoutTimer = window.setTimeout(function () {
                    $message.fadeOut(function () {
                        $message.remove();
                    });
                }, duration || 2000);
                if ($message)
                    $message.remove();
                $message = $('<div ></div>').html(msg).css({
                    margin: 0,
                    padding: 10,
                    background: "#000",
                    opacity: 0.7,
                    position: "fixed",
                    top: 10,
                    right: 10,
                    fontFamily: 'Tahoma',
                    color: '#fff',
                    fontSize: 12,
                    borderRadius: 12,
                    width: 'auto',
                    height: 'auto',
                    textAlign: 'center',
                    textDecoration: 'none'
                }).hide().fadeIn().appendTo('body');
            }
        };
    })(jQuery);

</script>


 <?php 
        $conn->query("COMMIT");

   

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


               

  echo "<script type=\"text/javascript\">
    window.location = \"../error.php?err=$content\";    
    </script>";
} 

$conn->close();

?>

</body>

</html>
