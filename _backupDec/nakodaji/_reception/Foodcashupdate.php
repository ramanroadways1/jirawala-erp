<?php
//done
require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

    $username=$conn->real_escape_string(htmlspecialchars($_POST["usename"]));
    $dep=$conn->real_escape_string(htmlspecialchars($_POST["dep"]));
    $recdate=$conn->real_escape_string(htmlspecialchars($_POST["recdate"]));
    $mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate"]));
    $formthe=$conn->real_escape_string(htmlspecialchars($_POST["bhojanshala"]));
    $debit="0";//$conn->real_escape_string(htmlspecialchars($_POST["debit"]));
    @$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
    $amt1=$conn->real_escape_string(htmlspecialchars($_POST["amt1"]));
    $vo=$conn->real_escape_string(htmlspecialchars($_POST["vo"]));
    $des=$conn->real_escape_string(htmlspecialchars($_POST["des"]));
    $lst=$conn->real_escape_string(htmlspecialchars($_POST["rec"]));
    $sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
    
    $amt="0";
    $over="0";
    $totalamt="0";
    $refundamt="0";
    $refundamt1="0";
    $adnvance_booking="0";
    $cancelamt="0";

    if(!empty($_POST["amt"]))
    {
    $amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
    }

    if(!empty($_POST["overall"]))
    {
    $over=$conn->real_escape_string(htmlspecialchars($_POST["overall"]));
    }
    if(!empty($_POST["totalamt"]))
    {
    $totalamt=$conn->real_escape_string(htmlspecialchars($_POST["totalamt"]));
    }
    if(!empty($_POST["refundamt"]))
    {
    $refundamt=$conn->real_escape_string(htmlspecialchars($_POST["refundamt"]));
    }
    if(!empty($_POST["refundamt1"]))
    {
    $refundamt1=$conn->real_escape_string(htmlspecialchars($_POST["refundamt1"]));
    }
    if(!empty($_POST["overall"]))
    {
    $adnvance_booking=$conn->real_escape_string(htmlspecialchars($_POST["overall"]));
    }
    if(!empty($_POST["cancelamt"]))
    {
    $cancelamt=$conn->real_escape_string(htmlspecialchars($_POST["cancelamt"]));
    } 
 
    $cash = "Deposit Confirmation of ".$sys." , Deposit Amount -> ".$totalamt."  , Refunded Amount from Reception -> ".$refundamt.", Cancel Amount -> ".$cancelamt;
    $cheque = "Deposit Confirmation of ".$sys." , Advance booking deposit Amount ->".$adnvance_booking." ,Refunded Amount from Bank ->".$refundamt1."  , Cancel Amount -> ".$cancelamt;

    $cash1 = "Donation Amount Confirmation of ".$sys." , Donation Amount -> ".$totalamt." , Cancel Amount -> ".$cancelamt;
 
try
 {
    $conn->query("START TRANSACTION"); 

    	if($formthe=="Booking_Rep")
        {

            $sql="SELECT * from repcash where username='$username' and mysqldate='$mysqldate' and formthe='$sys' and vou_type='Confirm_Booking_Deposit'";

            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
            }

            if($row=mysqli_fetch_array($res))
            {  
            
            echo ("<script LANGUAGE='JavaScript'>
            window.alert('Please Try After 24 Hours - Today's Confirmation Done');
            window.location.href='sjpjt_bookingcash.php';
            </script>");

            // echo "<script type=\"text/javascript\">
            //     window.location = \"Bookingcash.php?err='\";    
            //     </script>"; 
            }
            else
            {

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Booking'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 003 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Room_Booking_Desposit_Refunded_by_Bank'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 004 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Room_Booking_Desposit_Refunded'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 005 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Booking_Cancel'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 006 : ".mysqli_error($conn));             
                    }

                    $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$sys','Reception','$recdate','$mysqldate','-','-','0','0','0','Confirm_Booking_Status','$cash','0')";

                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 007 : ".mysqli_error($conn));             
                    }

                    $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$sys','Reception','$recdate','$mysqldate','-','-','0','0','0','Advance_Booking_Status','$cheque','0')";

                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 008 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Advance_Booking'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 009 : ".mysqli_error($conn));             
                    }

            echo ("<script LANGUAGE='JavaScript'>
            window.alert('Confirmed Successfully');
            window.location.href='sjpjt_bookingcash.php';
            </script>");

          // echo "<script type=\"text/javascript\">
          //       window.location = \"Bookingcash.php?err='Confirmation Successfully'\";    
          //       </script>"; 
            }
 
        }
        

        if($formthe=="Donation_Rep")
        {
            $b1="0";
            $b2="0";
             if(!empty($_POST["b1"]))
            {
            $b1=$conn->real_escape_string(htmlspecialchars($_POST["b1"]));
            }
            if(!empty($_POST["b2"]))
            {
            $b2=$conn->real_escape_string(htmlspecialchars($_POST["b2"]));
            }
           
            
            $cheque1 = "Donation Amount Confirmation of ".$sys." , Donation Amount -> ".$b1." , Cancel Amount -> ".$b2;

            $sql="SELECT * from repcash where username='$username' and mysqldate='$mysqldate' and formthe='$sys' and vou_type='Confirm_Donation_Amount'";

            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 0010 : ".mysqli_error($conn));   
            }

            if($row=mysqli_fetch_array($res))
            {  
            echo ("<script LANGUAGE='JavaScript'>
            window.alert('Please Try After 24 Hours - Today's Confirmation Done');
            window.location.href='sjpjt_donationcash.php';
            </script>");
            // echo "<script type=\"text/javascript\">
            //     window.location = \"Donationcash.php?err='Please Try After 24 Hours Today's Confirmation Done'\";    
            //     </script>"; 
            }
            else
            {

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Donation'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0011 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Donation_cheque'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0012 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Donation_Cancel'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0013 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set amount='0' WHERE username = '$sys' and Branch='Donation_cheque_cancel'";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0014 : ".mysqli_error($conn));             
                    }

                    $sql ="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$sys','Reception','$recdate','$mysqldate','-','-','0','0','0','Confirm_Donation_Status','$cash1','0')";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0015 : ".mysqli_error($conn));             
                    }

                    $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$sys','Reception','$recdate','$mysqldate','-','-','0','0','0','Bank_Donation_Status','$cheque1','0')";

                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0016 : ".mysqli_error($conn));             
                    }
    

                echo ("<script LANGUAGE='JavaScript'>
                window.alert('Confirmed Successfully');
                window.location.href='sjpjt_donationcash.php';
                </script>");

                // echo "<script type=\"text/javascript\">
                //             window.location = \"Donationcash.php?err='Confirmation Successfully\";    
                //             </script>"; 

            }

        }
        

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 
 
                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }

                echo ("<script LANGUAGE='JavaScript'>
                window.alert('Error: $content');
                </script>");

                // echo "
                // <script>
                // swal({
                // title: \"Error !\",
                // text: \"$content\",
                // icon: \"error\",
                // button: \"OK\",
                // });
                // </script>";    
} 
 
$conn->close();


?>