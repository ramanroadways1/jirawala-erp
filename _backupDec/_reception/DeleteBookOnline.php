<?php

require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$id=$conn->real_escape_string(htmlspecialchars($_POST["id"]));

try
{
    $conn->query("START TRANSACTION"); 

    $sql="UPDATE bookonline set refund='3', status2='1'  WHERE bookid =$id";
    if($conn->query($sql) === FALSE) {
        throw new Exception("Code 001 : ".mysqli_error($conn));             
    } 

    $conn->query("COMMIT");

    echo ("<script LANGUAGE='JavaScript'>
    window.alert('Booking Rejected Succesfully');
    window.location.href = \"sjpjt_onlinebookingreg.php\";
    </script>");

} catch(Exception $e) { 

        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
        if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo ("<script LANGUAGE='JavaScript'>
        window.alert('Error: $content');
        window.location.href = \"sjpjt_onlinebookingreg.php\";
        </script>");    
} 

$conn->close();

?>

