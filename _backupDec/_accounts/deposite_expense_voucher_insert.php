<?php

require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
$name=$conn->real_escape_string(htmlspecialchars($_POST["username"]));
$expense=$conn->real_escape_string(htmlspecialchars($_POST["expense"]));
$narration=$conn->real_escape_string(htmlspecialchars($_POST["comment"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["inword"]));
$date=$conn->real_escape_string(htmlspecialchars($_POST["date"]));
$mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate"]));
$type=$conn->real_escape_string(htmlspecialchars($_POST["type"]));
$cheque=$conn->real_escape_string(htmlspecialchars($_POST["cheque"]));
$chequetype=$conn->real_escape_string(htmlspecialchars($_POST["bank"]));
$nefttype=$conn->real_escape_string(htmlspecialchars($_POST["bank11"]));
$dep_type=$conn->real_escape_string(htmlspecialchars($_POST["dep_type"]));
$pan=$conn->real_escape_string(htmlspecialchars($_POST["pan"]));
$achol=$conn->real_escape_string(htmlspecialchars($_POST["achol"]));
$ifsc=$conn->real_escape_string(htmlspecialchars($_POST["ifsc"]));
$accc=$conn->real_escape_string(htmlspecialchars($_POST["accc"]));
$bank="";
if(!empty($_POST["bank"]))
  {
    $bank=$conn->real_escape_string(htmlspecialchars($_POST["bank"]));

  }
  if(!empty($_POST["bank11"]))
  {
    $bank=$conn->real_escape_string(htmlspecialchars($_POST["bank11"]));
    
  }

$vo="";

$des = "Room Booking Depsoit ".$expense." Date :".$date;
$form = "Room_Booking_deposit";
try {
    $conn->query("START TRANSACTION"); 
          
      $sql="SELECT COUNT(id) as count FROM expense";
      $res=$conn->query($sql);

      if($res===FALSE)
      {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
      }
      while($row=mysqli_fetch_array($res))
      {
      $d_b_id=(int)$row["count"]+1;
      $enter_amount =$amt;
      if($dep_type=="Room_Booking_deposit")
      {
        if($type=="CASH")
        {
            $sql="SELECT * from balance where Branch='pedi'";
            $res2=$conn->query($sql);

            if($res2===FALSE)
            {
              throw new Exception("Code 002: ".mysqli_error($conn));   
            }
            while($row2=mysqli_fetch_array($res2))
            {
              $pedi_balance=$row2["Balance"];
              $update_pedi_balance  = $pedi_balance + $enter_amount;
              $sql="UPDATE balance set Balance='$update_pedi_balance' WHERE Branch='pedi'";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 003 : ".mysqli_error($conn));             
              }

              $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$dep_type','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque',' ','$pan','$achol','$ifsc','$accc','0','0','0','0','Room_Booking_deposit','0')";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 004 : ".mysqli_error($conn));             
              }

              $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$name','main_office','$date','$mysqldate','','$expense','0','$enter_amount','$update_pedi_balance','$form','$narration','$d_b_id')";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 005 : ".mysqli_error($conn));             
              }
            }
        }
        else
        {
            $sql="SELECT * from balance where Branch='Bank'";
            $res2=$conn->query($sql);
            if($res2===FALSE)
            {
              throw new Exception("Code 006 : ".mysqli_error($conn));   
            }
            $sql="SELECT * from bank_name_with_amount where Bank_Name='$expense'";
            $res3=$conn->query($sql);
            if($res3===FALSE)
            {
              throw new Exception("Code 007: ".mysqli_error($conn));   
            }
            while($row2=mysqli_fetch_array($res2))
            {
            while($row3=mysqli_fetch_array($res3))
            {

            $fetchdata =$row3["Amount"]; /// Bank Amount
            $fetch_bank_data =$row2["Balance"];// total Bank Amount
            $update_totoal_bank_amount = $fetch_bank_data + $enter_amount; // update total bank amount
            $update_bank_amount = $fetchdata + $enter_amount;   // update particular bank Data
            $sql="UPDATE balance set Balance='$update_totoal_bank_amount' WHERE Branch='Bank'";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 008 : ".mysqli_error($conn));             
            }

            $sql="UPDATE bank_name_with_amount set Amount='$update_bank_amount' WHERE Bank_Name='$expense'";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 009 : ".mysqli_error($conn));             
            }

            $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$dep_type','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque','$bank','$pan','$achol','$ifsc','$accc','0','0','0','$expense','Room_Booking_deposit','0')";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 003 : ".mysqli_error($conn));             
            }

            $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$name','main_office','$date','$mysqldate','$expense','','0','$amt','$update_totoal_bank_amount','Room_Booking_deposit','$narration','$d_b_id')";

            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0010 : ".mysqli_error($conn));             
            }

            }
            }
        }
        // echo "<script type=\"text/javascript\">
        //  window.location = \"expense_Slip.php?id=$d_b_id\";    
        //    </script>";

      }

      if($dep_type=="Room_Booking_deposit_Refund")
      {
          if($type=="CASH")
          {
      
           $sql="SELECT * from balance where Branch='pedi'";
            $res2=$conn->query($sql);
            if($res2===FALSE)
            {
              throw new Exception("Code 0011 : ".mysqli_error($conn));   
            }
            while($row2=mysqli_fetch_array($res2))
            {
              $pedi_balance=$row2["Balance"];
              $update_pedi_balance  = $pedi_balance - $enter_amount;
              $sql="UPDATE balance set Balance='$update_pedi_balance' WHERE Branch='pedi'";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0012 : ".mysqli_error($conn));             
              }

              $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$dep_type','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque',' ','$pan','$achol','$ifsc','$accc','0','0','0','0','Room_Booking_deposit_Refund','0')";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0013 : ".mysqli_error($conn));             
              }

              $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$name','main_office','$date','$mysqldate','','$expense','$enter_amount','0','$update_pedi_balance','Room_Booking_deposit_Refund','$narration','$d_b_id')";

              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0014 : ".mysqli_error($conn));             
              }


            }
          }
          else
          {
            $sql="SELECT * from balance where Branch='Bank'";
            $res2=$conn->query($sql);
            if($res2===FALSE)
            {
              throw new Exception("Code 0015 : ".mysqli_error($conn));   
            }

            $sql="SELECT * from bank_name_with_amount where Bank_Name='$expense'";
            $res3=$conn->query($sql);
            if($res3===FALSE)
            {
              throw new Exception("Code 0016 : ".mysqli_error($conn));   
            }
            
            while($row2=mysqli_fetch_array($res2))
            {
            while($row3=mysqli_fetch_array($res3))
            {

            $fetchdata =$row3["Amount"]; /// Bank Amount
            $fetch_bank_data =$row2["Balance"];// total Bank Amount
            $update_totoal_bank_amount = $fetch_bank_data - $enter_amount; // update total bank amount
            $update_bank_amount = $fetchdata - $enter_amount;   // update particular bank Data
            $sql="UPDATE balance set Balance='$update_totoal_bank_amount' WHERE Branch='Bank'";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0017 : ".mysqli_error($conn));             
            }

            $sql="UPDATE bank_name_with_amount set Amount='$update_bank_amount' WHERE Bank_Name='$expense'";
            if($conn->query($sql) === FALSE) {
            throw new Exception("Code 0018 : ".mysqli_error($conn));             
            }

            $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$d_b_id','$name','$dep_type','$expense','$amt','$inword','$narration','$date','$mysqldate','$type','$cheque','$bank','$pan','$achol','$ifsc','$accc','0','0','0','$expense','Room_Booking_deposit_Refund','0')";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0019 : ".mysqli_error($conn));             
            }

            $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$name','main_office','$date','$mysqldate','$expense','','$amt','0','$update_totoal_bank_amount','Room_Booking_deposit_Refund','$narration','$d_b_id')";

            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0020 : ".mysqli_error($conn));             
            }

            }

            }

          }

         //   echo "<script type=\"text/javascript\">
         // window.location = \"expense_Slip.php?id=$d_b_id\";    
         //   </script>";
      }

 
      }


    $conn->query("COMMIT");
    
    echo "<script type=\"text/javascript\">
    window.alert('Voucher created successfully !');
    window.location = \"expense_Slip_deposit.php?id=$d_b_id\";    
    </script>";

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "<script type=\"text/javascript\">
            window.alert('$content');
            window.location = \"acc_deposite_voucher.php\";    
            </script>";

            // echo "
            // <script>
            // swal({
            // title: \"Error !\",
            // text: \"$content\",
            // icon: \"error\",
            // button: \"OK\",
            // });
            // </script>";    
} 

$conn->close();
?>