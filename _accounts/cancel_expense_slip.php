  <?php

  require "_session.php";

  $username1=$_SESSION["username"];
  $file_name = basename($_SERVER['PHP_SELF']);
  $id=$conn->real_escape_string(htmlspecialchars($_GET["id"]));
  date_default_timezone_set('Asia/Calcutta'); 
  $Check=date("d-m-Y");
  $Checkout=date("Y-m-d");

try {
    $conn->query("START TRANSACTION"); 


      $sql="SELECT * from expense where id = '$id'";
      $res=$conn->query($sql);
      if($res===FALSE)
      {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
      }
      if($row=mysqli_fetch_array($res))
      {  

        $type =$row["ptype"];
        $bank =$row["others1"];
        $amount =(int)$row["amt"];

        $des = "Cancel Expense voucher of ".$row["expense"]." id ".$id;    
        if($type=="CHEQUE")
        {

            $sql="SELECT * from balance where Branch='Bank'";
            $res1=$conn->query($sql);
            if($res1===FALSE)
            {
              throw new Exception("Code 002 : ".mysqli_error($conn));   
            }
            if($row1=mysqli_fetch_array($res1))
            {

            $sql="SELECT * from bank_name_with_amount where Bank_Name='$bank'";
            $res2=$conn->query($sql);
            if($res2===FALSE)
            {
              throw new Exception("Code 003 : ".mysqli_error($conn));   
            }

            if($row2=mysqli_fetch_array($res2))
            {
              $update_bank_amount =(int)$row1["Balance"]+ $amount;
              $update_bank_par =(int)$row2["Amount"]+ $amount;
              $expense=$row["expense"];
              $sql="SELECT * from balance where Branch='expense'";
              $res3=$conn->query($sql);
            if($res3===FALSE)
            {
              throw new Exception("Code 004 : ".mysqli_error($conn));   
            }
            if($row3=mysqli_fetch_array($res3))
            {
               
                $expense_update =(int)$row3["Balance"]-$amount;

                $sql="UPDATE balance set Balance='$expense_update' WHERE Branch='expense'";
                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 005 : ".mysqli_error($conn));             
                }
                        

                $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username1','main_office','$Check','$Checkout','$bank','$expense','$amount','0','$expense_update','Cancel_Expense_Voucher','$des','$id')";
                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 006 : ".mysqli_error($conn));             
                }
                        
            }

            $sql ="UPDATE balance set Balance='$update_bank_amount' WHERE Branch='Bank'";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 007 : ".mysqli_error($conn));             
            }
       
            $sql="UPDATE bank_name_with_amount set Amount='$update_bank_par' WHERE Bank_Name='$bank'";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 008 : ".mysqli_error($conn));             
            }
        
            $sql="UPDATE expense set status='1' WHERE id = '$id'";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 009 : ".mysqli_error($conn));             
            }
        
            $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username1','main_office','$Check','$Checkout','$bank','$expense','0','$amount','$update_bank_amount','Cancel_Expense_Voucher','$des','$id')";

            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0010 : ".mysqli_error($conn));             
            }
          }

          }

        }

        if($type=="NEFT")
        {

            $sql="SELECT * from balance where Branch='Bank'";
            $res1=$conn->query($sql);
            if($res1===FALSE)
            {
              throw new Exception("Code 0011 : ".mysqli_error($conn));   
            }

            if($row1=mysqli_fetch_array($res1))
            {

              $sql="SELECT * from bank_name_with_amount where Bank_Name='$bank'";
              $res2=$conn->query($sql);
              if($res2===FALSE)
              {
                throw new Exception("Code 0012 : ".mysqli_error($conn));   
              }
            if($row2=mysqli_fetch_array($res2))
            {
              $update_bank_amount =(int)$row1["Balance"]+ $amount;
              $update_bank_par =(int)$row2["Amount"]+ $amount;
              $expense=$row["expense"];

              $sql="SELECT * from balance where Branch='expense'";
              $res3=$conn->query($sql);
              if($res3===FALSE)
              {
                throw new Exception("Code 0013 : ".mysqli_error($conn));   
              }

            if($row3=mysqli_fetch_array($res3))
            {
               
              $expense_update =(int)$row3["Balance"]-$amount;

              $sql="UPDATE balance set Balance='$expense_update' WHERE Branch='expense'";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0014 : ".mysqli_error($conn));             
              }

              $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username1','main_office','$Check','$Checkout','$bank','$expense','$amount','0','$expense_update','Cancel_Expense_Voucher','$des','$id')";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0015 : ".mysqli_error($conn));             
              }

            }

            $sql ="UPDATE balance set Balance='$update_bank_amount' WHERE Branch='Bank'";

            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0016 : ".mysqli_error($conn));             
            }
            $sql="UPDATE bank_name_with_amount set Amount='$update_bank_par' WHERE Bank_Name='$bank'";

            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0017 : ".mysqli_error($conn));             
            }
            $sql="UPDATE expense set status='1' WHERE id = '$id'";

            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0018 : ".mysqli_error($conn));             
            }
        
        
            $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username1','main_office','$Check','$Checkout','$bank','$expense','0','$amount','$update_bank_amount','Cancel_Expense_Voucher','$des','$id')";
            if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0019 : ".mysqli_error($conn));             
            }
           }
          }
        }

        if($type=="CASH")
        {
            
            $sql="SELECT * from balance where Branch='pedi'";
            $res1=$conn->query($sql);
            if($res1===FALSE)
            {
              throw new Exception("Code 0020 : ".mysqli_error($conn));   
            }
            if($row1=mysqli_fetch_array($res1))
            {
                $fetch_cash_total_amt =$row1["Balance"];
                $tota_amount = (int)$row1["Balance"] +$amount;  
                $expense=$row["expense"];

                $sql="SELECT * from balance where Branch='expense'";
                $res2=$conn->query($sql);
                if($res2===FALSE)
                {
                  throw new Exception("Code 0021 : ".mysqli_error($conn));   
                }

            if($row2=mysqli_fetch_array($res2))
            {
                $expense_update =(int)$row2["Balance"]-$amount;;
                $sql="UPDATE balance set Balance='$expense_update' WHERE Branch='expense'";
                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0022 : ".mysqli_error($conn));             
                }

                $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$username1','main_office','$Check','$Checkout','Cash','$expense','$amount','0','$expense_update','Cancel_Expense_Voucher','$des','$id')";
                if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0023 : ".mysqli_error($conn));             
                }
            }


              $sql="UPDATE expense set status='1' WHERE id = '$id'";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0024 : ".mysqli_error($conn));             
              }
              
              $sql="UPDATE balance set Balance='$tota_amount' WHERE Branch='pedi'";
              if($conn->query($sql) === FALSE) {
                throw new Exception("Code 0025: ".mysqli_error($conn));             
              }
                  
              $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$username1','main_office','$Check','$Checkout','Cash','$expense','0','$amount','$tota_amount','Cancel_Expense_Voucher','$des','$id')";
              if($conn->query($sql) === FALSE) {
              throw new Exception("Code 0026 : ".mysqli_error($conn));             
              }

            }
          }
      }
       
      $conn->query("COMMIT");

      echo "<script type=\"text/javascript\">
      window.alert('Updated successfully !');
      window.location = \"acc_Expense_report.php\";    
      </script>";

} catch(Exception $e) { 

      $conn->query("ROLLBACK"); 
      $content = htmlspecialchars($e->getMessage());
      $content = htmlentities($conn->real_escape_string($content));

      $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
      if ($conn->query($sql) === TRUE) {
      // echo "New record created successfully";
      } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }

      echo "<script type=\"text/javascript\">
      window.alert('$content');
      window.location = \"acc_Expense_report.php\";    
      </script>";
} 

$conn->close();
?>