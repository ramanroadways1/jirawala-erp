<?php

require "_session.php";

$id=$conn->real_escape_string(htmlspecialchars($_GET["id"]));
$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

try
 {
    $conn->query("START TRANSACTION"); 

?>
<!DOCTYPE html>
<html lang="en">
    <script src="../assets/sweetalert.min.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
    <title> Shri Jirawala Parshwanath Jain Tirth - <?php  echo $id;  ?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
  #sidebar{
     display: none; 
  }
  .main-content{
     
   
        width: 200mm !important;
        padding: 0px !important;
        margin: auto !important;
        background: white !important;
  }
  #navbar{
    display: none !important;
    visibility: hidden !important;
  }

</style>

  <head>

  </head>

  <body class="no-skin">



    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
      </script>

    

      <div class="main-content">
        <div class="main-content-inner">
           

          <div class="page-content">
             
 <style type="text/css">
  .form-group{
    margin-bottom: 10px;
  }
  label{
    color: #000 !important;
  }
 </style>
 
      <script src="../assets/jquery.js"></script>  

    
      <div id="status"></div>
      <style type="text/css">
      input {text-transform: uppercase !important;}
      </style>
 
<div class="row">
  <div class="col-xs-12" style="border: 1px solid #999; border-radius: 5px; padding-bottom: 0px;">
    <div class="col-md-12" style="margin-top: 5px; margin-bottom:10px;">
  <a href="sjpjt_receiptreprt.php" class="btn btn-app btn-primary btn-sm ">
<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>

Go Back
</a>
</div>
                <div class="col-xs-12">

  <!-- <a href="ReceiptReprt.php"> <<< GO BACK </a> -->

                    <div class="panel panel-default">
                        
                        <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important; padding-bottom: 0px !important;">
                            <div class="row">

<div class="col-xs-12" style=" ">
<center><img src="../images/jp.jpg" width="500px" > <hr style="margin: 5px !important;"> </center> 
</div>
<div class="col-xs-12" style="padding-top: 5px !important;">
</div>

                                <div class="col-xs-6" >
       <?php  

       $sql="select * from bookroom where Bookid = '$id'";
       $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
            }

while($row=mysqli_fetch_array($res))
            {
       ?>


<div class="form-group">

                                            <label>Booking Id:</label>

                                           &nbsp; <?php echo $row["Bookid"] ; ?> </a>



                                      


                                        </div>

                                        <div class="form-group">

                                            <label>Name:</label>
                                          &nbsp;  <?php echo ucwords(strtolower($row["Name"])) ; ?>                                

                                        </div>

                                         <div class="form-group">

                                            <label><?php echo $row["identity"] ; ?> Card No: </label>

                                           &nbsp;  <?php echo $row["snumber"] ; ?>
                                        </div>
                                          
                                        <div class="form-group">
                                            <label>Address:</label>
                                          &nbsp;    <?php echo $row["address"] ; ?>            </div>

                                       
                                        
                                   
                                         
                                         
                                       
 <div class="form-group">
                                            <label>Mobile:</label>
                                         &nbsp;<?php echo $row["Mobilenumber"] ; ?> </div>


   <div class="form-group">
                                            <label>Check In:</label>
                                           &nbsp; <?php echo $row["checkindate"] ; ?>&nbsp; <?php echo $row["intime"] ; ?>
										   <br> <label>Check out:</label>
                                           &nbsp;  <?php echo $row["checkoutdate"] ; ?>&nbsp; <?php echo $row["outtime"] ; ?>   </div>
                                           
                                          
 <div class="form-group">
                                            <label>Adv Booking R. No:</label>
                                            &nbsp; <?php echo $row["AdvanceBookingNumber"] ; ?> </div>   
 


                                </div>
                      
                                 <div class="col-xs-6">
                                     
                                        
                                      
                                        
                                       
                                        
                                        
<div class="form-group">

                                            <label>Booking Date:</label>
                                             &nbsp;  <?php echo $row["Bookingtime"] ; ?> <br>                                            
<label>Donation Id: </label>
                              <th><a target="_blank" href="donationSlip.php?id=<?php echo $row["donationid"] ; ?>"><?php echo $row["donationid"] ; ?></a> </th>           
                                        </div>
                                        <div class="form-group" style="margin-bottom: 5px;">
                                        <div class="col-xs-12" style="margin: 0px !important;  padding: 0px !important;">
                                            <div class=" col-xs-9" style="padding: 0px;">
                                            <label> Male: <?php echo $row["male"] ; ?> Female: <?php echo $row["female"] ; ?>  Child: <?php echo $row["child"] ; ?> Driver: <?php echo $row["driver"] ; ?> </label>                                                                </div>
                                         
                                         
									
                                            </div>
                                        </div>

<div class="form-group" >
                                            <label style="margin-top: 10px;">Total Persons: </label>
                                            &nbsp; <?php echo $row["total"] ; ?>

                                                   </div>       
 <div class="form-group">
                                            <label>Total Deposit:</label>
                                            &nbsp; ₹ <?php echo $row["Deposit"] ; ?>  </div>   
 

                                        

                                         <div class="form-group" >
                                            <label  >In Words: </label>
                                            &nbsp;<?php echo ucwords(strtolower($row["Depositinword"])) ; ?> </div>       


<div class="form-group" style="color:red;">
<?php
$method1=$row["method1"];
if($method1=="checkout")
{
?>

<label>Real Check Out Date:</label>
&nbsp; <?php echo $row["checkoutdate1"] ; ?>&nbsp;(<?php echo $row["checkouttime1"] ; ?>)
<?php
}
else
{
?>

<label>Real Check Out Date:</label>
&nbsp; <?php echo $row["checkoutdate1"] ; ?>&nbsp;(<?php echo $row["checkouttime1"] ; ?>) <br>Status = Cancel 

<?php
}

?>


</h5></div>

                                  
                            </div>
<style type="text/css">
  .wordWrap {
    word-wrap: break-word;      /* IE 5.5-7 */ 
}
</style>
<div class="col-xs-12" style="padding-bottom: 12px;" >
<div style="border-radius: 5px;
 border:2px solid #999; padding-left: 10px; ">
<div class="form-group wordWrap" style=" width: 650px;
margin-bottom: 3px; border-bottom: 1px solid #ccc;">
<span style=" font-size: 15px; line-height:25px; letter-spacing:1px;"> <b>Yatrik Bhavan :&nbsp; </b><?php echo $row["BigDharmshala"]; ?> </span>  
</div> 
 
<div class="form-group wordWrap" style=" width: 650px;
margin-bottom: 3px; border-bottom: 1px solid #ccc;">
<span style=" font-size: 15px; line-height:25px; letter-spacing:1px;"> <b> VIP Bhavan :&nbsp; </b><?php echo $row["VishistAtithiti"]; ?></span>   
</div>
 
<div class="form-group wordWrap" style=" width: 650px; 
margin-bottom: 3px;" >
<span style=" font-size: 15px; line-height:25px; letter-spacing:1px;"> <b>Atithi Bhavan:&nbsp; </b><?php echo $row["SmallDhrm"]; ?> </span> 
</div>

</div>
</div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


<?php  
}


    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


                echo "
                <script>
                swal({
                title: \"Error !\",
                text: \"$content\",
                icon: \"error\",
                button: \"OK\",
                });
                </script>";    
} 



$conn->close();

 ?>
            </div>
            <!-- /.row -->



    </div>
 


    <div> &nbsp;</div>




    </div>

    
        </div><!-- /.page-content -->
        </div>
      </div><!-- /.main-content -->

 
      <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
      </a>
    </div><!-- /.main-container -->

 
  </body>
</html>