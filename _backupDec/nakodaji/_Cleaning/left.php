

<body>


    <div class="main-wrapper">
        <div class="header">

            <div class="header-left">
                <a href="index.php" class="logo">
                    <img src="../assets/logo.png" width="135" alt="">
                </a>
            </div>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars"></i></a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-danger float-right">0</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="#">View all Notifications</a>
                            <!-- activities.html -->
                        </div>
                    </div>
                </li>
               
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
                            <img class="rounded-circle" src="../assets/img/user.jpg" width="24" alt="Admin">
                            <span class="status online"></span>
                        </span>
                        <span><?php echo strtolower($_SESSION["username"]); ?> </span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">My Profile</a>
                        <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="../logout.php">Logout</a>
                </div>
            </div>
        </div>




        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">

                    <!-- 
                    <ul>
                         <li>
                            <a href="index.php"><i class="fa fa-dashboard"></i>DashBoard </a>
                        </li>
                        <li>
                            <a href="ass_issue_asstes.php"><i class="fa fa-calendar-check-o"></i>Issue Details </a>
                        </li>

                         <li class="submenu">
                            <a href="#"><i class="fa fa-plus"></i> <span>Add Details </span>
                             <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="ass_Add_Employee.php">Add Employee</a></li>
                                <li><a href="ass_add_it_asstes.php">Add It Assets</a></li> 
                                <li><a href="ass_add_ve_asstes.php">Add Vehicles</a></li> 
                                <li><a href="ass_add_Category.php">Add category </a></li> 
                                <li><a href="ass_add_party.php">Add Party</a></li> 
                            </ul>
                        </li>

                         <li class="submenu">
                            <a href="#"><i class="fa fa-edit"></i> <span>Update Details </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="ass_Update_Add_Employee.php">Update Employee</a></li>
                                <li><a href="ass_update_add_it_asstes.php">Update It Assets  </a></li> 
                                <li><a href="ass_update_add_ve_asstes.php">Update Vehicle </a></li> 
                                
                            </ul>
                        </li>

                         <li class="submenu">
                            <a href="#"><i class="fa fa-remove"></i> <span>Remove </span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="ass_lost_damage_it.php">Lost It Assets</a></li>
                                <li><a href="ass_lost_damage_vec.php">Lost Vehicle Assets  </a></li> 
                                <li><a href="ass_resign_emp.php">Resign Employee </a></li> 
                                  <li><a href="ass_remove_emp_from_asstes.php">Remove Employee from Asset </a></li> 
                                
                            </ul>
                        </li>
                    </ul>
 -->

                </div>
            </div>
        </div>