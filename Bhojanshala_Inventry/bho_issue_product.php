
<?php

require "_session.php";
include "../_header.php";
include "left.php";

try
 {
    $conn->query("START TRANSACTION"); 

      
    date_default_timezone_set('Asia/Calcutta'); 
    $Check=date("d-m-Y");
    $Checkout=date("Y-m-d");
    $hh=date("h:i:A");
       
              
?>
  

 <div class="page-wrapper">
        <div class="content">
<div class="row">

<div class="col-md-12" >

  <div class="wrapper">

    <section>

    <div class="col-lg-12">
                 
      <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                            <!-- <h3 class="panel-title"> List</h3> -->
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-8" >
                       
                       
                       
                       
  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a align="right" href="#myModal" class="btn btn-sm btn-primary" data-toggle="modal" style="margin-bottom: 10px;">
      <i class="fa fa-plus"></i>
    Create Issue Order</a>
    
  
    
    
    <div id="myModal" class="modal fade" >
        <div class="modal-dialog modal-lg" >
        <form  action="issue_pro.php" method="post" autocomplete="off">
            <div class="modal-content " >
                <div class="modal-header">
                    
                    <h4 class="modal-title">  Create Issue Order</h4>
                </div>
                <div class="modal-body "  >
                    
                       <div class="row">
                       
                         
             
                       <div class="col-md-3">
                        <div class="form-group">
                            <label for="inputName">Date & Time :</label>
                            <input type="text" value="<?php echo $Check; ?>( <?php echo $hh; ?>)" name="date" class="form-control" id="inputName" readonly="readonly">
                        
                        </div>
                        </div>
                   <div class="col-md-3">
         <label for="inputName">   Date : </label>    <input type='text' name="mysqldate"  id='tbDate' placeholder='DD-MM-YYYY' autocomplete="off" class="form-control" required="required"/>     
   </div>
                        
                        <div class="col-md-3">
                         <div class="form-group">
                            <label for="inputName">Receiver Name : </label>
                       
                           <input type="text" class="form-control" name="rec_name" >
                       
                        </div>
                       <input type="hidden" name="username" value="<?php echo $username; ?>">



                        </div>
                        
                        
                         <div class="col-md-3">
                           <label for="inputName">Where to Use  :</label>
                          <select name="where_use" class="form-control " required="required">
                      <option value=""> -- select -- </option>
                       <option value="BHOJANSHALA">BHOJANSHALA</option>
                       <option value="SANGH">SANGH</option>
                      
                       
                        <option value="NONE">NONE</option>
                        
                        </select>
                        
                        </div>
                        
                <div class="col-md-3">
                 <label for="inputName">Mobile Number : </label>
                    <input maxlength="10" class="form-control" name="mo_number" >
                </div>
                
                
                
 <?php
            $sql="SELECT * FROM jirawala_store.bhojanshala_item_name";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001: ".mysqli_error($conn));   
            }

 ?>
 
 <script>
  $(document)
      .ready(
          function() {
            var wrapper = $(".myFields");
            $(add_button)
                .click(
                    function(e) {
                      e.preventDefault();
                      $(wrapper)
                          .append(
                              '<div class="form-group"><div></div> <div class="form-group row">     <div class="col-md-3">        Item Name  : <select class="form-control" name="p_item[]" required>  <option value=""> -- select -- </option> <?php while($row=mysqli_fetch_array($res))
            { ?>  <option value="<?php echo $row["item_name"];  ?>"><?php echo $row["item_name"];  ?></option><?php } ?> </select>   </div>  <div class="col-md-2">Quantity : <br> <input type="text" class="form-control a" name="quantity_box[]" required="required"></div>  <div class="col-md-3">Unit : <select name="mes[]" class="form-control" required="required" ><option value="">-- select --</option><option value="KG">KG</option><option value="gm">GM</option><option value="Pieces">Pieces</option><option value="Packet">Packet</option><option value="TEN">TEN</option></select></div> <div class="col-md-3">Unit Cost : <br> <input type="text" class="form-control b" name="unit_price[]"  required="required"></div>                             <a href="#" class="button delFld"><br>Remove</a></div>'); //add fields
                    });
            $(wrapper).on("click", ".delFld", function(e) {
              e.preventDefault();
              $(this).parent('div').remove();
            })
          });
</script>
   <div class="container" style="margin-bottom: 10px; margin-top: 10px;">
    <div class="myFields"></div>
    <button id="add_button" class="addNew btn btn-success btn-sm">
      <span class="fa fa-plus"></span> Add New  
    </button>
  </div>
                             
            
                        
                        
                        
                        
                          <div class="col-md-4">
                        <label> Gatepass Type : </label>
                        <Select name="G_PASS" class="form-control " required>
                        <option value=""> -- select -- </option>
                        <option value="RETURN_TYPE">RETURN_TYPE</option>
                        <option value="NON_RETURN_TYPE">NON_RETURN_TYPE</option>
                       
                        
                        </Select>
                        </div>
                  

  </div>
                    
            </div>    
            
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary"> <i class="fa fa-check"></i> Save</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>                          
                       
              
                        </div>
                    </div>
                </div> 
                
    
                <div class="table-responsive">
                  <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                    <thead>
              <tr>
                <th>Id</th>
                <th>Order Id</th>
                <th>Date</th>
                <th>Receiver</th>
                <th>Items</th>
                <th>Amount</th>
                <th>Type</th>
                  <th>#</th> 
              </tr>
              </thead>
            <?php  
            $sql="SELECT issue_bhojanshala.*, sum(issue_deta.total) as itotal, sum(issue_deta.issue_qty) as iqty, count(issue_deta.item_name) as iname FROM jirawala_store.issue_bhojanshala left JOIN 
            jirawala_store.issue_deta on issue_deta.iss_id = issue_bhojanshala.iss_id
            GROUP by issue_bhojanshala.iss_id";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
              throw new Exception("Code 001: ".mysqli_error($conn));   
            }
            while($row=mysqli_fetch_array($res))
            { 
              $id=$row["id"];
              ?>
                <tr>
                <td><?php echo $row["id"]; ?></td>
                <td><?php echo $row["iss_id"]; ?></td>
                <td><?php echo $row["date"]; ?></td>
                <td><?php echo $row["rec_name"]; ?></td>
                <td><?php echo $row["iname"]; ?></td>
                <td><?php echo $row["itotal"]; ?></td>
                <td><?php echo $row["g_type"]; ?></td>
                <td><button  class="btn btn-danger btn-xs"><a style="color: #fff;" target="_blank" href="gate_pass.php?id=<?php echo $row["iss_id"]; ?>"> <i class="fa fa-print"></i> Print</a></button>
                </td>
                </tr>
                 <?php } ?>

            
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



  <script type="text/javascript">
        $('.a,.b').keyup(function(){
            var textValue1 =$(this).parent().find('.a').val();
            var textValue2 = $(this).parent().find('.b').val();

            $(this).parent().find('.price').val(textValue1 * textValue2); 
            var sum = 0;
            $(".price").each(function() {
              sum += +$(this).val();
            });

             $("#totalamt").val(sum);
          });
        
        </script>
  <script type="text/javascript">
    $(document).ready(function(){
       $('.btn-primary').click(function(){
           var id=$(this).attr("id");
           $.ajax({
              url:"getData.php",
              type:"post",
              data:"uid="+id,
              success:function(data){
                $("#show-data").html(data);
                 
              }
           });
       });
    });
</script>


<script>
$(document).ready(function(){
    $("#myModal").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });
});
</script>
<script>
    $(document).ready(function () {
        $('input[id$=tbDate]').datepicker({
            dateFormat: 'dd-mm-yy'      // Date Format "dd/mm/yy"
        });
    });
</script>

     
     <?php 
      $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


             
     echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");    
} 

$conn->close();

require "../_footer.php";

      ?>
