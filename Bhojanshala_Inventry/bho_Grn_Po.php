
<?php

require "_session.php";
include "../_header.php";
include "left.php";



 if(!empty($_GET["msg"]))
 {
  if($_GET["msg"]=="Success")
 {
 echo "
    <script>
    swal({
    title: \"Good job!\",
    text: \"Data inserted successfully\",
    icon: \"success\",
    button: \"OK\",
    });
    </script>";
}
 }

try
 {
    $conn->query("START TRANSACTION"); 

?>

 <div class="page-wrapper">
        <div class="content">
 
	
	  

<div class="row">
 

    

		<div class="col-lg-12">
			            
   
			<div class="panel panel-default">
                <div class="panel-heading">
                	<div class="row">
                    	<div class="col-lg-10 col-md-10 col-sm-8 col-xs-6" style="margin-bottom: 10px;">
  <a align=""  href="#myModal" class="btn btn-primary" data-toggle="modal"> <i class="fa fa-plus"></i> Create Order</a>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-8" >
                       
                       
                    <?php
      date_default_timezone_set('Asia/Calcutta'); 
		$Check=date("d-m-Y");
		$Checkout=date("Y-m-d");
		$hh=date("h:i:A");
       ?>   
                     
    
  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    
    <!-- Modal HTML -->
    
    
    <div id="myModal" class="modal fade" >
        <div class="modal-dialog modal-lg" >
        <form  action="insert_bhojan.php" method="post" autocomplete="off">
            <div class="modal-content " >
                <div class="modal-header">
                    
                    <h4 class="modal-title">Create Purchase Order</h4>
                </div>
                <div class="modal-body "  >
                    
                       <div class="row">
                       
                         
             
                       <div class="col-md-3">
                        <div class="form-group">
                            <label for="inputName">Date & Time :</label>
                            <input type="text" value="<?php echo $Check; ?>(<?php echo $hh; ?>)" name="date" class="form-control" id="inputName" readonly="readonly">
                            <input type="hidden" value="<?php echo $Checkout;  ?>" name="mysqldate" class="form-control" id="inputName" readonly="readonly">
                        
                        </div>
                        </div>
                
                        
                        <div class="col-md-3">
                         <div class="form-group">
                            <label >Supplier Name : </label>
                       
                          <Select onchange="yesnoCheck1(this);" class="form-control " name="s_name" id="form-field-select-1" required> 
                      
                          <option value="">-- select  -- </option>
                          <?php
						$sql="SELECT * FROM jirawala_store.supplier_name_bhojanshala";
						$rep=$conn->query($sql);
						if($rep===FALSE)
						{
						throw new Exception("Code 001: ".mysqli_error($conn));   
						}
						while($row=mysqli_fetch_array($rep))
						{
                          ?>
                        
                          <option value="<?php echo $row["supplier_name"];  ?>"><?php echo $row["supplier_name"];  ?></option>
                        
                           <?php }
                           ?>
                          </Select>
                     
                        <br>
                       
                        </div>
                       <input type="hidden" name="username" value="<?php echo $username; ?>">



                        </div>
                        
                        
                         <div class="col-md-3">
                           <label for="inputName">Invoice Number :</label>
                            <input type="text"  name="inv_num" class="form-control" id="inputName" required="required">
                         
              
                        
                        
                        
                        </div>
                        
                           <div class="col-md-3">
                           <label for="inputName">Gate Pass Number :</label>
                            <input type="text"  name="gate_pass" class="form-control" id="inputName" required="required">
                          </div>
                
                
             
                     <?php
                       $sql="SELECT * FROM jirawala_store.bhojanshala_item_name";
						$res=$conn->query($sql);
						if($res===FALSE)
						{
						throw new Exception("Code 002: ".mysqli_error($conn));   
						}

 ?>
 
 <script>
	$(document)
			.ready(
					function() {
						var wrapper = $(".myFields");
						$(add_button)
								.click(
										function(e) {
											e.preventDefault();
											$(wrapper)
													.append(


															'<div class="form-group"><div ></div>	<div class="form-group row">		 <div class="col-md-3">			 	 Item Name  : <select class="form-control select" name="p_item[]" required>	<option> -- select -- </option><?php while($row=mysqli_fetch_array($res))
						{ ?> <option value="<?php echo $row["item_name"]; ?>"><?php echo $row["item_name"]; ?></option>	<?php } ?></select>		</div>	<div class="col-md-2">Quantity : <br> <input type="text" class="form-control a" name="quantity_box[]" required></div><div class="col-md-3">Unit : <select name="mes[]" class="form-control select" required="required" ><option value="">-- select --</option><option value="KG">KG</option><option value="gm">GM</option><option value="Pieces">Pieces</option><option value="Packet">Packet</option><option value="TEN">TEN</option></select></div> <div class="col-md-2">Unit cost : <br> <input type="text" class="form-control b" name="unit_price[]"  required></div>		<a href="#"	class="button delFld"><br>Remove</a></div>'


															); //add fields
										});
						$(wrapper).on("click", ".delFld", function(e) {
							e.preventDefault();
							$(this).parent('div').remove();
						})
					});
</script>


   <div class="container" style="margin-top: 0px; padding-top: 0px;">
		<div class="myFields"></div>
		<button id="add_button" class="addNew btn btn-success btn-sm" style="margin-bottom: 10px;">
			<span class="fa fa-plus"></span> Add Item
		
		</button>
	</div>
     						             
            
                        
                        
                        
                        
                          <div class="col-md-3">
                        <label> Purchase By : </label>
                        <Select name="P_by" class="form-control " required>
                        <option value=""> -- select -- </option>
                        <option value="Kishore Gandhi">Kishore ji Gandhi</option>
                        <option value="Suresh Mehta">Suresh ji Mehta</option>
                        <option value="Popat Bhai Jain">Popat Bhai Jain</option>
                        
                        </Select>
                        </div>
                 
                   
                   
                   <div class="col-md-3">
  <label>GST  Type: </label>
  <select name="gstt" class="form-control " required="required">
  <option value="SGST_CGST">SGST + CGST</option>
  </select>
  </div>

  <div class="col-md-3">
  <label>Rate of % </label>
  <input type="text" class="form-control"value="" name="ratins" required="required">
  </div>

  <div class="col-md-3">
  <label>GST Amount</label>
  <input type="text" class="form-control" id="gst"  name="gstamount" required="required">
  </div>



  </div>
                    
            </div>    
            
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary"> <i class="fa fa-check"></i>  Save</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>                          

                </div>
                    </div>
                </div> 
                
    

                    <div class="table-responsive">
                            <table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
                                <thead>

                                <tr>
                                <th>S. No.</th>
                                <th>P.O. Id</th>
                                <th>Date</th>
                                <th>Supplier name</th>
                                <th>Total Amount</th>   
                                <th>Payment</th>
                                <th>#</th>
                                </tr>

                                </thead>
                               
                            <?php  
                        $sql="SELECT * from jirawala_store.create_purcahse_order_bhojanshala where checkIn='0' and astatus='0'";
                        $res=$conn->query($sql);
                        if($res===FALSE)
                        {
                        throw new Exception("Code 003: ".mysqli_error($conn));   
                        }
                        while($row=mysqli_fetch_array($res))
                        { 
                            $Po_id=$row["Po_id"];
                            $sql="SELECT SUM(total_amt) as amt from jirawala_store.bhojan_po_item_deatils_rec where po_id='$Po_id'";
                        $res1=$conn->query($sql);
                        if($res1===FALSE)
                        {
                        throw new Exception("Code 004: ".mysqli_error($conn));   
                        }
                        while($row1=mysqli_fetch_array($res1))
                        { 

                        $id=$row["id"];
                        $a=$row1["amt"];
                        $b=$row["gst_amt"];
                        $c=(int)$a+(int)$b;


                            ?>
                            <tr>
                            <td><?php echo $row["id"]; ?></td>
                            <td><?php echo $row["Po_id"]; ?></td>
                            <td><?php echo $row["dt"]; ?></td>
                            <td><?php echo $row["suppiler_name"]; ?></td>
                            <td><?php echo $c; ?></td>
                            <td><?php echo $row["payment"]; ?></td>
                            
                         <td><button  class="btn btn-primary btn-xs"> <i class="fa fa-print"></i> <a target="_blank" style="color: #fff;" href="Print_Invoice1.php?id=<?php echo $row["Mer"]; ?>">Print</a></button>
                                   </td>
                            
                            </tr>


                        <?php }} ?>

                            </table>
                        </div>







            </div>
        </div>
    
    </div>



 
  <script type="text/javascript">
  			$('.a,.b').keyup(function(){
  				  var textValue1 =$(this).parent().find('.a').val();
  				  var textValue2 = $(this).parent().find('.b').val();

  				  $(this).parent().find('.price').val(textValue1 * textValue2); 
  				  var sum = 0;
  				  $(".price").each(function() {
  				    sum += +$(this).val();
  				  });

  				   $("#totalamt").val(sum);
  				});
  			
  			</script>
  <script type="text/javascript">
    $(document).ready(function(){
       $('.btn-primary').click(function(){
           var id=$(this).attr("id");
           $.ajax({
              url:"getData.php",
              type:"post",
              data:"uid="+id,
              success:function(data){
                $("#show-data").html(data);
                 
              }
           });
       });
    });
</script>


<script>
$(document).ready(function(){
    $("#myModal").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });
});
</script>
	   
	   <?php 
	    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

}



 catch(Exception $e) { 


                $conn->query("ROLLBACK"); 
                $content = htmlspecialchars($e->getMessage());
                $content = htmlentities($conn->real_escape_string($content));

                $sql = "INSERT INTO jirawala_store.allerror(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
               

                if ($conn->query($sql) === TRUE) {
                // echo "New record created successfully";
                } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                }


     echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"../error.php?err=$content\";
          </script>");  
               



} 

$conn->close();



	   require "../_footer.php"; ?>
