<?php

require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
$lst=$conn->real_escape_string(htmlspecialchars($_POST["lst"]));
$branch = "Booking"; 
$state="";
$iduser=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
$name=$conn->real_escape_string(htmlspecialchars($_POST["name"]));
$address=$conn->real_escape_string(htmlspecialchars($_POST["address"]));
$Adv=$conn->real_escape_string(htmlspecialchars($_POST["adv"]));
$mnumber=$conn->real_escape_string(htmlspecialchars($_POST["mnumber"]));
$identity=$conn->real_escape_string(htmlspecialchars($_POST["identity"]));
$snumber=$conn->real_escape_string(htmlspecialchars($_POST["snumber"]));
$checkin=$conn->real_escape_string(htmlspecialchars($_POST["checkin"]));
$checkout=$conn->real_escape_string(htmlspecialchars($_POST["checkout"]));
$intime=$conn->real_escape_string(htmlspecialchars($_POST["intime"]));
$outtime=$conn->real_escape_string(htmlspecialchars($_POST["outtime"]));
$male=$conn->real_escape_string(htmlspecialchars($_POST["male"]));
$female=$conn->real_escape_string(htmlspecialchars($_POST["female"]));
$child=$conn->real_escape_string(htmlspecialchars($_POST["child"]));
$driver=$conn->real_escape_string(htmlspecialchars($_POST["driver"]));
$total=$conn->real_escape_string(htmlspecialchars($_POST["total"]));
$amount=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$By_book=$conn->real_escape_string(htmlspecialchars($_POST["mode"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["amtws"]));
$status=$conn->real_escape_string(htmlspecialchars($_POST["status"]));
$email21=$conn->real_escape_string(htmlspecialchars($_POST["email21"]));
$Bookingttime=$conn->real_escape_string(htmlspecialchars($_POST["BookingTime"]));
//$bankname=$conn->real_escape_string(htmlspecialchars($_POST["bankname"]));

$guestname=$conn->real_escape_string(htmlspecialchars($_POST["guestname"]));
$guesttype=$conn->real_escape_string(htmlspecialchars($_POST["guesttype"]));
$guestage=$conn->real_escape_string(htmlspecialchars($_POST["guestage"]));
$membernum=$conn->real_escape_string(htmlspecialchars($_POST["membernum"]));
$noofroom=$conn->real_escape_string(htmlspecialchars($_POST["noofroom"]));
$roomtype=$conn->real_escape_string(htmlspecialchars($_POST["roomtype"]));
//$vou_number=$conn->real_escape_string(htmlspecialchars($_POST["vou_number"]));
$apm=$conn->real_escape_string(htmlspecialchars($_POST["apm"]));
$Cash_Book_Entry=$conn->real_escape_string(htmlspecialchars($_POST["Remaining"]));
$d1=$conn->real_escape_string(htmlspecialchars($_POST["checkinm"]));
$d3=$conn->real_escape_string(htmlspecialchars($_POST["checkoutm"]));

$str1="";
$str2="";
$str3="";
$str4="0";
$allstatus = 1;
$all=$allstatus;
 
try
 {
    $conn->query("START TRANSACTION"); 
 
 
      if(empty($_POST["multiselect"]) && (empty($_POST["multiselect1"])) && (empty($_POST["multiselect2"])))
            {
              throw new Exception("Please select at least single room");   
            }

            $sql="SELECT COUNT(Bookid) as value FROM bookroom";
            $res=$conn->query($sql);
            if($res===FALSE)
            {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
            }

            while($row=mysqli_fetch_array($res))
            {
                $fetch_old_id_maine_bookingid =$row["value"];
                $id =(int)$fetch_old_id_maine_bookingid+1;
                
                    $sql1="SELECT * from overallcash where username='$lst' and Branch='Advance_Booking'";
                    $res1=$conn->query($sql1);
                    if($res1===FALSE)
                    {
                      throw new Exception("Code 002 : ".mysqli_error($conn));   
                    }

                    $sql2="SELECT * from overallcash where username='$lst' and Branch='$branch'";
                    $res2=$conn->query($sql2);
                    if($res2===FALSE)
                    {
                      throw new Exception("Code 003 : ".mysqli_error($conn));   
                    }

                    $sql3="SELECT * from balance where Branch='reception'";
                    $res3=$conn->query($sql3);
                    if($res3===FALSE)
                    {
                      throw new Exception("Code 004 : ".mysqli_error($conn));   
                    }
                     
                    $sql4="SELECT * from expense where d_b_id='$Adv'";
                    $res4=$conn->query($sql4);
                    if($res4===FALSE)
                    {
                      throw new Exception("Code 004 : ".mysqli_error($conn));   
                    }

                if($row4=mysqli_fetch_array($res4))
                 {
//  room status update start 

                      if(!empty($_POST["multiselect"]))
                      {
                        $firstempty=$_POST["multiselect"];
                        $str1=implode(",", $firstempty);
                        foreach ($_POST["multiselect"] as $room)
                        {
                          $sql="UPDATE bigdharmshala set status='1'  WHERE id=$room";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 005 : ".mysqli_error($conn));             
                          }
                        }
                      }

                      if(!empty($_POST["multiselect1"]))
                      {
                        $firstempty2=$_POST["multiselect1"];
                        $str2=implode(",", $firstempty2);
                        foreach ($_POST["multiselect1"] as $room1)
                        {
                          $sql="UPDATE bigdharmshala set status_vid='3'  WHERE vid ='$room1'";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 006 : ".mysqli_error($conn));             
                          }
                        }
                      }

                      if(!empty($_POST["multiselect2"]))
                      {
                        $firstempty3=$_POST["multiselect2"];
                        $str3=implode(",", $firstempty3);
                        foreach ($_POST["multiselect2"] as $room2)
                        {
                          $sql="UPDATE bigdharmshala set sstatus='5'  WHERE sid ='$room2'";
                          if($conn->query($sql) === FALSE) {
                            throw new Exception("Code 007 : ".mysqli_error($conn));             
                          }   
                        }
                      }

// room status update ended;

                 $Vou_type =$row4["narration"];
                 $expense_id =$row4["id"];
                 $Vou_narration = $Vou_type. " -> ".$id;
                 
                 if($row1=mysqli_fetch_array($res1))
                 {
                    $half_amount =(int)$amount;
                    $entry_amount =$half_amount /2;
                    $fetch_amount =$row1["amount"];
                    $add = $fetch_amount + $entry_amount;    
             
                    $sql="UPDATE repcash_chq set Des='$Vou_narration' WHERE d_b_id = '$expense_id'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 008 : ".mysqli_error($conn));             
                    }

                    $sql="UPDATE overallcash set Amount='$add' WHERE username='$lst' and Branch='Advance_Booking'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 009 : ".mysqli_error($conn));             
                    }
                 }
                 else
                 {
                    $half_amount =(int)$amount;
                    $entry_amount =$half_amount /2;
                    $sql="insert into overallcash (username,Branch,Amount) values ('$lst','Advance_Booking','$entry_amount')";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0010 : ".mysqli_error($conn));             
                    } 

                    $sql="UPDATE repcash_chq set Des='$Vou_narration' WHERE d_b_id = '$expense_id'";
                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0011 : ".mysqli_error($conn));             
                    }              
                 }


                 if($row3=mysqli_fetch_array($res3))
                 {
                    $fetch_amount_balance_reception =$row3["Balance"];
                    $half_amount =(int)$amount;
                    $entry_amount =$half_amount /2;
                    $total_amount = $fetch_amount_balance_reception + $entry_amount;
                    if($row2=mysqli_fetch_array($res2))
                    {
                      // in this section inter helf of amount in overallcash where branch is Booking
                      // i don't know why?
                      //according to me on both section update same amount
                      //insert helf of total amount in repcash. I don't know why
                      $amt =$entry_amount;
                      $amt1 =$row2["amount"];
                      $amt2 = $amt+$amt1;
                      $debit ="0";
                      $des ="Received room booking deposit and its deposit Receipt No. is ".$id;   
                      $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$checkin','$d1','-','-','$debit','$amt','$total_amount','Room_Booking','$des','$id')";
                     
                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0012 : ".mysqli_error($conn));             
                      }  

                      $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0013 : ".mysqli_error($conn));             
                      }  

                      $sql="UPDATE overallcash set amount='$amt2' WHERE Branch = 'Booking' and username='$lst'";
                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0014 : ".mysqli_error($conn));             
                      }  
                              
                   }
                   else
                   {
                      // in this section inter amount in overallcash where branch is Booking
                      // i don't know why? 
                      //according to me on both section update same amount
                      //insert total amount in repcash. I don't know why?
                      $des =" Received room booking deposit and its deposit Receipt No. is ".$id;
                      $sql="insert into repcash (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Description,d_b_id) values ('$lst','Reception','$checkin','$d1','-','-','0','$amount','$total_amount','Room_Booking','$des','$id')";
                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0015 : ".mysqli_error($conn));             
                      }  
                      $sql="UPDATE balance set Balance='$total_amount' WHERE Branch = 'reception'";
                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0016 : ".mysqli_error($conn));             
                      } 
                      // amount changed to entry_amount suggested by jitendra
                      $sql="insert into overallcash (username,Branch,Amount) values ('$lst','$branch','$entry_amount')";
                      if($conn->query($sql) === FALSE) {
                        throw new Exception("Code 0017 : ".mysqli_error($conn));             
                      }  
                 }
 
                 } 

                    $sql="UPDATE bookonline set status2='2' WHERE bookid =$Adv";
                    if($conn->query($sql) === FALSE) {
                    throw new Exception("Code 0018 : ".mysqli_error($conn));             
                    } 

                    $sql="insert into bookroom (Bookid,userid,mysqlin,mysqlout,Name,address,state,AdvanceBookingNumber,Mobilenumber,identity,snumber,checkindate,checkoutdate,intime,outtime,days,male,female,child,driver,total,BigDharmshala,VishistAtithiti,SmallDhrm,Food,Deposit,Depositinword,ramdom,Atatusid,email,noofroom,roomtype,guestname,guesttype,guestage,guestnumber,Bookingtime,allstatus) values ('$id','$iduser','$d1','$d3','$name','$address','$state','$Adv','$mnumber','$identity','$snumber','$checkin','$checkout','$intime','$outtime','$apm','$male','$female','$child','$driver','$total','$str1','$str2','$str3','$str4','$amount','$inword','$id','$status','$expense_id','$noofroom','$roomtype','$guestname','$guesttype','$guestage','$membernum','$Bookingttime','$all')";

                    if($conn->query($sql) === FALSE) {
                      throw new Exception("Code 0019 : ".mysqli_error($conn));   
                      //    echo "<script type=\"text/javascript\">
                      // window.location = \"BookRoom.php?msg= data is not inserted\";
                      // </script>";
                    }
                    else
                    {
                    // echo "<script type=\"text/javascript\">
                    //  window.location = \"BookSlip.php?id=$id\";
                    //  </script>";

                        $sql="insert into status_checkout (Bookid,status,book_By,gave_key,recevie_key) values ('$id','1','$lst','0','0')";
                        if($conn->query($sql) === FALSE) {
                          throw new Exception("Code 0020 : ".mysqli_error($conn));             
                        }
                    }

                  }
                  else
                  {
                    throw new Exception("Voucher not created !");             
                    // echo "<script type=\"text/javascript\">
                    //  window.location = \"OnlineBookingReg.php?msg='not_created_voucher'\";
                    //  </script>";
                  }
            }

    $conn->query("COMMIT");

    echo ("<script LANGUAGE='JavaScript'>
    window.alert('Succesfully Updated');
    window.location.href = \"BookSlip.php?id=$id\";
    </script>");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

          $conn->query("ROLLBACK"); 
          $content = htmlspecialchars($e->getMessage());
          $content = htmlentities($conn->real_escape_string($content));

          $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
          if ($conn->query($sql) === TRUE) {
          // echo "New record created successfully";
          } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
          }

          echo ("<script LANGUAGE='JavaScript'>
          window.alert('Error: $content');
          window.location.href = \"sjpjt_onlinebookingreg.php\";
          </script>");   

          // echo "
          // <script>
          // swal({
          // title: \"Error !\",
          // text: \"$content\",
          // icon: \"error\",
          // button: \"OK\",
          // });
          // </script>";    
} 

$conn->close();

?>