<?php

    require "_session.php";
    require "_header.php";
include "numbertoword.php";
$starttime=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
$endtime=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime1= date("Y-m-d", strtotime($starttime));
$endtime1= date("Y-m-d", strtotime($endtime));
$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));
$title=$conn->real_escape_string(htmlspecialchars($_POST["title"]));
$valueofk=$conn->real_escape_string(htmlspecialchars($_POST["valueofk"]));

try
 {
    $conn->query("START TRANSACTION"); 
?>

<div class="page-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
        <form action="ledgerwithdetails1_download.php" id="myform" method="post">
        <input type="hidden" name="sys" value="<?php echo $sys; ?>" >
        <input type="hidden" name="starttime" value="<?php echo $starttime;  ?>" >
        <input type="hidden" name="endtime" value="<?php echo $endtime;  ?>" >
        <input type="hidden" name="title" value="<?php echo $title;  ?>"  >
        <input type="hidden" name="valueofk" value="<?php echo $valueofk;  ?>" >
        <h4 class="page-title ">Details of <b><?php echo $title; ?></b> From <?php echo $starttime;  ?> To <?php echo $endtime;  ?>  
        <button type="submit" class="btn btn-success btn-rounded" > <i class="fa fa-download" aria-hidden="true"></i></button>
        </h4>
        </form>

            </div>
        </div>


        <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="BookTable" class="table table-border table-striped custom-table datatable m-b-0">
                                <thead>
                                    <tr>
                                        <th> Donation Id </th>
                                        <th> Date </th>
                                        <th>Name </th>
                                        <th>City</th>
                                        <th>Mobile No. </th>
                                        <th>Particular Amount</th>
                                        <th>Total Amount</th>
                                        <th>Booking Id</th>
                                        <th>Booking Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php  
                     $sql="";
                     $sql2="";
            if($sys=="0")
            {
            $sql="SELECT d.rec_id,d.date,d.name,d.city,d.number,d.$valueofk, d.amount, d.Bookingid, b.Deposit FROM donation as d LEFT JOIN bookroom as b ON d.Bookingid = b.Bookid WHERE d.date BETWEEN '$starttime1' and '$endtime1' and status='0'and $valueofk!='' ORDER BY `d`.`rec_id` DESC";
           $sql2="SELECT COALESCE(sum(d.$valueofk), 0) as particular, COALESCE(sum(d.amount), 0) as amount,  COALESCE(sum(b.Deposit), 0) as deposit FROM donation as d LEFT JOIN bookroom as b ON d.Bookingid = b.Bookid WHERE d.date BETWEEN '$starttime1' and '$endtime1' and status='0'and $valueofk!='' ORDER BY `d`.`rec_id` DESC";
            }
            else
            {
            $sql="SELECT d.rec_id,d.date,d.name,d.city,d.number,d.$valueofk, d.amount, d.Bookingid, b.Deposit FROM donation as d LEFT JOIN bookroom as b ON d.Bookingid = b.Bookid WHERE d.userid= '$sys' and d.date BETWEEN '$starttime1' and '$endtime1' and status='0'and $valueofk!='' ORDER BY `d`.`rec_id` DESC";
             $sql2="SELECT COALESCE(sum(d.$valueofk), 0) as particular, COALESCE(sum(d.amount), 0) as amount,  COALESCE(sum(b.Deposit), 0) as deposit FROM donation as d LEFT JOIN bookroom as b ON d.Bookingid = b.Bookid WHERE d.userid='$sys' and d.date BETWEEN '$starttime1' and '$endtime1' and status='0'and $valueofk!='' ORDER BY `d`.`rec_id` DESC";
            }

                   

                                   
                                    $res=$conn->query($sql);
                                    if($res===FALSE)
                                    {
                                    throw new Exception("Code 001 : ".mysqli_error($conn));   
                                    }

                                    $res2=$conn->query($sql2);
                                    if($res2===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $particular=0;
                                    $amount=0;
                                    $deposit=0;
                                    $particularword="Zero";
                                    $amountword="Zero";
                                    $depositword="Zero";
                                    while($row2=mysqli_fetch_array($res2))
                                    {
                                    $particular=(int)$row2["particular"];
                                    $amount=(int)$row2["amount"];
                                    $deposit=(int)$row2["deposit"];
                                    }
                                   
                                if($particular>0)
                                {
                                $particularword=getIndianCurrency((int)$particular);
                                }

                                if($amount>0)
                                {
                                $amountword=getIndianCurrency((int)$amount);
                                }
                                if($deposit>0)
                                {
                                $depositword=getIndianCurrency((int)$deposit);
                                }

                                   
                                
                                
                                

                                    while($row=mysqli_fetch_array($res))
                                        {
                                $date1=$row["date"];
                                $date2=date("d-m-Y", strtotime($date1));

                                        ?>
                     <tr>
                    <td><a href="donationSlip.php?id=<?php echo $row["rec_id"]; ?>">
                        <?php echo $row["rec_id"]; ?> </a></td>
                    <td><?php echo $date2; ?></td> 
                    <td><?php echo $row["name"]; ?></td>
                    <td><?php echo $row["city"]; ?></td> 
                    <td><?php echo $row["number"]; ?></td>
                    <td><?php echo $row[$valueofk]; ?></td> 
                    <td><?php echo $row["amount"]; ?></td> 
                    <?php  
                    $bookid=$row["Bookingid"];
                       if($bookid=="0")
                       {
                      echo '<td></td>';
                       }
                       else
                       {
                        ?>
                        <td>
                        <a href="bookSlip.php?id=<?php echo $bookid; ?>">
                        <?php echo $bookid; ?></td>
                        <?php
                  
                       }
            
                    ?>
                    
                     <td><?php echo $row["Deposit"]; ?></td>
                    
            </tr>

                                   <?php

                                     }  ?>
                                </tbody>
                            </table>

                       <table  class="table table-border table-striped custom-table datatable m-b-0">
                           
                    <tr>
                     <td>Total Particular Amount</td>   
                     <td><?php echo $particular; ?></td> 
                     <td><?php echo ucwords(strtolower($particularword)); ?></td> 
                    </tr>
                    <tr>
                     <td>Total Donation Amount</td>   
                     <td><?php echo $amount; ?></</td> 
                     <td><?php echo ucwords(strtolower($amountword)); ?></td> 
                    </tr>
                    <tr>
                     <td>Total Booking Amount</td>   
                     <td><?php echo $deposit; ?></</td> 
                     <td><?php echo ucwords(strtolower($depositword)); ?></td> 
                    </tr>



                       </table>
                   


                        </div>
                    </div>
                </div>

        </div>
    </div>


<script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Ledger_category_wise.xls"
                });
            });
        });
    </script>
       
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>