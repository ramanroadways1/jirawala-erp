<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
@$starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
@$endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime= date("Y-m-d", strtotime($starttime1));
$endtime= date("Y-m-d", strtotime($endtime1));
@$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));



try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  /*text-transform: uppercase !important;*/
  border: 1px solid #000 !important;
  }

  div.dataTables_wrapper div.dataTables_filter input{
  	max-height: 30px !important;
  	min-height: 30px !important;
  	height: 30px !important;
  }
</style>
			<div class="page-wrapper">
				<div class="content"> 
  					<div class="row">

				 
				<div class="col-md-6">
					<div class="card-box"> 
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Update - Fooding Amount</h4>
				            	</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="center">
											<th> Food Type </th>
											<th> Current Price </th>
											<th> Update  </th>
									</tr>
								</thead>
								<tbody>

									<?php
									$sql="SELECT * from small1";
									    
									    $res=$conn->query($sql);
									    if($res===FALSE)
									    {
									    throw new Exception("Code 001 : ".mysqli_error($conn));   
									    }
									    while ($row=mysqli_fetch_array($res))
									    {

									?>
											<tr align="center">
											<td align="left"><?php echo $row["ftype"]; ?></td>
											<td><?php echo $row["famt"]; ?></td>
											<td>
											<form action="adm_updateamt2.php" method="post">
											<div class="form-inline">
											<input type="hidden" name="nav" value="<?php echo $row["ftype"]; ?>" class=""  >
											<input type="number" name="update" onkeypress="return isNumber(event)" class="" style="" required="" placeholder="Enter Amount"> 
											&nbsp;  &nbsp; <button  class="btn btn-primary btn-sm pull-right"><span> <i class="fa fa-check-circle"></i> save </span></button>
											</div>
											</form>
											</td>
											</tr>

									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
 

	 
				<div class="col-md-6">
					<div class="card-box"> 
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Update - Booking Amount</h4>
				            	</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="center">
											<th>Room Type </th>
											<th> Current Price </th>
											<th> Update </th>
									</tr>
								</thead>
								<tbody>

									<?php
									  $sql="SELECT * from roomprice";
									        
									        $res=$conn->query($sql);
									        if($res===FALSE)
									        {
									        throw new Exception("Code 002 : ".mysqli_error($conn));   
									        }
									        while ($row=mysqli_fetch_array($res))
									        {

									?>
										<tr align="center">
											<td align="left"><?php echo $row["room_type"]; ?></td>
											<td><?php echo $row["Price"]; ?></td>
											<td>
											<form action="adm_updateroomprice.php" method="post">
											<div class="form-inline">
											<input type="hidden" name="nav" value="<?php echo $row["room_type"]; ?>" class="form-control"  >
											<input type="number" name="update" onkeypress="return isNumber(event)" class="" style="" required="" placeholder="Enter Amount"> 
									       &nbsp; &nbsp; <button  class="btn btn-primary btn-sm pull-right"><span> <i class="fa fa-check-circle"></i> save </span></button>
									        </div>
									        </form>
									        </td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
 
			</div>

				<div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
					<div class="card-box"> 
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;">Update - Dharamshala Rooms Details</h4>
				            	</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="center">
										<th align="left"> Room Type </th>
										<th> Total Beds  </th>
										<th> Bister Set  </th>
										<th> Total Member Allowed </th>
										<th> Total Child Allowed </th>
										<th>  Update </th>	
										<th> Quantity </th>	
										<th> # </th>	
									</tr>
								</thead>
								<tbody>

									<?php
									$sql="SELECT * from roomprice";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 003 : ".mysqli_error($conn));   
									}
									while ($row=mysqli_fetch_array($res))
									{
									?>

										<tr align="center">
										<td align="left"><?php echo $row["room_type"]; ?></td>
										<td><?php echo $row["qua_beds"]; ?></td>
										<td><?php echo $row["qua_bister"]; ?></td>
										<td><?php echo $row["qua_member"]; ?></td>
										<td><?php echo $row["qua_child"]; ?></td>
										<td>
										<form action="adm_updatealltype.php" method="post">
										<select class="select" name="update" required="required">
										<option value=""> --select--</option>
										<option value="Total_beds"> Total Beds </option>
										<option value="Bister_set">  Bister Set </option>
										<option value="total_member">  Total Member </option>
										<option value="child"> Total Child </option>
										</select>
										<td>
										<input type="number" min='0' name="quantity" class="" required="">
										<input type="hidden" min='0' value="<?php echo $row["room_type"]; ?>" name="type" class="form-control" >
										</td>
										<td><button  class="btn btn-primary btn-sm pull-right"><span> <i class="fa fa-check-circle"></i> update </span></button>
										</td>
										</form>
										</td>
										</tr>

									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
 
                </div>
	    	</div>
		</div>
 
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>