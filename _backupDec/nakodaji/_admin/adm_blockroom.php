<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

	date_default_timezone_set('Asia/Calcutta'); 
	$date=date("d-m-Y");
	$time=date("h:i");
	$intime=date("h:i:A");
	$time1=date("A");

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);
@$starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
@$endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
$starttime= date("Y-m-d", strtotime($starttime1));
$endtime= date("Y-m-d", strtotime($endtime1));
@$sys=$conn->real_escape_string(htmlspecialchars($_POST["sys"]));



try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
  input{
  /*text-transform: uppercase !important;*/
  border: 1px solid #000 !important;
  }

  div.dataTables_wrapper div.dataTables_filter input{
  	max-height: 30px !important;
  	min-height: 30px !important;
  	height: 30px !important;
  }
</style>
			<div class="page-wrapper">
				<div class="content"> 
  					<div class="row">

				 
				<div class="col-md-6">
					<div class="card-box"> 
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> Yatrik Bhavan <br> <sup> {Block Room} </sup></h4>
				            	</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="center">
											<th> Room No </th>
											<th> Room Type </th>
											<th> Status </th>
											<th> Unblock </th>
											</tr>
									</tr>
								</thead>
								<tbody>
									<?php 
									    $sql="select * from bigdharmshala where id!=0";
									    $vip=$conn->query($sql);
									    if($vip===FALSE)
									    {
									      throw new Exception("Code 001 : ".mysqli_error($conn));   
									    }
									   
									while ($row=mysqli_fetch_array($vip))
									{
										$id=$row["id"];
										$status=$row["status"];
									?>
										<tr align="center">
										<td><?php echo $row["id"]; ?></td>
										<td><?php echo $row["big"]; ?></td>
										<td><?php echo $row["status"]; ?></td>
										<td>
                                        <div class="dropdown action-label">
                                           <?php
                    
                                           if($status=='16')
                                           {
												?>
												<a class="custom-badge status-red dropdown-toggle" href="" data-toggle="dropdown" aria-expanded="false">
												Block
												</a>
												<div class="dropdown-menu">
												<a class="dropdown-item" href="adm_updateamt7.php?nav=<?php echo $id; ?>&update=0">Unblock</a>
												</div>
												<?php
                                           }
                                           else
                                           {
										?>
										<a class="custom-badge status-green dropdown-toggle" href="" data-toggle="dropdown" aria-expanded="false">
										Unblock
										</a>
										<div class="dropdown-menu">
										<a class="dropdown-item" href="adm_updateamt7.php?nav=<?php echo $id; ?>&update=16">Block</a>
										
										</div>
										<?php
                                           }
                                           ?>
										</div>


										</td>
										</tr>
									<?php
									}

									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
 

	 
				 <div class="col-md-6">
					<div class="card-box"> 
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> VIP Bhavan <br> <sup> {Block Room} </sup></h4>
				            	</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="center">
											<th> Room No </th>
											<th> Room Type </th>
											<th> Status </th>
											<th> Unblock </th>
											</tr>
									</tr>
								</thead>
								<tbody>
									<?php 
									    $sql="select * from bigdharmshala where vid!=0";
									    $vip=$conn->query($sql);
									    if($vip===FALSE)
									    {
									      throw new Exception("Code 001 : ".mysqli_error($conn));   
									    }
									   
									while ($row=mysqli_fetch_array($vip))
									{
										$vid=$row["vid"];
										$status_vid=$row["status_vid"];
									?>
										<tr align="center">
										<td><?php echo $row["vid"]; ?></td>
										<td><?php echo $row["vtype"]; ?></td>
										<td><?php echo $row["status_vid"]; ?></td>
										<td>
                                       <div class="dropdown action-label">
                                           <?php
                    
                                           if($status_vid=='17')
                                           {
												?>
												<a class="custom-badge status-red dropdown-toggle" href="" data-toggle="dropdown" aria-expanded="false">
												Block
												</a>
												<div class="dropdown-menu">
												<a class="dropdown-item" href="adm_updateamt8.php?nav=<?php echo $vid; ?>&update=2">Unblock</a>
												</div>
												<?php
                                           }
                                           else
                                           {
										?>
										<a class="custom-badge status-green dropdown-toggle" href="" data-toggle="dropdown" aria-expanded="false">
										Unblock
										</a>
										<div class="dropdown-menu">
										<a class="dropdown-item" href="adm_updateamt8.php?nav=<?php echo $vid; ?>&update=17">Block</a>
										
										</div>
										<?php
                                           }
                                           ?>
										</div>
									
										</td>
										</tr>
									<?php
									}

									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>

				<div class="col-md-8">
					<div class="card-box"> 
				            	<div class="col-md-12">
				                <h4 class="card-title" style="text-align:center; border-bottom: 1px dotted #444;"> Atithi Bhavan <br> <sup> {Block Room} </sup></h4>
				            	</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table  table-hover custom-table datatable m-b-0">
								<thead>
									<tr align="center">
											<th> Room No </th>
											<th> Room Type </th>
											<th> Status </th>
											<th> Unblock </th>
											</tr>
									</tr>
								</thead>
								<tbody>
									<?php 
									    $sql="select * from bigdharmshala where sid!=0";
									    $vip=$conn->query($sql);
									    if($vip===FALSE)
									    {
									      throw new Exception("Code 001 : ".mysqli_error($conn));   
									    }
									   
									while ($row=mysqli_fetch_array($vip))
									{
										$sid=$row["sid"];
										$sstatus=$row["sstatus"];
									?>
										<tr align="center">
										<td><?php echo $row["sid"]; ?></td>
										<td><?php echo $row["stype"]; ?></td>
										<td><?php echo $row["sstatus"]; ?></td>
										<td>
									
										<div class="dropdown action-label">
                                           <?php
                    
                                           if($sstatus=='18')
                                           {
												?>
												<a class="custom-badge status-red dropdown-toggle" href="" data-toggle="dropdown" aria-expanded="false">
												Block
												</a>
												<div class="dropdown-menu">
												<a class="dropdown-item" href="adm_updateamt9.php?nav=<?php echo $sid; ?>&update=4">Unblock</a>
												</div>
												<?php
                                           }
                                           else
                                           {
										?>

										<a class="custom-badge status-green dropdown-toggle" href="" data-toggle="dropdown" aria-expanded="false">
												Unblock
												</a>

									
										<div class="dropdown-menu">
										<a class="dropdown-item" href="adm_updateamt9.php?nav=<?php echo $sid; ?>&update=18">Block</a>
										
										</div>
										<?php
                                           }
                                           ?>
										</div>



										







										</td>
										</tr>
									<?php
									}

									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
 
                </div>
	    	</div>
		</div>
 
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>