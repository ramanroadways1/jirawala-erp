<?php
//done
    require "_session.php";

    $username=$_SESSION["username"];
    $file_name = basename($_SERVER['PHP_SELF']);
    $starttime1=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
    // $endtime1=$conn->real_escape_string(htmlspecialchars($_POST["endtime"]));
    $starttime= date("Y-m-d", strtotime($starttime1));
    // $endtime= date("Y-m-d", strtotime($endtime1));
    

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <script src="assets/sweetalert.min.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="../assets/fav.jpg">
  <title> Shri Jirawala Parshwanath Jain Tirth </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>



<script >        
function DisableBackButton() {
     window.history.forward()
 }
 DisableBackButton();
 window.onload = DisableBackButton;
 window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
 window.onunload = function () { void (0) }
</script>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>


<div class="main-content">
<div class="main-content-inner">
<div class="page-content">



 

          <div class="row">
             <div class="col-xs-12">
                <div class="panel panel-default" style="border-color: #000 !important;">
                     <div class="panel-body" style="padding: 10px !important; padding-top: 3px !important; padding-left: 15px !important;">
                            <div class="row">

                              <div class="col-xs-12" style=" ">
                              <center><img src="../images/jp.jpg" width="500px" >
                               <hr style="margin: 5px !important; padding-bottom: 10px !important;"> </center> 
                              </div> 
<div class="col-xs-12" style="padding-bottom: 10px !important;">
 
<div class="col-xs-6" >
<div class="form-group" style="margin-bottom: 5px !important;">
 <label style="font-size: 12px !important;">BOOKED & AVAILABLE ROOMS REPORT</label>

 <!-- <input type="button" class="btn btn-app btn-success  btn-xs" id="btnExport" value="Export" /> -->
  

</div>

    </div>  


<div class="form-group" style="font-size: 12px !important; margin-bottom: 1px !important;">

    <label style="font-size: 12px !important;">DATE:
    &nbsp;<?php echo $starttime1; ?> </label>
  
  

    </div>
</div>


 

</div>


 <div class="col-xs-12">
  
  <?php

try {
    $conn->query("START TRANSACTION"); 


  ?>

  
      <div class="tableFixHead">
  <table class="table table-bordered" id="tblCustomers"  style="table-layout:  !important;">

<tr align="center">
<th colspan="7" style="color:red; text-align: center; text-transform: uppercase;"> Available </th>
<th colspan="5" style="background-color:#FFE5EA; color:red; text-align: center; text-transform: uppercase;"> Booked </th>
</tr>  



<thead  >
    <tr align="center" >
    <th></th>
    <th >Date</th>
    <th>VIP Room</th>
    <th>AC Room </th>
    <th>Non Ac Room </th>
    <th>Non Ac Hall</th>
    <th>Ac Hall </th>
    <th style="background-color:#FFE5EA; "> VIP Room</th>
    <th style="background-color:#FFE5EA; ">AC Room </th>
    <th style="background-color:#FFE5EA; ">Non Ac Room </th>
    <th style="background-color:#FFE5EA; "> Non Ac Hall</th>
    <th style="background-color:#FFE5EA; "> Ac Hall </th>
    </tr>
      </thead>

  <tbody>

      <?php

        //$sql="select * from onlinerooms where Date between '$starttime' and '$endtime'";
       
   $sql="select * from onlinerooms where Date='$starttime'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
        while($row=mysqli_fetch_array($res))
        {
            
            $date=date("d-m-Y", strtotime($row["Date"]));
            $vip_room=$row["vip_room"];
            $ac_room=$row["ac_room"];
            $non_ac_room=$row["non_ac_room"];
            $non_ac_hall=$row["non_ac_hall"];
            $ac_hall=$row["ac_hall"];
            $booked_vip_room=$row["booked_vip_room"];
            $booked_ac_room=$row["booked_ac_room"];
            $booked_non_ac_room=$row["booked_non_ac_room"];
            $booked_non_ac_hall=$row["booked_non_ac_hall"];
            $booked_ac_hall=$row["booked_ac_hall"];

      ?>

  <tr>
  <th>Online</th>
  <th><?php echo $date ;?></th>
  <th><?php echo $vip_room; ?></th>
  <th> <?php echo $ac_room;?></th>
  <th><?php echo $non_ac_room ;?></th>
  <th><?php echo $non_ac_hall;?></th>
  <th><?php echo $ac_hall;?></th>
  <th style="background-color:#FFE5EA; "><?php echo $booked_vip_room;?></th>
  <th style="background-color:#FFE5EA; "><?php echo $booked_ac_room;?></th>
  <th style="background-color:#FFE5EA; "><?php echo $booked_non_ac_room;?></th>
  <th style="background-color:#FFE5EA; "><?php echo $booked_non_ac_room;?></th>
  <th style="background-color:#FFE5EA; "><?php echo $booked_ac_hall;?></th>
</tr>

  <?php


}
 

$viproom=0;
$acroom=0;
$nonacroom=0;
$nonachall=0;
$achall=0;
$sql="SELECT BigDharmshala, VishistAtithiti,SmallDhrm FROM `bookroom` WHERE mysqlin='$starttime' AND AdvanceBookingNumber='0'";
        $resm=$conn->query($sql);
        if($resm===FALSE)
        {
        throw new Exception("Code 002 : ".mysqli_error($conn));   
        }
      while($row=mysqli_fetch_array($resm))
        {
            
                if(!empty($row["BigDharmshala"]))
                    {
                        
                      $BigDharmshala=$row["BigDharmshala"];
                        $token = strtok($BigDharmshala, ",");
                        while ($token !== false)
                                    {
                                    $sql="select * from bigdharmshala where id= '$token'";
                                    $res1=$conn->query($sql);
                                    if($res1===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $token = strtok(",");
                                    while($row1=mysqli_fetch_array($res1))
                                    {
                                        $big=$row1["big"];
                                        if($big=="(AC)")
                                        {
                                        $acroom++;
                                        }
                                        if($big=="(HALL)")
                                        {
                                        $achall++;
                                        }

                                    } }


                    }
               if(!empty($row["VishistAtithiti"]))
                    {
                      
                       $VishistAtithiti=$row["VishistAtithiti"];
                        $token1 = strtok($VishistAtithiti, ",");
                        while ($token1 !== false)
                                    {
                                   $sql="select * from bigdharmshala where vid= '$token1'";
                                    $res1=$conn->query($sql);
                                    if($res1===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $token1 = strtok(",");
                                    while($row1=mysqli_fetch_array($res1))
                                    {
                                     $vtype=$row1["vtype"];
                                        if($vtype=="(AC)")
                                        {
                                        $viproom++;
                                        }
                                        

                                    } }

                    }
                if(!empty($row["SmallDhrm"]))
                    {
                      
                    $SmallDhrm=$row["SmallDhrm"];
                        $token2 = strtok($SmallDhrm, ",");
                        while ($token2 !== false)
                                    {
                                 $sql="select * from bigdharmshala where sid= '$token2'";
                                    $res2=$conn->query($sql);
                                    if($res2===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $token2 = strtok(",");

                                    while($row1=mysqli_fetch_array($res2))
                                    {
                                       
                                     $stype=$row1["stype"];
                                        if($stype=="(AC)")
                                        {
                                        $acroom++;
                                        }
                                        
                                       if($stype=="(HALL)"||$stype=="(SB HALL)"||$stype=="(P HALL)"||$stype=="(M-HALL)")
                                        {
                                        $nonachall++;
                                        }
                                        if($stype=="(NON-AC)")
                                        {
                                        $nonacroom++;
                                        }
                                        

                                    } }



                    }

          




        }


?>
<tr>
<th>Offline</th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th ><?php echo $viproom; ?></th>
  <th ><?php echo $acroom; ?></th>
  <th ><?php echo $nonacroom; ?></th>
  <th ><?php echo $nonachall; ?></th>
  <th ><?php echo $achall; ?></th>
</tr>


<?php
$viproom=0;
$acroom=0;
$nonacroom=0;
$nonachall=0;
$achall=0;
$sql="SELECT BigDharmshala, VishistAtithiti,SmallDhrm FROM `bookroom` WHERE mysqlin='$starttime' AND AdvanceBookingNumber!='0'";
        $resm=$conn->query($sql);
        if($resm===FALSE)
        {
        throw new Exception("Code 002 : ".mysqli_error($conn));   
        }
      while($row=mysqli_fetch_array($resm))
        {
            
                if(!empty($row["BigDharmshala"]))
                    {
                        
                      $BigDharmshala=$row["BigDharmshala"];
                        $token = strtok($BigDharmshala, ",");
                        while ($token !== false)
                                    {
                                    $sql="select * from bigdharmshala where id= '$token'";
                                    $res1=$conn->query($sql);
                                    if($res1===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $token = strtok(",");
                                    while($row1=mysqli_fetch_array($res1))
                                    {
                                        $big=$row1["big"];
                                        if($big=="(AC)")
                                        {
                                        $acroom++;
                                        }
                                        if($big=="(HALL)")
                                        {
                                        $achall++;
                                        }

                                    } }


                    }
               if(!empty($row["VishistAtithiti"]))
                    {
                      
                       $VishistAtithiti=$row["VishistAtithiti"];
                        $token1 = strtok($VishistAtithiti, ",");
                        while ($token1 !== false)
                                    {
                                   $sql="select * from bigdharmshala where vid= '$token1'";
                                    $res1=$conn->query($sql);
                                    if($res1===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $token1 = strtok(",");
                                    while($row1=mysqli_fetch_array($res1))
                                    {
                                     $vtype=$row1["vtype"];
                                        if($vtype=="(AC)")
                                        {
                                        $viproom++;
                                        }
                                        

                                    } }

                    }
                if(!empty($row["SmallDhrm"]))
                    {   
                    $SmallDhrm=$row["SmallDhrm"];
                        $token2 = strtok($SmallDhrm, ",");
                        while ($token2 !== false)
                                    {
                                 $sql="select * from bigdharmshala where sid= '$token2'";
                                    $res2=$conn->query($sql);
                                    if($res2===FALSE)
                                    {
                                    throw new Exception("Code 002 : ".mysqli_error($conn));   
                                    }
                                    $token2 = strtok(",");

                                    while($row1=mysqli_fetch_array($res2))
                                    {
                                       
                                     $stype=$row1["stype"];
                                        if($stype=="(AC)")
                                        {
                                        $acroom++;
                                        }
                                        if($stype=="(HALL)"||$stype=="(SB HALL)"||$stype=="(P HALL)"||$stype=="(M-HALL)")
                                        {
                                        $nonachall++;
                                        }
                                        if($stype=="(NON-AC)")
                                        {
                                        $nonacroom++;
                                        }
                                        

                                    } }



                    }

          




        }


?>
<tr>
<th>Online allot</th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th ><?php echo $viproom; ?></th>
  <th ><?php echo $acroom; ?></th>
  <th ><?php echo $nonacroom; ?></th>
  <th ><?php echo $nonachall; ?></th>
  <th ><?php echo $achall; ?></th>
</tr>

<?php
$total_vip=0;
$total_acroom=0;
$total_nonacroom=0;
$total_nonachall=0;
$total_achall=0;

         $sql="SELECT COALESCE(sum(noofroom), 0) as total_vip FROM bookonline as book INNER JOIN bankpayment as p on book.bookid=p.bookind_id WHERE book.country='$starttime' and p.response='E000' and book.rtype='VIP Rooms'";
        $res1=$conn->query($sql);
        if($res1===FALSE)

        {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
       
        if($row1=mysqli_fetch_array($res1))
        {
        $total_vip=$row1["total_vip"];
        }
        $sql="SELECT COALESCE(sum(noofroom), 0) as total_acroom FROM bookonline as book INNER JOIN bankpayment as p on book.bookid=p.bookind_id WHERE book.country='$starttime' and p.response='E000' and book.rtype='Ac Room'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
        if($row=mysqli_fetch_array($res))
        {
        $total_acroom=$row["total_acroom"];
        }
         $sql="SELECT COALESCE(sum(noofroom), 0) as total_nonacroom FROM bookonline as book INNER JOIN bankpayment as p on book.bookid=p.bookind_id WHERE book.country='$starttime' and p.response='E000' and book.rtype='Non Ac Hall'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
        if($row=mysqli_fetch_array($res))
        {
        $total_nonacroom=$row["total_nonacroom"];
        }
        $sql="SELECT COALESCE(sum(book.noofroom), 0) as total_nonachall FROM bookonline as book INNER JOIN bankpayment as p on book.bookid=p.bookind_id WHERE book.country='$starttime'and p.response='E000'and book.rtype='Non Ac Room'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
        if($row=mysqli_fetch_array($res))
        {
        $total_nonachall=$row["total_nonachall"];
        }


        $sql="SELECT COALESCE(sum(noofroom), 0) as total_achall FROM bookonline as book INNER JOIN bankpayment as p on book.bookid=p.bookind_id WHERE book.country='$starttime' and p.response='E000' and book.rtype='Ac Hall'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
        throw new Exception("Code 001 : ".mysqli_error($conn));   
        }
        if($row=mysqli_fetch_array($res))
        {
        $total_achall=$row["total_achall"];
        }


?>

<tr>
<th>Online book</th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th ><?php echo $total_vip; ?></th>
  <th ><?php echo $total_acroom; ?></th>
  <th ><?php echo $total_nonacroom; ?></th>
  <th ><?php echo $total_nonachall; ?></th>
  <th ><?php echo $achall; ?></th>
</tr>




</div>



</tbody>
     </table>




     
     </div>
     
     <?php







 $conn->query("COMMIT");

//     echo "
//     <script>
//    swal({
//   title: \"Good job!\",
//   text: \"You clicked the button!\",
//   icon: \"success\",
//   button: \"OK\",
// });
//     </script>";

} catch(Exception $e) { 

$conn->query("ROLLBACK"); 
$content = htmlspecialchars($e->getMessage());
$content = htmlentities($conn->real_escape_string($content));

$sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

if ($conn->query($sql) === TRUE) {
  // echo "New record created successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}





//     echo "
//     <script>
//    swal({
//   title: \"Error !\",
//   text: \"$content\",
//   icon: \"error\",
//   button: \"OK\",
// });
//     </script>";    
} 

$conn->close();

  ?>
</div><script type="text/javascript">
function myFunction() { 
window.print();
}
</script>
          </div>
          </div>
          </div>
          </div>

                  <center class="hidden-print">
      <a href="sjpjt_compus_adv.php" class="btn btn-app btn-danger">
      <i class="ace-icon fa fa-remove bigger-160"></i>
      Exit
      </a>
      &nbsp; &nbsp; 
      <button onclick="myFunction()" class="btn btn-app btn-primary">
      <i class="ace-icon fa fa-print bigger-160"></i>
      Print
      </button>
&nbsp; &nbsp;

      <button class="btn btn-app btn-success" id="btnExport">
      <i class="ace-icon fa fa-download bigger-160"></i>
      Export
      </button>

       <!-- <input type="submit" class="btn btn-app btn-success"   value="Export" /> -->

  </center>
<br>




<script>

function calc1(){
             var textValue1 = Number(document.getElementById('per').value);
             var textValue2 = Number(document.getElementById('vip').value);
            
             document.getElementById('final').value = Number((textValue2 * textValue1) / 100); 

          alert(final);         

</script>

 
          </div>
        </div>      




<!--   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
      </a> -->
    </div>

<!-- 
<center class="hidden-print">

<a href="ReceiptReprt.php" class="btn btn-app btn-danger  btn-xs">
<i class="ace-icon fa fa-remove bigger-160"></i>
Exit
</a>
 &nbsp; &nbsp; &nbsp; &nbsp;
<button onclick="myFunction()" class="btn btn-app btn-primary  btn-xs">
<i class="ace-icon fa fa-print bigger-160"></i>
Print
</button>
   <input type="button" class="btn btn-app btn-success  btn-xs" id="btnExport" value="Export" />
  
</center> -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="table2excel.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "Book&Available_RoomReport.xls"
                });
            });
        });
    </script>

</div>  </div>
    </div>
</div>
          </div>
          </div>
      
      </div>
      
      </div>
      
      </div>
      
      
      
      

</body>
</html>