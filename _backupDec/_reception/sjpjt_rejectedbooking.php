<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username1=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);

if(isset($_POST["starttime"])){
	$dat=$conn->real_escape_string(htmlspecialchars($_POST["starttime"]));
	$dat= str_replace("/","-",$dat);
	$d1= date("Y-m-d", strtotime($dat)); 
}

try
 {
    $conn->query("START TRANSACTION"); 
?>

<style>
tbody th
{
	color: #444;
}
	</style>

<div class="page-wrapper">
	<div class="content">
	    <div class="row" style="">
	        <div class="col-sm-12">
	            <h4 class="page-title" > <a onclick="window.history.go(-1)" class="btn btn-app btn-danger btn-sm ">
									<i style="color:#fff;" class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a> Rejected Online Room Bookings <button type="button" class="btn btn-success btn-rounded"  id="btnExport"> <i class="fa fa-download" aria-hidden="true"></i></button> </h4>
	        </div>
	    </div>
  
		<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0" id="BookTable">
								<thead>
									<tr>
										<th> Id </th>
										<th> Full Name </th>
										<th> Address </th>
										<th> Mobile </th>
										<th> No of Room </th>
										<th> Room type </th>
										<th> Paid Amount </th>
										<th> Check-In Date  </th>
										<th> Check-Out Date </th>	
										<th> Status </th>
									</tr>
								</thead>
								<tbody>
								<?php

									$sql="select * from bookonline where status2='1'";
									$res=$conn->query($sql);
									if($res===FALSE)
									{
									throw new Exception("Code 001 : ".mysqli_error($conn));   
									}
									if ($res->num_rows > 0) {
									while($row=mysqli_fetch_array($res))
									{

									$status2=$row["status2"];
							  	?>
 
				 						<tr>
												<th><?php echo $row["bookid"]; ?> </th>
												<th><?php echo  ucwords(strtolower($row["name"])); ?> </th>
												<th><?php echo  ucwords(strtolower($row["city"])); ?></th>
												<th><?php echo $row["Phone"]; ?> </th>
												<th><?php echo $row["noofroom"]; ?></th>
												<th><?php echo $row["rtype"]; ?></th>
												<th><?php echo $row["remaing"]; ?></th>
												<th><?php echo $row["arrive"]; ?> </th>
												<th><?php echo $row["depart"]; ?> <br>  </th>
												<th>Rejected</th>
										</tr>

								<?php  } } else {

									echo "<tr> <td colspan=12> No booking data available ! </td> </tr>";
								}?>
								</tbody>
							</table>
						</div>
					</div>
                </div>

	    </div>
	</div>

	<script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#BookTable").table2excel({
                    filename: "Rejected_Bookings.xls"
                });
            });
        });
    </script>
<?php 

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>