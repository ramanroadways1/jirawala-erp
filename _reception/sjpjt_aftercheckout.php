<?php
//done
    require "_session.php";
    require "_header.php";
 
	$username=$_SESSION["username"];
	$file_name = basename($_SERVER['PHP_SELF']);
	$id=$conn->real_escape_string(htmlspecialchars($_GET["id"]));

	date_default_timezone_set('Asia/Calcutta'); 
	$Check=date("d-m-Y"); 
	$Check1=date("Y-m-d");
	$hh=date("h:i:A");

try
 {
    $conn->query("START TRANSACTION"); 
?>
<style>
	.sidebar {
		display: none;
	}
  input{
  text-transform: uppercase !important;
  border: 1px solid #a3a3a3 !important;
  }
  .customInput {
  	padding: 0px !important;
  	padding-left: 10px !important;
  	min-height: 28px !important;
  }

  .row4 {
  		border-top:1px solid #fff !important;
  		border-bottom:1px solid #fff !important;
  }

  .row2, .row6{
  	vertical-align: middle !important;
  	font-weight: bold !important;
  	color: #444 !important;
  	padding-left: 20px !important;
  }

  .row1, .row5 {
  	text-align: center;
  	vertical-align: middle !important;
  }


</style>
<script type="text/javascript">
function validateform(){  
//	var name=document.myform.name.value; 
	if(document.myform.name.value == "" || !isNaN(document.myform.name.value) )
	{
		alert( "Name is not valid !" );
		document.myform.name.focus() ;
		return false;
	}

	if(document.myform.city.value == "" || !isNaN(document.myform.city.value) )
	{
		alert( "City is not valid !" );
		document.myform.city.focus() ;
		return false;
	}

	if(document.myform.number.value == "" || isNaN(document.myform.number.value) || document.myform.number.value.length != 10 )
		{
			alert( "Mobile number cannot be blank, should be 10 digit number !" );
			document.myform.number.focus() ;
			return false;
		}
 
	if(document.myform.total.value == "" || isNaN(document.myform.total.value) || document.myform.total.value  <= '1' )
		{
			alert( "Please enter valid deposit amount !" );
			document.myform.total.focus() ;
			return false;
		}
	 
	return true;
} 

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<div class="page-wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-sm-12">
	            <h4 class="page-title"> Donation Receipt  &nbsp;{<?php echo $Check; ?>&nbsp; <?php echo $hh; ?>} </h4>
	        </div>
	    </div>

  <div class="row">
                    <div class="col-md-10">
                        <div class="card-box">
                         <!--    <h4 class="card-title">Two Column Horizontal Form</h4>
                            <h4 class="card-title">Personal Information</h4> -->
						<form action="insert_aftercheckout.php" method="POST" name="myform" onsubmit="return validateform()" autocomplete="off">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="row">

  <?php
 

	$s1="select * from admin where username = '$_SESSION[username]' and fullname='$_SESSION[dep]'";
	$r1=$conn->query($s1);
	if($r1===FALSE)
	{
		throw new Exception("Code 071 : ".mysqli_error($conn));   
	}
	$d1=mysqli_fetch_array($r1);


	$sql="select * from bookroom where Bookid = '$id'";

	$res=$conn->query($sql);
	if($res===FALSE)
	{
	throw new Exception("Code 001 : ".mysqli_error($conn));   
	}
	while ($row=mysqli_fetch_array($res))
	{ 
  ?>

							<div class="col-md-2 form-group">
							<label>BOOKING ID</label>
							<input type="text" class="form-control"  name="Bookingid1212"  value="<?php echo $row["Bookid"];  ?>" readonly>
							</div>

							<div class="col-md-4 form-group">
							<label>FULL NAME</label>
							<input type="text" class="form-control"  name="name" id="tname" value="<?php echo $row["Name"];  ?>" readonly>
							</div>
							 
							<div class="col-md-3 form-group">
							<label>CITY</label>
							<input type="text" class="form-control"  name="city" id="city" value="<?php echo $row["address"];  ?>" readonly>
							</div>

							<div class="col-md-3 form-group">
							<label>MOBILE No.</label>
							<input type="text" class="form-control"  name="number" id="number" maxlength="10" value="<?php echo $row["Mobilenumber"];  ?>" readonly>
							</div>

							<input type="hidden" name="sys" value="<?php echo $d1["id"]; ?>">
							<input type="hidden" name="bookingby" value="<?php echo $row["userid"]; ?>">
							<input type="hidden" id="total_booking_amount" name="total_booking_amount" value="<?php echo $row["Deposit"]; ?>">
							<input type="hidden" name="userbyid" value="<?php echo $row["userid"]; ?>">
							<input type="hidden" name="asid" value="<?php echo $row["Atatusid"]; ?>">
							<input type="hidden" name="sta" value="0">
							<input type="hidden" name="email21" class="form-control" value="<?php echo $row["email"]; ?>"/>
							<input type="hidden" name="recdate" value="<?php echo $Check1; ?>">
							<input type="hidden" name="rectime" value="<?php echo $hh; ?>">
							<input type="hidden" name="real" value="<?php echo $Check; ?>">
							<input type="hidden" name="lst" value="<?php echo $username; ?>">
							<input type="hidden" name="split" value="refund">
							<input type="hidden" name="branch" value="Room_Booking_Desposit_Refunded">
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<input type="hidden" name="refund1" value="CASH">

 <div class="col-md-12 form-group">

 <table class="table table-bordered" >
	<thead>
	  <tr>
	    <th  style="text-align: center !important;">क्र.  </th>
	    <th  style="text-align: center !important;">दान की विगत </th>
	    <th  style="text-align: center !important;">रुपये </th>
	    <th style="background-color: #fff !important; 
	    border-top: 1px solid #fff !important;
	    border-bottom: none;"> 
	    &nbsp; </th>
	    <th  style="text-align: center !important;">क्र.  </th>
	    <th  style="text-align: center !important;">दान की विगत </th>
	    <th  style="text-align: center !important;">रुपये </th>
	  </tr>
	</thead>
  
	<tbody>
	  <tr>
	    <td class="row1">1.</td>
	    <td class="row2">श्री  जिर्णोधार  खाते </td>
	    <td class="row3"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k1" name="k1" class="form-control customInput"></td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5">17.</td>
	    <td class="row6">श्री शुभ साधारण खाते </td>
	    <td class="row7"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k2" name="k2" class="form-control customInput"></td>
	  </tr>

	  <tr>
	    <td class="row1">2.</td>
	    <td class="row2">श्री देवद्रव्य खाते  </td>
	    <td class="row3"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k3" name="k3" class="form-control customInput"></td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5">18.</td>
	    <td class="row6"> - श्री शुभ साधारण </td>
	    <td class="row7"><input type="text"  onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k4" name="k4" class="form-control customInput"></td>
	  </tr>

	  <tr>
	    <td class="row1"> 3.</td>
	    <td class="row2"> - श्री भण्डार खाते  </td>
	    <td class="row3"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k5" name="k5" class="form-control customInput"></td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 19. </td>
	    <td class="row6"> श्री धर्मशाला खाते  </td>
	    <td class="row7"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k6" name="k6" class="form-control customInput"></td>
	  </tr>

	  <tr>
	    <td class="row1">4. </td>
	    <td class="row2"> - श्री धीरत चढ़ावा खाते   </td>
	    <td class="row3"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k7" name="k7" class="form-control customInput"></td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 20. </td>
	    <td class="row6"> श्री भोजनशाला खाते   </td>
	    <td class="row7"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k8" name="k8" class="form-control customInput"></td>
	  </tr>

	  <tr>
	    <td class="row1"> 5. </td>
	    <td class="row2"> श्री देवद्रव्य साधारण खाते  </td>
	    <td class="row3"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k9" name="k9" class="form-control customInput"></td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 21. </td>
	    <td class="row6"> श्री भाता खाते  </td>
	    <td class="row7"><input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k10" name="k10" class="form-control customInput"></td>
	  </tr>

	  <tr>
	    <td class="row1">6.  </td>
	    <td class="row2"> - श्री स्नात्र पूजा खाते  </td>
	    <td class="row3"> <input type="text"  onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k11" name="k11" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 22. </td>
	    <td class="row6"> श्री आयंबिल खाते  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k12" name="k12" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1">7.  </td>
	    <td class="row2"> - श्री अखण्ड दीपक खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k13" name="k13" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 23. </td>
	    <td class="row6"> श्री गर्म पानी खाते  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)"  onkeyup="findTotal()" id="k14" name="k14" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1"> 8. </td>
	    <td class="row2"> - श्री पक्षाल खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k15" name="k15" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 24. </td>
	    <td class="row6"> श्री स्वामी वात्सल्य  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k16" name="k16" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1"> 9. </td>
	    <td class="row2"> - श्री आंगी खाते </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k17" name="k17" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 25. </td>
	    <td class="row6" style="font-size: 14px !important;"> श्री तीर्थ विकास भक्ति स्थायी फण्ड  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k18" name="k18" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1"> 10. </td>
	    <td class="row2"> - श्री पूजा खाते </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k19" name="k19" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 26. </td>
	    <td class="row6"  style="font-size: 13px !important;"> श्री साधना संकुलन विकास स्थायी फण्ड  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k20" name="k20" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1">11.  </td>
	    <td class="row2"> - श्री केसर खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k21" name="k21" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 27. </td>
	    <td class="row6"  style="font-size: 14px !important;"> श्री देवद्रव्य जीणोद्धार स्थायी फण्ड  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k22" name="k22" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1"> 12. </td>
	    <td class="row2"> - श्री बगीचा फूल खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k23" name="k23" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 28. </td>
	    <td class="row6"> श्री साधारण स्थाई फंड </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k24" name="k24" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1">13.  </td>
	    <td class="row2"> - वार्षिक अष्टप्रकारी पूजा  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k25" name="k25" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 29. </td>
	    <td class="row6"> श्री देवद्रव्य चढ़ावे  खाते  </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k26" name="k26" class="form-control customInput"> </td>
	  </tr>

	  <tr>
	    <td class="row1"> 14. </td>
	    <td class="row2"> श्री वैयावच्छ खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k27" name="k27" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 30. </td>
	    <td class="row6">सात   क्षेत्र</td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k28" name="k28" class="form-control customInput" > </td>
	  </tr>

	  <tr>
	    <td class="row1"> 15. </td>
	    <td class="row2"> श्री ज्ञान खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k29" name="k29" class="form-control customInput"> </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 31.   </td>
	    <td class="row6"> अन्य </td>
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k30" name="k30" class="form-control customInput" > </td>
	  </tr>

	  <tr>
	    <td class="row1"> 16. </td>
	    <td class="row2"> श्री जीवदया खाते  </td>
	    <td class="row3"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k31" name="k31" class="form-control customInput" > </td>
	    <td class="row4"> &nbsp; </td>
	    <td class="row5"> 32. </td>
	    <td class="row6">  </td> 
	    <td class="row7"> <input type="text" onkeypress="return isNumber(event)" onkeyup="findTotal()" id="k32" name="k32" class="form-control customInput" readonly="readonly"> </td>
	  </tr>

	</tbody>
</table>
</div>


<div class="col-md-3 form-group">
<label>REFUND TYPE</label>
    <select  onchange="yesnoCheck(this);" class="form-control"  name="paytype" id="form-field-select-1" required="required"  >
   <option value=""> --- Select --- </option>
    <option value="CASH">Offline</option>
    <option value="CHEQUE">Online Booking</option>
   </select>
</div>

<script>
	function yesnoCheck(that) {

		if (that.value == "CASH") { 
		    document.getElementById("ifYes").style.display = "block";
		    document.getElementById("q").required = true;
		} else {
		    document.getElementById("ifYes").style.display = "none";
		  	document.getElementById("q").required = false;
		  	document.getElementById('q').value= "" ;
		}


		if (that.value == "CHEQUE") { 
		     document.getElementById("ifYes2").style.display = "block";
		     document.getElementById("y").required = true;
		     document.getElementById("r").required = true;
		} else {
		  	document.getElementById("ifYes2").style.display = "none";
		  	document.getElementById("y").required = false;
		  	document.getElementById('y').value= "" ;
		  	document.getElementById("r").required = false;
		  	document.getElementById('r').value= "" ;
		}

		if (that.value == "online_booking_site") { 
	    	document.getElementById("ifYes3").style.display = "block";
	    	document.getElementById("z").required = true;
		} else {
	    	document.getElementById("ifYes3").style.display = "none";
	  		document.getElementById("z").required = false;
		}
	}
</script>





	<div class="col-md-3" id="ifYes" style="display: none;">
	<div class="row"> 
	<div class="col-md-12 form-group">
		<label> PAY BY </label>
		<select name="refund1" class="form-control" id="q">
		<option value="CASH" selected="selected">CASH </option>
		</select>
	</div>
	</div>
	</div>

	<div class="col-md-6" id="ifYes2" style="display: none;">
	<div class="row"> 

	<div class="col-md-7 form-group">
		<label>BANK NAME</label>
		<select name="refund2" class="form-control" id="y">
		<option value=""> -- Select -- </option>
		<Option value="State_Bank_of_India_Reodar">State Bank of India Reodar </Option>
		<option value="Bank_of_Baroda_Reodar">Bank of Baroda Reodar </option>
		<option value="Union_Bank_Ahemdabad">Union Bank Ahemdabad </option>
		<option value="Bank_of_Maharashtra_Mumbai"> Bank of Maharashtra Mumbai </option>
		<option value="ICICI_Bank">ICICI Bank</option>
		</select>
	</div>

	<div class="col-md-5 form-group">
		<label>PAYMENT MODE</label>
		<select name="refund4" class="form-control" id="r">
		<option value=""> -- Select -- </option>
		<option value="CHEQUE">CHEQUE </option>
		<option value="NEFT">NEFT </option>
		<option Value="TRANSFER">TRANSFER</option>
		<option Value="OTHERS">OTHERS</option>
		</select>
	</div>

	</div>
	</div>



<div class="col-md-3 form-group">
<label>CREATE VOUCHER</label>
	<select name="vou_type" class="form-control" required="required">
	<option value="">-- Select --</option>
	<option value="yes">Yes</option>
	<option value="no">No</option>
	</select>
</div>
 
	  
<div class="col-md-3 form-group">
<label>TOTAL DEPOSIT</label>
<input type="text" id="total" name="amt" min="1" class="form-control" placeholder="Deposit Amount" readonly="readonly"  />
</div>

  
<div class="col-md-6 form-group">
<label> &nbsp; </label>
<input type="text" class="form-control" id="amt" placeholder="Total deposit in words"  name="amtws" readonly="readonly">
</div>

					</div>
                    </div>
                    </div> 
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-check-circle" aria-hidden="true"></i>  CONFIRM & SAVE  </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
window.onbeforeunload = function () {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == "button" || inputs[i].type == "submit") {
            inputs[i].disabled = true;
        }
    }
};
 
$(document).ready(function () {

    $("#formABC").submit(function (e) {
        e.preventDefault();
        $("#btnSubmit").attr("disabled", true);
        $("#btnTest").attr("disabled", true);

        return true;

    });
});
 
function findTotal(){
	var arr1 = document.getElementById('k1').value; 
	var arr2 = document.getElementById('k2').value; 
	var arr3 = document.getElementById('k3').value; 
	var arr4 = document.getElementById('k4').value; 
	var arr5 = document.getElementById('k5').value; 
	var arr6 = document.getElementById('k6').value; 
	var arr7 = document.getElementById('k7').value; 
	var arr8 = document.getElementById('k8').value; 
	var arr9 = document.getElementById('k9').value; 
	var arr10 = document.getElementById('k10').value; 
	var arr11 = document.getElementById('k11').value; 
	var arr12 = document.getElementById('k12').value; 
	var arr13 = document.getElementById('k13').value; 
	var arr14 = document.getElementById('k14').value; 
	var arr15 = document.getElementById('k15').value; 
	var arr16 = document.getElementById('k16').value; 
	var arr17 = document.getElementById('k17').value; 
	var arr18 = document.getElementById('k18').value; 
	var arr19 = document.getElementById('k19').value; 
	var arr20 = document.getElementById('k20').value; 
	var arr21 = document.getElementById('k21').value; 
	var arr22 = document.getElementById('k22').value; 
	var arr23 = document.getElementById('k23').value; 
	var arr24 = document.getElementById('k24').value; 
	var arr25 = document.getElementById('k25').value; 
	var arr26 = document.getElementById('k26').value; 
	var arr27 = document.getElementById('k27').value;
	var arr28 = document.getElementById('k28').value; 
	var arr29 = document.getElementById('k29').value; 
	var arr30 = document.getElementById('k30').value; 
	var arr31 = document.getElementById('k31').value;
	var arr32 = document.getElementById('k32').value; 
	var tot =  Number(arr1) + Number(arr2) + Number(arr3) + Number(arr4) + Number(arr5) + Number(arr6) + Number(arr7) + Number(arr8) + Number(arr9) + Number(arr10) + Number(arr11) + Number(arr12) + Number(arr13) + Number(arr14) + Number(arr15) + Number(arr16) + Number(arr17) + Number(arr18) + Number(arr19) + Number(arr20) + Number(arr21) + Number(arr22) + Number(arr23) + Number(arr24) + Number(arr25) + Number(arr26) + Number(arr27) + Number(arr28) + Number(arr29) + Number(arr30) + Number(arr31) + Number(arr32) +0;document.getElementById('total').value = tot;
	document.getElementById('amt').value=convertNumberToWords(tot);
}


function convertNumberToWords(amount) {
		var words = new Array();
		words[0] = '';
		words[1] = 'One';
		words[2] = 'Two';
		words[3] = 'Three';
		words[4] = 'Four';
		words[5] = 'Five';
		words[6] = 'Six';
		words[7] = 'Seven';
		words[8] = 'Eight';
		words[9] = 'Nine';
		words[10] = 'Ten';
		words[11] = 'Eleven';
		words[12] = 'Twelve';
		words[13] = 'Thirteen';
		words[14] = 'Fourteen';
		words[15] = 'Fifteen';
		words[16] = 'Sixteen';
		words[17] = 'Seventeen';
		words[18] = 'Eighteen';
		words[19] = 'Nineteen';
		words[20] = 'Twenty';
		words[30] = 'Thirty';
		words[40] = 'Forty';
		words[50] = 'Fifty';
		words[60] = 'Sixty';
		words[70] = 'Seventy';
		words[80] = 'Eighty';
		words[90] = 'Ninety';
		amount = amount.toString();
		var atemp = amount.split(".");
		var number = atemp[0].split(",").join("");
		var n_length = number.length;
		var words_string = "";
		if (n_length <= 9) {
		var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
		var received_n_array = new Array();
		for (var i = 0; i < n_length; i++) {
		received_n_array[i] = number.substr(i, 1);
		}
		for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
		n_array[i] = received_n_array[j];
		}
		for (var i = 0, j = 1; i < 9; i++, j++) {
		if (i == 0 || i == 2 || i == 4 || i == 7) {
		if (n_array[i] == 1) {
		n_array[j] = 10 + parseInt(n_array[j]);
		n_array[i] = 0;
		}
		}
		}
		value = "";
		for (var i = 0; i < 9; i++) {
		if (i == 0 || i == 2 || i == 4 || i == 7) {
		value = n_array[i] * 10;
		} else {
		value = n_array[i];
		}
		if (value != 0) {
		words_string += words[value] + " ";
		}
		if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Crores ";
		}
		if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Lakhs ";
		}
		if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
		words_string += "Thousand ";
		}
		if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
		words_string += "Hundred and ";
		} else if (i == 6 && value != 0) {
		words_string += "Hundred ";
		}
		}
		words_string = words_string.split("  ").join(" ");
		}
		return words_string;
}
</script>
	   
<?php 
}  

    $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));
            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";
            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }

            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 

  $conn->close();
 
  require "_footer.php"; ?>