<?php

require "_session.php";

$username1=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$mysqldate=$conn->real_escape_string(htmlspecialchars($_POST["mysqldate"]));
$date=$conn->real_escape_string(htmlspecialchars($_POST["date"]));
$username=$conn->real_escape_string(htmlspecialchars($_POST["username"]));
$expense=$conn->real_escape_string(htmlspecialchars($_POST["expense"]));
$narration=$conn->real_escape_string(htmlspecialchars($_POST["comment"]));
$inword=$conn->real_escape_string(htmlspecialchars($_POST["inword"]));
$chq=$conn->real_escape_string(htmlspecialchars($_POST["chq"]));
$chq_number=$conn->real_escape_string(htmlspecialchars($_POST["chq_number"]));
$achol=$conn->real_escape_string(htmlspecialchars($_POST["achol"]));
$accc=$conn->real_escape_string(htmlspecialchars($_POST["accc"]));
$type=$conn->real_escape_string(htmlspecialchars($_POST["type"]));
$amt1=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));
$amt=$conn->real_escape_string(htmlspecialchars($_POST["amt"]));


try {
    $conn->query("START TRANSACTION"); 
    $sql="SELECT COUNT(id) as total FROM expense";
     $res=$conn->query($sql);

      if($res===FALSE)
      {
      throw new Exception("Code 001 : ".mysqli_error($conn));   
      }
      while($row=mysqli_fetch_array($res))
      {

      $exp_id=(int)$row["total"]+1;
     if($type=="Payment_Voucher")
     {
      $usname=$conn->real_escape_string(htmlspecialchars($_POST["usname"]));
      $frm=$conn->real_escape_string(htmlspecialchars($_POST["frm"]));
      $to=$conn->real_escape_string(htmlspecialchars($_POST["to"]));

       $d1 = date("Y-m-d", strtotime($frm));
       $d3 = date("Y-m-d", strtotime($to));

      $sql="SELECT * from fdr_status where FDR='$usname'";
      $res=$conn->query($sql);
      if($res===FALSE)
      {
      throw new Exception("Code 002 : ".mysqli_error($conn));   
      }
      if($row=mysqli_fetch_array($res))
      {

                      throw new Exception("FDR alreday exist ! ");             

      // echo "<script type=\"text/javascript\">
      //    window.location = \"acc_Investment_voucher.php?msg= 'FDR Alreday exit'\";    
      //   </script>";
      }
      else
      {
          $sql="SELECT * from balance where Branch='Bank'";
          $res2=$conn->query($sql);
          if($res2===FALSE)
          {
          throw new Exception("Code 003 : ".mysqli_error($conn));   
          }

          $sql="SELECT * from bank_name_with_amount where Bank_Name='$expense'";
          $res3=$conn->query($sql);
          if($res3===FALSE)
          {
          throw new Exception("Code 004 : ".mysqli_error($conn));   
          }
          $sql="SELECT * from balance where Branch='FD'";
          $res4=$conn->query($sql);
          if($res4===FALSE)
          {
          throw new Exception("Code 005 : ".mysqli_error($conn));   
          }
          while($row2=mysqli_fetch_array($res2))
          {
              while($row3=mysqli_fetch_array($res3))
              {
              while($row4=mysqli_fetch_array($res4))
              {

                  $fd =$row4["Balance"];
                  $update_fd = (int)$fd +(int)$amt1;

                  $balance_bank=$row2["Balance"];; // Balance bank
                  $bank_name_with_amount=$row3["Amount"];; // Balance bank_name_with_amount

                  $update_balance_bank= (int)$balance_bank - (int)$amt1; //update  Balance bank
                  $update_name_with_amount= (int)$bank_name_with_amount - (int)$amt1; //update  Balance bank_name_with_amount

                  $sql="UPDATE balance set Balance='$update_balance_bank' WHERE Branch='Bank'";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 006 : ".mysqli_error($conn));             
                  } 

                  $sql="UPDATE bank_name_with_amount set Amount='$update_name_with_amount' WHERE Bank_Name='$expense'";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 007 : ".mysqli_error($conn));             
                  } 

                  $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$exp_id','$username','$expense','$usname','$amt','$inword','$narration','$date','$mysqldate','$chq','$chq_number','$expense','0','$achol','$accc','0','0','0','0','$expense','FDR_Voucher','0')";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 008 : ".mysqli_error($conn));             
                  } 

                  $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username','main_office','$date','$mysqldate','$expense','0','$amt1','0','$update_balance_bank','FDR_Voucher','$narration','$exp_id')";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 009 : ".mysqli_error($conn));             
                  } 


                  $sql="insert into fdr_status (FDR,status,bank,fromdate,todate,d_b_id,blac1,cl_blac2,interset,matutrity_value,tobank) values ('$usname','opening','$expense','$d1','$d3','$exp_id','$amt1','0','$achol','$accc','0')";

                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0010 : ".mysqli_error($conn));             
                  } 

                  $sql="UPDATE balance set Balance='$update_fd' WHERE Branch='FD'";

                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0011 : ".mysqli_error($conn));             
                  } 


              }

              }

          }


      }


      // echo "<script type=\"text/javascript\">
      //          window.location = \"FDR_Slip.php?id=$exp_id\";    
      // </script>";



     }


     if($type=="Receipt_Voucher")
     {
      $pf=$conn->real_escape_string(htmlspecialchars($_POST["pf"])); // FDR NAME

            $sql="SELECT * from balance where Branch='Bank'";
            $res2=$conn->query($sql);
            if($res2===FALSE)
            {
            throw new Exception("Code 0012 : ".mysqli_error($conn));   
            }
            $sql="SELECT * from bank_name_with_amount where Bank_Name='$expense'";
            $res3=$conn->query($sql);
            if($res3===FALSE)
            {
            throw new Exception("Code 0013 : ".mysqli_error($conn));   
            }

            $sql="SELECT * from balance where Branch='FD'";
            $res4=$conn->query($sql);
            if($res4===FALSE)
            {
            throw new Exception("Code 0014 : ".mysqli_error($conn));   
            }

           while($row2=mysqli_fetch_array($res2))
              {
                 while($row3=mysqli_fetch_array($res3))
              {
                 while($row4=mysqli_fetch_array($res4))
              {
                  $fd =$row4["Balance"];
                  $update_fd = $fd - $amt1;


                  $balance_bank=$row2["Balance"]; // Balance bank
                  $bank_name_with_amount=$row3["Amount"]; // Balance bank_name_with_amount

                  $update_balance_bank= $balance_bank + $amt1; //update  Balance bank

                  $update_name_with_amount= $bank_name_with_amount + $amt1; //update  Balance bank_name_with_amount

                  $sql="UPDATE balance set Balance='$update_balance_bank' WHERE Branch='Bank'";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0015 : ".mysqli_error($conn));             
                  } 

                  $sql="UPDATE bank_name_with_amount set Amount='$update_name_with_amount' WHERE Bank_Name='$expense'";

                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0016 : ".mysqli_error($conn));             
                  } 
                  $sql="insert into expense(id,name,given,expense,amt,inword,narration,date,mysqldate,ptype,pcheque,pbank,ppan,pachol,pifsc,paccount,status,pending,others,others1,vou_type,d_b_id) values ('$exp_id','$username','$pf','$expense','$amt','$inword','$narration','$date','$mysqldate','$chq','$chq_number','$expense','0','$achol','$accc','0','0','0','0','$expense','FDR_Voucher','0')";

                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0017 : ".mysqli_error($conn));             
                  } 
                  $sql="insert into repcash_chq (username,dep,recdate,mysqldate,formtheuser,formthe,debit,credit,balance,vou_type,Des,d_b_id) values ('$username','main_office','$date','$mysqldate','$expense','0','0','$amt1','$update_balance_bank','FDR_Voucher','$narration','$exp_id')";

                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0018 : ".mysqli_error($conn));             
                  } 

                  $sql ="UPDATE fdr_status set status='closing' , cl_blac2='$amt1' WHERE FDR='$pf'";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0019 : ".mysqli_error($conn));             
                  } 

                  $sql="UPDATE balance set Balance='$update_fd' WHERE Branch='FD'";
                  if($conn->query($sql) === FALSE) {
                  throw new Exception("Code 0020 : ".mysqli_error($conn));             
                  } 

                
              }
              }
              }

      //    echo "<script type=\"text/javascript\">
      //          window.location = \"FDR_Slip.php?id=$exp_id\";    
      // </script>";
     

     }

      }

       

    $conn->query("COMMIT");


    echo "<script type=\"text/javascript\">
    window.alert('Voucher created successfully !');
    window.location = \"FDR_Slip.php?id=$exp_id\";    
    </script>";
    
    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username1','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }


            echo "<script type=\"text/javascript\">
            window.alert('$content');
            window.location = \"acc_Investment_voucher.php\";    
            </script>";
            // echo "
            // <script>
            // swal({
            // title: \"Error !\",
            // text: \"$content\",
            // icon: \"error\",
            // button: \"OK\",
            // });
            // </script>";    
} 


$conn->close();
?>