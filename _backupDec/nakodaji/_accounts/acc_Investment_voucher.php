<?php
require "_session.php";
require "_header.php";

date_default_timezone_set('Asia/Calcutta'); 
$date=date("d-m-Y");
$Check1=date("Y-m-d");
// $time=date("h:i:A");
$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

try {
  $conn->query("START TRANSACTION"); 

?>

 


  <div class="page-wrapper">
  <div class="content">
  <div class="row">
  <div class="col-sm-12">
  <h4 class="page-title"> Investment Voucher  {<?php echo $date;  ?>}</h4>
  </div>
  </div>

  <div class="row">
  <div class="col-md-8">
  <div class="card-box">

 <form class="" action="inv_fdr_insert.php" method="post" >

 <input type="hidden" name="mysqldate" value="<?php echo $Check1; ?>">
 <input type="hidden" name="date" value="<?php echo $date; ?>">
 <input type="hidden" name="username" value="<?php echo $username; ?>">

<div class="col-md-12">
<div class="row">

<div class="col-md-4 form-group">

 <label > Type</label>
<Select onchange="yesnoCheck(this);" class="form-control" id="form-field-select-1" name="type" required="required">
<option value=""> --Select an Option--</option>
<option value="Receipt_Voucher">Receipt (Credit Saving) Voucher  </option>
<option value="Payment_Voucher">Payment (Debit Saving) Voucher </option>

</Select>
 
 
 </div>
 
 
 
 

<script>
    function yesnoCheck(that) {
       
        if (that.value == "Receipt_Voucher") { 
            document.getElementById("ifYes2").style.display = "block";
            document.getElementById("e").required = true;
                    
        } else {
            document.getElementById("ifYes2").style.display = "none";
          document.getElementById("e").required = false;
          
          document.getElementById('e').value= "" ;
             
        
        }


          if (that.value == "Payment_Voucher") { 
            document.getElementById("ifYes").style.display = "block";
            document.getElementById("v").required = true;
             document.getElementById("q").required = true;
            document.getElementById("w").required = true;
            
        } else {
            document.getElementById("ifYes").style.display = "none";
            document.getElementById("v").required = false;
          document.getElementById("q").required = false;
          document.getElementById("w").required = false;

          document.getElementById("v").required = false;
          document.getElementById('q').value= "" ;
          document.getElementById('w').value= "" ;
          
        }
        
       


                 
        
    }


    
</script>

     <div id="ifYes" class="col-md-12" style="display: none;">

<div class="row">
  
<div class="col-md-4 form-group">
<label> Name :- </label>
<input type="text" name="usname" aria-describedby="name-format" id="v"  placeholder="Enter Name" autocomplete="off" class="form-control" required="required">
</div>


<div class="col-md-4 form-group">

     <label >From Date : </label>
      <input type="text" name="frm" id="tbDate2"  id="q"   placeholder="Enter Starting Date"  class="form-control" />
  </div>

  <div class="col-md-4 form-group">
   <label >To Date: </label>
   
      <input type="text" name="to"  aria-describedby="name-format" id="tbDate" id="w" placeholder="Enter Ending Date"   class="form-control"  />
</div>
  </div>

  
       
        </div>
        
  <div id="ifYes2" style="display: none;">

  <div class=" form-group">

    <label >Previous FDR  : </label>
    <select name="pf" class="form-control" > 
    <?php 
    $sql="SELECT * from fdr_status where status='opening'";
    $res=$conn->query($sql);

    if($res===FALSE)
    {
    throw new Exception("Code 001 : ".mysqli_error($conn));   
    }
    while($row=mysqli_fetch_array($res))
    {
    ?>
    <option value="<?php echo $row["FDR"]; ?>"><?php echo $row["FDR"]; ?></option>

    <?php } ?>

    </select>
</div>
  </div>






  <div class="col-md-4 form-group">



<label> Credit / Debit Banks :- </label>
<Select class="form-control" name="expense" onchange="yesno(this);" required="required">
<option value="">-- select --</option>

<Option value="State_Bank_of_India_Reodar">State Bank of India Reodar </Option>
   <option value="Bank_of_Baroda_Reodar">Bank of Baroda Reodar </option>
   <option value="Union_Bank_Ahemdabad">Union Bank Ahemdabad </option>
   <option value="Bank_of_Maharashtra_Mumbai"> Bank of Maharashtra Mumbai </option>
   <option value="ICICI_Bank">ICICI Bank</option>


</Select>

</div>


  <div class="col-md-4 form-group">


<label> Amount :- </label>
<input type="text" min="1" name="amt"  id="amount" onkeypress="return isNumber(event)"  onkeyup="document.getElementById('amt').value=convertNumberToWords(this.value)" placeholder="Enter Amount" autocomplete="off" class="form-control" required="required">
</div>

  <div class="col-md-4 form-group">


<label> Amount in word :- </label>

<input type="text" name="inword"  id="amt"   placeholder="Enter Amount" autocomplete="off" class="form-control" required="required" readonly="readonly">
</div>



        
        
        


 
  <div class="col-md-4 form-group">
<label>Select Type : - </label>
<select name="chq" class="form-control" required="required">
<option value="CHEQUE">Cheque </option>

</select>
</div>

  <div class="col-md-4 form-group">

       <label>Cheque No </label>
       <input type="text" name="chq_number" id="e"  placeholder="Enter Cheque No"  class="form-control" />
      </div>

        <div class="col-md-4 form-group">

     
       <label >Interest Rate </label>
      <input type="text" name="achol" id="r"   aria-describedby="name-format"  placeholder="Enter Interest Rate"   class="form-control"  />
   </div>

     <div class="col-md-4 form-group">

       <label >Maturity Amount  : </label>
      <input type="text" name="accc"   id="t" onkeypress="return isNumber(event)"  placeholder="Enter Maturity Amount"   class="form-control"  />
   </div>
      
   <div class="col-md-4 form-group">


<label> Narration (Description) :-  </label>


<textarea name="comment" rows="4" cols="50" class="form-control" placeholder="Enter Narration here ..." required="required"></textarea>
</div>



<div class="col-md-12">
  <button type="submit" id="btnSubmit" class="btn btn-primary pull-right" value="Submit" > <i class="fa fa-check-circle"></i> Save </button>
  </div>  

      <script type="text/javascript">
        window.onbeforeunload = function () {
            var inputs = document.getElementsByTagName("INPUT");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "button" || inputs[i].type == "submit") {
                    inputs[i].disabled = true;
                }
            }
        };
    </script>
      <script>
  
</script>


</form>
</div>
 

</div>


</div>








 <div>

 </div>
</div>
     
 
<script>
$('input[id$=tbDate]').datepicker({
    dateFormat: 'dd-mm-yy'
  
});
$('input[id$=tbDate2]').datepicker({
    dateFormat: 'dd-mm-yy'
  
});

</script>    
    
<script type="text/javascript">
function convertNumberToWords(amount) {
var words = new Array();
words[0] = '';
words[1] = 'One';
words[2] = 'Two';
words[3] = 'Three';
words[4] = 'Four';
words[5] = 'Five';
words[6] = 'Six';
words[7] = 'Seven';
words[8] = 'Eight';
words[9] = 'Nine';
words[10] = 'Ten';
words[11] = 'Eleven';
words[12] = 'Twelve';
words[13] = 'Thirteen';
words[14] = 'Fourteen';
words[15] = 'Fifteen';
words[16] = 'Sixteen';
words[17] = 'Seventeen';
words[18] = 'Eighteen';
words[19] = 'Nineteen';
words[20] = 'Twenty';
words[30] = 'Thirty';
words[40] = 'Forty';
words[50] = 'Fifty';
words[60] = 'Sixty';
words[70] = 'Seventy';
words[80] = 'Eighty';
words[90] = 'Ninety';
amount = amount.toString();
var atemp = amount.split(".");
var number = atemp[0].split(",").join("");
var n_length = number.length;
var words_string = "";
if (n_length <= 9) {
var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
var received_n_array = new Array();
for (var i = 0; i < n_length; i++) {
received_n_array[i] = number.substr(i, 1);
}
for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
n_array[i] = received_n_array[j];
}
for (var i = 0, j = 1; i < 9; i++, j++) {
if (i == 0 || i == 2 || i == 4 || i == 7) {
if (n_array[i] == 1) {
n_array[j] = 10 + parseInt(n_array[j]);
n_array[i] = 0;
}
}
}
value = "";
for (var i = 0; i < 9; i++) {
if (i == 0 || i == 2 || i == 4 || i == 7) {
value = n_array[i] * 10;
} else {
value = n_array[i];
}
if (value != 0) {
words_string += words[value] + " ";
}
if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
words_string += "Crores ";
}
if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
words_string += "Lakhs ";
}
if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
words_string += "Thousand ";
}
if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
words_string += "Hundred and ";
} else if (i == 6 && value != 0) {
words_string += "Hundred ";
}
}
words_string = words_string.split("  ").join(" ");
}
return words_string;
}
</script> 
     
     
     
     <?php 
     

 $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 


$conn->close();


     require "_footer.php"; ?>
