<?php


require "_session.php";
    require "_header.php";



date_default_timezone_set('Asia/Calcutta'); 
    $date=date("d-m-Y");
     $Check1=date("Y-m-d");
   $username=$_SESSION["username"];
$id=$conn->real_escape_string(htmlspecialchars($_POST["Enter_id"]));
$file_name = basename($_SERVER['PHP_SELF']);
try {
    $conn->query("START TRANSACTION"); 

    $sql="SELECT * from donation where rec_id ='$id'";
     $res=$conn->query($sql);

      if($res===FALSE)
      {
      throw new Exception("Code 001 : ".mysqli_error($conn));   
      }
      if ($res->num_rows==0) {
            
         echo ("<script LANGUAGE='JavaScript'>
            window.alert('Error: please inter valid number');
            window.location.href = \"acc_Fetch_Donation.php\";
            </script>");  
      }
      if($row=mysqli_fetch_array($res))
      {

      $paymentype=$row["paymentype"];
?>
  <div class="page-wrapper">

  <div class="content">
  <div class="row">
  <div class="col-sm-12">
  <h4 class="page-title"> Donation Deposit {<?php echo $date; ?>} </h4>
  </div>
  </div>

  <div class="row">
  <div class="col-md-8">
  <div class="card-box">
 
 
 
 <form class="" action="Fetch_Donation_Submit.php" method="post" >
 <input type="hidden" name="mysqldate" value="<?php echo $Check1; ?>">
 <input type="hidden" name="date" value="<?php echo $date; ?>">
 <input type="hidden" name="username" value="<?php echo $username; ?>">
  
		
		
	
<div class="col-md-12">
<div class="row">

<div class="col-md-4 form-group">

<label> Name :- </label>
<input type="text" name="usname" aria-describedby="name-format"  placeholder="Enter Name" autocomplete="off" class="form-control" required="required">
</div>

<div class="col-md-4 form-group">

<label > To bank </label>
   <input type="text"  name="bank_name" value="<?php  echo $row["payname"]; ?>" class="form-control" readonly="readonly">
</div>
   
<div class="col-md-4 form-group">

<label > Type</label>

 <input type="text"  name="type" value="<?php  echo $row["paymentype"]; ?>" class="form-control" readonly="readonly" >
</div>

<div class="col-md-4 form-group">


<label> Amount :- </label>
<input type="text" min="1" name="amt" value="<?php  echo $row["amount"]; ?>" class="form-control" readonly="readonly">
</div>


<div class="col-md-4 form-group">


<label> Amount in word :- </label>

<input type="text" name="inword"   class="form-control" value="<?php  echo $row["amountwrd"]; ?>"  readonly="readonly">


</div>

 
<script>
    function yesno(that) {
        if (that.value == "head") { 
            document.getElementById("ifYes11").style.display = "block";
            document.getElementById("pay").required = true;
            document.getElementById("payname").required = true;
        } else{
        	document.getElementById("ifYes11").style.display = "none";
        	document.getElementById("pay").required = false;
        	document.getElementById("payname").required = false;
        	document.getElementById('pay').value= "" ;
        	document.getElementById('payname').value= "" ;
        }
    }
</script>

 
<input type="hidden" value="<?php  echo $row["payname"]; ?>" name="expense" class="form-control">
  
<?php
if($paymentype=="CHEQUE")
{
	?>

<div class="col-md-4 form-group">
      <label >Cheque Number : </label>
      <input type="text" name="cheque" value="<?php  echo $row["payno"]; ?>"  class="form-control" readonly="readonly" />
  </div>


<div class="col-md-4 form-group">
       <label >Bank Name : </label>
   
      <input type="text" name="bank"  value="<?php  echo $row["payname"]; ?>" class="form-control" value="form-control"  readonly="readonly" />
    </div>
	<?php

}
if($paymentype=="NEFT")
{
	?>

<div class="col-md-4 form-group">
 <label >Cheque Number : </label>
      <input type="text" name="cheque" value="<?php  echo $row["payno"]; ?>"  class="form-control" readonly="readonly" />
  </div>


<div class="col-md-4 form-group">
       <label >Bank Name : </label>
   
      <input type="text" name="bank"  value="<?php  echo $row["payname"]; ?>" class="form-control" value="form-control"  readonly="readonly" />
    </div>
	<?php
}

?>

 

 <div class="col-md-4 form-group">


<label> Narration (Description) :-  </label>


<textarea name="comment" rows="2" cols="50" class="form-control" placeholder="Enter Narration here ..." required="required"></textarea>
</div>



 <div class="text-right col-md-12">
              <button type="submit" class="btn btn-primary"> <i class="fa fa-check-circle" aria-hidden="true"></i> SAVE  </button>
              </div>    <script type="text/javascript">
        window.onbeforeunload = function () {
            var inputs = document.getElementsByTagName("INPUT");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "button" || inputs[i].type == "submit") {
                    inputs[i].disabled = true;
                }
            }
        };
    </script>
      <script>
  
</script>


</form>
</div>
 

</div>

</div>



 </div>

 </div>
</div>


	   
	   
	   
	   
	   
	   
	   <?php 

  }
 $conn->query("COMMIT");

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

            $conn->query("ROLLBACK"); 
            $content = htmlspecialchars($e->getMessage());
            $content = htmlentities($conn->real_escape_string($content));

            $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

            if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
            echo "
            <script>
            swal({
            title: \"Error !\",
            text: \"$content\",
            icon: \"error\",
            button: \"OK\",
            });
            </script>";    
} 


$conn->close();
	   
	   require "_footer.php"; ?>
