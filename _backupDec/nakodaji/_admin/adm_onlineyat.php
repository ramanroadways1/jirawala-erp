<?php
//done
require "_session.php";

$username=$_SESSION["username"];
$file_name = basename($_SERVER['PHP_SELF']);

$date = $conn->real_escape_string(htmlspecialchars($_POST["arrive"]));
$vip = $conn->real_escape_string(htmlspecialchars($_POST["vip"]));
$acroom = $conn->real_escape_string(htmlspecialchars($_POST["acroom"]));
$nonacroom = $conn->real_escape_string(htmlspecialchars($_POST["nonacroom"]));
$achall = $conn->real_escape_string(htmlspecialchars($_POST["achall"]));
$nonachall = $conn->real_escape_string(htmlspecialchars($_POST["nonachall"]));

$date1 = date("Y-m-d", strtotime($date));

try
 {
    $conn->query("START TRANSACTION"); 

        $sql="select * from onlinerooms where Date='$date1'";
        $res=$conn->query($sql);
        if($res===FALSE)
        {
            throw new Exception("Code 001 : ".mysqli_error($conn));   
        }

        if($row=mysqli_fetch_array($res))
        { 
            $sql="update onlinerooms set vip_room='$vip' ,ac_room ='$acroom', non_ac_room ='$nonacroom',non_ac_hall = '$nonachall',ac_hall='$achall' Where  Date='$date1'";
            if($conn->query($sql) === FALSE) 
            {
                throw new Exception("Code 002 : ".mysqli_error($conn));  
            }
        }
        else
        {
            $sql="insert into onlinerooms (Date,vip_room,ac_room,non_ac_room,non_ac_hall,ac_hall,booked_vip_room,booked_ac_room,booked_non_ac_room,booked_non_ac_hall,booked_ac_hall) values ('$date1','$vip','$acroom','$nonacroom','$nonachall','$achall','0','0','0','0','0') ";
            if($conn->query($sql) === FALSE) 
            {
                throw new Exception("Code 003 : ".mysqli_error($conn));  
            }

        }


         // echo "<script type=\"text/javascript\">
         //    window.location = \"CheckDate.php\";
         //    </script>";

    $conn->query("COMMIT");

    echo "<script type=\"text/javascript\">
    window.alert(' Updated Successfully !');
    window.location = \"adm_changestatus.php\";    
    </script>";

    // echo "
    // <script>
    // swal({
    // title: \"Good job!\",
    // text: \"You clicked the button!\",
    // icon: \"success\",
    // button: \"OK\",
    // });
    // </script>";

} catch(Exception $e) { 

        $conn->query("ROLLBACK"); 
        $content = htmlspecialchars($e->getMessage());
        $content = htmlentities($conn->real_escape_string($content));

        $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name','$username','$content')";

        if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
        } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        }

        echo "<script type=\"text/javascript\">
        window.alert('$content');
        window.location = \"adm_changestatus.php\";    
        </script>";
} 

$conn->close();


?>